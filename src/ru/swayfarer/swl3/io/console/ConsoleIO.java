package ru.swayfarer.swl3.io.console;

import java.io.PrintStream;
import java.util.Scanner;

import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.markers.ConcattedString;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.string.StringUtils;

public class ConsoleIO {

	@Internal
	public DataInStream dis = DataInStream.of(System.in);
	
	@Internal
	public Scanner scan = new Scanner(System.in);
	
	public void info(@ConcattedString Object... text)
	{
		log(System.out, StringUtils.concatWithSpaces(text));
	}
	
	public void log(PrintStream stream, String str)
	{
		stream.println(str);
	}

	public void error(@ConcattedString Object... text)
	{
		log(System.err, StringUtils.concatWithSpaces(text));
	}
	
	public Boolean readBoolean(String message, boolean isReTry)
	{
		String ln = readFilteredString(message, isReTry, StringUtils::isBoolean);
		return StringUtils.isEmpty(ln) ? null : Boolean.valueOf(ln);
	}
	
	public Integer readInt(String message, boolean isReTry)
	{
		String ln = readFilteredString(message, isReTry, StringUtils::isInteger);
		return StringUtils.isEmpty(ln) ? null : Integer.valueOf(ln);
	}
	
	public Long readLong(String message, boolean isReTry)
	{
		String ln = readFilteredString(message, isReTry, StringUtils::isLong);
		return StringUtils.isEmpty(ln) ? null : Long.valueOf(ln);
	}
	
	public Float readFloat(String message, boolean isReTry)
	{
		String ln = readFilteredString(message, isReTry, StringUtils::isFloat);
		return StringUtils.isEmpty(ln) ? null : Float.valueOf(ln);
	}
	
	public Double readDouble(String message, boolean isReTry)
	{
		String ln = readFilteredString(message, isReTry, StringUtils::isDouble);
		return StringUtils.isEmpty(ln) ? null : Double.valueOf(ln);
	}
	
	public String readFilteredString(String message, boolean isReTry, IFunction1<String, Boolean> filter)
	{
		boolean isFirst = true;
		
		while (isFirst || isReTry)
		{
			if (!StringUtils.isBlank(message))
			{
				info(message);
			}
			
			String ln = readString();
			
			if (ln == null)
			{
				return null;
			}
			
			if (filter == null || filter.apply(ln))
				return ln;
			
			isFirst = false;
		}
		
		return null;
	}
	
	public String readString()
	{
		return dis.readLine();
	}
	
}