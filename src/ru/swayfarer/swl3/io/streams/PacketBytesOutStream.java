package ru.swayfarer.swl3.io.streams;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data @Accessors(chain = true) @EqualsAndHashCode(callSuper = true)
public class PacketBytesOutStream extends BytesOutStream {

	@NonNull
	public DataOutStream parent;
	
	public PacketBytesOutStream(DataOutStream parent)
	{
		this.parent = parent;
		this.configuration = parent.configuration;
	}
	
	public DataOutStream send()
	{
		close();
		parent.write(toBytesArray());
		
		return parent;
	}
}
