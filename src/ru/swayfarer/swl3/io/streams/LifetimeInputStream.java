package ru.swayfarer.swl3.io.streams;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

@Data @EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class LifetimeInputStream extends InputStream {

    public AtomicBoolean expired = new AtomicBoolean(false);
    
    /**
     * Лайфтайм обертки
     * Каждый вызов метода {@link #read()} уменьшает лайфтайм на 1 <br>
     * Как только лайфтайм закончится, обертка перестанет работать 
     * и будет переадресовывать вызовы обернутому потоку <br>
     */
    public AtomicLong lifetime = new AtomicLong(0);
    
    /**
     * Максимальный лайфтайм обертки
     * @see #lifetime
     */
    public AtomicLong maxLifetime = new AtomicLong(128);
    
    @NonNull
    public InputStream wrappedStream;
    
    @NonNull
    public InputStream tempolaryInputStream;

    @NonNull
    public IFunction0NoR resetWrappedFun;
    
    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
    
    public void incrementLifetime()
    {
        lifetime.incrementAndGet();
    }
    
    public boolean isLifetimeExpired()
    {
        if (expired.get())
            return true;
        
        long maxLifetime = this.maxLifetime.get();
        boolean b = maxLifetime > 0 && lifetime.get() > maxLifetime;
        
        if (b)
            expired.set(true);
        
        return b;
    }
    
    @Override
    public int read() throws IOException
    {
        var result = tempolaryInputStream.read();

        incrementLifetime();

        if (isLifetimeExpired()) {
            resetWrappedFun.apply();
        }
        
        return result;
    }

    @Override
    public int available() throws IOException
    {
        return wrappedStream.available();
    }

    @Override
    public void close() throws IOException
    {
        wrappedStream.close();
        tempolaryInputStream.close();
        super.close();
    }


    @SuppressWarnings("serial")
    public static class CantFindSkipNBytesException extends RuntimeException {

        public CantFindSkipNBytesException()
        {
            super();
        }

        public CantFindSkipNBytesException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
        {
            super(message, cause, enableSuppression, writableStackTrace);
        }

        public CantFindSkipNBytesException(String message, Throwable cause)
        {
            super(message, cause);
        }

        public CantFindSkipNBytesException(String message)
        {
            super(message);
        }

        public CantFindSkipNBytesException(Throwable cause)
        {
            super(cause);
        }
    }
}
