package ru.swayfarer.swl3.io.streams;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipOutputStream;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;

import lombok.Data;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.io.xz.tukaani.XZOutputStream;

@Data
@Accessors(chain = true)
public class DataOutStreamConverter {

    @NonNull
    public DataOutStream dataInStream;
    
    public DataOutStream zip()
    {
        return wrap(ZipOutputStream::new);
    }
    
    public DataOutStream gz()
    {
        return wrap(GZIPOutputStream::new);
    }

    public DataOutStream xz()
    {
        return wrap(XZOutputStream::new);
    }
    
    public DataOutStream crypt(@NonNull Cipher cipher)
    {
        return wrap((stream) -> new CipherOutputStream(stream, cipher));
    }

    @SneakyThrows
    public BufferedWriter writer()
    {
        return new BufferedWriter(new OutputStreamWriter(dataInStream, dataInStream.getConfiguration().getEncoding()));
    }

    public DataOutStream wrap(@NonNull IFunction1<OutputStream, OutputStream> source)
    {
        return exceptionsHandler().safeReturn(
                () -> dataInStream.setDataOutputStream(
                        new DataOutputStream(
                                source.apply(dataInStream.getDataOutputStream())
                        )
                ),
                null,
                "Error while wrapping stream"
        );
    }
    
    public DataOutStream wrap(@NonNull IFunction0<OutputStream> source)
    {
        return exceptionsHandler().safeReturn(
                () -> dataInStream.setDataOutputStream(new DataOutputStream(source.apply())), 
                null,
                "Error while wrapping stream"
        );
    }
    
    public ExceptionsHandler exceptionsHandler()
    {
        return dataInStream.getExceptionsHandler();
    }
}
