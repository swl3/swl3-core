package ru.swayfarer.swl3.io.streams;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

import lombok.NonNull;
import ru.swayfarer.swl3.markers.Internal;


@SuppressWarnings("resource")
public class BytesInStream extends DataInStream{

	/** Обернутый поток */
	@Internal
	public ByteArrayInputStream bis;
	
	/** Получить байты */
	public byte[] getBytes()
	{
		return StreamsUtils.readAllAndClose(bis);
	}
	
	/** Создать поток */
	public static BytesInStream of(byte[] bytes)
	{
		return of(bytes, StreamConfiguration.defaultConfiguration());
	}
	
	/** Создать поток */
	public static BytesInStream of(byte[] bytes, @NonNull StreamConfiguration streamConfiguration)
	{
		if (bytes == null)
			return null;
		
		ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
		
		BytesInStream ret = (BytesInStream) new BytesInStream()
				.setConfiguration(streamConfiguration)
				.setDataInputStream(new DataInputStream(bis))
		;
		
		ret.bis = bis;
		
		return ret;
	}
}
