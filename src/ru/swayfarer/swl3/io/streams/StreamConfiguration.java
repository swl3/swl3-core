package ru.swayfarer.swl3.io.streams;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
public class StreamConfiguration {

	@NonNull
	public Charset encoding;
	@NonNull
	public String lineSplitter;
	
	public static StreamConfiguration defaultConfiguration()
	{
		return StreamConfiguration.builder()
			.encoding(StandardCharsets.UTF_8)
			.lineSplitter(System.lineSeparator())
		.build();
	}
	
}
