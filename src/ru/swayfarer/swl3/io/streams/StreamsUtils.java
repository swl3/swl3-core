package ru.swayfarer.swl3.io.streams;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import lombok.NonNull;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.io.buffers.DynamicByteBuffer;
import ru.swayfarer.swl3.io.streams.reader.StreamCopier;

public class StreamsUtils {

	/** Размер буффреа, используемого для операций с потоками */
	public static final int BUFFER_SIZE = 8096;

	/** Прочитать безопасно поток полностью и закрыть его. */
	public static byte[] readAllAndClose(InputStream is)
	{
		if (is == null)
			return null;

		try
		{
			byte[] bytes = readAll(is);
			is.close();
			return bytes;
		}
		catch (Throwable e)
		{
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Прочитать поток как строку
	 * 
	 * @param encoding  Кодировка строки
	 * @param stream Читаемый поток
	 * @return Полученная строка
	 */
	public static String readAll(String encoding, InputStream stream) throws IOException
	{
		if (stream == null)
			return null;

		InputStreamReader reader = encoding == null ? new InputStreamReader(stream) : new InputStreamReader(stream, encoding);

		char[] buffer = new char[1];

		StringBuilder builder = new StringBuilder();

		while (reader.read(buffer) != -1)
		{
			builder.append(buffer);
		}

		reader.close();

		return builder.toString();
	}

	/**
	 * Прочитать поток полностью
	 * 
	 * @throws IOException
	 */
	public static DynamicByteBuffer readAllToBuffer(InputStream stream) throws IOException
	{
		DynamicByteBuffer ret = DynamicByteBuffer.allocateDirect();
		
		int next = -1;
		
		while ((next = stream.read()) != -1)
		{
			ret.putUnsigned(next);
		}
		
		ret.position(0);
		
		return ret;
	}
	
	/** Прочитать поток полностью */
	public static byte[] readAll(InputStream stream)
	{
		try
		{
			byte[] buffer = new byte[BUFFER_SIZE];

			int read;
			int totalLength = 0;
			while ((read = stream.read(buffer, totalLength, buffer.length - totalLength)) != -1)
			{
				totalLength += read;

				// Extend our buffer
				if (totalLength >= buffer.length - 1)
				{
					byte[] newBuffer = new byte[buffer.length + BUFFER_SIZE];
					System.arraycopy(buffer, 0, newBuffer, 0, buffer.length);
					buffer = newBuffer;
				}
			}

			final byte[] result = new byte[totalLength];
			System.arraycopy(buffer, 0, result, 0, totalLength);
			return result;
		}
		catch (Throwable t)
		{
			ExceptionsUtils.throwException(t);
		}

		return null;
	}

	/** Копировать содержимое одного потока в другой */
	public static long copyStream(InputStream istream, OutputStream ostream, boolean isCloseIn, boolean isCloseOut) throws IOException
	{
		return copyStream(istream, ostream, null, isCloseIn, isCloseOut);
	}
	
	public static StreamCopier copy()
	{
	    return new StreamCopier();
	}
	
	/** Копировать содержимое одного потока в другой */
	public static long copyStream(@NonNull InputStream istream, @NonNull OutputStream ostream, IFunction1NoR<Long> progressListener, boolean isCloseIn, boolean isCloseOut) throws IOException
	{
		byte[] buffer = new byte[BUFFER_SIZE];

		long totalLen = 0;
		
		int len = istream.read(buffer);
		
		while (len != -1)
		{
		    ostream.write(buffer, 0, len);
			totalLen += len;
			
			if (progressListener != null) {
				progressListener.apply(totalLen);
			}
			
			len = istream.read(buffer);
		}

		if (isCloseOut)
		    ostream.close();

		if (isCloseIn)
		    istream.close();
		
		return totalLen;
	}
	
}
