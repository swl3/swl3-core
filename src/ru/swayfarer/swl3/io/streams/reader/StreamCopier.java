package ru.swayfarer.swl3.io.streams.reader;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.io.streams.StreamsUtils;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class StreamCopier implements IFunction2<InputStream, OutputStream, Long>{

    @NonNull
    public IFunction1NoR<Long> progressListener = (l) -> {};
    
    public int bufferSize = StreamsUtils.BUFFER_SIZE;
    
    public long maxBytesToRead = Long.MAX_VALUE;

    public long totalLenReaded = 0;
    
    public boolean isCloseIn;
    public boolean isCloseOut;
    
    public byte[] toBytes(@NonNull InputStream is)
    {
        var bos = new ByteArrayOutputStream();
        start(is, bos);
        return bos.toByteArray();
    }
    
    public long start(@NonNull InputStream is, @NonNull OutputStream os)
    {
        return apply(is, os);
    }
    
    @Override
    public Long applyUnsafe(InputStream is, OutputStream os) throws Throwable
    {
        byte[] buffer = new byte[getNextBufferSize()];
        
        int len = is.read(buffer);
        
        while (len != -1)
        {
            os.write(buffer, 0, len);
            totalLenReaded += len;
            
            if (progressListener != null) {
                progressListener.apply(totalLenReaded);
            }
            
            if (maxBytesToRead >= 0)
            {
                if (totalLenReaded > maxBytesToRead)
                {
                    throw new IndexOutOfBoundsException("Readed more than allowed (" + totalLenReaded + "/" + maxBytesToRead + ")");
                }
                
                if (totalLenReaded == maxBytesToRead)
                {
                    break;
                }
            }
            
            len = is.read(buffer);
        }

        if (isCloseOut)
            os.close();

        if (isCloseIn)
            os.close();
        
        return totalLenReaded;
    }
    
    public int getNextBufferSize()
    {
        if (maxBytesToRead < 0)
            return bufferSize;
        
        return (int) Math.min(maxBytesToRead - totalLenReaded, bufferSize);
    }
}
