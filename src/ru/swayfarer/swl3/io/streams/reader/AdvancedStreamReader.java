package ru.swayfarer.swl3.io.streams.reader;

import java.io.InputStream;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.io.streams.StreamsUtils;
import ru.swayfarer.swl3.threads.lock.NotifyLock;

@Getter
@Setter
@Accessors(chain = true)
public class AdvancedStreamReader {

    public AtomicLong maxReadTime = new AtomicLong();
    public AtomicLong maxByteTimeout = new AtomicLong(-1);
    public AtomicLong maxReadBytes = new AtomicLong(-1);
    
    public AdvancedStreamReader setMaxReadTime(long value, TimeUnit timeUnit)
    {
        maxReadTime.set(timeUnit.toMillis(value));
        return this;
    }
    
    public AdvancedStreamReader setMaxByteTime(long value, TimeUnit timeUnit)
    {
        maxByteTimeout.set(timeUnit.toMillis(value));
        return this;
    }
    
    public AdvancedStreamReader setMaxReadBytes(long value, TimeUnit timeUnit)
    {
        maxReadBytes.set(timeUnit.toMillis(value));
        return this;
    }
    
    public byte[] read(InputStream stream) 
    {
        var reader = new ReaderThread(stream);
        
        reader.getReadAllTimeout().set(maxReadTime.get());
        reader.getNextByteTimeout().set(maxByteTimeout.get());
        reader.getMaxReadBytes().set(maxReadBytes.get());
        
        reader.start();
        reader.notifyLock.waitFor();
        return reader.result;
    }
    
    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @Accessors(chain = true)
    public static class ShutdownThread extends Thread {
        
        @NonNull
        public ReaderThread readerThread;
        
        public AtomicBoolean isCopied = new AtomicBoolean();
        
        public ShutdownThread(ReaderThread readerThread)
        {
            setDaemon(true);
            this.readerThread = readerThread;
        }
        
        @SneakyThrows
        @Override
        public void run()
        {
            for (;;)
            {
                if (isCopied.get())
                {
                    return;
                }
                
                if (readerThread.isStarted.get())
                {
                    long startCopyTime = readerThread.getStartTime().get();
                    long lastCopyTime = readerThread.getLastCopyTime().get();
                    long currentTime = System.currentTimeMillis();
                    
                    long readAllTimeout = this.readerThread.readAllTimeout.get();
                    
                    if (readAllTimeout > 0)
                    {
                        if ((currentTime - startCopyTime) > readAllTimeout)
                        {
                            System.out.println("Readall timeout!");
                            readerThread.onCopied();
                            readerThread.interrupt();
                            return;
                        }
                    }
                    
                    long nextByteTimeout = this.readerThread.nextByteTimeout.get();
                    
                    if (nextByteTimeout > 0)
                    {
                        long l = currentTime - lastCopyTime;
                        if (l > nextByteTimeout)
                        {
                            System.out.println(l);
                            System.out.println("Next byte timeout (" + l + " > " + nextByteTimeout + ")");
                            readerThread.onCopied();
                            readerThread.interrupt();
                            return;
                        }
                    }
                }
                
                Thread.sleep(1);
            }
        }
    }
    
    @Getter
    @Setter
    @EqualsAndHashCode(callSuper = false)
    @Accessors(chain = true)
    public static class ReaderThread extends Thread {

        public AtomicLong readAllTimeout = new AtomicLong();
        public AtomicLong nextByteTimeout = new AtomicLong(1);
        
        @NonNull
        public InputStream stream;

        public AtomicBoolean isStarted = new AtomicBoolean(false);
        public AtomicLong startTime = new AtomicLong(System.currentTimeMillis());
        public AtomicLong lastCopyTime = new AtomicLong(System.currentTimeMillis());
        public AtomicLong maxReadBytes = new AtomicLong(-1);
        
        public volatile byte[] result;
        
        public ShutdownThread shutdownThread;
        
        public NotifyLock notifyLock = new NotifyLock();
        
        public ReaderThread(InputStream stream)
        {
            this.shutdownThread = new ShutdownThread(this);
            this.stream = stream;
        }
        
        @Override
        public void run()
        {
            shutdownThread.start();
            startTime.set(System.currentTimeMillis());
            isStarted.set(true);
            
            try
            {
                byte[] buffer = new byte[StreamsUtils.BUFFER_SIZE];

                int read;
                int totalLength = 0;
                long maxReadBytes = this.maxReadBytes.get();
                
                if (maxReadBytes >= 0 && maxReadBytes > totalLength)
                    return;
                
                while ((read = stream.read(buffer, totalLength, buffer.length - totalLength)) != -1)
                {
                    lastCopyTime.set(System.currentTimeMillis());
                    totalLength += read;
                    result = buffer;

                    // Extend our buffer
                    if (totalLength >= buffer.length - 1)
                    {
                        byte[] newBuffer = new byte[buffer.length + StreamsUtils.BUFFER_SIZE];
                        System.arraycopy(buffer, 0, newBuffer, 0, buffer.length);
                        buffer = newBuffer;
                    }
                }

                final byte[] result = new byte[totalLength];
                System.arraycopy(buffer, 0, result, 0, totalLength);
                this.result = result;
            }
            catch (Throwable t)
            {
                this.result = new byte[0];
            }
            
            onCopied();
        }
        
        public void onCopied()
        {
            notifyLock.notifyLockAll();
            shutdownThread.isCopied.set(true);
        }
    }

    public static void main(String[] args)
    {
    }
}
