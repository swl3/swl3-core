package ru.swayfarer.swl3.io.streams;

import java.io.ByteArrayOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicBoolean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.threads.lock.NotifyLock;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class OutToInStream extends FilterOutputStream {

    public volatile InStream inStream = new InStream();

    public OutToInStream()
    {
        this(new ByteArrayOutputStream());
    }
    
    public OutToInStream(OutputStream out)
    {
        super(out);
    }
    
    @Override
    public void write(int b) throws IOException
    {
        inStream.add(b);
        super.write(b);
    }
    
    @Override
    public void close() throws IOException
    {
        inStream.close();
        super.close();
    }
    
    public static class InStream extends InputStream {

        public AtomicBoolean isClosed = new AtomicBoolean();
        public NotifyLock lock = new NotifyLock();
        
        public Queue<Integer> bytes = new ConcurrentLinkedDeque<>();
        
        public InStream add(int b)
        {
            bytes.add(b);
            lock.notifyLock();
            return this;   
        }
        
        @Override
        public int read() throws IOException
        {
            return readOrWaitByte();
        }
        
        @Override
        public int available() throws IOException
        {
            return bytes.size();
        }
        
        public int readOrWaitByte() throws IOException
        {
            if (isClosed())
            {
                return -1;
            }
            
            var ret = bytes.poll();
            
            if (ret == null)
            {
                lock.waitFor();
                
                return readOrWaitByte();
            }
            
            return ret;
        }
        
        public boolean isClosed()
        {
            return bytes.isEmpty() && isClosed.get();
        }
        
        @Override
        public void close() throws IOException
        {
            isClosed.set(true);
            lock.notifyLockAll();
            super.close();
        }
    }
}
