package ru.swayfarer.swl3.io.streams;

import java.io.DataInputStream;
import java.io.InputStream;

import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;

public class UnreadHelper {

    public DataInputStream wrappedStream;
    public UnreadableInStream unreadableInStream;
    public LifetimeInputStream lifetimeInputStream;
    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
    
    public UnreadHelper(DataInStream dataInStream, DataInputStream wrappedStream)
    {
        this.wrappedStream = wrappedStream;
        this.unreadableInStream = new UnreadableInStream(wrappedStream);
        this.lifetimeInputStream = new LifetimeInputStream(wrappedStream, unreadableInStream, () -> dataInStream.dataInputStream = wrappedStream);
        lifetimeInputStream.exceptionsHandler.andAfter(exceptionsHandler);
    }
    
    public long getMaxLifetime()
    {
        return lifetimeInputStream.getMaxLifetime().get();
    }
    
    public int getBufferSize()
    {
        return unreadableInStream.getMaxBufferLenght();
    }
    
    public UnreadHelper setBufferSize(int size)
    {
        unreadableInStream.setMaxBufferLenght(size);
        return this;
    }
    
    public UnreadHelper unread(int count)
    {
        unreadableInStream.unread(count);
        return this;
    }
    
    public UnreadHelper setMaxLifetime(long lifetime)
    {
        lifetimeInputStream.getMaxLifetime().set(lifetime);
        return this;
    }
    
    public LifetimeInputStream getInputStream()
    {
        return lifetimeInputStream;
    }
    
    public boolean isEnabled()
    {
        return !lifetimeInputStream.isLifetimeExpired();
    }
}
