package ru.swayfarer.swl3.io.streams;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.crypto.CipherPair;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.io.StringLinesReadingHelper;
import ru.swayfarer.swl3.io.buffers.DynamicByteBuffer;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

@Getter @Setter @Accessors(chain = true)
public class DataInStream extends InputStream {

    public UnreadableInStream unreadableInStream;
	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	public StreamConfiguration configuration;
	public DataInputStream dataInputStream;
	
	public UnreadHelper unreadHelper;
	
	public StringLinesReadingHelper linesReadingHelper = new StringLinesReadingHelper();
	
	public Boolean readBoolean()
	{
		return exceptionsHandler.safeReturn(() -> dataInputStream.readBoolean(), null);
	}
	
	public Byte readByte() 
	{
		return exceptionsHandler.safeReturn(() -> dataInputStream.readByte(), null);
	}
	
	public Short readShort() 
	{
		return exceptionsHandler.safeReturn(() -> dataInputStream.readShort(), null);
	}
	
	public Integer readInt() 
	{
		return exceptionsHandler.safeReturn(() -> dataInputStream.readInt(), null);
	}
	
	public Long readLong() 
	{
		return exceptionsHandler.safeReturn(() -> dataInputStream.readLong(), null);
	}
	
	public Float readFloat() 
	{
		return exceptionsHandler.safeReturn(() -> dataInputStream.readFloat(), null);
	}
	
	public Double readDouble() 
	{
		return exceptionsHandler.safeReturn(() -> dataInputStream.readDouble(), null);
	}

	public String readUTF8()
	{
		return exceptionsHandler.safeReturn(() -> {
			return dataInputStream.readUTF();
		}, null);
	}
	public String readLengthedString()
	{
		return exceptionsHandler.safeReturn(() -> {
			int length = readInt();
			var bytes = readBytes(length);
			return new String(bytes, getConfiguration().getEncoding());
		}, null);
	}

	public byte[] readBytes(int count) throws IOException
	{
		var bytes = new byte[count];
		readBytes(bytes, 0, count);
		return bytes;
	}

	public int readBytes(byte[] b) throws IOException
	{
		return readBytes(b, 0, b.length);
	}

	public int readBytes(byte[] b, int off, int len) throws IOException
	{
		Objects.requireNonNull(b);
		if (off < 0 || len < 0 || len > b.length - off)
			throw new IndexOutOfBoundsException();
		int n = 0;
		while (n < len)
		{
			int count = read(b, off + n, len - n);
			if (count < 0)
				break;
			n += count;
		}
		return n;
	}
	
	public DataInStreamConverter to()
	{
	    return new DataInStreamConverter(this);
	}
	
	public boolean hasNext(int bytesCount)
	{
		enableUnread(bytesCount, bytesCount);
		for (int i1 = 0; i1 < bytesCount; i1 ++)
		{
			int next = read();
			
			if (next == -1)
			{
				unread(i1);
				return false;
			}
		}
		unread(bytesCount);
		
		return true;
	}

	public ExtendedStream<String> lines()
	{
		var nextLineRef = new AtomicReference<String>();
		nextLineRef.set(readLine());

		var iterator = new Iterator<String>()
		{
			@Override
			public boolean hasNext()
			{
				return nextLineRef.get() != null;
			}

			@Override
			public String next()
			{
				var result = nextLineRef.get();
				nextLineRef.set(readLine());
				return result;
			}
		};

		return CollectionsSWL.oneShootExStream(iterator);
	}

	public DataInStream crypt(@NonNull CipherPair cipherPair)
	{
		return crypt(cipherPair.getDecrypt());
	}

	public DataInStream crypt(@NonNull Cipher cipher)
	{
		this.dataInputStream = new DataInputStream(new CipherInputStream(dataInputStream, cipher));
		return this;
	}
	
	public boolean hasNext()
	{
	    enableUnread(1, 1);
	    int next = read();
	    unread(1);
        return next != -1;
	}
	
	public DynamicByteBuffer readAllToBuffer()
	{
		return exceptionsHandler.safeReturn(() -> {
			return StreamsUtils.readAllToBuffer(dataInputStream);
		}, null);
	}
	
	public String readAllAsString()
	{
	    return exceptionsHandler.safeReturn(() -> {
	        return new String(readAll(), configuration.getEncoding());
	    }, null);
	}
	
	public byte[] readAll()
	{
		return exceptionsHandler.safeReturn(() -> {
			return StreamsUtils.readAll(dataInputStream);
		}, null);
	}

	public UUID readUUID()
	{
		return new UUID(this.readLong(), this.readLong());
	}
	
	@Override
	public int available()
	{
		return exceptionsHandler.safeReturn(() -> super.available(), -1, "Error while getting available bytes of", this);
	}
	
	public String readLine()
	{
		return exceptionsHandler.safeReturn(() -> {
			return linesReadingHelper.readLine(this);
		}, null);
	}
	
	public boolean isEmpty()
	{
		return available() <= 0;
	}
	
	public static DataInStream of(InputStream is)
	{
		return of(is, StreamConfiguration.defaultConfiguration());
	}
	
	@SuppressWarnings("resource")
	public static DataInStream of(InputStream is, StreamConfiguration configuration)
	{
		if (is == null)
			return null;
		
		return new DataInStream()
				.setConfiguration(configuration)
				.setDataInputStream(new DataInputStream(is))
		;
	}
	
	public boolean isUnreadEnabled()
	{
	    return unreadHelper != null && unreadHelper.isEnabled();
	}
	
	public DataInStream enableUnread(int bufferSize, int lifetime)
	{
	    if (!isUnreadEnabled())
	    {
	        unreadHelper = new UnreadHelper(this, dataInputStream);
	        unreadHelper.exceptionsHandler.andAfter(getExceptionsHandler());
	    }
	    
	    if (unreadHelper.getMaxLifetime() < lifetime)
	        unreadHelper.setMaxLifetime(lifetime);
	    
	    if (unreadHelper.getBufferSize() < bufferSize)
	        unreadHelper.setBufferSize(bufferSize);
	    
	    return setWrappedStream(unreadHelper.getInputStream());
	}
    
    public DataInStream unread(int count)
    {
        if (!isUnreadEnabled())
            throw new UnsupportedOperationException("Unread is not active in " + this);
        
        unreadHelper.unread(count);
        return this;
    }
	
	public DataInStream setWrappedStream(InputStream is)
	{
	    dataInputStream = new DataInputStream(is);
	    return this;
	}
	
	@Override
	public boolean markSupported() {
		return dataInputStream.markSupported();
	}
	
	@Override
	public synchronized void mark(int readlimit) {
		dataInputStream.mark(readlimit);
	}
	
	@Override
	public int read() {
		return exceptionsHandler.safeReturn(() -> {
			return dataInputStream.read();
		}, -1);
	}
	
	@Override
	public int read(byte[] b) throws IOException
	{
	    return exceptionsHandler.safeReturn(() -> {
            return dataInputStream.read(b);
        }, -1);
	}
	
	@Override
	public int read(byte[] b, int off, int len) throws IOException
	{
	    return exceptionsHandler.safeReturn(() -> {
            return dataInputStream.read(b, off, len);
        }, -1);
	}
    
    @Override
    public synchronized void close() {
        exceptionsHandler.safe(() -> {
            dataInputStream.close();
        });
    }
	
	@Override
	public synchronized void reset() {
		exceptionsHandler.safe(() -> {
			dataInputStream.reset();
		});
	}
	
	@Override
	public long skip(long n) {
		return exceptionsHandler.safeReturn(() -> dataInputStream.skip(n), -1l);
	}

}
