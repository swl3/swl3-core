package ru.swayfarer.swl3.io.streams;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.crypto.CipherPair;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;

@Setter @Getter @Accessors(chain = true)
@SuppressWarnings({"unchecked", "resource"})
public class DataOutStream extends OutputStream {

    @Internal
    public AtomicBoolean isClosed = new AtomicBoolean();
    
	/** Событие закрытия потока */
	@Internal
	public IObservable<DataOutStream> eventClose = Observables.createObservable();
	
	@NonNull 
	public StreamConfiguration configuration;
	
	@NonNull 
	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	
	@NonNull 
	public DataOutputStream dataOutputStream;

	public <T extends DataOutStream> T writeStream(InputStream inputStream)
	{
		exceptionsHandler.safe(() -> {
			if (inputStream != null)
			{
				StreamsUtils.copy()
						.isCloseIn(true)
						.isCloseOut(false)
						.start(inputStream, this)
				;
			}
		});

		return (T) this;
	}

	public <T extends DataOutStream> T writeUTF8(@NonNull String str)
	{
		exceptionsHandler.safe(() -> {
			dataOutputStream.writeUTF(str);
		});
		
		return (T) this;
	}
	
	public DataOutStreamConverter to()
	{
	    return new DataOutStreamConverter(this);
	}
	
	public <T extends DataOutStream> T writeBoolean(boolean b)
	{
		exceptionsHandler.safe(() -> {
			dataOutputStream.writeBoolean(b);
		});
		
		return (T) this;
	}
	
	public <T extends DataOutStream> T copy(InputStream is)
	{
		exceptionsHandler.safe(() -> {
			StreamsUtils.copyStream(is, this, true, false);
		});
		
		return (T) this;
	}
	
	public <T extends DataOutStream> T writeBytes(byte[] b)
	{
		exceptionsHandler.safe(() -> {
			dataOutputStream.write(b);
		});
		
		return (T) this;
	}
	
	public DataInStream getInStream()
	{
	    return exceptionsHandler.safeReturn(() -> {
	        
	        if (isClosed())
	            throw new IllegalStateException("Out stream is closed!");
	        
	        var os = new OutToInStream(dataOutputStream);
	        dataOutputStream = new DataOutputStream(
	                os
	        );
	        
	        return DataInStream.of(os.getInStream());
	    }, null, "Error while creating out-to-in data stream");
	}
	
	public <T extends DataOutStream> T writeByte(int b)
    {
	    return writeByte((byte) b);
    }
	
	public <T extends DataOutStream> T writeByte(byte b)
	{
		exceptionsHandler.safe(() -> {
			dataOutputStream.writeByte(b);
		});
		
		return (T) this;
	}

	public <T extends DataOutStream> T crypt(@NonNull CipherPair cipherPair)
	{
		return crypt(cipherPair.getEncrypt());
	}
	
	public <T extends DataOutStream> T crypt(@NonNull Cipher cipher)
	{
	    this.dataOutputStream = new DataOutputStream(new CipherOutputStream(dataOutputStream, cipher));
	    return (T) this;
	}
	
	@Override
	public void write(byte[] b)
	{
		exceptionsHandler.safe(() -> {
			dataOutputStream.write(b);
		});
	}
	
	@Override
	public void write(byte[] b, int off, int len)
	{
		exceptionsHandler.safe(() -> {
			dataOutputStream.write(b, off, len);
		});
	}
	
	public <T extends DataOutStream> T writeShort(short i)
	{
		exceptionsHandler.safe(() -> {
			dataOutputStream.writeShort(i);
		});
		
		return (T) this;
	}
	
	public PacketBytesOutStream packet()
	{
		return new PacketBytesOutStream(this);
	}
	
	public <T extends DataOutStream> T writeInt(int i)
	{
		exceptionsHandler.safe(() -> {
			dataOutputStream.writeInt(i);
		});
		
		return (T) this;
	}
	
	public <T extends DataOutStream> T writeLong(long l)
	{
		exceptionsHandler.safe(() -> {
			dataOutputStream.writeLong(l);
		});
		
		return (T) this;
	}
	
	public <T extends DataOutStream> T writeFloat(float f)
	{
		exceptionsHandler.safe(() -> {
			dataOutputStream.writeFloat(f);
		});
		
		return (T) this;
	}
	
	public <T extends DataOutStream> T writeDouble(double d)
	{
		exceptionsHandler.safe(() -> {
			dataOutputStream.writeDouble(d);
		});
		
		return (T) this;
	}
	
	public <T extends DataOutStream> T writeString(@NonNull Object str)
	{
		exceptionsHandler.safe(() -> {
			dataOutputStream.write(String.valueOf(str).getBytes(getConfiguration().getEncoding()));
		});
		
		return (T) this;
	}
	
	public <T extends DataOutStream> T writeLine(@NonNull Object str)
	{
		writeString(str);
		writeString(getConfiguration().getLineSplitter());
		
		return (T) this;
	}
	
	public <T extends DataOutStream> T writeLengthedString(@NonNull String str)
	{
		exceptionsHandler.safe(() -> {
			var bytes = String.valueOf(str).getBytes(getConfiguration().getEncoding());
			dataOutputStream.writeInt(bytes.length);
			dataOutputStream.write(bytes);
		});
		
		return (T) this;
	}

	public <T extends DataOutStream> T writeUUID(@NonNull UUID uuid)
	{
		this.writeLong(uuid.getMostSignificantBits());
		this.writeLong(uuid.getLeastSignificantBits());
		return (T) this;
	}
	
	public static DataOutStream of(OutputStream os)
	{
		return of(os, StreamConfiguration.defaultConfiguration());
	}
	
	public static DataOutStream of(OutputStream os, StreamConfiguration configuration)
	{
		return new DataOutStream()
				.setDataOutputStream(new DataOutputStream(os))
				.setConfiguration(configuration)
		;
	}

	@Override
	public void write(int b) throws IOException {
		dataOutputStream.write(b);
	}
	
	@Override
	public void close() {
	    isClosed.set(true);
		eventClose.next(this);
		exceptionsHandler.safe(() -> {
			super.close();
			dataOutputStream.close();
		});
	}
	
	public boolean isClosed()
	{
	    return isClosed.get();
	}
	
	@Override
	public void flush() {
		exceptionsHandler.safe(() -> {
			dataOutputStream.flush();
		});
	}
	
}
