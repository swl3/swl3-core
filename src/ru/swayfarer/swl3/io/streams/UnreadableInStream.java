package ru.swayfarer.swl3.io.streams;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;

/**
 * Поток, байты которого можно восстановить ("рачтитать" назад) после прочтения <br>
 * <p>
 * Имеет буффер, который хранит последние прочитанные байты <br>
 * Размер буффера ограничен, при прочтении байтов сверх размера буффера старые байты будут удалены 
 * @author swayfarer
 *
 */
@Data @EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class UnreadableInStream extends InputStream {

    /**
     * Максимальный размер буффера, хранящего возможные для восстановления байты
     */
    public int maxBufferLenght = 128;
    
    /**
     * Количество байтов, которые будут восстановлены
     */
    public int unreadedPos;
    
    /**
     * Буффер, хранящий байты, доступные для восстановления <br>
     * Новые байты добавляются в начало, при восстановлении берутся с конца.
     */
    public LinkedList<Integer> buffer = new LinkedList<>();
    
    /**
     * Обернутый поток
     */
    @NonNull
    public InputStream wrappedStream;
    
    @Override
    public int read() throws IOException
    {
        if (unreadedPos > 0 && buffer.size() > 0)
        {
            int b = buffer.get(buffer.size() - unreadedPos);
            unreadedPos --;
            
            return b;
        }
        
        int ret = wrappedStream.read();
        
        addToBuffer(ret);
        
        return ret;
    }

    public void addToBuffer(int i)
    {
        buffer.addLast(i);
        
        int size = buffer.size();
        
        while (size > maxBufferLenght)
        {
            buffer.removeFirst();
            size --;
        }
    }
    
    @Override
    public int available() throws IOException
    {
        return wrappedStream.available() + unreadedPos;
    }

    @Override
    public void close() throws IOException
    {
        wrappedStream.close();
        super.close();
    }

    @Override
    public synchronized void mark(int readlimit)
    {
        cleanBuffer();
        wrappedStream.mark(readlimit);
    }

    @Override
    public synchronized void reset() throws IOException
    {
        if (wrappedStream.markSupported())
        {
            cleanBuffer();
            wrappedStream.reset();
        }
    }

    @Override
    public boolean markSupported()
    {
        return wrappedStream.markSupported();
    }
    
    public void cleanBuffer()
    {
        unreadedPos = 0;
        buffer.clear();
    }
    
    public void unread(int count)
    {
        unreadedPos += count;
        
        if (unreadedPos > buffer.size() || unreadedPos > maxBufferLenght)
            throw new IndexOutOfBoundsException("Can't unread more than buffer size!");
    }
}
