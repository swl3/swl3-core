package ru.swayfarer.swl3.io.streams;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipInputStream;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;

import lombok.Data;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.io.xz.tukaani.XZInputStream;

@Data
@Accessors(chain = true)
public class DataInStreamConverter {

    @NonNull
    public DataInStream dataInStream;
    
    public DataInStream zip()
    {
        return wrap(ZipInputStream::new);
    }
    
    public DataInStream gz()
    {
        return wrap(GZIPInputStream::new);
    }

    public DataInStream xz()
    {
        return wrap(XZInputStream::new);
    }
    
    public DataInStream crypt(@NonNull Cipher cipher)
    {
        return wrap((stream) -> new CipherInputStream(stream, cipher));
    }
    
    public DataInStream wrap(@NonNull IFunction1<InputStream, InputStream> source)
    {
        return exceptionsHandler().safeReturn(
                () -> dataInStream.setDataInputStream(
                        new DataInputStream(
                                source.apply(dataInStream.getDataInputStream())
                        )
                ),
                null,
                "Error while wrapping stream"
        );
                
    }

    @SneakyThrows
    public Scanner scanner()
    {
        return new Scanner(new InputStreamReader(dataInStream, StandardCharsets.UTF_8));
    }

    public BufferedReader reader()
    {
        return new BufferedReader(new InputStreamReader(dataInStream, dataInStream.getConfiguration().getEncoding()));
    }
    
    public DataInStream wrap(@NonNull IFunction0<InputStream> source)
    {
        return exceptionsHandler().safeReturn(
                () -> dataInStream.setDataInputStream(new DataInputStream(source.apply())), 
                null,
                "Error while wrapping stream"
        );
    }
    
    public ExceptionsHandler exceptionsHandler()
    {
        return dataInStream.getExceptionsHandler();
    }
}
