package ru.swayfarer.swl3.io.streams;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

import ru.swayfarer.swl3.markers.Internal;

/**
 * {@link ByteArrayOutputStream}, с которым можно работать как с {@link DataOutputStreamSWL} 
 * @author swayfarer
 */
@SuppressWarnings("resource")
public class BytesOutStream extends DataOutStream {

	/** Обернутый {@link ByteArrayOutputStream}*/
	@Internal
	public ByteArrayOutputStream bytesOut;
	
	/** Получить все байты */
	public byte[] toBytesArray()
	{
		return bytesOut == null ? null : bytesOut.toByteArray();
	}
	
	/** Создать поток */
	public static BytesOutStream createStream()
	{
		return createStream(StreamConfiguration.defaultConfiguration());
	}
	
	/** Создать поток */
	public static BytesOutStream createStream(StreamConfiguration streamConfiguration)
	{
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		
		BytesOutStream ret = (BytesOutStream) new BytesOutStream()
				.setConfiguration(streamConfiguration)
				.setDataOutputStream(new DataOutputStream(bytes))
		;
				
		ret.bytesOut = bytes;
		return ret;
	}
}
