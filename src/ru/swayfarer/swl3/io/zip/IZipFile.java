package ru.swayfarer.swl3.io.zip;

import java.util.zip.ZipEntry;

import lombok.NonNull;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.io.streams.DataInStream;

public interface IZipFile {
    
    public ExtendedList<ZipEntry> findAllEntries();
    public ZipEntry findEntry(@NonNull String path);
    public DataInStream getInputStream(@NonNull ZipEntry zipEntry);
    
    public default ZipFileIOEntryProvider asIOEntryProvider()
    {
        return new ZipFileIOEntryProvider(this);
    }
}
