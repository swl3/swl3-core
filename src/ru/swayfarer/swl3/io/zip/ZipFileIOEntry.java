package ru.swayfarer.swl3.io.zip;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.io.entry.IIOEntry;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ZipFileIOEntry implements IIOEntry{
    
    @NonNull
    public String entryName;

    @Override
    public String getName()
    {
        return entryName;
    }
}
