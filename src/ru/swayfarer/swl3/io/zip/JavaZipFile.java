package ru.swayfarer.swl3.io.zip;

import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import lombok.Data;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.io.streams.DataInStream;

@Data
@Accessors(chain = true)
public class JavaZipFile implements IZipFile{

    @NonNull
    public ZipFile wrappedZipFile;
    
    @Override
    public ZipEntry findEntry(@NonNull String path)
    {
        return wrappedZipFile.getEntry(path);
    }

    @SneakyThrows
    @Override
    public DataInStream getInputStream(@NonNull ZipEntry zipEntry)
    {
        var is = wrappedZipFile.getInputStream(zipEntry);
        return is == null ? null : DataInStream.of(is);
    }

    @Override
    public ExtendedList<ZipEntry> findAllEntries()
    {
        var entries = wrappedZipFile.entries();
        var ret = new ExtendedList<ZipEntry>();
        
        while (entries.hasMoreElements())
        {
            ret.add(entries.nextElement());
        }
        
        return ret;
    }
}
