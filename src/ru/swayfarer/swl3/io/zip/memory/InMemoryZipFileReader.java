package ru.swayfarer.swl3.io.zip.memory;

import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.io.streams.StreamsUtils;

public class InMemoryZipFileReader {

    public InMemoryZipFile read(@NonNull InputStream is)
    {
        try
        {
            var ret = new InMemoryZipFile();
            
            ZipInputStream zis = null;
            
            if (is instanceof ZipInputStream)
            {
                zis = (ZipInputStream) is;
            }
            else
            {
                zis = new ZipInputStream(is);
            }
            
            ZipEntry nextEntry = null;
            
            while ((nextEntry = zis.getNextEntry()) != null)
            {
                var entryWrapper = new InMemoryZipFile.ZipEntryWrapper();
                
                var content = StreamsUtils.copy()
                    .toBytes(zis)
                ;
                
                entryWrapper.setEntry(nextEntry);
                entryWrapper.setContentBytes(content);
                
                ret.addEntry(entryWrapper);
                
                zis.closeEntry();
            }
            
            ExceptionsUtils.safe(zis::close);
            
            return ret;
        }
        catch (Throwable e)
        {
            throw new InMemotyZipReadingException(e);
        }
    }
}
