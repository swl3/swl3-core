package ru.swayfarer.swl3.io.zip.memory;

import java.util.zip.ZipEntry;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.io.streams.BytesInStream;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.zip.IZipFile;
import ru.swayfarer.swl3.markers.Alias;

@Data
@Accessors(chain = true)
public class InMemoryZipFile implements IZipFile{

    @NonNull
    public ExtendedList<ZipEntryWrapper> entries = new ExtendedList<>().synchronyzed();
    
    @NonNull
    public ExtendedMap<String, ZipEntryWrapper> fastZipEntryAccessor = new ExtendedMap<>().synchronize();
    
    public ZipEntryWrapper findEntryWrapper(@NonNull String name)
    {
        var ret = fastZipEntryAccessor.get(name);
        
        if (ret != null)
        {
            return ret;
        }
        
        return this.entries.exStream().first((entry) -> entry.getEntry().getName().equals(name));
    }
    
    public ZipEntryWrapper findEntryInList(@NonNull String name)
    {
        return this.entries.exStream().first((entry) -> entry.getEntry().getName().equals(name));
    }
    
    @Alias("putEntry")
    public InMemoryZipFile addEntry(@NonNull ZipEntryWrapper wrapper) 
    {
        return putEntry(wrapper);
    }
    
    public InMemoryZipFile putEntry(@NonNull ZipEntryWrapper wrapper) 
    {
        var entry = wrapper.getEntry();
        fastZipEntryAccessor.put(entry.getName(), wrapper);
        this.entries.add(wrapper);
        return this;
    }
    
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @SuperBuilder
    @Accessors(chain = true)
    public static class ZipEntryWrapper {
        public byte[] contentBytes;
        public ZipEntry entry;
        
        public DataInStream in()
        {
            return BytesInStream.of(contentBytes);
        }
    }

    @Override
    public ZipEntry findEntry(@NonNull String path)
    {
        var wrapper = findEntryWrapper(path);
        return wrapper == null ? null : wrapper.getEntry();
    }

    @Override
    public DataInStream getInputStream(@NonNull ZipEntry zipEntry)
    {
        var wrapper = findEntryWrapper(zipEntry.getName());
        return wrapper == null ? null : wrapper.in();
    }

    @Override
    public ExtendedList<ZipEntry> findAllEntries()
    {
        return entries.map(ZipEntryWrapper::getEntry);
    }
}
