package ru.swayfarer.swl3.io.zip.memory;

@SuppressWarnings("serial")
public class InMemotyZipReadingException extends RuntimeException {

    public InMemotyZipReadingException()
    {
        super();
    }

    public InMemotyZipReadingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public InMemotyZipReadingException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public InMemotyZipReadingException(String message)
    {
        super(message);
    }

    public InMemotyZipReadingException(Throwable cause)
    {
        super(cause);
    }
}
