package ru.swayfarer.swl3.io.zip;

import java.util.zip.ZipEntry;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.io.entry.IIOEntry;
import ru.swayfarer.swl3.io.entry.IIOEntryProvider;
import ru.swayfarer.swl3.io.streams.DataInStream;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ZipFileIOEntryProvider implements IIOEntryProvider {

    @NonNull
    public IZipFile zipFile;
    
    @Override
    public DataInStream getInputStream(@NonNull IIOEntry entry)
    {
        return zipFile.getInputStream(zipFile.findEntry(entry.getName()));
    }

    @Override
    public IIOEntry findEntry(@NonNull String entryPath)
    {
        var zipEntry = zipFile.findEntry(entryPath);
        return zipEntry == null ? null : createEntry(zipEntry);
    }

    @Override
    public ExtendedList<IIOEntry> findAllEntries()
    {
        return zipFile.findAllEntries()
                .exStream()
                .nonNull()
                .map(this::createEntry)
                .map((e) -> (IIOEntry) e)
                .toExList()
        ;
    }
    
    public ZipFileIOEntry createEntry(@NonNull ZipEntry zipEntry)
    {
        return ZipFileIOEntry.builder()
                .entryName(zipEntry.getName())
                .build()
        ;
    }
}
