package ru.swayfarer.swl3.io;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.io.buffers.DynamicByteBuffer;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.markers.Internal;

@Getter @Setter @Accessors(chain = true)
@SuppressWarnings("unchecked")
public class StringLinesReadingHelper {

	/** Разделитель строк  */
	public String lineSplitter = "\n";
	
	public Charset encoding = StandardCharsets.UTF_8;
	
	/** Буфер прочитанных байтов */
	@Internal
	public DynamicByteBuffer buffer = new DynamicByteBuffer();
	
	/** В этот буффер попадают символы, которые подохреваются на совпадение с {@link #lineSplitter}. После отмены подохрений забрасываются в прочитанную строку. */
	@Internal
	public DynamicByteBuffer waiting = new DynamicByteBuffer(64);

	/** Задать разделитель строк */
	public <T extends StringLinesReadingHelper> T setLineSplitter(String splitter)
	{
		this.lineSplitter = splitter;
		return (T) this;
	}
	
	/** Прочитать строку */
	public synchronized String readLine(DataInStream in) throws IOException
	{
		waiting.clear();
		buffer.clear();
		
		int unsignedByte;
		int splitterCompareIndex = 0;
		char ch;
		
		while ((unsignedByte = in.read()) > 0)
		{
			ch = (char) unsignedByte;
			
			if (ch == lineSplitter.charAt(splitterCompareIndex))
			{
				
				splitterCompareIndex ++;
				waiting.putUnsigned(unsignedByte);
			}
			else 
			{
				if (splitterCompareIndex > 0)
				{
					buffer.put(waiting);
					waiting.clear();
					splitterCompareIndex = 0;
				}
				
				buffer.putUnsigned(unsignedByte);
			}
			
			if (splitterCompareIndex == lineSplitter.length())
			{
				break;
			}
		}

		var bytes = buffer.getOnlyUsedBytes();

		return CollectionsSWL.isNullOrEmpty(bytes) ? null : new String(buffer.getOnlyUsedBytes(), encoding);
	}
	
}
