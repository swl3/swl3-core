package ru.swayfarer.swl3.io.entry;

import lombok.NonNull;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.io.streams.DataInStream;

public interface IIOEntryProvider {
    public DataInStream getInputStream(@NonNull IIOEntry entry);
    public IIOEntry findEntry(@NonNull String entryPath);
    public ExtendedList<IIOEntry> findAllEntries();
}
