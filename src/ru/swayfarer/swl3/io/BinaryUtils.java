package ru.swayfarer.swl3.io;

import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.io.buffers.DynamicByteBuffer;
import ru.swayfarer.swl3.io.streams.BytesInStream;
import ru.swayfarer.swl3.io.streams.BytesOutStream;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.unit.UnitRegistry;

public class BinaryUtils
{
	public static BinaryUtils INSTANCE = new BinaryUtils();

	@Internal
	public UnitRegistry sizeUnitRegistry = new UnitRegistry().unit("kb", 1024).unit("mb", 1024 * 1024).unit("gb", 1024 * 1024 * 1024);

	@Internal
	public byte[] hexArray = "0123456789ABCDEF".getBytes(StandardCharsets.US_ASCII);

	@Internal
	public ThreadLocal<DynamicByteBuffer> threadLocalBuffer = new ThreadLocal<DynamicByteBuffer>()
	{
		protected DynamicByteBuffer initialValue()
		{
			return DynamicByteBuffer.allocateDirect();
		}

		;
	};

	public int SHORT_BYTES_SIZE = 2;
	public int INT_BYTES_SIZE = 4;
	public int FLOAT_BYTES_SIZE = 4;
	public int DOUBLE_BYTES_SIZE = 8;
	public int LONG_BYTES_SIZE = 8;

	public ByteOrder DEFAULT_BYTE_ORDER = ByteOrder.LITTLE_ENDIAN;

	/**
	 * Записать unsigned byte в int
	 */
	public int toUnsignedByte(byte b)
	{
		if (b >= 0)
			return b;

		return ((int) b) - ((int) Byte.MIN_VALUE) * 2;
	}

	/**
	 * Записать unsigned int в long
	 */
	public long toUnsignedInt(int b)
	{
		if (b >= 0)
			return b;

		return ((long) b) - ((long) Integer.MIN_VALUE) * 2;
	}

	/**
	 * Записать unsigned float в double
	 */
	public double toUnsignedFloat(float b)
	{
		if (b >= 0)
			return b;

		return ((double) b) - ((double) Float.MIN_VALUE) * 2;
	}

	public String toHexString(byte[] bytes)
	{
		byte[] hexChars = new byte[bytes.length * 2];
		for (int j = 0; j < bytes.length; j++)
		{
			int v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars, StandardCharsets.UTF_8);
	}

	public byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
					+ Character.digit(s.charAt(i+1), 16));
		}
		return data;
	}

	/**
	 * Получить байты числа в {@link #DEFAULT_BYTE_ORDER}
	 */
	public byte[] toBytes(short i)
	{
		DynamicByteBuffer buffer = threadLocalBuffer.get();
		buffer.putShort(i);
		byte[] ret = buffer.getOnlyUsedBytes();
		buffer.clear();
		return ret;
	}

	/**
	 * Получить байты числа в {@link #DEFAULT_BYTE_ORDER}
	 */
	public byte[] toBytes(int i)
	{
		DynamicByteBuffer buffer = threadLocalBuffer.get();
		buffer.putInt(i);
		byte[] ret = buffer.getOnlyUsedBytes();
		buffer.clear();
		return ret;
	}

	/**
	 * Получить байты числа в {@link #DEFAULT_BYTE_ORDER}
	 */
	public byte[] toBytes(long i)
	{
		DynamicByteBuffer buffer = threadLocalBuffer.get();
		buffer.putLong(i);
		byte[] ret = buffer.getOnlyUsedBytes();
		buffer.clear();
		return ret;
	}

	/**
	 * Получить байты числа в {@link #DEFAULT_BYTE_ORDER}
	 */
	public byte[] toBytes(float i)
	{
		DynamicByteBuffer buffer = threadLocalBuffer.get();
		buffer.putFloat(i);
		byte[] ret = buffer.getOnlyUsedBytes();
		buffer.clear();
		return ret;
	}

	/**
	 * Получить байты числа в {@link #DEFAULT_BYTE_ORDER}
	 */
	public byte[] toBytes(double i)
	{
		DynamicByteBuffer buffer = threadLocalBuffer.get();
		buffer.putDouble(i);
		byte[] ret = buffer.getOnlyUsedBytes();
		buffer.clear();
		return ret;
	}

	/**
	 * Преобразовать float, записанный double, в signed
	 */
	public float toSignedFloat(double i)
	{
		if (i > Float.MAX_VALUE)
			i = i + Float.MIN_VALUE * 2;

		if (i > Float.MAX_VALUE)
			throw new IllegalArgumentException("Bigger double!");

		if (i < Float.MIN_VALUE)
			throw new IllegalArgumentException("Smaller double!");

		return (float) i;
	}

	/**
	 * Преобразовать int, записанный long, в signed
	 */
	public int toSignedInt(long i)
	{
		if (i > Integer.MAX_VALUE)
			i = i + Integer.MIN_VALUE * 2;

		if (i > Integer.MAX_VALUE)
			throw new IllegalArgumentException("Bigger int!");

		if (i < Integer.MIN_VALUE)
			throw new IllegalArgumentException("Smaller int!");

		return (int) i;
	}

	/**
	 * Преобразовать byte, записанный int, в signed
	 */
	public byte toSignedByte(int i)
	{
		if (i > Byte.MAX_VALUE)
			i = i + Byte.MIN_VALUE * 2;

		if (i > Byte.MAX_VALUE)
			throw new IllegalArgumentException("Bigger byte!");

		if (i < Byte.MIN_VALUE)
			throw new IllegalArgumentException("Smaller byte!");

		return (byte) i;
	}

	public byte[] ensureMultiplicity(int multiplicity, byte[] source)
	{
		if (source.length % multiplicity != 0)
		{
			var i = multiplicity - (source.length % multiplicity);
			var newBytes = new byte[source.length + i];
			System.arraycopy(source, 0, newBytes, 0, source.length);
			return newBytes;
		}

		return source;
	}

	public byte[] unpackFromGz(byte[] source)
	{
		return BytesInStream.of(source).to().gz().readAll();
	}

	public byte[] packToGz(byte[] source)
	{
		var bos = BytesOutStream.createStream();
		bos.to().gz().writeBytes(source);
		bos.flush();
		bos.close();
		return bos.toBytesArray();
	}

	public long bytesOf(@NonNull String bytesWithUnit)
    {
        return sizeUnitRegistry.parse(bytesWithUnit).longValue();
    }

    public static BinaryUtils getInstance()
	{
		return INSTANCE;
	}
}
