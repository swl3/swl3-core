package ru.swayfarer.swl3.io.path;

import lombok.NonNull;

/**
 * Хэлпер для работы с путями
 * @author swayfarer
 *
 */
public class PathHelper {

	
	/** 
	 * Заменить двойные/тройные слеши на одинарные, бэкслеши - на слеши. 
	 * @param path Обрабатываемая строка
	 * @return Обработанная строка
	 */
	public static String fixSlashes(String path)
	{
		if (path == null)
			return null;
		
		while (path.contains("//"))
			path = path.replace("//", "/");
		
		while (path.contains("\\"))
			path = path.replace("\\", "/");
		
		return path;
	}
	/**
	 * Получить простое имя ресурса без расширения: <br>
	 * <code> /path/to/resource.txt </code> -> <code> resource </code>
	 * @param resourcePath Путь до ресурса
	 * @return Искомое имя 
	 */
	public static String getSimpleNameWithoutExtension(@NonNull String resourcePath)
	{
		resourcePath = fixSlashes(resourcePath);
		
		String name = getSimpleName(resourcePath);
		String extension = getExtension(resourcePath);
		
		if (extension.isEmpty())
			return name;
		
		return name.substring(0, name.length() - extension.length() - 1);
	}
	
	/**
	 * Получить путь до родителя ресурса: <br>
	 * <code> /path/to/resource.txt </code> -> <code> /path/to/ </code>
	 * @param resourcePath Путь до ресурса
	 * @return Искомый путь
	 */
	public static String getResourceParentPath(@NonNull String resourcePath)
	{
		resourcePath = fixSlashes(resourcePath);
		
		String link = resourcePath;
		int indexOfSlash = link.lastIndexOf("/");
		
		if (link.length() - 1 == indexOfSlash)
		{
			String tmp = link.substring(0, link.length() - 1);
			
			indexOfSlash = tmp.lastIndexOf("/");
		}
		
		if (indexOfSlash > 0)
		{
			return link.substring(0, indexOfSlash);
		}
		
		return link;
	}
	
	/**
	 * Получить расширение ресурса: <br>
	 * <code> /path/to/resource.txt </code> -> <code> txt </code>
	 * @param resourcePath Путь до ресурса
	 * @return Расширение ресурса 
	 */
	public static String getExtension(@NonNull String resourcePath)
	{
		resourcePath = fixSlashes(resourcePath);
		
		String name = getSimpleName(resourcePath);
		
		String ret = "";
		
		char[] chars = name.toCharArray();
		
		for (char ch : chars)
		{
			if (ch == '.')
			{
				ret = "";
			}
			else
			{
				ret += ch;
			}
		}
		
		if (ret.equalsIgnoreCase(name))
			return "";
		
		return ret;
	}
	
	/**
	 * Получить простое имя ресурса: <br>
	 * <code> /path/to/resource.txt </code> -> <code> resource.txt </code>
	 * @param resourcePath Путь до ресурса
	 * @return Искомое имя
	 */
	public static String getSimpleName(@NonNull String resourcePath)
	{
		resourcePath = fixSlashes(resourcePath);
		
		String link = resourcePath;
		int indexOfSlash = link.lastIndexOf("/");
		
		if (link.length() - 1 == indexOfSlash)
		{
			String tmp = link.isEmpty() ? link : link.substring(0, link.length() - 1);
			
			indexOfSlash = tmp.lastIndexOf("/");
		}
		
		if (indexOfSlash > 0)
		{
			return link.substring(indexOfSlash + 1);
		}
		
		return link;
	}
	
	/**
	 * Получить имя родительского ресурса: <br>
	 * <code> /path/to/resource.txt </code> -> <code> to </code>
	 * @param resourcePath Путь до ресурса
	 * @return Искомое имя 
	 */
	public String getParentName(@NonNull String resourcePath)
	{
		resourcePath = fixSlashes(resourcePath);
		
		String link = resourcePath;
		int indexOfSlash = resourcePath.lastIndexOf("/");
		
		if (link.length() - 1 == indexOfSlash)
		{
			String tmp = link.substring(0, link.length() - 1);
			
			indexOfSlash = tmp.lastIndexOf("/");
		}
		
		if (indexOfSlash > 0)
		{
			return link.substring(0, indexOfSlash);
		}
		
		return link;
	}
	
}
