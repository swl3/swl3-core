package ru.swayfarer.swl3.io.path;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.io.path.rules.StandartPathPackers;
import ru.swayfarer.swl3.io.path.rules.StandartPathTransformers;
import ru.swayfarer.swl3.markers.Internal;

/**
 * Преобразователь путей для ресурсов <br> 
 * Содержит кастомные правила для преобразования путей, при помощи которых можно создавать интерактивные выражения, 
 * вроде даты в указанном формате, до простых шаблонов, вроде %appDir% = корень приложения
 * @author swayfarer
 *
 */
@Getter @Setter
@Accessors(chain = true)
public class PathTransforms {
	
	/** Преобразователи (<code> %appDir% </code> -> <code> корень_приложения </core> )*/
	@Internal
	public List<IPathTransformer> transformers = new ArrayList<>();
	
	/** Обратные преобразователи (<code> корень_приложения </core> -> <code> %appDir% </code> )*/
	@Internal
	public List<IPathPacker> packers = new ArrayList<>();
	
	/**
	 * Преобразовать путь, применив {@link #transformers}
	 * @param path Путь
	 * @return Преобразованный путь
	 */
	public String transform(String path)
	{
		for (var fun : transformers)
		{
			path = fun.apply(path);
		}
		
		return path;
	}

	/**
	 * Обратно преобразовать путь, применив {@link #packers}
	 * @param path Путь
	 * @return Преобразованный путь
	 */
	public String pack(String path)
	{
		for (var fun : packers)
		{
			path = fun.apply(path);
		}
		
		return path;
	}
	
	/**
	 * Зарегистрировать преобразователь путей
	 * @param packer Преобразователь
	 * @return this
	 */
	public PathTransforms transformer(IPathTransformer packer)
	{
		this.transformers.add(packer);
		return this;
	}
	
	/**
	 * Зарегистрировать обратный преобразователь путей
	 * @param packer Обратный преобразователь
	 * @return this
	 */
	public PathTransforms packer(IPathPacker packer)
	{
		this.packers.add(packer);
		return this;
	}
	
	/**
	 * Создать {@link PathTransforms} со стандартным набором преобразований 
	 * @return Созданный {@link PathTransforms}
	 */
	public static PathTransforms defaultTransforms()
	{
		return new PathTransforms()
				
				.packer(StandartPathPackers.appDir)
				.packer(StandartPathPackers.java)
				.packer(StandartPathPackers.javaHome)
				.packer(StandartPathPackers.temp)
				.packer(StandartPathPackers.user)
				
				.transformer(StandartPathTransformers.nowDate)
				.transformer(StandartPathTransformers.startDate)
				.transformer(StandartPathTransformers.appDir)
				.transformer(StandartPathTransformers.java)
				.transformer(StandartPathTransformers.javaHome)
				.transformer(StandartPathTransformers.randUUID)
				.transformer(StandartPathTransformers.temp)
				.transformer(StandartPathTransformers.user)
		;
	}
	
	public static interface IPathTransformer extends IFunction1<String, String>  {}
	
	public static interface IPathPacker extends IFunction1<String, String> {}
	
}
