package ru.swayfarer.swl3.io.path.rules;

import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.io.path.PathTransforms.IPathTransformer;
import ru.swayfarer.swl3.string.StringUtils;

@Getter @Setter
@Accessors(chain = true)
public class DateTransformerRule implements IPathTransformer {
	
	@NonNull
	public IFunction0<Date> dateFun;
	
	@NonNull
	public String dateRegex;
	
	@NonNull
	public String prefix;
	
	@NonNull
	public String postfix;
	
	public DateTransformerRule(IFunction0<Date> dateFun, String prefix, String postfix)
	{
		this.prefix = prefix;
		this.postfix = postfix;
		this.dateFun = dateFun;
		updateRegex();
	}
	
	public DateTransformerRule updateRegex()
	{
		dateRegex = StringUtils.regex()
				.text(prefix)
					.some()
					.not(prefix, postfix)
				.text(postfix)
		.build();
		
		return this;
	}
	
	public static DateTransformerRule now(String prefix, String postfix)
	{
		return new DateTransformerRule(() -> new Date(), prefix, postfix);
	}
	
	public static DateTransformerRule of(Date date, String prefix, String postfix)
	{
		return new DateTransformerRule(() -> date, prefix, postfix);
	}
	
	@Override
	public String applyUnsafe(@NonNull String str) 
	{
		var matches = StringUtils.findAllMatches(dateRegex, str);
		
		for (var match : matches)
		{
			var dateFormat = match.substring(prefix.length(), match.length() - postfix.length());
			var date = dateFun.apply();
			var formatter = new SimpleDateFormat(dateFormat);
			str = str.replace(match, formatter.format(date));
		}
		
		return str;
	}

}
