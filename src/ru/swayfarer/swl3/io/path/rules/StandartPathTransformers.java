package ru.swayfarer.swl3.io.path.rules;

import java.util.Date;
import java.util.UUID;

import ru.swayfarer.swl3.io.path.PathTransforms.IPathTransformer;
import ru.swayfarer.swl3.system.SystemUtils;

public interface StandartPathTransformers {

	public static final SystemUtils SYSTEM_UTILS = new SystemUtils();
	
	public static final ReplaceTransformerRule temp = ReplaceTransformerRule.of("%temp%", SYSTEM_UTILS.isWindows() ? System.getProperty("user.home")+"/AppData/Temp/" : "/tmp/");
	
	public static final ReplaceTransformerRule user = ReplaceTransformerRule.of("%user%", System.getProperty("user.home"));
	
	public static final ReplaceTransformerRule java = ReplaceTransformerRule.of("%java%", System.getProperty("java.home")+"/bin/java"+(SYSTEM_UTILS.isWindows() ? ".exe" : ""));
	public static final ReplaceTransformerRule javaHome = ReplaceTransformerRule.of("%javaHome%", System.getProperty("java.home"));
	
	public static final ReplaceTransformerRule appDir = ReplaceTransformerRule.of("%appDir%", System.getProperty("user.dir"));
	
	public static final ReplaceTransformerRule randUUID = new ReplaceTransformerRule("%rand%", () -> UUID.randomUUID().toString());
	
	public static final IPathTransformer nowDate = DateTransformerRule.now("%date{", "}%");
	public static final IPathTransformer startDate = DateTransformerRule.of(new Date(), "%date{", "}%");
	
}
