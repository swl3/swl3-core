package ru.swayfarer.swl3.io.path.rules;

import ru.swayfarer.swl3.io.path.PathTransforms.IPathPacker;

public interface StandartPathPackers {

	public static final IPathPacker temp = StandartPathTransformers.temp.asPacker();
	
	public static final IPathPacker user = StandartPathTransformers.user.asPacker();
	
	public static final IPathPacker java = StandartPathTransformers.java.asPacker();
	public static final IPathPacker javaHome = StandartPathTransformers.javaHome.asPacker();
	
	public static final IPathPacker appDir = StandartPathTransformers.appDir.asPacker();
	
}
