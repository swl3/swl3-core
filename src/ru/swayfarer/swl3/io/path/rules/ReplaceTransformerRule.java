package ru.swayfarer.swl3.io.path.rules;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.io.path.PathTransforms.IPathPacker;
import ru.swayfarer.swl3.io.path.PathTransforms.IPathTransformer;

@Data @AllArgsConstructor
public class ReplaceTransformerRule implements IPathTransformer{

	@NonNull
	public String target;
	
	@NonNull
	public IFunction0<String> replacementFun;
	
	@Override
	public String applyUnsafe(String str) {
		return str.replace(target, replacementFun.apply());
	}
	
	public IPathPacker asPacker()
	{
		return (str) -> str.replace(replacementFun.apply(), target);
	}
	
	public static ReplaceTransformerRule of(@NonNull String target, @NonNull String replacement)
	{
		return new ReplaceTransformerRule(target, () -> replacement);
	}

}
