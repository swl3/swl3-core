package ru.swayfarer.swl3.io.file;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.exception.LibTags;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayDeque;

@Getter
@Setter
@Accessors(chain = true)
public class FileTreeWalker implements LibTags.Exceptions
{
    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
    public ArrayDeque<Path> thisLayer = new ArrayDeque<>();

    public FileTreeWalker(@NonNull Path file)
    {
        readFileOrDir(file, true);
    }

    public Path next()
    {
        var hasNextElement = hasNext();

        if (!hasNextElement)
            return null;

        var file = thisLayer.poll();

        if (file != null)
            readFileOrDir(file, false);

        return file;
    }

    public boolean hasNext()
    {
        return !thisLayer.isEmpty();
    }

    public void readFileOrDir(Path file, boolean selfAdd)
    {
        if (Files.isDirectory(file))
        {
            exceptionsHandler.handle()
                .tag(tagFilesWalking)
                .message("Error while walking to file", file.getFileName())
                .start(
                    () -> Files.newDirectoryStream(file)
                        .forEach((path) -> thisLayer.push(path))
                )
            ;
        }

        else if (selfAdd)
        {
            thisLayer.push(file);
        }
    }
}
