package ru.swayfarer.swl3.io.file;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.io.entry.IIOEntry;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class FileIOEntry implements IIOEntry{

    @NonNull
    public FileSWL rootDir;
    
    @NonNull
    public FileSWL file;
    
    @Override
    public String getName()
    {
        return file.getLocalPath(rootDir);
    }
}
