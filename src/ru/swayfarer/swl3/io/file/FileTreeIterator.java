package ru.swayfarer.swl3.io.file;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.markers.Internal;

import java.nio.file.Path;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.StreamSupport;

@Getter
@Setter
@Accessors(chain = true)
public class FileTreeIterator implements Iterator<Path>
{
    @Internal
    public FileTreeWalker fileTreeWalker;

    public FileTreeIterator(@NonNull FileTreeWalker fileTreeWalker)
    {
        this.fileTreeWalker = fileTreeWalker;
    }

    @Override
    public boolean hasNext()
    {
        return fileTreeWalker.hasNext();
    }

    @Override
    public Path next()
    {
        return fileTreeWalker.next();
    }

    public static ExtendedStream<Path> walk(FileSWL file)
    {
        return walk(file, ExceptionsHandler.getContextHandler());
    }

    public static ExtendedStream<Path> walk(FileSWL file, ExceptionsHandler exceptionsHandler)
    {
        return new ExtendedStream<>(() -> {
            var treeWalker = new FileTreeWalker(file.toPath());
            treeWalker.setExceptionsHandler(exceptionsHandler);

            Spliterator<Path> spliterator =
                    Spliterators.spliteratorUnknownSize(new FileTreeIterator(treeWalker), Spliterator.DISTINCT);

            return StreamSupport
                    .stream(spliterator, false)
            ;
        });
    }
}
