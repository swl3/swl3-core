package ru.swayfarer.swl3.io.file;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.exception.LibTags.Exceptions;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.io.path.PathHelper;
import ru.swayfarer.swl3.io.path.PathTransforms;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.markers.ConcattedString;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.string.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.URI;
import java.net.URL;
import java.nio.file.Path;
import java.util.Iterator;

@Getter @Setter @Accessors(chain = true)
@SuppressWarnings("serial")
public class FileSWL extends File implements Iterable<FileSWL> {

	@NonNull
	@Internal
	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler()
	    .configure()
	        .rule()
	            .type(FileNotFoundException.class)
	            .fromMethods("in", "out", "append")
	            .fromClasses(FileSWL.class)
	            .thanSkip()
	            .throwEx()
        .handler()
    ;
	
	@NonNull
	@Internal
	public PathTransforms pathTransforms = PathTransforms.defaultTransforms();
	
	protected FileSWL() 
	{
		super(System.getProperty("user.dir"));
		pathTransforms = PathTransforms.defaultTransforms();
	}
	
	protected FileSWL(@NonNull File parent, @NonNull String child) 
	{
		super(parent, child);
	}
	
	/** Получить подфайл */
    public FileSWL subFile(@ConcattedString Object... filepath)
    {
        return new FileSWL(this, StringUtils.concat(filepath));
    }
	
	public FileSWL getParentFile()
	{
		var parent = super.getParent();
        return parent == null ? null : FileSWL.of(parent);
	}

	protected FileSWL(@NonNull String parent, @NonNull String child) 
	{
		super(parent, child);
	}

	protected FileSWL(@NonNull String pathname) 
	{
		super(pathname);
	}

	protected FileSWL(@NonNull URI uri) 
	{
		super(uri);
	}
    
    public int getSubfilesCount()
    {
        String[] subs = list();
        
        return subs == null ? 0 : subs.length;
    }
	
	/** Удалить (если папка, то включая подпапки и подфайлы) */
	public boolean remove()
	{
		findAllSubfiles().each((file) -> {
			if (!this.equals(file))
				file.remove();
		});
		
		return delete();
	}
	
	/** Скопировать в (Если копируется директория, то будут скопированы все ее файлы) */
	public FileSWL copyToIfExists(FileSWL file)
	{
		if (exists())
		{
			copyTo(file);
			return file;
		}
		
		return this;
	}
	
	/** Скопировать в (Если копируется директория, то будут скопированы все ее файлы) */
	public FileSWL copyTo(FileSWL file)
	{
		if (isDirectory())
		{
			file.createIfNotFoundDir();
			findSubfiles().parallelStream().forEach((subFile) -> subFile.copyTo(new FileSWL(file, subFile.getName())));
		}
		else
		{
			file.createIfNotFound();
			file.out().copy(in()).close();
		}
		
		return file;
	}
	
	public FileConversions as()
	{
		return new FileConversions(this);
	}
	
	public String getExtension()
	{
		return PathHelper.getExtension(getName());
	}
	
	public String getNameWithoutExtension()
	{
		return PathHelper.getSimpleNameWithoutExtension(getName());
	}

	/** Лежит ли файл в указанной директории? */
	public boolean isIn(FileSWL file)
	{
		return exceptionsHandler.handle()
			.message("Error while checking file", this, "location in", file)
			.tag(Exceptions.tagIsInChecking)
			.andReturn(() -> {
				if (!file.isDirectory())
					return false;

				String targetDir = file.getCanonicalPath();
				String fileDir = getCanonicalPath();

				targetDir = PathHelper.fixSlashes(targetDir);
				fileDir = PathHelper.fixSlashes(fileDir);

				if (targetDir.endsWith("./"))
					targetDir = targetDir.substring(0, targetDir.length() - 2);

				if (fileDir.endsWith("./"))
					fileDir = targetDir.substring(0, fileDir.length() - 2);

				return fileDir.startsWith(targetDir);
			})
		;
	}
	
	@SneakyThrows
	public URL asURL()
	{
	    return toURI().toURL();
	}

	/** Поток чтения */
	public DataInStream in()
	{
		return exceptionsHandler.safeReturn(() -> DataInStream.of(new FileInputStream(this)), null, "Error while creating input stream for file", this);
	}

	public ExtendedOptional<DataInStream> optionalIn()
	{
		return ExtendedOptional.of(in());
	}

	public ExtendedOptional<DataOutStream> optionalOut()
	{
		return ExtendedOptional.of(out());
	}
	
	/** Поток добавления (записи в конец) */
	public DataOutStream append()
	{
		createIfNotFound();
		return exceptionsHandler.safeReturn(() -> DataOutStream.of(new FileOutputStream(this, true)), null, "Error while creating append stream for file", this);
	}
	
	/** Поток записи */
	public DataOutStream out()
	{
		createIfNotFound();
		return exceptionsHandler.safeReturn(() -> DataOutStream.of(new FileOutputStream(this)), null, "Error while creating out stream for file", this);
	}
	
	/** Получить путь до файла, по возможности относительно рабочей директории */
	public @NonNull String getLocalPath()
	{
		return getLocalPath(new FileSWL());
	}
	
	/** Получить путь до файла, по возможности относительно указанной директории */
	public @NonNull String getLocalPath(@NonNull FileSWL root)
	{
		String workDir = PathHelper.fixSlashes(root.getAbsolutePath());
		String currentDir = PathHelper.fixSlashes(getAbsolutePath());
		
		if (workDir.endsWith("./"))
			workDir = workDir.substring(0, workDir.length() - 2);
		
		var ret = currentDir.replace(workDir, "");
		
		while (ret.startsWith("/"))
		    ret = ret.substring(1);
		
        return ret;
	}
	
	/** Создать, если не сущесвует, эту директорию */
	public @NonNull FileSWL createIfNotFoundDir()
	{
		mkdirs();
		return this;
	}
	
	/** Создать, если не сущесвует, этот файл */
	public @NonNull FileSWL createIfNotFound()
	{
		exceptionsHandler.safe(() -> {
			if (!this.exists())
			{
				File parent = getParentFile();
				
				if (parent != null)
					parent.mkdirs();
				
				if (this.isDirectory())
				{
					this.mkdirs();
				}
				else
				{
					this.createNewFile();
				}
			}
		}, "Error while creating file", this);
		
		return this;
	}
	
	public @NonNull ExtendedList<FileSWL> findSubfiles()
	{
		var ret = new ExtendedList<FileSWL>();
		
		var files = list();
		
		if (files != null)
		{
			for (var fileName : files)
			{
				ret.add(new FileSWL(this, fileName)
						.setPathTransforms(getPathTransforms())
				);
			}
		}
		
		return ret;
	}

	public ExtendedStream<FileSWL> findAllSubfiles()
	{
//		return new ExtendedStream<>(
//				() -> FileTreeIterator.walk(this, exceptionsHandler)
//						.map((path) -> FileSWL.of(path.toFile()).setPathTransforms(getPathTransforms()))
//						.filter(Objects::nonNull)
//						.createStream()
//		);

		return CollectionsSWL.exStream(this).nonNull();
	}

	public static FileSWL current()
	{
		return new FileSWL();
	}
	
	@SneakyThrows
	public static FileSWL of(URL url)
	{
		return new FileSWL(url.getPath());
	}

	public static FileSWL of(Path path)
	{
		return of(path.toFile());
	}
	
	public static FileSWL of(File parent, String path)
	{
		return of(parent, path, PathTransforms.defaultTransforms());
	}
	
	public static FileSWL of(File parent, String path, PathTransforms pathTransforms)
	{
		return new FileSWL(parent, pathTransforms.transform(path));
	}
	
	public static FileSWL of(String path)
	{
		return of(path, PathTransforms.defaultTransforms());
	}
	
	public static FileSWL of(String path, PathTransforms pathTransforms)
	{
		return new FileSWL(pathTransforms.transform(path)).setPathTransforms(pathTransforms);
	}

	public static FileSWL of(File file)
	{
		if (file == null)
			return null;

		return of(file.getAbsolutePath());
	}

	@Override
	public Iterator<FileSWL> iterator()
	{
		return CollectionsSWL.mapIterator(
				new FileTreeIterator(new FileTreeWalker(toPath())),
				(path) -> FileSWL.of(path).setPathTransforms(getPathTransforms())
		);
	}
}
