package ru.swayfarer.swl3.io.file;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.zip.ZipFile;

import lombok.Data;
import lombok.NonNull;
import lombok.SneakyThrows;

@Data
public class FileConversions {

	@NonNull
	public FileSWL targerFile;
	
	@SneakyThrows
	public ZipFile zip()
	{
		return new ZipFile(targerFile);
	}
	
	@SneakyThrows
	public Scanner scanner()
	{
	    return new Scanner(targerFile);
	}

	@SneakyThrows
	public BufferedReader reader()
	{
		return new BufferedReader(new InputStreamReader(targerFile.in()));
	}
}
