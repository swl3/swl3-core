package ru.swayfarer.swl3.io.file;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.io.entry.IIOEntry;
import ru.swayfarer.swl3.io.entry.IIOEntryProvider;
import ru.swayfarer.swl3.io.streams.DataInStream;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class FileIOEntryProvider implements IIOEntryProvider{

    @NonNull
    public FileSWL file;
    
    @Override
    public DataInStream getInputStream(@NonNull IIOEntry entry)
    {
        return file.subFile(entry.getName()).in();
    }

    @Override
    public IIOEntry findEntry(@NonNull String entryPath)
    {
        var subfile = file.subFile(entryPath);
        return createEntry(subfile);
    }

    @Override
    public ExtendedList<IIOEntry> findAllEntries()
    {
        return file.findSubfiles()
                .exStream()
                .map(this::createEntry)
                .nonNull()
                .map((e) -> (IIOEntry) e)
                .toExList()
        ;
    }
    
    public FileIOEntry createEntry(@NonNull FileSWL file)
    {
        if (!file.isFile() || !file.exists())
        {
            return null;
        }
        
        var ret = FileIOEntry.builder()
                .file(file)
                .rootDir(this.file)
                .build()
        ;
        
        return ret;
    }
}
