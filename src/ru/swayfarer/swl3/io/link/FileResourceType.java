package ru.swayfarer.swl3.io.link;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;

import java.net.URL;

public class FileResourceType implements IResourceType {

	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	
	@Override
	public boolean isAcceptsPrefix(@NonNull String prefix)
	{
		return "file".equals(prefix) || "f".equals(prefix);
	}

	@Override
	public DataInStream getInputStream(@NonNull ResourceLink rlink)
	{
		var file = createFile(rlink);
		return file.in();
	}

	@Override
	public DataOutStream getOutputStream(@NonNull ResourceLink rlink)
	{
		var file = createFile(rlink);
		return file.out();
	}

	@Override
	public DataOutStream getAppendStream(@NonNull ResourceLink rlink)
	{
		var file = createFile(rlink);
		return file.append();
	}

	@Override
	public boolean isExists(@NonNull ResourceLink rlink)
	{
		var file = createFile(rlink);
		return file.exists();
	}

	@Override
	public URL getURL(@NonNull ResourceLink rlink)
	{
		var file = createFile(rlink);
		return exceptionsHandler.safeReturn(
				() -> file.toURI().toURL(), 
				null, 
				"Error while getting URL from file", file
		);
	}

	@Override
	public boolean isUrlAvailable(@NonNull ResourceLink rlink)
	{
		return true;
	}

	@Override
	public boolean isWritable(@NonNull ResourceLink rlink)
	{
		return true;
	}
	
	public FileSWL createFile(@NonNull ResourceLink rlink)
	{
		return FileSWL.of(rlink.getPath());
	}

	@Override
	public String getPrefix() 
	{
		return "file";
	}

}
