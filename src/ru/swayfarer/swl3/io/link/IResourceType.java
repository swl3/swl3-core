package ru.swayfarer.swl3.io.link;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import lombok.NonNull;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;

/**
 * Тип ресурсов. <br>
 * Описывает взаимодействие с ресурсами через {@link ResourceLink}
 * @author swayfarer
 *
 */
public interface IResourceType {
	
	/**
	 * Принимает ли префикс?
	 * @param prefix Префикс
	 * @return True, если принимает
	 */
	public boolean isAcceptsPrefix(@NonNull String prefix);
	
	/**
	 * Получить {@link InputStream} из ресурса
	 * @param rlink Ссылка на ресурс
	 * @return Поток или null
	 */
	public DataInStream getInputStream(@NonNull ResourceLink rlink);
	
	/**
	 * Получить {@link OutputStream} из ресурса
	 * @param rlink Ссылка на ресурс
	 * @return Поток или null
	 */
	public DataOutStream getOutputStream(@NonNull ResourceLink rlink);
	
	/**
	 * Получить {@link OutputStream} из ресурса для добавления информации (append)
	 * @param rlink Ссылка на ресурс
	 * @return Поток или null
	 */
	public DataOutStream getAppendStream(@NonNull ResourceLink rlink);
	
	/**
	 * Существует ли ресурс по ссылке? 
	 * @param rlink Ссылка на ресурс
	 * @return True, если существует.
	 */
	public boolean isExists(@NonNull ResourceLink rlink);
	
	/**
	 * Получить {@link URL} ресурса
	 * @param rlink Ссылка на ресурс
	 * @return {@link URL} или null
	 */
	public URL getURL(@NonNull ResourceLink rlink);
	
	/**
	 * Возможно ли получить {@link URL} из ресурса?
	 * @param rlink Ссылка на ресурс
	 * @return True, если возможно
	 */
	public boolean isUrlAvailable(@NonNull ResourceLink rlink);
	
	/**
	 * Возможна ли запись в ресурс?
	 * @param rlink Ссылка на ресурс
	 * @return True, если запить возможна
	 */
	public boolean isWritable(@NonNull ResourceLink rlink);
	
	public String getPrefix();
}
