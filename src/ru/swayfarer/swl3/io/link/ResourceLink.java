package ru.swayfarer.swl3.io.link;

import java.net.URL;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.exception.LibTags;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.io.path.PathHelper;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.io.streams.StreamsUtils;
import ru.swayfarer.swl3.markers.Internal;

/**
 * Ссылка на ресурс
 * @author swayfarer
 *
 */
@Getter
@Setter
@Accessors(chain = true)
public class ResourceLink {

	/** Тип ресурса */
	@Internal
	@NonNull
	public IResourceType resourceType;
	
	/** Обработчик ошибок, возникающих при работе с ресурсом */
	@Internal
	@NonNull
	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	
	/** Путь до ресурса */
	@Internal
	@NonNull
	public String path;
	
	/**
	 * Конструктор
	 * @param resourceType {@link #resourceType}
	 * @param path {@link #path}
	 */
	public ResourceLink(@NonNull IResourceType resourceType, @NonNull String path) 
	{
		this.resourceType = resourceType;
		this.path = path;
	}

	public ExtendedOptional<DataInStream> optionalIn()
	{
		return ExtendedOptional.of(in());
	}
	
	/** 
	 * Получить поток чтения данных
	 * @return поток или null, если поток получить не удастся
	 */
	public DataInStream in()
	{
		return exceptionsHandler.handle()
			.tag(LibTags.Exceptions.tagRlinkIn)
			.message("Error while getting input stream from", toRlinkString())
			.andReturn(() -> resourceType.getInputStream(this))
		;
	}
	
	public String getSimpleNameWithoutExtension()
	{
		return PathHelper.getSimpleNameWithoutExtension(PathHelper.fixSlashes(getPath()));
	}
	
	public String getExtension()
	{
		return PathHelper.getExtension(PathHelper.fixSlashes(getPath()));
	}
	
	public String getSimpleName()
	{
		return PathHelper.getSimpleName(PathHelper.fixSlashes(getPath()));
	}
	
	/**
	 * Получить {@link URL} из ссылки
	 * @return Полученный {@link URL}
	 */
	public URL asUrl()
	{
		return resourceType.getURL(this);
	}
	
	/** 
	 * Получить поток записи данных
	 * @return поток или null, если поток получить не удастся
	 */
	public DataOutStream out()
	{
		return exceptionsHandler.handle()
			.tag(LibTags.Exceptions.tagRlinkIn)
			.message("Error while getting output stream from", toRlinkString())
			.andReturn(() -> resourceType.getOutputStream(this))
		;
	}
	
	/** 
	 * Получить поток добавления данных (append)
	 * @return поток или null, если поток получить не удастся
	 */
	public DataOutStream append()
	{
		return resourceType.getAppendStream(this);
	}
	
	/**
	 * Прочитать ресурс как массив байт
	 * @return массив байт или null, если не удастся прочитать
	 */
	public byte[] asBytes()
	{
		var inStream = in();
		if (inStream == null)
			return null;
		
		inStream.setExceptionsHandler(exceptionsHandler);
		
		return inStream.readAll();
	}
	
	/**
	 * Прочитать ресурс как строку
	 * @param encoding Кодировка строки
	 * @return Строка или null, если не удастся прочитать
	 */
	public String asSingleString(@NonNull String encoding)
	{
		var outStream = in();
		if (outStream == null)
			return null;
		
		return exceptionsHandler.safeReturn(() -> StreamsUtils.readAll(encoding, outStream), null, "Error while reading resource", this);
	}

	public ExtendedOptional<String> asText(@NonNull String encoding)
	{
		var str = asSingleString(encoding);
		return ExtendedOptional.of(str);
	}
	
	/**
	 * Существует ли ресурс?
	 * @return True, если существует
	 */
	public boolean isExists()
	{
		return resourceType.isExists(this);
	}
	
	/**
	 * Возможна ли запить в ресурс?
	 * @return True, если возможна
	 */
	public boolean isWritable()
	{
		return resourceType.isWritable(this);
	}
	
	/**
	 * Возможна ли конвертация ссылки в {@link URL}?
	 * @return True, если возможна
	 */
	public boolean isUrlAvailable()
	{
		return resourceType.isUrlAvailable(this);
	}
	
	public String toRlinkString()
	{
		return resourceType.getPrefix() + ":" + getPath();
	}
}
