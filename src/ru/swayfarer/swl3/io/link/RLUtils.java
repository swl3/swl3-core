package ru.swayfarer.swl3.io.link;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.markers.ConcattedString;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;
import ru.swayfarer.swl3.thread.group.ThreadGroupLocal;

/**
 * Утилиты для работы с ресурсами
 * @author swayfarer
 *
 */
@Getter
@Setter
@Accessors(chain = true)
public class RLUtils {

	public static RLUtils INSTANCE = RLUtils.defaultUtils();
	
    /** Хранилка для контекстного обработчика ошибок */
    @Internal
    public static ThreadGroupLocal<RLUtils> contextRLUtils = new ThreadGroupLocal<>();
    
	/** Обработчик ошибок */
	@Internal
	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	
	/** 
	 * Зарегистрированные ресурсы <br> 
	 * Используются для работы с ресурсами по путям, передаваемым в утилиты. <br>
	 * См. {@link IResourceType}, {@link ResourceLink}
	 */
	@Internal
	public List<IResourceType> registeredResourceTypes = new ArrayList<>();

	/**
	 * Тип ресурса, который будет использован, если не подберется ни одного из {@link #registeredResourceTypes} 
	 */
	@Internal
	public IResourceType defaultResourceType;
	
    /**
     * Задать контекстный обработчик ошибок
     * @param exceptionsHandler
     */
    public static void setContextUtils(@NonNull RLUtils exceptionsHandler)
    {
        contextRLUtils.value(exceptionsHandler);
    }
    
    /**
     * Получить контекстный обработчик событий
     * @return Обработчик или <code>null</code>
     */
    public static RLUtils getContextUtils()
    {
        return contextRLUtils.getValue();
    }
    
    /**
     * Получить контекстный обработчик событий или создать его
     * @return Обработчик
     */
    public static RLUtils contextUtils()
    {
        var ret = getContextUtils();
        
        if (ret == null)
        {
            synchronized (contextRLUtils)
            {
                ret = getContextUtils(); 
                
                if (ret == null)
                {
                    ret = INSTANCE;
                    setContextUtils(ret);
                }
            }
        }
        
        return ret;
    }
	
	/**
	 * Зарегистрировать тип ресурсов
	 * @param type Регистрируемый тип ресурсов
	 * @return this
	 */
	public RLUtils type(@NonNull IResourceType type) 
	{
		this.registeredResourceTypes.add(type);
		return this;
	}
	
	public static RLUtils defaultUtils()
	{
	    return RLUtils.create().defaultTypes(ExceptionsHandler.getOrCreateContextHandler()).build();
	}
	
	/**
	 * Использовать класспас как тип ресурса по-умолчанию
	 * @return this
	 */
	public RLUtils useClasspathByDefault()
	{
		this.defaultResourceType = URLResourceType.ofClassloader(
				getExceptionsHandler(),
				getClass()
		).setPrefixes(CollectionsSWL.list("cp", "classpath", ""));

		return this;
	}
	
	/**
	 * Подобрать тип под путь до ресурса
	 * @param path Путь до ресурса
	 * @return Подобранный тип или null
	 */
	@Internal
	public IResourceType findType(@NonNull String path)
	{
		var prefix = getResourcePrefix(path);
		
		for (var type : registeredResourceTypes)
		{
			if (type.isAcceptsPrefix(prefix))
				return type;
		}
		
		return defaultResourceType;
	}
	
	/**
	 * Получить префикс ресурса
	 * @param path Путь до ресурса
	 * @return Префикс без двоеточия
	 */
	public String getResourcePrefix(@NonNull String path)
	{
		var indexOfSplitter = path.indexOf(":");
		return indexOfSplitter >= 0 && indexOfSplitter < path.length() ? path.substring(0, indexOfSplitter) : "";
	}
	
	/**
	 * Получить {@link InputStream} из пути
	 * @param path Путь до ресурса
	 * @return Поток или null, если не найдется
	 */
	public DataInStream in(String path)
	{
		var rlink = createLink(path);
		
		if (rlink == null)
			return null;
		
		return rlink.in();
	}
	
	/**
	 * Получить {@link OutputStream} из пути
	 * @param path Путь до ресурса
	 * @return Поток или null, если не найдется
	 */
	public DataOutStream out(String path)
	{
		var rlink = createLink(path);
		
		if (rlink == null)
			return null;
		
		return rlink.out();
	}
	
	/**
	 * Получить {@link OutputStream} из пути, настроенный на добавление данных (append)
	 * @param path Путь до ресурса
	 * @return Поток или null, если не найдется
	 */
	public DataOutStream append(String path)
	{
		var rlink = createLink(path);
		
		if (rlink == null)
			return null;
		
		return rlink.append();
	}
	
	/**
	 * Прочитать ресурс как UTF-8 строку
	 * @param path Путь до ресурса
	 * @return Строка или null, если не удастся прочитать
	 */
	public String asUTF8String(@NonNull String path)
	{
		return asString(path, "UTF-8");
	}
	
	/**
	 * Прочитать ресурс как строку
	 * @param path Путь до ресурса
	 * @param encoding Кодировка строки
	 * @return Строка или null, если не удастся прочитать
	 */
	public String asString(@NonNull String path, @NonNull String encoding)
	{
		var rlink = createLink(path);
		
		if (rlink == null)
			return null;
		
		return rlink.asSingleString(encoding);
	}
	
	/**
	 * Прочитать ресурс как массив байт
	 * @param path Путь до ресурса
	 * @return Байты или null, если не удастся прочитать
	 */
	public byte[] asBytes(@NonNull String path)
	{
		var rlink = createLink(path);
		
		if (rlink == null)
			return null;
		
		return rlink.asBytes();
	}
	
	/**
	 * Создать ссылку на ресурс
	 * @param path Путь до ресурса
	 * @return Созданная ссылка
	 */
	public ResourceLink createLink(@NonNull String path)
	{
		var resourceType = findType(path);
		
		if (resourceType == null)
			ExceptionsUtils.ThrowToCaller(IllegalArgumentException.class, "Path '" + path + "' must starts with registered prefix! Please, register it or use another prefix!");
	
		var pathWithoutPrefix = getPathWithoutPrefix(path);
		
		var resourceLink = new ResourceLink(resourceType, pathWithoutPrefix);
		
		resourceLink.exceptionsHandler = exceptionsHandler;
		
		return resourceLink;
	}
	
	/**
	 * Получить путь, убрав из него пефикс с типом ресурса
	 * @param path Путь
	 * @return Искомый путь
	 */
	public String getPathWithoutPrefix(@NonNull String path)
	{
		int splitterIndex = path.indexOf(":");
		
		if (splitterIndex >= 0 && splitterIndex - 1 < path.length())
			return path.substring(splitterIndex + 1);
		
		return path;
	}
	
	/**
	 * Создать ссылку на ресурс
	 * @param rlinkPath Путь до ресурса
	 * @return Созданная ссылка
	 */
	public ResourceLink createLink(@NonNull @ConcattedString Object... rlinkPath)
	{
		return createLink(StringUtils.concat(rlinkPath));
	}
	
	/**
	 * Создать новые {@link RLUtils}
	 * @return Экземпляр конфигуратора
	 */
	public static Configurator create()
	{
		return new Configurator(new RLUtils());
	}
	
	public static String fromThisClass(@NonNull String pathFromCallerClass)
	{
	    var callerClassName = ExceptionsUtils.caller().getClassName();
	    var str = new DynamicString();
	    
	    str.append(ExceptionsUtils.getClassPackage(callerClassName).replace(".", "/"));
	    
	    if (!pathFromCallerClass.startsWith("/"))
	        str.append("/");
	    
	    str.append(pathFromCallerClass);
	    
	    return str.toString();
	}
	
	/**
	 * Конфигуратор {@link RLUtils} <br>
	 * Позволяет настраивать утилиты для работы с {@link ResourceLink}'ами
	 * @author swayfarer
	 *
	 */
	@Internal
	public static class Configurator {
		
		/** Настраиваемые утилиты */
		@Internal
		public RLUtils result;
		
		/**
		 * Конструктор
		 * @param result Настраиваемые утилиты 
		 */
		public Configurator(RLUtils result)
		{
			this.result = result;
		}
		
		/**
		 * Установить тип ресурсов по-умолчанию
		 * @param resourceType Тип
		 * @return this
		 */
		public Configurator defaultType(@NonNull IResourceType resourceType)
		{
			result.defaultResourceType = resourceType;
			return this;
		}
		
		/**
		 * Зарегистрировать тип ресурсов
		 * @param resourceType Тип
		 * @return this
		 */
		public Configurator type(@NonNull IResourceType resourceType)
		{
			result.type(resourceType);
			return this;
		}
		
		/**
		 * Установить обработчик ошибок
		 * @param exceptionsHandler Обработчик
		 * @return this
		 */
		public Configurator exceptionsHandler(@NonNull ExceptionsHandler exceptionsHandler)
		{
			result.setExceptionsHandler(exceptionsHandler);
			return this;
		}
		
		/**
		 * Зарегистрировать стандартные типы ресурсов
		 * @param exceptionsHandler Обработчик ошибок, который будет использоваться по-умолчанию 
		 * @return this
		 */
		public Configurator defaultTypes(ExceptionsHandler exceptionsHandler)
		{
			// Тип, работающий через File
			type(new FileResourceType());
			
			// Тип, работающий через URL
			var urlResourceType = URLResourceType.of(exceptionsHandler, (rlink) -> exceptionsHandler.safeReturn(
					() -> new URL(rlink), 
					null, 
					"Error while creating url from", rlink
			));
			
			var prefixes = urlResourceType.getPrefixes();
			prefixes.add("u");
			prefixes.add("url");
			
			type(urlResourceType);
			
			// Тип, работающий через classpath
			var classPathResourceType = URLResourceType.ofClassloader(exceptionsHandler, RLUtils.class); 
			
			prefixes = classPathResourceType.getPrefixes();
			prefixes.add("classpath");
			prefixes.add("cp");
			prefixes.add("");
			
			type(classPathResourceType);
			
			defaultType(classPathResourceType);
			
			return this;
		}
		
		/**
		 * Сконфигурировать утилиты
		 * @return Настроенные утилиты
		 */
		public RLUtils build()
		{
			return result;
		}
	}
	
	public static RLUtils getInstance()
	{
		return INSTANCE;
	}
}
