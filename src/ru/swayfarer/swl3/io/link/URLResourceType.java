package ru.swayfarer.swl3.io.link;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.io.streams.DataInStream;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.string.StringUtils;

/**
 * Тип ресурса, доступ к которому осуществляется по {@link URL} <br>
 * Только для чтения 
 * <p> 
 * Для изменения логики создания {@link URL} необходимо заменить {@link #linkToUrlFun}
 * 
 * @author swayfarer
 *
 */
@SuppressWarnings("unchecked")
@Getter
@Setter
@Accessors(chain = true)
public class URLResourceType implements IResourceType {

	/** Функция, создающие {@link URL}, используя путь до ресурса */
	@Internal
	public IFunction1<ResourceLink, URL> linkToUrlFun;
	
	@Internal
	public ExtendedList<String> prefixes = new ExtendedList<>();
	
	/** Обработчик ошибок */
	@Internal
	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	
	public <T extends URLResourceType> T prefix(@NonNull String prefix)
	{
		this.prefixes.add(prefix);
		return (T) this;
	}
	
	@Override
	public boolean isAcceptsPrefix(@NonNull String prefix)
	{
		for (var s : prefixes)
			if (prefix.equals(s))
				return true;
		
		return false;
	}

	@Override
	public DataInStream getInputStream(@NonNull ResourceLink rlink)
	{
		var url = getURL(rlink);
		return url == null ? null : exceptionsHandler.safeReturn(() -> DataInStream.of(url.openStream()), null, "Error while creating stream from", rlink);
	}

	@Override
	public DataOutStream getOutputStream(@NonNull ResourceLink rlink)
	{
		return null;
	}

	@Override
	public DataOutStream getAppendStream(@NonNull ResourceLink rlink)
	{
		return null;
	}

	@Override
	public boolean isExists(@NonNull ResourceLink rlink)
	{
		var inputStream = getInputStream(rlink);
		boolean ret = inputStream != null;
		
		if (ret)
		    exceptionsHandler.safe(() -> inputStream.close());
		
		return ret;
	}

	@Override
	public URL getURL(@NonNull ResourceLink rlink)
	{
		if (linkToUrlFun != null)
			return linkToUrlFun.apply(rlink);
		return null;
	}

	@Override
	public boolean isUrlAvailable(@NonNull ResourceLink rlink)
	{
		return true;
	}

	@Override
	public boolean isWritable(@NonNull ResourceLink rlink)
	{
		return false;
	}
	
	/**
	 * Создать источник ресурсов, обращаюшийся к класслоадеру переданного класса
	 * @param exceptionsHandler Обработчик ошибок, возникающих во время работы с ресурсами
	 * @param classFromClasspath Класс, из которого будет браться класслоадер
	 * @return Новый тип ресурсов
	 */
	public static URLResourceType ofClassloader(@NonNull ExceptionsHandler exceptionsHandler, @NonNull Class<?> classFromClasspath)
	{
		var classLoader = classFromClasspath.getClassLoader();
		
		if (classLoader == null)
			ExceptionsUtils.ThrowToCaller(IllegalArgumentException.class, "Class from classpath must have non-null classloader! Plase, change class", classFromClasspath.getName(), "to another and try again!");
		
		return of(exceptionsHandler, (path) -> {
			StringUtils.strip(path);
			
			while (path.startsWith("/"))
			{
				path = path.substring(1);
			}
			
			return classLoader.getResource(path);
		});
	}
	
	/**
	 * Создать источник ресурсов {@link URLResourceType}, создающий {@link URL} при помощи переданной функции
	 * @param exceptionsHandler Обработчик ошибок, возникающих во время работы с ресурсами
	 * @param urlCreationFun Функция, создающая {@link URL} из передаваемых строк
	 * @return Новый тип ресурсов
	 */
	public static URLResourceType of(@NonNull ExceptionsHandler exceptionsHandler, @NonNull IFunction1<String, URL> urlCreationFun)
	{
		return new URLResourceType()
			.setExceptionsHandler(exceptionsHandler)
			.setLinkToUrlFun((link) -> urlCreationFun.apply(link.getPath()))
		;
	}

	@Override
	public String getPrefix() 
	{
		return prefixes.first();
	}
}
