package ru.swayfarer.swl3.exception;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.thread.ThreadsUtils;

@Getter
@Setter
@Accessors(chain = true)
public class LazyThrowable<Throwable_Type extends Throwable> {
    
    @NonNull
    public Throwable_Type throwable;

    public LazyThrowable(@NonNull Throwable_Type throwable)
    {
        this.throwable = throwable;
    }

    public IFunction0<ExtendedList<StackTraceElement>> stacktraceFun;
    
    {
        stacktraceFun = ThreadsUtils.singleton(() -> CollectionsSWL.list(throwable.getStackTrace()));
    }
    
    public ExtendedList<StackTraceElement> getStacktrace()
    {
        return stacktraceFun.apply();
    }
}
