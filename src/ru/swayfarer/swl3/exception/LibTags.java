package ru.swayfarer.swl3.exception;

public interface LibTags {
	public String prefix = "swl3-tags|";
	
	public static interface Exceptions {
		public String tagMassOpenAccess = prefix + "reflectionMassAccess";
		public String tagShortPrint = prefix + "shortPrint";
		public String tagTrace = prefix + "trace";

		public String tagLibPrimaryFunctional = prefix + "lib/primary";
		public String tagLoggerRefactor = prefix + "loggerRefactor";
		public String tagRlinkIn = prefix + "rlinkIn";
		public String tagRlinkOut = prefix + "rlinkOut";
		public String tagSerialize = prefix + "serialize";
		public String tagAppStartup = prefix + "appStartup";
		public String tagEventBus = prefix + "eventBus";
		public String tagFilesWalking = prefix + "io/filesWalking";
		public String tagIsInChecking = prefix + "io/fileIsInChecking";
		public String tagDynamicClassDefineAccess = prefix + "reflection/dynamicClassDefine";
		public String tagObservable = prefix + "observable";
		public String tagObservableSubscription = prefix + "observable/subscription";

		public String keyObservableSubscription = prefix + "observable/subscription-cause";

		public String tagTryEnableAnsiOutput = prefix + "console/tryEnableAnsi";
	}


	public static interface Events {
		public String tagEventBus = prefix + "events/busSubscriber";
	}
}
