package ru.swayfarer.swl3.exception;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.funs.IUnsafeRunnable;
import ru.swayfarer.swl3.funs.IUnsafeRunnableWithReturn;
import ru.swayfarer.swl3.markers.Alias;
import ru.swayfarer.swl3.markers.ConcattedString;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ClassUtil;
import ru.swayfarer.swl3.string.StringUtils;

public class ExceptionsUtils {

	/** Если метод вызван не из одного из указанных классов, то будет {@link Exception} */
	@SneakyThrows
	public static void IfCallerNot(Class<?>... callers)
	{
		String callerClass = getClassNameAt(2);
		
		if (!Stream.of(callers).anyMatch((caller) -> caller != null && caller.getName().equals(callerClass)))
		{
			throw createThrowable(IllegalAccessException.class, StringUtils.concatWithSpaces("Class", callerClass, "can't invoke this method! Permission denied!"), 2);
		}
	}

	/** Если метод вызван не из своего класса, то будет {@link Exception} */
	@SneakyThrows
	public static void IfCallerNotSelf()
	{
		String caller = getClassNameAt(2);
		
		if (!caller.equals(getClassNameAt(1)))
		{
			throw createThrowable(IllegalAccessException.class, StringUtils.concatWithSpaces("Class", caller, "can't invoke this method! Permission denied!"), 2);
		}
	}
	
	/**
	 * Получить класс, откуда вызван метод
	 * @return Искомый класс
	 */
	@SneakyThrows
	public static Class<?> callerClass()
	{
		return ClassUtil.forName(getClassNameAt(2));
	}
    
    /**
     * Получить класс, откуда вызван метод
     * @return Искомый класс
     */
    @SneakyThrows
    public static Class<?> classAt(int offset)
    {
        return ClassUtil.forName(getClassNameAt(offset));
    }

	/**
	 * Бросить исключение, если выполняется условие
	 * @param condition Условие
	 * @param classOfThrowable Класс создаваемого исключения
	 * @param message Сообщение исключения
	 */
	@SneakyThrows
	public static void If(boolean condition, Class<? extends Throwable> classOfThrowable, @ConcattedString Object... message)
	{
		if (condition)
			throw createThrowable(classOfThrowable, StringUtils.concatWithSpaces(message), 2);
	}

	/**
	 * Бросить исключение, если проверяемый объект null
	 * @param object Проверяемый объект
	 * @param classOfThrowable Класс создаваемого исключения
	 * @param message Сообщение исключения
	 */
	@SneakyThrows
	public static void IfNull(Object object, Class<? extends Throwable> classOfThrowable, @ConcattedString Object... message)
	{
		if (object == null)
			throw createThrowable(classOfThrowable, StringUtils.concatWithSpaces(message), 2);
	}

	/**
	 * Бросить исключение, если проверяемый объект null
	 * @param object Проверяемый объект
	 * @param message Сообщение исключения
	 */
	@SneakyThrows
	public static void IfNullArg(Object object, @ConcattedString Object... message)
	{
		if (object == null)
			throw createThrowable(IllegalArgumentException.class, StringUtils.concatWithSpaces(message), 2);
	}

	/**
	 * Бросить исключение, если проверяемая строка пуста
	 * @param str Проверяемая строка
	 * @param classOfThrowable Класс создаваемого исключения
	 * @param message Сообщение исключения
	 */
	@SneakyThrows
	public static void IfEmpty(@ConcattedString Object[] str, Class<? extends Throwable> classOfThrowable, @ConcattedString Object... message)
	{
		if (StringUtils.isEmpty(str))
			throw createThrowable(classOfThrowable, StringUtils.concatWithSpaces(message), 2);
	}

	@SneakyThrows
	public static void throwException(Throwable e)
	{
		StacktraceCleaner.clearStacktrace(e);
		throw e;
	}

	/**
	 * Бросить исключение, если проверяемая строка пуста
	 * @param str Проверяемая строка
	 * @param classOfThrowable Класс создаваемого исключения
	 * @param message Сообщение исключения
	 */
	@SneakyThrows
	public static void IfEmpty(String str, Class<? extends Throwable> classOfThrowable, @ConcattedString Object... message)
	{
		if (StringUtils.isEmpty(str))
			throw createThrowable(classOfThrowable, StringUtils.concatWithSpaces(message), 2);
	}

	/**
	 * Бросить исключение, если проверяемый объект не null
	 * @param object Проверяемый объект
	 * @param classOfThrowable Класс создаваемого исключения
	 * @param message Сообщение исключения
	 */
	@SneakyThrows
	public static void IfNotNull(Object object, Class<? extends Throwable> classOfThrowable, @ConcattedString Object... message)
	{
		if (object != null)
			throw createThrowable(classOfThrowable, StringUtils.concatWithSpaces(message), 2);
	}

	/**
	 * Бросить исключение, если не выполняется условие
	 * @param condition Условие
	 * @param classOfThrowable Класс создаваемого исключения
	 * @param message Сообщение исключения
	 */
	@SneakyThrows
	public static void IfNot(boolean condition, Class<? extends Throwable> classOfThrowable, @ConcattedString Object... message)
	{
		if (!condition)
			throw createThrowable(classOfThrowable, StringUtils.concatWithSpaces(message), 2);
	}
	
	/**
	 * Бросить исключение
	 * @param classOfThrowable Класс создаваемого исключения
	 * @param message Сообщение исключения
	 */
	@SneakyThrows
	public static void Throw(Class<? extends Throwable> classOfThrowable, @ConcattedString Object... message)
	{
		Throw(1, classOfThrowable, message);
	}
	
	/**
	 * Бросить исключение
	 * @param classOfThrowable Класс создаваемого исключения
	 * @param message Сообщение исключения
	 */
	@SneakyThrows
	public static void ThrowToCaller(Class<? extends Throwable> classOfThrowable, @ConcattedString Object... message)
	{
		Throw(2, classOfThrowable, message);
	}

	/**
	 * Бросить исключение
	 * @param classOfThrowable Класс создаваемого исключения
	 * @param message Сообщение исключения
	 */
	@SneakyThrows
	public static void illegalState(@ConcattedString Object... message)
	{
		Throw(1, IllegalStateException.class, message);
	}
	
	/**
	 * Бросить исключение
	 * @param classOfThrowable Класс создаваемого исключения
	 * @param message Сообщение исключения
	 */
	@SneakyThrows
	public static void illegalArg(@ConcattedString Object... message)
	{
		Throw(1, IllegalArgumentException.class, message);
	}
	
	/**
	 * Бросить исключение
	 * @param classOfThrowable Класс создаваемого исключения
	 * @param message Сообщение исключения
	 */
	@SneakyThrows
	public static void Throw(int offset, Class<? extends Throwable> classOfThrowable, @ConcattedString Object... message)
	{
		throw createThrowable(classOfThrowable, StringUtils.concatWithSpaces(message), 2 + offset);
	}
	
	@SneakyThrows
	public static void doOnExceptionAndThrow(@NonNull IFunction0NoR fun, @NonNull IFunction1NoR<Throwable> ifExFun)
	{
		try
		{
			fun.apply();
		}
		catch (Throwable e)
		{
			ifExFun.apply(e);
			throw e;
		}
	}

	/** Бросить исключение о неподдерживаемой операции */
	public static void unsupportedOperation(@ConcattedString Object... text)
	{
		throw new UnsupportedOperationException(StringUtils.concatWithSpaces(text));
	}
	
	/**
	 * Создать {@link Throwable} с указанным сообщением
	 * @param classOfThrowable Класс исключения, которое будет создано
	 * @param message Сообщение
	 * @param stacktraceOffset Смещение начала стактрейса
	 * @return Созданный {@link Throwable}, Null, если не выйдет создать
	 */
	@Internal
	public static <Throwable_Type extends Throwable> Throwable_Type createThrowable(Class<Throwable_Type> classOfThrowable, String message, int stacktraceOffset)
	{
		try
		{
			Constructor<Throwable_Type> constructor = classOfThrowable.getConstructor(String.class);
			Throwable_Type ret =  constructor.newInstance(message);
		
			// Удаляем лишние элементы стактрейса
			
			StackTraceElement[] stacktrace = ret.getStackTrace();
			
			int offset = 0;
			
			// Ищем в стактрейсе этот метод, игнорируя всю рефлексию
			for (StackTraceElement st : stacktrace)
			{
				if (st.getClassName().equals(ExceptionsUtils.class.getName()) && st.getMethodName().equals("createThrowable"))
				{
					break;
				}
				offset ++;
			}
			
			var shiftStacktrace = shiftStacktrace(stacktrace, offset + stacktraceOffset);
			ret.setStackTrace(shiftStacktrace.toArray(new StackTraceElement[shiftStacktrace.size()]));
			
			return ret;
		}
		catch (NoSuchMethodException e)
		{
			System.err.println("[ExceptionsUtils]: Can't create exception of type " + classOfThrowable.getCanonicalName() + ". Returning null...");
			e.printStackTrace();
		}
		catch (Throwable e)
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Безопасно выполнить действие
	 * @return Исключение, если появится 
	 */
	public static Throwable safe(IUnsafeRunnable r)
	{
		try
		{
			r.apply();
		}
		catch (Throwable e)
		{
			return e;
		}
		
		return null;
	}

	/**
	 * <h1> Внимание: </h1>
	 * Для безопасного выполнения с логированием ошибки используй {@link ILogger#safeReturn(IUnsafeRunnableWithReturn, Object, Object...)}
	 * <br> <br>
	 * Безопасно вернуть значение
	 * @param run Выполнявемая функция
	 * @param ifException Вернется, если случился {@link Throwable}
	 * @return Результат выполнения {@link IUnsafeRunnable} или значение по-умолчанию
	 */
	public static <T> T safeReturn(IUnsafeRunnableWithReturn<T> run, T ifException)
	{
		try
		{
			return run.apply();
		}
		catch (Throwable e)
		{
			// Nope!
		}
		
		return ifException;
	}

	/**
	 * Получить актуальный стактрейс
	 * @param stacktraceStartOffset - Смещение начала стактейса. (Столько элементов будет из него удалено)
	 * @return Актуальный стактейс с указанным смещением
	 */
	public static List<StackTraceElement> getThreadStacktrace(int stacktraceStartOffset)
	{
		return shiftStacktrace(new Throwable().getStackTrace(), 1 + stacktraceStartOffset);
	}
	
	/**
	 * Получить имя класса в текущем стактрейсе под указанным номером
	 * @param classIndex номер класса
	 * @return Имя класса
	 */
	public static String getClassNameAt(int classIndex)
	{
		var stackTraceElements = getThreadStacktrace(StacktraceOffsets.OFFSET_CALLER);
		
		return stackTraceElements.get(classIndex).getClassName();
	}
	
	/**
	 * Получить имя (без пакта) класса в текущем стактрейсе под указанным номером
	 * @param classIndex номер класса
	 * @return Имя класса
	 */
	public static String getSimpleClassNameAt(int classIndex)
	{
		String name = getClassNameAt(classIndex + 1);
		name = getClassSimpleName(name);
		return name;
	}

	/**
	 * Получить имя (без пакта) класса в текущем стактрейсе под указанным номером
	 * @param classIndex номер класса
	 * @return Имя класса
	 */
	public static String getPackageAt(int classIndex)
	{
		String name = getClassNameAt(classIndex + 1);
		name = getClassPackage(name);
		return name;
	}

	/** Стактрейс источника вызова */
	public static StackTraceElement getCaller()
	{
		return getThreadStacktrace(StacktraceOffsets.OFFSET_CALLER + 1).get(0);
	}

	/** Стактрейс источника вызова */
	@Alias("getCallerStacktrace")
	public static StackTraceElement caller()
	{
		return getThreadStacktrace(StacktraceOffsets.OFFSET_CALLER + 1).get(0);
	}
	
	/** Очистить от лямбд в генерированных классах */
	public static List<StackTraceElement> cleanFromGeneratedLambdas(List<StackTraceElement> stackTraceElements)
	{
		return stackTraceElements.stream()
				.filter((st) -> !st.getClassName().contains("$$Lambda$"))
			.collect(Collectors.toList());
	}
	
	/** Получить простое имя класса из каноничного */
	public static String getClassSimpleName(String canonicalName)
	{
		if (canonicalName.contains(".") && !canonicalName.endsWith("."))
		{
			canonicalName = canonicalName.substring(canonicalName.lastIndexOf(".") + 1);
		}
		
		return canonicalName;
	}
	
	/** Получить простое имя класса из каноничного */
	public static String getClassPackage(String canonicalName)
	{
		if (canonicalName.contains(".") && !canonicalName.endsWith("."))
		{
			canonicalName = canonicalName.substring(0, canonicalName.lastIndexOf("."));
		}
		
		return canonicalName;
	}
	
	/**
	 * Удалить несколько первыъ элементов стакстрейса 
	 * @param stacktrace Исходный стактрейс
	 * @param stacktraceOffset Смещение стактрейса
	 * @return Смещенный стакрейс
	 */
	public static List<StackTraceElement> shiftStacktrace(StackTraceElement[] stacktrace, int stacktraceOffset)
	{
		StackTraceElement[] newStackTrace = new StackTraceElement[stacktrace.length - stacktraceOffset];
		System.arraycopy(stacktrace, stacktrace.length - newStackTrace.length, newStackTrace, 0, newStackTrace.length);
		
		return Stream.of(newStackTrace).collect(Collectors.toList());
	}

	public static interface StacktraceOffsets {

		/** Используйте этот оффсет, чтобы получить стакстрейс текущего метода*/
		public static final int OFFSET_CURRENT = 0;
		
		/** Используйте этот оффсет, чтобы получить стакстрейс предыдущего (вызывающего) метода */
		public static final int OFFSET_CALLER = 1;
	}
}
