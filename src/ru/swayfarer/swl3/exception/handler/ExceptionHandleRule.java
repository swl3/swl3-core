package ru.swayfarer.swl3.exception.handler;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

@Data @Accessors(chain = true)
public class ExceptionHandleRule {
	
	public boolean useSkiped;
	public IFunction1<ExceptionEvent, Boolean> filterFun;
	public IFunction1NoR<ExceptionEvent> handleFun;
	public IFunction1<ExceptionEvent, ExceptionEvent> mapperFun = (e) -> e;
	
	public ExceptionHandleRule apply(ExceptionEvent event)
	{
		if (handleFun != null)
			handleFun.apply(mapperFun.apply(event));

		return this;
	}
	
	public boolean isAccepting(ExceptionEvent event)
	{
		if (event.isCanceled() && !useSkiped)
			return false;
		
		return filterFun == null || filterFun.apply(event) == Boolean.TRUE;
	}
}