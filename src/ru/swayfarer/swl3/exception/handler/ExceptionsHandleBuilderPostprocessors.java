package ru.swayfarer.swl3.exception.handler;

import java.lang.reflect.InvocationTargetException;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

@Data
@Accessors(chain = true)
public class ExceptionsHandleBuilderPostprocessors {
	
	@NonNull
	public ExceptionsHandleBuilder builder;
	
	public ExceptionsHandleBuilder unwrapReflection()
	{
		return unwrap(InvocationTargetException.class);
	}
	
	public ExceptionsHandleBuilder unwrap(Class<?>... types)
	{
		var tStream = ExtendedStream.of(types);
		
		return append((ex) -> {
			var e = ex;
			var type = e.getClass();
			
			while (tStream.contains(type))
			{
				var cause = ex.getCause();
				
				if (cause == null)
					break;
				
				e = cause;
				type = e.getClass();
			}
			
			return e;
		});
	}
	
	public ExceptionsHandleBuilder wrap(IFunction1<Throwable, Throwable> fun)
	{
		return append(fun);
	}
	
	public ExceptionsHandleBuilder append(IFunction1<Throwable, Throwable> fun)
	{
		builder.exceptionPostProcessor = builder.exceptionPostProcessor.andApply(fun);
		return builder;
	}
}
