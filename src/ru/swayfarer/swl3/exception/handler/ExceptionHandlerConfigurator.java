package ru.swayfarer.swl3.exception.handler;

import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.LibTags;
import ru.swayfarer.swl3.exception.StacktraceCleaner;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.reflection.ClassUtil;
import ru.swayfarer.swl3.string.StringUtils;

@Accessors(chain = true)
public class ExceptionHandlerConfigurator {

	@Setter
	public ExceptionsHandler exceptionsHandler;
	
	public ExceptionHandlerConfigurator throwByType(@NonNull Class<?>... types)
	{
	    return rule()
            .type(types)
            .thanSkip()
            .throwEx()
        ;
	}
	
	@SneakyThrows
	public ExceptionHandlerConfigurator logAny() 
	{
		String callerClassName = new Throwable().getStackTrace()[1].getClassName();
		Class<?> callerClass = ClassUtil.forName(callerClassName);
		return rule(Integer.MAX_VALUE).any().log(LogFactory.getLogger(callerClass));
	}
	
	public ExceptionHandlerConfigurator throwAny()
	{
		return rule(0)
				.any()
				.thanSkip()
				.throwEx()
		;
	}
	
	public ExceptionHandlerConfigurator crashOnStartupFail()
	{
		return rule(0)
					.tags(LibTags.Exceptions.tagAppStartup)
					.thanSkip()
					.throwEx()
		;
	}
	
	public RuleConfigurator rule(int priority)
    {
	    var rule = new RuleConfigurator()
                .setRule(new ExceptionHandleRule())
                .setConfigurator(this)
        ;
        
        exceptionsHandler.rules.add(rule.rule, priority);
        
        return rule;
    }
	
	public RuleConfigurator rule()
	{
		return rule(0);
	}
	
	public ExceptionsHandler handler()
	{
		return exceptionsHandler;
	}
	
	@Accessors(chain = true)
	public static class RuleConfigurator {
		
		@Setter
		public ExceptionHandleRule rule;
		
		@Setter
		public ExceptionHandlerConfigurator configurator;
		
		public RuleConfigurator useSkiped()
		{
			rule.useSkiped = true;
			return this;
		}
		
		public RuleConfigurator any()
		{
			return filter((e) -> !e.isCanceled());
		}
		
		public RuleConfigurator tags(@NonNull String... tags)
		{
			return filter(StandartExceptionsFilters.tags(tags));
		}
		
		public RuleConfigurator fromMethods(@NonNull String... methodNames)
		{
		    return filter(StandartExceptionsFilters.fromMethods(methodNames));
		}
		
		public RuleConfigurator typeAssignable(@NonNull Class<?>... types)
		{
			return filter(StandartExceptionsFilters.assignable(types));
		}
        
        public RuleConfigurator fromClasses(@NonNull Class<?>... classes)
        {
            return filter(StandartExceptionsFilters.fromClasses(classes));
        }
		
		public RuleConfigurator fromClasses(@NonNull String... classNames)
		{
		    return filter(StandartExceptionsFilters.fromClasses(classNames));
		}
		
		public RuleConfigurator runtimes()
		{
			return filter(StandartExceptionsFilters.checked());
		}
		
		public RuleConfigurator exceptions()
		{
			return filter(StandartExceptionsFilters.exceptions());
		}
		
		public RuleConfigurator errors()
		{
			return filter(StandartExceptionsFilters.errors());
		}
		
		public RuleConfigurator type(@NonNull Class<?>... types)
		{
			return filter(StandartExceptionsFilters.type(types));
		}
		
		public RuleConfigurator filter(@NonNull IFunction1<ExceptionEvent, Boolean> filter)
		{
			if (rule.filterFun == null)
				rule.filterFun = filter;
			else
				rule.filterFun = rule.filterFun.and(filter);
			
			return this;
		}
		
		@SneakyThrows
		private void throwException(Throwable e)
		{
			throw StacktraceCleaner.clearStacktrace(e);
		}
		
		@SneakyThrows
		protected Throwable getWrappedThrowable(Class<?> classOfWrapper, Throwable wrapped, StackTraceElement wrapLine)
		{
			var constructor = classOfWrapper.getDeclaredConstructor(Throwable.class);
			var ret = (Throwable) constructor.newInstance(wrapped);
			ret.setStackTrace(new StackTraceElement[] {wrapLine} );
			return ret;
		}
		
		public RuleConfigurator unwrap(Class<?>... types)
		{
			return map((e) -> {
				
				var exception = e.getException();
				var exceptionType = exception.getClass();
				
				for (var type : types)
				{
					if (exceptionType == type)
					{
						e.setException(exception.getCause());
					}
				}
				
				return e;
			});
		}
		
		public RuleConfigurator map(IFunction1<ExceptionEvent, ExceptionEvent> mapper)
		{
			rule.mapperFun = rule.mapperFun.andApply(mapper);
			return this;
		}
		
		public RuleConfigurator wrap(Class<? extends Throwable> classOfWrapper)
		{
			var caller = ExceptionsUtils.getCaller();
			return map((evt) -> {
				evt.setException(getWrappedThrowable(classOfWrapper, evt.getException(), caller));
				return evt;
			});
		}
		
		public ExceptionHandlerConfigurator throwEx()
		{
			return handle((e) -> throwException(e.getException()));
		}
		
		public ExceptionHandlerConfigurator handle(@NonNull IFunction1NoR<ExceptionEvent> handler)
		{
			if (rule.handleFun == null)
				rule.handleFun = handler;
			else
				rule.handleFun = rule.handleFun.andAfter(handler);
			
			return configurator;
		}
		
		public ExceptionHandlerConfigurator log(@NonNull ILogger logger)
		{
			return handle((evt) -> {
				var logMessage = evt.getMessage() + " | (tags: " + evt.getTags() + ") ";
				var ex = evt.getException();
				StacktraceCleaner.clearStacktrace(ex);

				if (evt.hasTag(LibTags.Exceptions.tagShortPrint))
				{

					logger.error(logMessage + ":", ex);
				}
				else
				{
					logger.error(ex, logMessage);
				}
			});
		}
		
		public RuleConfigurator thanSkip()
		{
			return map((evt) -> evt.setCanceled(true));
		}
		
		public ExceptionHandlerConfigurator ignore()
		{
			return handle((evt) -> evt.setCanceled(true));
		}
	}
}
