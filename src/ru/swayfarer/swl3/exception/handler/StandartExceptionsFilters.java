package ru.swayfarer.swl3.exception.handler;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.string.ExpressionsList;

public interface StandartExceptionsFilters
{
	public static IFunction1<ExceptionEvent, Boolean> checked()
	{
		return (evt) -> !(evt.getException() instanceof RuntimeException);
	}
	
	public static IFunction1<ExceptionEvent, Boolean> unchecked()
	{
		return (evt) -> evt.getException() instanceof RuntimeException;
	}
	
	public static IFunction1<ExceptionEvent, Boolean> exceptions()
	{
		return (evt) -> evt.getException() instanceof Exception;
	}
	
	public static IFunction1<ExceptionEvent, Boolean> tags(@NonNull String... tags)
	{
		return (evt) -> evt.getTags().containsSome((Object[]) tags);
	}
	
	public static IFunction1<ExceptionEvent, Boolean> errors()
	{
		return (evt) -> evt.getException() instanceof Error;
	}
    
    public static IFunction1<ExceptionEvent, Boolean> fromMethods(@NonNull String... methodNames)
    {
        var expressionsList = new ExpressionsList(methodNames);
        return (evt) -> {
            return expressionsList.isMatches(evt.getSource().getSource().getMethodName());
        };
    }
    
    public static IFunction1<ExceptionEvent, Boolean> fromClasses(@NonNull Class<?>... classes)
    {
        var classesNamesList = ExtendedStream.of(classes)
                .map(Class::getName)
                .toArray(String.class)
        ;
        
        return fromClasses(classesNamesList);
    }
	
	public static IFunction1<ExceptionEvent, Boolean> fromClasses(@NonNull String... packages)
    {
	    var expressionsList = new ExpressionsList(packages);
	    return (evt) -> {
	        return expressionsList.isMatches(evt.getSource().getSource().getClassName());
	    };
    }
	
	public static IFunction1<ExceptionEvent, Boolean> type(@NonNull Class<?>... types)
	{
		return (evt) -> {
			var exceptionType = evt.getException().getClass();
			
			for (var type : types)
			{
				if (exceptionType == type)
					return true;
			}
			
			return false;
		};
	}
	
	public static IFunction1<ExceptionEvent, Boolean> assignable(@NonNull Class<?>... types)
	{
		return (evt) -> {
			var exceptionType = evt.getException().getClass();
			
			for (var type : types)
			{
				if (type.isAssignableFrom(exceptionType))
					return true;
			}
			
			return false;
		};
	}
}
