package ru.swayfarer.swl3.exception.handler;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.ConcattedString;
import ru.swayfarer.swl3.observable.event.AbstractEvent;
import ru.swayfarer.swl3.observable.event.EventSource;
import ru.swayfarer.swl3.string.StringUtils;

@Data
@Accessors(chain = true)
public class ExceptionsHandleBuilder {
	
	public int handleOffset = 0;
	
	@NonNull
	public ExceptionsHandler exceptionsHandler;
	
	@NonNull
	public IFunction1<Throwable, Throwable> exceptionPostProcessor = (e) -> e;
	
	@NonNull
	public ExceptionEvent exceptionEvent = ExceptionEvent.empty();
	
	public Throwable exception;

	public ExceptionsHandleBuilder message(@NonNull String str)
	{
		return message(() -> str);
	}

	public ExceptionsHandleBuilder message(@NonNull IFunction0<String> str)
	{
		exceptionEvent.setMessage(str);
		return this;
	}
	
	public ExceptionsHandleBuilder message(@NonNull @ConcattedString Object... str)
	{
		return message(() -> StringUtils.concatWithSpaces(str));
	}
	
	public ExceptionsHandleBuilder throwable(@NonNull Throwable e)
	{
		this.exception = e;
		return this;
	}
	
	public ExceptionsHandleBuilder tag(@NonNull String... tags)
	{
		exceptionEvent.tag(tags);
		return this;
	}

	public ExceptionsHandleBuilder tag(@NonNull Iterable<String> tags)
	{
		exceptionEvent.tag(tags);
		return this;
	}

	public ExceptionsHandleBuilder data(String key, Object value)
	{
		exceptionEvent.getCustomAttributes().put(key, value);
		return this;
	}
	
	public ExceptionsHandleBuilderPostprocessors and()
	{
		return new ExceptionsHandleBuilderPostprocessors(this);
	}
	
	public ExceptionsHandleBuilder start()
	{
		handleOffset = 2;
		completeInit(exception);
		exceptionsHandler.handle(exceptionEvent);

		return this;
	}
	
	public ExceptionsHandleBuilder start(IFunction0NoR fun)
	{
		try
		{
			fun.apply();
		}
		catch (Throwable e)
		{
			handleOffset = 2;
			completeInit(e);
			exceptionsHandler.handle(exceptionEvent);
		}
		
		return this;
	}
	
	public <T> T andReturn(IFunction0<T> fun)
	{
		return andReturn(fun, null);
	}
	
	public <T> T andReturn(IFunction0<T> fun, T ifEx)
	{
		T ret = ifEx;
		
		try
		{
			ret = fun.apply();
		}
		catch (Throwable e)
		{
			handleOffset = 2;
			completeInit(e);
			exceptionsHandler.handle(exceptionEvent);
		}
		
		return ret;
	}
	
	public void completeInit(@NonNull Throwable e)
	{
		e = exceptionPostProcessor.apply(e);
		setException(e);
		exceptionEvent.setSource(new EventSource(handleOffset + 1));
		exceptionEvent.setException(exceptionPostProcessor.apply(exception));
	}
}
