package ru.swayfarer.swl3.exception.handler;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.collections.list.PriorityList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.LibTags;
import ru.swayfarer.swl3.exception.LibTags.Exceptions;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.funs.IUnsafeRunnable;
import ru.swayfarer.swl3.funs.IUnsafeRunnableWithReturn;
import ru.swayfarer.swl3.markers.ConcattedString;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.thread.group.ThreadGroupLocal;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Обработчик исключений <br> 
 * Настраивается через кастомные правила обработки <br> 
 * Позволяет гибко настроить обработку исключений
 * @author swayfarer
 *
 */
@Getter
@Setter
@Accessors(chain = true)
public class ExceptionsHandler {

	public static int stateNotConfigured = 0;
	public static int stateFallbackConfiguring = 1;
	public static int stateFallbackConfigured = 2;
	public static int stateConfigured = 3;

	public static AtomicInteger staticState = new AtomicInteger();

	public static ExceptionsHandler INSTANCE = new ExceptionsHandler();

	/** Хранилка для контекстного обработчика ошибок */
	@Internal
	public static ThreadGroupLocal<ExceptionsHandler> contextExceptionsHandler = new ThreadGroupLocal<>();
	
	/**
	 * Правила обработки ошибок <br>
	 * Правила вызываются друг за другом, пока хотя бы одно не подойдет под событие. 
	 * После этого обработка события прервыается
	 */
	@Internal
	public PriorityList<ExceptionHandleRule> rules = new PriorityList<>();
	
	/** Использовать ли контекстный обработчик ошибок, если ни одно правило не подойдет?*/
	@Internal
	public boolean useContextHandler = true;

	/**
	 * Очитить правила
	 * @return this
	 */
	public ExceptionsHandler clear()
	{
		rules.clear();
		return this;
	}
	
	/**
	 * Задать контекстный обработчик ошибок
	 * @param exceptionsHandler
	 */
	public static void setContextHandler(@NonNull ExceptionsHandler exceptionsHandler)
	{
		contextExceptionsHandler.value(exceptionsHandler);
	}
	
	/**
	 * Получить контекстный обработчик событий
	 * @return Обработчик или <code>null</code>
	 */
	public static ExceptionsHandler getContextHandler()
	{
		return contextExceptionsHandler.getValue();
	}
	
	/**
	 * Получить контекстный обработчик событий или создать его
	 * @return Обработчик
	 */
	public static ExceptionsHandler getOrCreateContextHandler()
	{
		var ret = getContextHandler();

		if (ret == null)
		{
			synchronized (contextExceptionsHandler)
			{
				ret = getContextHandler(); 
				
				if (ret == null)
				{
					ret = INSTANCE;
					setContextHandler(ret);
				}
			}
		}

		if (staticState.get() == stateNotConfigured)
		{
			staticState.set(stateFallbackConfiguring);
			defaultConfiguration(ret);
			staticState.set(stateFallbackConfigured);
		}
		
		return ret;
	}
	
	public static ExceptionsHandler logByDefault()
	{
	    var ret = getOrCreateContextHandler();

		if (staticState.get() != stateConfigured)
		{
			ret.reconfigure();
			staticState.set(stateConfigured);
		}

	    ret.configure()
	    	.rule(Integer.MAX_VALUE)
	    		.any()
	    		.thanSkip()
	    		.log(LogFactory.getLogger(ExceptionsUtils.callerClass()))
		;

	    return ret;
	}

	public static ExceptionsHandler defaultConfiguration()
	{
		return defaultConfiguration(getOrCreateContextHandler());
	}

	public static ExceptionsHandler defaultConfiguration(ExceptionsHandler eh)
	{
		if (staticState.get() != stateConfigured && staticState.get() != stateFallbackConfiguring)
		{
			eh.reconfigure();
			staticState.set(stateConfigured);
		}

		eh.configure()
				.rule()
					.tags(Exceptions.tagLibPrimaryFunctional)
					.thanSkip()
					.throwEx()
				.rule()
					.tags(Exceptions.tagAppStartup)
					.thanSkip()
					.throwEx()
				.rule()
					.tags(Exceptions.tagSerialize)
					.thanSkip()
					.throwEx()
				.logAny()
		;

		return eh;
	}
	
	/**
	 * Перенастроить обработчик (удалить старые правила)
	 * @return Конфигуратор
	 */
	public ExceptionHandlerConfigurator reconfigure()
	{
		return clear().configure();
	}
	
	/**
	 * Настроить обработчик
	 * @return Конфигуратор
	 */
	public ExceptionHandlerConfigurator configure()
	{
		return new ExceptionHandlerConfigurator()
				.setExceptionsHandler(this)
		;
	}
	
	/**
	 * Зарегистрировать правило обработки исключения
	 * @param filter Функция-фильтр. Если возвращает True, то обработчик может заняться исключением
	 * @param handler Функция-обработчик. Вызывается, если фильтр пропускает исключение
	 * @return this
	 */
	public ExceptionsHandler rule(@NonNull IFunction1<ExceptionEvent, Boolean> filter, @NonNull IFunction1NoR<ExceptionEvent> handler) 
	{
		this.rules.add(new ExceptionHandleRule()
				.setFilterFun(filter)
				.setHandleFun(handler)
		, 2);
		
		return this;
	}
	
	/**
     * Обработать исключение
     * @param e Исключение
     * @param message Сообщение к исключению
     */
    public void handle(@NonNull Throwable e, @ConcattedString Object... message)
    {
        handle(2, e, message);
    }
    
    public ExceptionsHandleBuilder handle()
    {
    	return new ExceptionsHandleBuilder(this);
    }
	
	/**
	 * Обработать исключение
	 * @param e Исключение
	 * @param message Сообщение к исключению
	 */
	public void handle(int offset, @NonNull Throwable e, @ConcattedString Object... message)
	{
	    if (message == null)
	    {
	        message = new Object[] {"Error while processing safe action"};
	    }

	    var s = message;
		var event = ExceptionEvent.builder()
				.exception(e)
				.message(() -> StringUtils.concatWithSpaces(s))
				.build()
		;
		
		event.getSource().offset += offset + 2;

		handle(event);
	}
	
	@Internal
	public void handle(ExceptionEvent event)
	{
		if (event.visitedHandlers.contains(this))
			return;
		
		event.visitedHandlers.add(this);

		if (rules.asList().isEmpty())
		{
			traceException(event, "[ExceptionTrace] No rules for handle" + event.getException());
		}

		for (var rule : rules)
		{
			if (rule.isAccepting(event))
			{
				rule.apply(event);
				traceException(event, "[ExceptionTrace] exception event " + event.getException() + " was accepted by rule " + rule);
			}
			else
			{
				traceException(event, "[ExceptionTrace] exception event " + event.getException() + " was NOT accepted by rule " + rule);
			}
		}

		if (!event.isCanceled() && useContextHandler)
		{
			var contextHandler = ExceptionsHandler.getOrCreateContextHandler();

			if (contextHandler != null && contextHandler != this)
				contextHandler.handle(event);
		}
	}

	public void traceException(ExceptionEvent event, String msg)
	{
		if (event.hasTag(LibTags.Exceptions.tagTrace))
			System.out.println("[ExceptionTrace]" + msg);
	}
	
	public ExceptionsHandler andAfter(ExceptionsHandler exceptionsHandler)
	{
		configure()
			.rule()
				.any()
				.handle(exceptionsHandler::handle)
		;
		
		return this;
	}
	
	/**
	 * Выполнить действие и обработать исключения, если вылетят
	 * @param run Выполняемое действие
	 */
	public void safe(@NonNull IUnsafeRunnable run)
	{
		try
		{
			run.apply();
		}
		catch (Throwable e)
		{
			handle(2, e);
		}
	}
	
	/**
	 * Безопасно выполнить действие и обработать исключения, если вылетят
	 * @param run Выполняемое действие
	 * @param message Сообщение к обрабатываемому исключению
	 */
	public void safe(@NonNull IUnsafeRunnable run, @ConcattedString Object... message)
	{
		try
		{
			run.apply();
		}
		catch (Throwable e)
		{
			handle(2, e, message);
		}
	}
	
	/**
	 * Безопасно выполнить действие и обработать исключения, если вылетят. <br> 
	 * Возвращает значение.
	 * @param run Выполняемое действие
	 * @param ifEx Значение, которое вернется, если бросится {@link Exception}
	 * @return Значение, которое вернет действие, или значение по-умолчанию
	 */
	public <T> T safeReturn(@NonNull IUnsafeRunnableWithReturn<T> run, T ifEx)
	{
		try
		{
			return run.apply();
		}
		catch (Throwable e)
		{
			handle(2, e);
		}
		
		return ifEx;
	}
	
	/**
	 * Безопасно выполнить действие и обработать исключения, если вылетят. <br> 
	 * Возвращает значение.
	 * @param run Выполняемое действие
	 * @param ifEx Значение, которое вернется, если бросится {@link Exception}
	 * @param message Сообщение к обрабатываемому исключению
	 * @return Значение, которое вернет действие, или значение по-умолчанию
	 */
	public <T> T safeReturn(@NonNull IUnsafeRunnableWithReturn<T> run, T ifEx, @ConcattedString Object... message)
	{
		try
		{
			return run.apply();
		}
		catch (Throwable e)
		{
			handle(2, e, message);
		}
		
		return ifEx;
	}
	
	public static ExceptionsHandler getInstance()
	{
		return INSTANCE;
	}
}
