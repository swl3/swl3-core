package ru.swayfarer.swl3.exception.handler;

import java.util.ArrayList;
import java.util.List;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import lombok.experimental.Tolerate;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.markers.HierarchySensitive;
import ru.swayfarer.swl3.observable.event.AbstractCancelableEvent;
import ru.swayfarer.swl3.observable.event.EventSource;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true) 
@SuperBuilder
public class ExceptionEvent extends AbstractCancelableEvent {
	public Throwable exception;
	public IFunction0<String> message;
	
	@Builder.Default
	public List<ExceptionsHandler> visitedHandlers = new ArrayList<>();
	
	@Builder.Default
	@HierarchySensitive
	public EventSource source = new EventSource(2);

	public String getMessage()
	{
		return message == null ? null : message.apply();
	}

	@Tolerate
	public ExceptionEvent setMessage(String msg)
	{
		this.message = () -> msg;
		return this;
	}

	public ExceptionEvent setMessage(IFunction0<String> msg)
	{
		this.message = msg.memorized();
		return this;
	}
	
	public static ExceptionEvent empty()
	{
		return ExceptionEvent.builder().build();
	}
}