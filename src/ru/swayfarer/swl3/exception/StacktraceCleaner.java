package ru.swayfarer.swl3.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import lombok.var;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.system.SystemUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Predicate;

public class StacktraceCleaner
{
    public static AtomicInteger staticState = new AtomicInteger();
    public static int stateNotLoaded = 0;
    public static int stateLoading = 1;
    public static int stateLoaded = 2;

    @Internal
    public static AtomicReference<List<StacktraceClassFilteringEntry>> stacktraceExcludes = new AtomicReference<>(new CopyOnWriteArrayList<>());

    @Internal
    public static AtomicReference<List<String>> enabledClassLevels = new AtomicReference<>(new CopyOnWriteArrayList<>());

    @Internal
    public static List<Predicate<StackTraceElement>> stacktraceFilters = new CopyOnWriteArrayList<>();

    @Internal
    public static AtomicBoolean stacktraceFiltersEnabled = new AtomicBoolean(false);

    public static boolean isStacktraceElementPassFilter(StackTraceElement stackTraceElement)
    {
        for (var filter : stacktraceFilters)
        {
            try
            {
                if (!filter.test(stackTraceElement))
                    return false;
            }
            catch (Throwable e)
            {
                // Nope!
                return false;
            }
        }

        return true;
    }

    public static void addFilter(Predicate<StackTraceElement> filter)
    {
        if (filter != null)
        {
            stacktraceFilters.add(filter);
        }
    }

    public static void addFilter(Predicate<StackTraceElement> filter, String hideLevel)
    {
        if (filter != null)
        {
            Predicate<StackTraceElement> newFilter = (st) -> isHideLevelEnabled(hideLevel) && filter.test(st);
            addFilter(newFilter);
        }
    }

    public static void clearClassFromStacktraces(String classname, String level)
    {
        var entry = StacktraceClassFilteringEntry.builder()
                .level(level)
                .content(classname)
                .build()
        ;

        stacktraceExcludes.get().add(entry);
    }

    public static void enable()
    {
        stacktraceFiltersEnabled.set(true);
    }

    public static void disable()
    {
        stacktraceFiltersEnabled.set(false);
    }

    public static void enableAllDefaultLevels()
    {
        enable(Levels.funs);
        enable(Levels.javaCollections);
        enable(Levels.javaReflection);
        enable(Levels.observables);
        enable(Levels.swlCollections);
        enable(Levels.swlIoc);
        enable(Levels.swlReflection);
        enable(Levels.swlExceptionsHandlers);
    }

    public static void enable(String hideLevel)
    {
        if (!isHideLevelEnabled(hideLevel))
        {
            if (staticState.get() == stateLoading)
            {
                var systemUtils = SystemUtils.getInstance();
                var propertyName = "swl3.stacktrace.cleaner.level." + hideLevel + ".auto";
                var levelAutoLoadEnabled = systemUtils.findAppProperty(propertyName).map((v) -> v.getValue(Boolean.class)).orElse(true);
                if (!levelAutoLoadEnabled)
                {
                    return;
                }
            }

            enabledClassLevels.get().add(hideLevel);
        }
    }

    public static void disable(String hideLevel)
    {
        enabledClassLevels.get().remove(hideLevel);
    }

    public static boolean isEnabled()
    {
        if (staticState.get() == stateNotLoaded)
        {
            var propertyName = "swl3.stacktrace.cleaner.enabled";

            staticState.set(stateLoading);

            var systemUtils = SystemUtils.getInstance();
            var stacktraceCleanerEnabled = systemUtils.findAppProperty(propertyName).map((v) -> v.getValue(Boolean.class)).orElse(true);

            if (stacktraceCleanerEnabled)
            {
                var logger = LogFactory.getLogger();
                logger.warn("Stacktrace cleaner enabled. Some stacktrace info may be cleaned in swl3 context. To disable it function, please, set", propertyName, "property to false or execute", StacktraceCleaner.class.getName(), "disable() method!");
                enable();
                enableAllDefaultLevels();
            }

            staticState.set(stateLoaded);
        }

        return stacktraceFiltersEnabled.get();
    }

    public static boolean isHideLevelEnabled(String hideLevel)
    {
        return enabledClassLevels.get().contains(hideLevel);
    }

    public static Throwable clearStacktrace(Throwable e)
    {
        if (!isEnabled())
            return e;

        var origEx = e;

        while (e != null)
        {
            var stackTrace = e.getStackTrace();
            List<StackTraceElement> newStacktrace = new ArrayList<>();

            for (var stacktraceElement : stackTrace)
            {
                if (StacktraceCleaner.isStacktraceElementPassFilter(stacktraceElement))
                {
                    newStacktrace.add(stacktraceElement);
                }
            }

            var newStacktraceArray = newStacktrace.toArray(new StackTraceElement[0]);

            try
            {
                e.setStackTrace(newStacktraceArray);
            }
            catch (Throwable t)
            {
                System.out.println("123456");
                // Nope!
            }

            e = e.getCause();
        }

        return origEx;
    }

    static {
        clearClassFromStacktraces("ru.swayfarer.swl3.observable.sub.Subscription", Levels.observables);
        clearClassFromStacktraces("ru.swayfarer.swl3.observable.Observable", Levels.observables);
        clearClassFromStacktraces("ru.swayfarer.swl3.observable.SubscriptionHelper", Levels.observables);

        clearClassFromStacktraces("ru.swayfarer.swl3.collections.stream.ExtendedStream", Levels.swlCollections);
        clearClassFromStacktraces("ru.swayfarer.swl3.funs.GeneratedFuns", Levels.funs);

        clearClassFromStacktraces("java.util.HashMap$ValueSpliterator", Levels.javaCollections);
        clearClassFromStacktraces("java.util.stream", Levels.javaCollections);
        clearClassFromStacktraces("java.util.ArrayList", Levels.javaCollections);
        clearClassFromStacktraces("java.util.Spliterators", Levels.javaCollections);

        clearClassFromStacktraces("java.lang.reflect", Levels.javaReflection);

        clearClassFromStacktraces("ru.swayfarer.iocv3", Levels.swlIoc);
        clearClassFromStacktraces("ru.swayfarer.swl3.reflection", Levels.swlReflection);

        clearClassFromStacktraces("ru.swayfarer.swl3.exception", Levels.swlExceptionsHandlers);
        clearClassFromStacktraces("ru.swayfarer.swl3.collections", Levels.swlCollections);

        addFilter((st) -> {
            var excludes = stacktraceExcludes.get();
            for (var entry : excludes)
            {
                if (isHideLevelEnabled(entry.getLevel()))
                {
                    var classname = entry.getContent();

                    if (st.getClassName().startsWith(classname))
                        return false;
                }
            }

            return true;
        });
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    @NoArgsConstructor
    @AllArgsConstructor
    @SuperBuilder
    public static class StacktraceClassFilteringEntry {
        public String level;
        public String content;
    }

    public static class Levels {
        public static final String observables = "swl3-stacktrace-observable";
        public static final String swlCollections = "swl3-stacktrace-collections";
        public static final String swlIoc = "swl3-stacktrace-ioc";
        public static final String swlReflection = "swl3-stacktrace-reflection";
        public static final String swlExceptionsHandlers = "swl3-stacktrace-exceptionsHandler";
        public static final String funs = "swl3-stacktrace-funs";

        public static final String javaCollections = "java-stacktrace-collections";
        public static final String javaReflection = "java-stacktrace-reflection";
    }
}
