package ru.swayfarer.swl3.string;

import java.util.List;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;

public class ExpressionsList extends ExtendedList<String> {

    public ExpressionsList()
    {
        super();
    }

    public ExpressionsList(@NonNull List<String> wrappedList)
    {
        super(wrappedList);
    }
    
    public ExpressionsList(@NonNull String... elements)
    {
        this((Object[]) elements);
    }
    
    public ExpressionsList(@NonNull Object... elements)
    {
        for (var elem : elements)
        {
            if (elem != null)
            {
                add(String.valueOf(elem));
            }
        }
    }
    
    public ExpressionsList regex(Object obj)
    {
        if (obj != null)
        {
            String str = String.valueOf(obj);
            add("regex:" + str);
        }
        
        return this;
    }
    
    public ExpressionsList mask(Object obj)
    {
        if (obj != null)
        {
            String str = String.valueOf(obj);
            add("mask:" + str);
        }
        
        return this;
    }
    
    public boolean isMatches(Object obj)
    {
        for (var expression : this)
        {
            if (StringUtils.isMatchesByExpression(expression, obj))
                return true;
        }
        
        return false;
    }
}
