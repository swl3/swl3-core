package ru.swayfarer.swl3.string.filtering;

import lombok.NonNull;
import ru.swayfarer.swl3.collections.stream.AbstractFiltering;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.string.StringUtils;

public class StringFiltering<Ret_Type> extends AbstractFiltering<String, StringFiltering<Ret_Type>, Ret_Type>{

	public StringFiltering(@NonNull IFunction1<IFunction1<String, Boolean>, Ret_Type> filterFun)
	{
		super(filterFun);
	}
	
	public Ret_Type regex(@NonNull String regex)
	{
		return filter((s) -> StringUtils.isMatchesByRegex(regex, s));
	}
	
	public Ret_Type mask(@NonNull String mask)
	{
		return filter((s) -> StringUtils.isMatchesByMask(mask, s));
	}
	
	public Ret_Type is(@NonNull String... strs)
	{
		return filter((s) -> ExtendedStream.of(strs).anyMatches((str) -> str.equals(s)));
	}
}
