package ru.swayfarer.swl3.string;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;

@Getter
@Setter
@Accessors(chain = true)
public class FilteringList {

    @NonNull
    public transient ExtendedList<String> includePrefixes = CollectionsSWL.list("i:", "include:", "inc:", "in:");
    
    @NonNull
    public transient ExtendedList<String> excludePrefixes = CollectionsSWL.list("e:", "exclude:", "exc:", "ex:");
    
    @NonNull
    public ExpressionsList includes = new ExpressionsList();
    
    @NonNull
    public ExpressionsList excludes = new ExpressionsList();
    
    public FilteringList exclude(@NonNull Object exclude)
    {
        excludes.add(exclude.toString());
        return this;
    }
    
    public FilteringList include(@NonNull Object include)
    {
        includes.add(include.toString());
        return this;
    }
    
    public FilteringList add(@NonNull Object expressionWithPrefix)
    {
        var str = expressionWithPrefix.toString();
        
        for (var includePrefix : includePrefixes)
        {
            if (str.length() > includePrefix.length() && str.startsWith(includePrefix))
            {
                str = str.substring(includePrefix.length());
                include(str);
                break;
            }
        }
        
        for (var excludePrefix : excludePrefixes)
        {
            if (str.length() > excludePrefix.length() && str.startsWith(excludePrefix))
            {
                str = str.substring(excludePrefix.length());
                exclude(str);
                break;
            }
        }
        
        return this;
    }
    
    public FilteringList addAll(FilteringList filteringList)
    {
        if (filteringList == null)
            return this;
        
        if (!CollectionsSWL.isNullOrEmpty(filteringList.excludes))
            excludes.addAll(filteringList.excludes);
        
        if (!CollectionsSWL.isNullOrEmpty(filteringList.includes))
            excludes.addAll(filteringList.includes);
        
        return this;
    }
    
    public boolean isMatches(@NonNull Object obj)
    {
        if (excludes.isMatches(obj))
            return false;
        
        if (includes.isNotEmpty())
            return includes.isMatches(obj);
        
        return true;
    }
    
    public FilteringList addAll(@NonNull Iterable<String> strs)
    {
        for (var str : strs)
        {
            add(str);
        }
        
        return this;
    }
    
    public static FilteringList of(@NonNull Iterable<String> strings)
    {
        var ret = new FilteringList();
        ret.addAll(strings);
        return ret;
    }

    public boolean isEmpty()
    {
        return excludes.isEmpty() && includes.isEmpty();
    }
}
