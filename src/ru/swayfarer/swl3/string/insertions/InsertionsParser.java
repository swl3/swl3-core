package ru.swayfarer.swl3.string.insertions;

import java.util.HashMap;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.StringValue;
import ru.swayfarer.swl3.string.converters.StringConverters;
import ru.swayfarer.swl3.string.dynamic.DynamicString;
import ru.swayfarer.swl3.string.dynamic.StrReader;

public class InsertionsParser {

    public StringConverters stringConverters = new StringConverters().defaultTypes();
    
    public ExtendedList<String> insertionStarts = CollectionsSWL.list("${");
    public ExtendedList<String> insertionEnds = CollectionsSWL.list("}");
    
    public String argSplitter = ",";
    public String argNameSplitter = ":";
    
    public int maxInsertionPriority = Integer.MAX_VALUE;
    
    public Insertions parse(@NonNull String str)
    {
        var result = new ExtendedList<Insertion>();
        var reader = new StrReader(str);
        int pos = -1;
        ParsingState state = null;
        
        while (!reader.atEnd())
        {
            pos = reader.pos;
            
            if (reader.skipSome(insertionStarts))
            {
                state = new ParsingState(state);
                
                if (state.getInsertionPriority() > maxInsertionPriority)
                    throw new IndexOutOfBoundsException("Insertion priority bigger than max!");
                
                state.getResultInfo().setStartChar(pos);
            }
            else if (reader.skipSome(insertionEnds))
            {
                if (state != null)
                {
                    var resultInfo = state.getResultInfo();
                    
                    resultInfo.setEndChar(reader.pos);
                    resultInfo.takeInserionStrFrom(str);
                    resultInfo.priority = state.insertionPriority;
                    
                    var parentState = state.getParent();
                    parseArgs(reader, state);
                    
                    var resultBuilder = state.getResultBuilder();
                    resultBuilder.pos(state.getResultInfo());
                    resultBuilder.stringConverters(stringConverters);
                    
                    var resultEntry = resultBuilder.build();
                    
                    result.add(resultEntry);
                    state = parentState;
                }
            }
            else
            {
                var nextChar = reader.next();
                
                if (state != null)
                {
                    if (nextChar != null)
                        state.getArgsStr().append(nextChar);
                }
            }
        }
        
        reader.close();
        
        var insertions = Insertions.builder()
                .insertions(
                    result.exStream()
                        .distinctBy((o) -> o.getPos().getInsertionStr())
                        .toExList()
                )
                .build()
        ;
        
        return insertions;
    }
    
    public void parseArgs(@NonNull StrReader reader, @NonNull ParsingState state)
    {
        var argsStr = state.getArgsStr().toString();
        var argsMap = new HashMap<String, StringValue>();
        
        if (!StringUtils.isBlank(argsStr))
        {
            var args = CollectionsSWL.list(argsStr.split(argSplitter));
            
            if (args.isNotEmpty())
            {
                for (var arg : args)
                {
                    arg = StringUtils.strip(arg);
                    
                    var argNameAndValue = CollectionsSWL.list(arg.split(argNameSplitter));
                    
                    String name = argNameAndValue.size() > 1 ? argNameAndValue.first() : "value";
                    name = StringUtils.strip(name);
                    
                    String value = argNameAndValue.size() < 3 ? argNameAndValue.last() : StringUtils.concatWith(argNameSplitter, 1, -1, argNameAndValue.toArray());
                    value = StringUtils.strip(value);
                    
                    final var finalValue = value;
                    var property = new StringValue(stringConverters, () -> finalValue);
                    property.updateValue();
                    
                    argsMap.put(name, property);
                }
            }
            
            state.getResultBuilder().args(argsMap);
        }
    }
    
    @Data
    @Accessors(chain = true)
    public static class ParsingState {
        
        public DynamicString argsStr = new DynamicString();
        public ParsingState parent;
        public int insertionPriority;
        public Insertion.InsertionBuilder resultBuilder = Insertion.builder();
        public InsertionPosInString resultInfo = new InsertionPosInString();
        
        public ParsingState(ParsingState parentState)
        {
            this.parent = parentState;
            
            if (parentState != null)
            {
                insertionPriority = parentState.insertionPriority + 1;
            }
        }
    }
}
