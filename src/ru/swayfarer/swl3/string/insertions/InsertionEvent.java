package ru.swayfarer.swl3.string.insertions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.observable.event.AbstractEvent;

@Data @EqualsAndHashCode(callSuper = true)
@SuperBuilder
@Accessors(chain = true)
public class InsertionEvent extends AbstractEvent {
    
    @NonNull
    public Insertion insertion;
    
    @Data @EqualsAndHashCode(callSuper = true)
    @SuperBuilder
    @Accessors(chain = true)
    public static class WithResult extends InsertionEvent {
        public String result;
        
        public static WithResult of(@NonNull InsertionEvent event)
        {
            return builder()
                    .customAttributes(event.getCustomAttributes())
                    .insertion(event.getInsertion())
                    .build()
            ;
        }
    }
}