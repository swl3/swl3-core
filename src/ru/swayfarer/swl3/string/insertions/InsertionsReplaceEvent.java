package ru.swayfarer.swl3.string.insertions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.observable.event.AbstractEvent;

@Data @EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@SuperBuilder
public class InsertionsReplaceEvent extends AbstractEvent{
    
    @NonNull
    public String replaceString;
}
