package ru.swayfarer.swl3.string.insertions;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.string.StringValue;
import ru.swayfarer.swl3.string.converters.StringConverters;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Insertion {
    
    @NonNull
    public InsertionPosInString pos;
    
    @NonNull
    public StringConverters stringConverters;
    
    @NonNull
    public Map<String, StringValue> args;
    
    public ExtendedOptional<StringValue> getArg(@NonNull String... names)
    {
        for (var name : names)
        {
            var ret = args.get(name);
            
            if (ret != null)
            {
                return ExtendedOptional.of(ret);
            }
        }
        
        return ExtendedOptional.empty();
    }
    
    public <T> ExtendedOptional<T> getArg(@NonNull Class<T> type, @NonNull String... names)
    {
        var arg = getArg(names);
        if (arg != null && arg.isPresent())
        {
            return ExtendedOptional.of(arg.get().getValue(type));
        }
        
        return ExtendedOptional.empty();
    }
    
    public boolean hasValue()
    {
        return getValue() != null;
    }
    
    public StringValue getArgOrDefault(@NonNull String defaultValue, @NonNull String... names)
    {
        return getArg(names).orElse(new StringValue(stringConverters, () -> defaultValue).updateValue());
    }
    
    public ExtendedOptional<String> getArgString(@NonNull String... names)
    {
        var arg = getArg(names);
        return ExtendedOptional.of(arg.isPresent() ? arg.get().getStringValue() : null);
    }
    
    public <T> T getArgOrDefault(@NonNull Class<T> type, @NonNull String defaultValue, @NonNull String... names)
    {
        return getArgOrDefault(defaultValue, names).getValue(type);
    }

    public ExtendedOptional<String> getValueAsString()
    {
        return getArgString("value");
    }
    
    public StringValue getValue()
    {
        return args.get("value");
    }
}
