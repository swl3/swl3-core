package ru.swayfarer.swl3.string.insertions.replacer;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.string.dynamic.DynamicString;
import ru.swayfarer.swl3.string.insertions.IInsertionReplacer;
import ru.swayfarer.swl3.string.insertions.InsertionEvent;
import ru.swayfarer.swl3.string.insertions.InsertionsParser;
import ru.swayfarer.swl3.string.insertions.InsertionsReplaceEvent;

@Getter
@Setter
@Accessors(chain = true)
public class NoDeepInsertionsReplacer implements IInsertionReplacer {

    public ExtendedMap<String, ReplaceStragegy> cachedStrategies = new ExtendedMap<>().synchronize();
    
    public InsertionsParser insertionsParser = new InsertionsParser();
    
    @NonNull
    public FunctionsChain1<InsertionEvent, String> insertionFuns = new FunctionsChain1<>();
    
    @Override
    public String replace(@NonNull InsertionsReplaceEvent event)
    {
        var strategy = getStrategy(event);
        
        if (strategy == null)
        {
            return event.getReplaceString();
        }
        
        return strategy.construct(event);
    }
    
    public ReplaceStragegy getStrategy(@NonNull InsertionsReplaceEvent event)
    {
        var replaceString = event.getReplaceString().replace("\r", "");
        var ret = cachedStrategies.get(replaceString);
        
        if (ret == null)
        {
            ret = createStrategy(event);
            cachedStrategies.put(replaceString, ret);
        }
        
        return ret;
    }
    
    public ReplaceStragegy createStrategy(@NonNull InsertionsReplaceEvent event)
    {
        var replaceString = event.getReplaceString();
        var insertions = insertionsParser.parse(replaceString).getInsertionsWithMaxPriority(new ExtendedList<>());
        var ret = new ReplaceStragegy();
        var insertionsList = insertions.getInsertions();

        var prevInsertionEnd = 0;
        
        for (var insertion : insertionsList)
        {
            var pos = insertion.getPos();
            
            var startChar = pos.getStartChar();
            
            if (startChar > prevInsertionEnd)
            {
                var stringPrevToInsertion = replaceString.substring(prevInsertionEnd, startChar);
                ret.entries.add((insEvent) -> stringPrevToInsertion);
            }
            
            ret.entries.add((insertionsEvent) -> {
               var insertionEvent = InsertionEvent.builder()
                       .customAttributes(insertionsEvent.getCustomAttributes())
                       .insertion(insertion)
                       .build()
               ;
               
               return insertionFuns.apply(insertionEvent);
            });
            
            prevInsertionEnd = pos.getEndChar();
        }
        
        if (prevInsertionEnd < replaceString.length())
        {
            var stringNextToInsertion = replaceString.substring(prevInsertionEnd);
            ret.entries.add((insEvent) -> stringNextToInsertion);
        }
        
        return ret;
    }
    
    public static class ReplaceStragegy {
        public ExtendedList<IFunction1<InsertionsReplaceEvent, String>> entries = new ExtendedList<>();
        
        public String construct(@NonNull InsertionsReplaceEvent event)
        {
            var str = new DynamicString();
            
            for (var entry : entries)
            {
                var s = entry.apply(event);

                if (s != null && !s.isEmpty())
                    str.append(s);
            }
            
            return str.toString();
        }
    }
}
