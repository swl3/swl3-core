package ru.swayfarer.swl3.string.insertions.replacer;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.observable.event.EventHandlingEvent;
import ru.swayfarer.swl3.string.insertions.EventInserionRule;
import ru.swayfarer.swl3.string.insertions.InsertionEvent;

public class StandartInsertionRules {
    
    public static <T extends InsertionEvent> IFunction1<EventHandlingEvent<T>, Boolean> filterByKey(@NonNull String... keys)
    {
        var keysList = CollectionsSWL.list(keys);
        
        return (handleEvent) -> {
            var e = handleEvent.getEventObject();
            var insertion = e.getInsertion();
            var value = insertion.getValueAsString().orNull();
            
            if (keysList.contains(value))
            {
                return true;
            }
            
            return false;
        };
    }
    
    public static EventInserionRule replaceByKey(@NonNull IFunction1NoR<InsertionEvent.WithResult> fun, @NonNull String... keys)
    {
        var keysList = CollectionsSWL.list(keys);
        var ret = new EventInserionRule();
        
        ret.eventInsert.subscribe().by((insEvent) -> {
            var insertion = insEvent.getInsertion();
            var value = insertion.getValueAsString().orNull();
            
            if (keysList.contains(value))
            {
                fun.apply(insEvent);
            }
        });
        
        return ret;
    }
}
