package ru.swayfarer.swl3.string.insertions.replacer;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.string.dynamic.DynamicString;
import ru.swayfarer.swl3.string.insertions.IInsertionReplacer;
import ru.swayfarer.swl3.string.insertions.Insertion;
import ru.swayfarer.swl3.string.insertions.InsertionEvent;
import ru.swayfarer.swl3.string.insertions.Insertions;
import ru.swayfarer.swl3.string.insertions.InsertionsParser;
import ru.swayfarer.swl3.string.insertions.InsertionsReplaceEvent;
import ru.swayfarer.swl3.string.insertions.Insertions.InsertionsWithPriority;

@Data
@Accessors(chain = true)
public class InsertionsReplacer implements IInsertionReplacer{
    
    @NonNull
    public InsertionsParser insertionsParser = new InsertionsParser();
    
    @NonNull
    public FunctionsChain1<InsertionEvent, String> insertionFuns = new FunctionsChain1<>();
    
    @NonNull
    public IFunction2<String, ExtendedList<String>, InsertionsWithPriority> insertionsParseFun = (s, skiped) -> getInsertionsParser().parse(s).getInsertionsWithMaxPriority(skiped);
    
    public String replace(@NonNull InsertionsReplaceEvent event)
    {
        var str = event.getReplaceString();
        var buffer = new DynamicString(str);
        var skipedInsertions = new ExtendedList<String>();
        var insertionsWithMaxPriority = parseInsertions(str, skipedInsertions);
        var insertionsToReplace = insertionsWithMaxPriority.getInsertions();
        
        while (!CollectionsSWL.isNullOrEmpty(insertionsToReplace))
        {
            for (var insertion : insertionsToReplace)
            {
                var insertionAsString = insertion.getPos().getInsertionStr();
                
                var insertionEvent = InsertionEvent.builder()
                        .customAttributes(event.getCustomAttributes())
                        .insertion(insertion)
                        .build()
                ;
                
                var replaceInsertionValue = getInsertionValue(insertionEvent);
                
                if (replaceInsertionValue != null)
                {
                    replaceInsertionValue = replaceInsertionValue.replace("\r", "");
                    buffer.replace(insertionAsString, replaceInsertionValue);
                }
                else
                {
                    skipedInsertions.add(insertionAsString);
                }
            }
            
            if (insertionsWithMaxPriority.getPriority() == 0)
            {
                break;
            }
            
            insertionsWithMaxPriority = parseInsertions(buffer.toString(), skipedInsertions);
            insertionsToReplace = insertionsWithMaxPriority.getInsertions();
        }
        
        return buffer.toString();
    }
    
    public InsertionsWithPriority parseInsertions(@NonNull String str, @NonNull ExtendedList<String> skiped)
    {
        return insertionsParseFun.apply(str, skiped);
    }
    
    public String getInsertionValue(InsertionEvent insertion)
    {
        return insertionFuns.apply(insertion);
    }
}
