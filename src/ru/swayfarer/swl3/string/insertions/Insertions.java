package ru.swayfarer.swl3.string.insertions;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;

@Data
@Accessors(chain = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Insertions {
    
    @Builder.Default
    public ExtendedList<Insertion> insertions = new ExtendedList<>();
    
    public InsertionsWithPriority getInsertionsWithMaxPriority(ExtendedList<String> skipedInsertions)
    {
        var maxPriority = getMaxPriority(skipedInsertions);
        
        var insertions = this.insertions.exStream()
                .filter((o) -> !skipedInsertions.contains(o.getPos().getInsertionStr()))
                .filter((o) -> o.getPos().getPriority() >= maxPriority)
                .toExList()
        ;
        
        return InsertionsWithPriority.builder()
                .insertions(insertions)
                .priority(maxPriority)
                .build()
        ;
    }
    
    public int getMaxPriority(ExtendedList<String> skipedInsertions)
    {
        if (CollectionsSWL.isNullOrEmpty(insertions))
            return Integer.MIN_VALUE;
        
        var value = insertions.stream()
                .filter((o) -> !skipedInsertions.contains(o.getPos().getInsertionStr()))
                .max((o1, o2) -> o1.getPos().getPriority() - o2.getPos().getPriority())
        ;
        
        return value.isPresent() ? value.get().getPos().getPriority() : Integer.MIN_VALUE;
    }
    
    @Data
    @Builder
    public static class InsertionsWithPriority {
        public int priority;
        public ExtendedList<Insertion> insertions;
    }
    
    public void move(@NonNull Insertion insertion, @NonNull String newValue) 
    {
        var pos = insertion.getPos();
        var startChar = pos.getStartChar();
        var endChar = pos.getEndChar();
        
        var insertionAsStringLenght = insertion.getPos().getInsertionStr().length();
        var valueLenght = newValue.length();
        
        var insertionOffset = insertionAsStringLenght - valueLenght;

        for (var ins : insertions)
        {
            var insPos = ins.getPos();
            var insStartChar = insPos.getStartChar();
            var insEndChar = insPos.getEndChar();
            
            if (endChar < insStartChar)
            {
                continue;
            }
            
            if (startChar > insEndChar) 
            {
                continue;
            }
            
            if (insStartChar > startChar)
            {
                
            }
        }
    }
}
