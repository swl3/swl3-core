package ru.swayfarer.swl3.string.insertions;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InsertionPosInString {
    
    public int startChar;
    
    public int endChar;
    
    public String insertionStr;
    
    public int priority;
    
    public InsertionPosInString takeInserionStrFrom(@NonNull String str)
    {
        this.insertionStr = str.substring(startChar, endChar);
        return this;
    }
}
