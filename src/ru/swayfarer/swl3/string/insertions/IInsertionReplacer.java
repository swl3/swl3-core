package ru.swayfarer.swl3.string.insertions;

import lombok.NonNull;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;

public interface IInsertionReplacer extends IFunction1<InsertionsReplaceEvent, String> {
    public String replace(@NonNull InsertionsReplaceEvent name);
    
    @Override
    default String applyUnsafe(InsertionsReplaceEvent name) throws Throwable
    {
        return replace(name);
    }
    
    public FunctionsChain1<InsertionEvent, String> getInsertionFuns();
}
