package ru.swayfarer.swl3.string.insertions;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;

@Data
@Accessors(chain = true)
public class EventInserionRule implements IFunction1<InsertionEvent, String> {

    public IObservable<InsertionEvent.WithResult> eventInsert = Observables.createObservable();
    
    @Override
    public String applyUnsafe(InsertionEvent insertionEvent) throws Throwable
    {
        var event = InsertionEvent.WithResult.of(insertionEvent);
        
        eventInsert.next(event);
        
        return event.getResult();
    }
}
