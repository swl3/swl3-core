package ru.swayfarer.swl3.string.insertions;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.string.insertions.Insertions.InsertionsWithPriority;

@Data
@Accessors(chain = true)
public class CachedInsertionsParserFun implements IFunction2<String, ExtendedList<String>, InsertionsWithPriority> {

    @NonNull
    public InsertionsParser insertionsParser;
    
    @NonNull
    public ExtendedMap<String, InsertionsWithPriority> cache = new ExtendedMap<>().synchronize();
    
    @Override
    public InsertionsWithPriority applyUnsafe(String str, ExtendedList<String> skiped) throws Throwable
    {
        return cache.getOrCreate(str, () -> insertionsParser.parse(str).getInsertionsWithMaxPriority(skiped));
    }
}
