package ru.swayfarer.swl3.string;

import java.util.concurrent.atomic.AtomicBoolean;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

public class CharFilters {
    
    public static ExtendedList<Character> numChars = CollectionsSWL.list(
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0'
    );
    
    public static IFunction1<Character, Boolean> nums()
    {
        return numChars::contains;
    }
    
    public static IFunction1<Character, Boolean> its(@NonNull Character... chars)
    {
        return (e) -> {
            return ExtendedStream.of(chars)
                .anyMatches((o) -> o.equals(e))
            ;
        };
    }
    
    public static IFunction1<Character, Boolean> not(@NonNull IFunction1<Character, Boolean> fun)
    {
        return (e) -> {
            var result = fun.apply(e);
            
            if (result == null)
                return null;
            
            return !result;
        };
    }
    
    public static IFunction1<Character, Boolean> oneShot(@NonNull IFunction1<Character, Boolean> fun)
    {
        var isResultGiven = new AtomicBoolean();
        
        return (e) -> {
            if (!isResultGiven.get())
            {
                var result = fun.apply(e);
                
                if (Boolean.TRUE.equals(result))
                {
                    synchronized (isResultGiven)
                    {
                        if (!isResultGiven.get())
                        {
                            isResultGiven.set(true);
                            return true;
                        }
                    }
                }
            }
            
            return false;
        };
    }
}
