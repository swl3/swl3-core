package ru.swayfarer.swl3.string.converters;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;

@Accessors(chain = true)
@SuppressWarnings("unchecked")
public abstract class AbstractStringConverter implements IFunction2<String, Class<?>, Object> {

	public List<Class<?>> acceptableClasses = new ArrayList<>();
	
	@Setter @Getter
	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	
	public abstract Object convert(String str, Class<?> classOfObj) throws Throwable;
	
	@Override
	public Object applyUnsafe(String str, Class<?> classOfObj)
	{
		if (acceptableClasses.contains(classOfObj))
			return exceptionsHandler.safeReturn(
				() -> {
					try
					{
						return convert(str, classOfObj);
					}
					catch (Throwable e)
					{
						var exception = new StringConvertingException("", e);
						exception.setStackTrace(new StackTraceElement[] {exception.getStackTrace()[0]});
						throw exception;
					}
				}, 
				null, 
				"Error while converting string", str, "to type", classOfObj
			);
		
		return null;
	}
	
	public <T extends AbstractStringConverter> T acceptable(Class<?>... types)
	{
		for (var type : types)
		{
			acceptableClasses.add(type);
		}
		
		return (T) this;
	}
	
	public AbstractStringConverter appendExceptionsHandler(ExceptionsHandler exceptionsHandler)
	{
		this.exceptionsHandler.andAfter(exceptionsHandler);
		return this;
	}
}
