package ru.swayfarer.swl3.string.converters;

import java.io.File;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.io.link.ResourceLink;

@Getter
@Setter
@Accessors(chain = true)
@SuppressWarnings("unchecked")
public class StringConverters {

	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	public ExtendedList<IFunction2<String, Class<?>, Object>> registeredConverters = new ExtendedList<>();
	
	public StringConverters converter(@NonNull IFunction2<String, Class<?>, Object> converterFun)
	{
		registeredConverters.add(converterFun);
		return this;
	}
	
	public <T> T convert(Class<T> classOfT, String str)
	{
		for (var fun : registeredConverters)
		{
			var ret = fun.apply(str, classOfT);
			
			if (ret != null)
				return (T) ret;
		}
		
		return (T) null;
	}
	
	public StringConverters valueOfConverter(Class<?> classOfInstance, Class<?>... acceptableTypes)
	{
		return converter(ValueOfConverter.of(classOfInstance, acceptableTypes)
				.appendExceptionsHandler(exceptionsHandler)
			);
	}
	
	public StringConverters valueOfConverter(String methodName, Class<?> classOfInstance, Class<?>... acceptableTypes)
	{
		return converter(ValueOfConverter.of(classOfInstance, methodName, acceptableTypes)
					.appendExceptionsHandler(exceptionsHandler)
				);
	}
	
	public StringConverters defaultTypes()
    {
	    var rlutils = RLUtils.contextUtils();
	    return defaultTypes(rlutils);
    }
	
	public StringConverters defaultTypes(@NonNull RLUtils rlutils)
	{
		return this
				
		        .converter((str, cl) -> String.class == cl ? String.valueOf(str) : null)
		        .converter((str, cl) -> cl == ResourceLink.class ? rlutils.createLink(str) : null)
		        
				.valueOfConverter(Byte.class, byte.class, Byte.class)
				.valueOfConverter(Short.class, short.class, Short.class)
				.valueOfConverter(Integer.class, int.class, Integer.class)
				.valueOfConverter(Long.class, long.class, Long.class)
				
				.valueOfConverter(Float.class, float.class, Float.class)
				.valueOfConverter(Double.class, double.class, Double.class)

				.valueOfConverter(Boolean.class, boolean.class, Boolean.class)
				
				.valueOfConverter("of", FileSWL.class, File.class, FileSWL.class)
				.valueOfConverter("fromString", UUID.class, UUID.class)

                .valueOfConverter("ofPattern", DateTimeFormatter.class, DateTimeFormatter.class)
		;
	}

	public static StringConverters INSTANCE = new StringConverters().defaultTypes();

	public static StringConverters getInstance()
	{
		return INSTANCE;
	}
}
