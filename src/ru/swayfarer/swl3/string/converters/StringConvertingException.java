package ru.swayfarer.swl3.string.converters;

@SuppressWarnings("serial")
public class StringConvertingException extends RuntimeException {

	public StringConvertingException()
	{
		super();
	}

	public StringConvertingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public StringConvertingException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public StringConvertingException(String message)
	{
		super(message);
	}

	public StringConvertingException(Throwable cause)
	{
		super(cause);
	}
}
