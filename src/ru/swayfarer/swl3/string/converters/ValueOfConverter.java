package ru.swayfarer.swl3.string.converters;

import java.lang.reflect.Method;

import lombok.NonNull;
import lombok.SneakyThrows;

public class ValueOfConverter extends AbstractStringConverter {

	public Class<?> numberClass;
	public Method method;
	
	public ValueOfConverter(@NonNull Class<?> numberClass)
	{
		this(numberClass, "valueOf");
	}
	
	@SneakyThrows
	public ValueOfConverter(@NonNull Class<?> numberClass, @NonNull String methodName)
	{
		method = numberClass.getDeclaredMethod(methodName, String.class);
		this.numberClass = numberClass;
	}
	
	@Override
	public Object convert(String str, Class<?> classOfObj) throws Throwable
	{
		return method.invoke(null, str);
	}
	
	public static ValueOfConverter of(Class<?> numberClass, Class<?>... acceptableTypes)
	{
		return new ValueOfConverter(numberClass).acceptable(acceptableTypes);
	}
	
	public static ValueOfConverter of(Class<?> numberClass, String methodName, Class<?>... acceptableTypes)
	{
		return new ValueOfConverter(numberClass, methodName).acceptable(acceptableTypes);
	}
}
