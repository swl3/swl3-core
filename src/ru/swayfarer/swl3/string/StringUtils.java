package ru.swayfarer.swl3.string;

import java.lang.reflect.Array;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Alias;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

public class StringUtils {

    public static DecimalFormat decimalFormat = new DecimalFormat("#########################################.#########################################");
    
	/** Пробельный символ */
	public static final String SPACE = " ";
	
	/** Символ CR */
	public static final String CR  = ""+(char) 0x0D;
	
	/** Символ RF*/
	public static final String LF  = ""+(char) 0x0A; 
	
	/** Символ CRLF*/
	public static final String CRLF  = CR+LF;
	
	/** Символ BOM*/
	public static final String BOM = "\ufeff";
	
	/** Символ табуляции */
	public static final String TAB = "	";
	
	public static int indexOf(@NonNull String str, @NonNull IFunction1<Character, Boolean> filter)
    {
	    return indexOf(str, filter, 0);
    }
	
	public static int indexOfNonWhitespace(@NonNull String str)
	{
	    return indexOfNonWhitespace(str, 0);
	}
	
	public static int indexOfNonWhitespace(@NonNull String str, int startIndex)
	{
	    return indexOf(str, (ch) -> !Character.isWhitespace(ch), startIndex);
	}
	
	public static String concatLines(@NonNull Object... lines) 
	{
		return concatWith(LF, lines);
	}

	public static String[] splitByWhitespaces(Object str)
	{
		if (str == null)
			return null;

		return str.toString().split("\\s+");
	}

	public static String detectLineSplitter(@NonNull String str)
	{
	    if (str.contains("\r\n"))
        {
            return "\r\n";
        }
        
        else if (str.contains("\n"))
        {
            return "\n";
        }
        
        else if (str.contains("\r"))
        {
            return "\r";
        }
	    
	    return null;
	}
	
	public static int indexOf(@NonNull String str, @NonNull IFunction1<Character, Boolean> filter, int startIndex)
	{
	    for (int i1 = startIndex; i1 < str.length(); i1 ++)
	    {
	        var ch = str.charAt(i1);
	        
	        if (Boolean.TRUE.equals(filter.apply(ch)))
	        {
	            return i1;
	        }
	    }
	    
	    return -1;
	}
	
	public static boolean isBlank(Object obj)
	{
		if (obj == null)
			return true;

		if (isEmpty(obj))
			return true;

		var str = obj instanceof String ? (String) obj : String.valueOf(obj);

		for (var i1 = 0; i1 < str.length(); i1 ++)
		{
			if (!Character.isWhitespace(str.charAt(i1)))
				return false;
		}

		return true;
	}
	
	public static boolean isEmpty(Object obj)
	{
		return obj == null || String.valueOf(obj).isEmpty();
	}
	
	public static ExtendedList<String> getWords(@NonNull String str)
    {
        var tokenizer = new StringTokenizer(str);
        
        return CollectionsSWL.list(tokenizer)
                .exStream()
                .nonNull()
                .map(Object::toString)
                .toExList()
        ;
    }

	public static String replaceAll(String str, String regex, String replacement)
	{
		return replaceAll(str, Pattern.compile(regex), replacement);
	}

	public static String replace(String str, String target, String replacement)
	{
		return replaceAll(str, Pattern.compile(target, Pattern.LITERAL), replacement);
	}

    public static String replaceAll(String str, Pattern pattern, String replacement)
	{
		var matcher = pattern.matcher(str);
		var buffer = new DynamicString();
		var appendIndex = 0;
		var isEnd = false;

		while (matcher.find())
		{
			var start = matcher.start();
			var end = matcher.end();

			if (end == str.length())
				isEnd = true;

			if (appendIndex < start && appendIndex < str.length())
			{
				for (var i1 = appendIndex; i1 < start; i1 ++)
					buffer.append(str.charAt(i1));
			}

			buffer.append(replacement);

			appendIndex = end;
		}

		if (!isEnd)
			if (appendIndex < str.length())
			{
				for (var i1 = appendIndex; i1 < str.length(); i1 ++)
					buffer.append(str.charAt(i1));
			}

		return buffer.toString();
	}

	/** Соединить все объекты в строку */
	public static String concat(Object... text)
	{
		return concat(null, text);
	}

	@SneakyThrows
	public static String decodeURLSymbols(String str)
	{
		return URLDecoder.decode(str, "UTF-8");
	}
	
	/**
     * Начинается ли строка с одного из вариантов (игнорируя пробелы перед началом)?
     * @param str Проверяемая строка
     * @param starts Варианты начала строки
     * @return True, если начинается
     */
    public static boolean isStringStartsWith(@NonNull String str, @NonNull Iterable<? extends Object> starts)
    {
        for (var start : starts)
        {
            if (str.startsWith(String.valueOf(start)))
                return true;
        }
        
        return false;
    }
	
	/**
     * Начинается ли строка с одного из вариантов (игнорируя пробелы перед началом)?
     * @param str Проверяемая строка
     * @param starts Варианты начала строки
     * @return True, если начинается
     */
    public static boolean isStringStartsWith(@NonNull String str, @NonNull Object... starts)
    {
        return isStringStartsWith(str, CollectionsSWL.iterable(starts));
    }

    /**
     * Заканчивается ли строка с одного из вариантов (игнорируя пробелы перед началом)?
     * @param str Проверяемая строка
     * @param ends Варианты окончания строки
     * @return True, если заканчивается
     */
    public static boolean isStringEndsWith(@NonNull String str, @NonNull Iterable<Object> ends)
    {
        return Stream.of(ends).anyMatch((s) -> str.endsWith(s+""));
    }

    /**
     * Заканчивается ли строка с одного из вариантов (игнорируя пробелы перед началом)?
     * @param str Проверяемая строка
     * @param ends Варианты окончания строки
     * @return True, если заканчивается
     */
    public static boolean isStringEndsWith(@NonNull String str, @NonNull Object... ends)
    {
        return isStringEndsWith(str, CollectionsSWL.iterable(ends));
    }
	
	public static String subString(int fromStart, int fromEnd, String str)
	{
		if (fromStart < 0)
			fromStart = 0;
		
		if (fromEnd < 0)
			fromEnd *= -1;
		
		return str.substring(fromStart, str.length() - fromEnd);
	}
	
	public static String tryToString(Object obj)
	{
	    if (obj == null)
	        return "null";
	    
	    String ret = null;
	    
	    try
	    {
	        ret = obj.toString();
	    }
	    catch (Throwable e)
	    {
	        e.printStackTrace();
	    }
	    
	    return orEmpty(ret);
	}
	
	/**
	 * Перевести первый символ в заглавный регистр
	 * @param s Строка, первый символ которой переводим в заглавный регистр
	 * @return Полученная строка
	 */
	public static String firstCharToUpperCase(String s)
	{
		return s == null ? null : s.isEmpty() ? "" : (s.charAt(0)+"").toUpperCase()+(s.length() > 1 ? s.substring(1) : "");
	}
	
	/**
	 * Перевести первый символ в прописной регистр
	 * @param s Строка, первый символ которой переводим в прописной регистр
	 * @return Полученная строка
	 */
	public static String firstCharToLowerCase(String s)
	{
		return s == null ? null : s.isEmpty() ? "" : (s.charAt(0)+"").toLowerCase()+(s.length() > 1 ? s.substring(1) : "");
	}
	
	/**
	 * Составить Camel Case строку из слов
	 * @param lowerCaseToAll Сделать ли заглавные буквы только в начале слов?
	 * @param words Слова
	 * @return Полученная строка
	 */
	public static String toCamelCase(boolean lowerCaseToAll, Object... words) 
	{
		List<String> strings = new ArrayList<>();
		int lenght = 0;
		
		for (Object obj : words)
		{
			String word = ""+obj;
			strings.add(word);
			lenght += word.length();
		}
		
		StringBuilder sb = new StringBuilder(lenght);
		
		boolean isFirst = true;
		
		for (String word : strings)
		{
			if (lowerCaseToAll)
				word = word.toLowerCase();
			
			sb.append(isFirst ? word : firstCharToUpperCase(word));
			isFirst = false;
		}
		
		return sb.toString();
	}
	
	/**
	 * Сколько раз встречается подстрока в строке ?
	 * @param str Строка, в которой ищем регулярки
	 * @param sub Подстрока
	 * @return Кол-во соотвествий
	 */
	public static int countMatches(String str, String sub)
	{
		if (isEmpty(str) || isEmpty(sub))
		{
			return 0;
		}
		int count = 0;
		int idx = 0;
		while ((idx = str.indexOf(sub, idx)) >= 0)
		{
			count++;
			idx += sub.length();
		}
		return count;
	}
	
	public static String limit(@NonNull Object strObj, int maxLen)
	{
	    var str = strObj.toString();
	    
	    if (maxLen <= 0)
	        return str;
	    
	    if (maxLen < str.length())
	    {
	        return str.substring(0, maxLen);
	    }
	    
	    return str;
	}
	
	/** Соединить объекты, разместив разделитель между ними */
	public static String concat(String splitter, Object... text)
	{
		return concatWith(splitter, 0, text.length, text);
	}
	
	/**
	 * Является ли строка Float'ом?
	 * @param s Проверяемая строка
	 * @return True, если является
	 */
	public static boolean isFloat(Object s)
	{
		try
		{
			Float.valueOf(s+"");
			return true;
		}
		catch (Throwable e) {}
		
		return false;
	}
	
	/**
	 * Является ли строка Double'ом?
	 * @param s Проверяемая строка
	 * @return True, если является
	 */
	public static boolean isDouble(Object s)
	{
		try
		{
			Double.valueOf(s+"");
			return true;
		}
		catch (Throwable e) {}
		
		return false;
	}
	
	/**
	 * Является ли строка {@link Integer}'ом?
	 * @param s Проверяемая строка
	 * @return True, если является
	 */
	public static boolean isInteger(String s)
	{
		try
		{
			Integer.valueOf(s);
			
			return true;
		}
		catch (Throwable e)
		{
			return false;
		}
	}
    
	public static String toNumberString(Object objToString)
    {
	    return isBlank(objToString) || !isDouble(objToString) ? null : toNumberString(Double.valueOf(objToString.toString()));
    }
	
    public static String toNumberString(Double d)
    {
        return d == null ? null : toNumberString(d.doubleValue());
    }
    
	public static String toNumberString(double d)
	{
	    return decimalFormat.format(d);
	}
	
	/**
	 * Является ли строка {@link Long}'ом?
	 * @param s Проверяемая строка
	 * @return True, если является
	 */
	public static boolean isLong(String s)
	{
		try
		{
			Long.valueOf(s);
			
			return true;
		}
		catch (Throwable e)
		{
			return false;
		}
	}
	
	/**
	 * Является ли строка {@link Boolean}'ом?
	 * @param s Проверяемая строка
	 * @return True, если является
	 */
	public static boolean isBoolean(String s)
	{
		try
		{
			Boolean.valueOf(s);
			
			return true;
		}
		catch (Throwable e)
		{
			return false;
		}
	}
	
	/** Получить все совпадения по регулярке в строке */
	public static List<String> findAllMatches(@NonNull String regex, @NonNull Object text)
	{
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		
		String s = String.valueOf(text);
		Matcher matcher = pattern.matcher(s);
		
		List<String> ret = new ArrayList<>();
		
		while (matcher.find())
		{
			ret.add(s.substring(matcher.start(), matcher.end()));
		}
		
		return ret;
	}
    
    public static boolean isMatchesByExpression(String expression, Object text)
    {
        if (isEmpty(expression))
            return true;
        
        if (isEmpty(text))
            text = new Object[] {""};
        
        if (expression.startsWith("regex:"))
        {
            return isMatchesByRegex(expression.substring(6), text);
        }
        else if (expression.startsWith("mask:"))
        {
            return isMatchesByMask(expression.substring(5), text);
        }
        
        return StringUtils.concat(text).startsWith(expression);
    }
	
	public static boolean isMatchesByMask(String mask, Object str)
	{
		return isMatchesByRegex(getRegexFromMask(mask), str);
	}
	
	public static boolean isMatchesByRegex(String regex, Object str)
	{
		if (regex == null)
			return false;
		
		return String.valueOf(str).matches(regex);
	}
	
	public static String getRegexFromMask(String str)
	{
		// Присваиваем в качестве результата маску
		String res = str;
		// Этап 1: экранируем служебные символы
		String[] arr = new String[] { "\\", "#", "|", "(", ")", "[", "]", "{", "}", "^", "$", "+", "." };

		int len = arr.length;

		for (int i = 0; i < len; i++)
		{
			res = res.replace(arr[i], "\\" + arr[i]);
		}

		// Этап 2: заменяем служебные символы маски
		// на соответствующие аналоги в регулярных выражениях
		res = res.replace("*", ".*");
		res = res.replace("?", ".");

		// Этап 3: добавляем символы начала и конца строки
		return "^" + res + "$";
	}
	
	public static RegexBuilder regex()
	{
		return new RegexBuilder();
	}
	
	/** Соединить объекты, разместив разделитель между ними */
	public static String concatWithSpaces(Object... text)
	{
		return concatWith(" ", 0, text == null ? 0 : text.length, text);
	}

	/** Соединить объекты, разместив разделитель между ними */
	public static String concatWithSpaces(Iterable<?> text)
	{
		return concatWith(" ", -1, -1, text);
	}
	
	/** Соединить объекты, разместив разделитель между ними */
	public static String concatWith(String splitter, @NonNull Object... text)
	{
		return concatWith(splitter, -1, -1, text);
	}

	/** Соединить объекты, разместив разделитель между ними */
	public static String concatWith(String splitter, @NonNull Iterable<?> text)
	{
		return concatWith(splitter, -1, -1, text);
	}

	/** Соединить объекты, разместив разделитель между ними */
	public static String concatWith(String splitter, int start, int end, @NonNull Object... text)
	{
		return concatWith(splitter, start, end, CollectionsSWL.iterable(text));
	}

	/** Соединить объекты, разместив разделитель между ними */
	public static String concatWith(String splitter, int start, int end, @NonNull Iterable<?> text)
	{
		if (text == null)
			return null;

		var buffer = new StringBuilder();

		var firstSplitterSkiped = false;
		var somePartWasNotNull = false;
		var countConcated = 0;
		var countToSkip = Math.max(0, start);
		var countToConcat = end > start ? Math.max(0, end - start) : -1;
		var someIterated = false;

		for (var part : text)
		{
			someIterated = true;

			var partAsStr = part instanceof String ? (String) part : String.valueOf(part);

			if (countToSkip > 0)
			{
				countToSkip --;
				continue;
			}

			if (countToConcat < 0 || countConcated < countToConcat)
			{
				if (part != null)
				{
					if (part.getClass().isArray())
					{
						partAsStr = getStringFromArray(part);
					}

					somePartWasNotNull = true;
				}

				if (splitter != null)
				{
					if (firstSplitterSkiped)
						buffer.append(splitter);
					else
						firstSplitterSkiped = true;
				}

				buffer.append(partAsStr);

				countConcated ++;
			}
		}

		return somePartWasNotNull ? buffer.toString() : someIterated ? null : "";
	}
	
	public static String strip(@NonNull String str)
	{
	    str = str.replaceAll("^\\s+", "");
	    str = str.replaceAll("\\s+$", "");
	    
	    return str;
	}

	public static String createSpacesSeq(int lenght)
	{
		var sb = new StringBuilder();
		for (int i1 = 0; i1 < lenght; i1 ++)
			sb.append(" ");
		return sb.toString();
	}
	
	/**
	 * Сколько раз встречается текст, соответсвтуюший регулярке ?
	 * @param str Строка, в которой ищем регулярки
	 * @param regex Регулярка
	 * @return Кол-во соотвествий
	 */
	public static int countMatchesRegex(String str, String regex)
	{
		if (isEmpty(str) || isEmpty(regex))
		{
			return 0;
		}
		
		int count = 0;
		int idx = 0;
		
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		
		while (matcher.find(idx))
		{
			idx += matcher.end() - matcher.start();
			count ++;
		}
		
		return count;
	}
	
	@Alias("orIfEmpty")
	public static String orDefault(Object objToString, Object defaultString)
    {
	    return orIfEmpty(objToString, defaultString);
    }
	
	public static String orIfEmpty(Object objToString, Object defaultString)
	{
	    return isEmpty(objToString) ? String.valueOf(defaultString) : objToString.toString();
	}
	
	public static String orEmpty(Object objToString)
	{
	    return objToString == null ? "" : objToString.toString();
	}



	public static String getStringFromArray(Object arrayObj)
	{
		return getStringFromArray(arrayObj, new ExtendedList<>());
	}

	private static String getStringFromArray(Object arrayObj, ExtendedList<Object> alreadyVisited)
	{
		if (arrayObj == null)
			return null;

		var size = Array.getLength(arrayObj);
		var buf = new StringBuilder();
		buf.append("[ ");

		var appendSplitter = false;

		for (var i1 = 0; i1 < size; i1 ++)
		{
			var text = Array.get(arrayObj, i1);

			if (text != null && text.getClass().isArray())
			{
				var ftext = text;
				if (!alreadyVisited.exStream().contains((e) -> ftext == e))
				{
					text = getStringFromArray(text, alreadyVisited);
					alreadyVisited.add(text);
				}
			}

			if (appendSplitter)
				buf.append(", ");

			buf.append(text);

			appendSplitter = true;
		}

		buf.append(" ]");

		return buf.toString();
	}
}
