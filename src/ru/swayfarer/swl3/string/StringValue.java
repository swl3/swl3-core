package ru.swayfarer.swl3.string;

import java.util.concurrent.atomic.AtomicBoolean;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.string.converters.StringConverters;

@Getter
@Setter
@Accessors(chain = true)
public class StringValue {
	
	public AtomicBoolean isAutoUpdate = new AtomicBoolean();
	
	@NonNull
	public IFunction0<String> valueFun;
	
	public String cachedValue;

	public AtomicBoolean isAnyValueGenerated = new AtomicBoolean();
	
	@NonNull
	public StringConverters stringConverters;
	
	public StringValue(@NonNull StringConverters valueConverter, @NonNull IFunction0<String> valueFun)
	{
		this.valueFun = valueFun;
		this.stringConverters = valueConverter;
	}
	
	public StringValue updateValue()
	{
		cachedValue = valueFun.apply();
		return this;
	}
	
	public <T> T getValue(Class<T> typeOfValue)
	{
		return stringConverters.convert(typeOfValue, getStringValue());
	}
	
	public String getStringValue()
	{
		if (!isAnyValueGenerated.get())
		{
			isAnyValueGenerated.set(true);
			updateValue();
		}
		else if (isAutoUpdate())
			updateValue();
		
		return cachedValue;
	}
	
	public boolean isAutoUpdate()
	{
		return isAutoUpdate.get();
	}
	
	public StringValue appendExceptionsHandler(ExceptionsHandler exceptionsHandler)
	{
		stringConverters.getExceptionsHandler().andAfter(exceptionsHandler);
		return this;
	}
	
	public StringValue setExceptionsHandler(ExceptionsHandler exceptionsHandler)
	{
		stringConverters.setExceptionsHandler(exceptionsHandler);
		return this;
	}
}
