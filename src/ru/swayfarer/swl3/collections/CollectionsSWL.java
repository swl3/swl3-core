package ru.swayfarer.swl3.collections;

import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.collections.iterator.ArrayIterable;
import ru.swayfarer.swl3.collections.iterator.EnumerationIterator;
import ru.swayfarer.swl3.collections.iterator.MappedIterator;
import ru.swayfarer.swl3.collections.iterator.ReflectionArrayIterator;
import ru.swayfarer.swl3.collections.iterator.ReflectionArrayIterator.ReflectionArrayIterable;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

@SuppressWarnings("unchecked")
public class CollectionsSWL {

	public static <T> ExtendedList<T> list()
	{
		return new ExtendedList<>();
	}

	public static <T> ExtendedList<T> list(Enumeration<T> enumeration)
	{
		if (enumeration == null)
			return null;

		ExtendedList<T> extendedList = new ExtendedList<>();
		
		while (enumeration.hasMoreElements())
			extendedList.add(enumeration.nextElement());
		
		return extendedList;
	}

	public static <T> Iterable<T> reflectionIterable(Object arrayObj)
	{
		if (arrayObj == null)
			return null;

		ExceptionsUtils.IfNot(
				arrayObj.getClass().isArray(),
				IllegalArgumentException.class,
				"Object is not array:", arrayObj
		);

		return new ReflectionArrayIterable<>(arrayObj);
	}
	
	public static <T> Iterable<T> iterable(@NonNull Enumeration<T> e)
	{
	    return () -> new EnumerationIterator<>(e);
	}
    
    public static <T> ExtendedList<T> list(Iterable<T> elements)
    {
    	if (elements == null)
    		return null;

    	return new ExtendedList<T>()
                .setAll(elements)
        ;
    }
	
	public static <T> ExtendedList<T> list(T... elements)
	{
		if (elements == null)
			return null;

		return new ExtendedList<T>()
				.setAll(elements)
		;
	}
	
	/** Создать {@link ExtendedMap} */
    public static <K, V> ExtendedMap<K, V> exMap(Object... content)
    {
        return new ExtendedMap<>(map(content));
    }
	
	/** Создать {@link HashMap} */
	public static <K, V> Map<K, V> map(Object... content)
	{
		if (!CollectionsSWL.isNullOrEmpty(content))
		{
			ExceptionsUtils.If(content.length % 2 != 0, IllegalArgumentException.class, "Content lenght % 2 must be a zero! Accepted length: ", content.length, "content:", Arrays.toString(content));
			
			Map<K, V> map = new HashMap<>();
			
			Object key = null;
			boolean isKey = true;
			
			for (Object obj : content)
			{
				if (isKey)
					key = obj;
				else
					map.put((K) key, (V) obj);
				
				isKey = !isKey;
			}
			
			return map;
		}
		
		return new HashMap<>();
	}
	/** Коллекция пуста или равна null*/
	public static boolean isNullOrEmpty(Collection<?> collection)
	{
		return collection == null || collection.isEmpty();
	}
	
	/** Карта пуста или равна null*/
	public static boolean isNullOrEmpty(Map<?, ?> map)
	{
		return map == null || map.isEmpty();
	}
	
	/** Массив пуст или равен null */
	public static boolean isNullOrEmpty(Object[] arr)
	{
		return arr == null || arr.length == 0;
	}
	
	/** Массив пуст или равен null */
	public static boolean isNullOrEmpty(byte[] arr)
	{
		return arr == null || arr.length == 0;
	}
	
	/**
	 * Получить итерируемую обертку над массивом. <br>
	 * Обертка работает с оригинальным массивом <br> 
	 * @param elements Массив
	 * @return {@link ArrayIterable}
	 */
	public static <T> ArrayIterable<T> iterable(T... elements)
	{
		return new ArrayIterable<>(elements);
	}
	
	/**
	 * Начинается ли группа элементов с элементов, эквивалетных другой группе?
	 * @param array Группа, начало которой будет сравниваться
	 * @param fromStarts Группа, с началом которой будем сравнивать
	 * @return True, если начинается. 
	 */
	public static boolean isStartsWith(@NonNull Object[] array, @NonNull Object... fromStarts)
	{
		return isStartsWith(iterable(array), iterable(fromStarts));
	}
	
	/**
	 * Начинается ли группа элементов с элементов, эквивалетных другой группе?
	 * @param iterable Группа, начало которой будет сравниваться
	 * @param fromStarts Группа, с началом которой будем сравнивать
	 * @return True, если начинается. 
	 */
	public static boolean isStartsWith(@NonNull Iterable<?> iterable, @NonNull Iterable<?> fromStarts)
	{
		var $iterable = iterable.iterator();
		var $fromStarts = fromStarts.iterator();
		
		while ($fromStarts.hasNext())
		{
			if (!$iterable.hasNext())
				return false;
			
			var iterableNext = $iterable.next();
			var fromStartsNext = $fromStarts.next();
			
			if (iterableNext == null && fromStartsNext != null)
				return false;
			else if (!iterableNext.equals(fromStartsNext))
				return false;
		}
		
		return true;
	}

	public static <T> ExtendedStream<T> exStream(Iterable<T> iterable)
	{
		return new ExtendedStream<>(() -> oneShootStream(iterable.iterator()));
	}

	public static <T> ExtendedStream<T> exStream(Enumeration<T> enumeration)
	{
		return new ExtendedStream<>(() -> oneShootStream(enumeration));
	}

	public static <T> ExtendedStream<T> oneShootExStream(Iterator<T> iterator)
	{
		return new ExtendedStream<>(() -> oneShootStream(iterator));
	}

	public static <T> Stream<T> oneShootStream(Enumeration<T> enumeration)
	{
		if (enumeration == null)
			return Stream.empty();

		return oneShootStream(iterable(enumeration).iterator());
	}

	public static <T> Stream<T> oneShootStream(Iterator<T> iterator)
	{
		var spliterator = Spliterators.spliteratorUnknownSize(iterator, Spliterator.DISTINCT);

		return StreamSupport
				.stream(spliterator, false)
		;
	}

	public static <O, N> Iterator<N> mapIterator(Iterator<O> wrapped, IFunction1<O, N> mapperFun)
	{
		return new MappedIterator<>(mapperFun, wrapped);
	}

	/** Массив пуст или равен null */
	public static boolean isNullOrEmpty(int[] arr)
	{
		return arr == null || arr.length == 0;
	}
}
