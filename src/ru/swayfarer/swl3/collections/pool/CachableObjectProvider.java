package ru.swayfarer.swl3.collections.pool;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.markers.Internal;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class CachableObjectProvider<Element_Type>
{
    @Internal
    @NonNull
    public IFunction1NoR<Element_Type> cleanFun;

    @Internal
    @NonNull
    public IFunction0<Element_Type> newValueFun;

    @Internal
    public int maxSize;

    @Internal
    public Queue<Element_Type> elements = new ConcurrentLinkedQueue<>();

    public CachableObjectProvider(@NonNull IFunction0<Element_Type> newValueFun, int maxSize)
    {
        this( newValueFun, (e) -> {},maxSize);
    }

    public CachableObjectProvider(@NonNull IFunction0<Element_Type> newValueFun, @NonNull IFunction1NoR<Element_Type> cleanFun, int maxSize)
    {
        this.cleanFun = cleanFun;
        this.newValueFun = newValueFun;
        this.maxSize = maxSize;
    }

    public void release(Element_Type elem)
    {
        if (elem == null)
            return;

        if (elements.size() > maxSize)
            return;

        cleanFun.apply(elem);
        elements.add(elem);
    }

    public Element_Type request()
    {
        var result = elements.poll();

        if (result == null)
            return newValueFun.apply();

        return result;
    }
}
