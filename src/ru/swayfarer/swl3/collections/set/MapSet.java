package ru.swayfarer.swl3.collections.set;

import lombok.var;
import ru.swayfarer.swl3.markers.Internal;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;

public abstract class MapSet<Element_Type> extends AbstractSet<Element_Type>
{
    @Internal
    public Map<Element_Type, Object> content = createMap();

    @Override
    public boolean contains(Object o)
    {
        return content.containsKey(o);
    }

    @Override
    public Iterator<Element_Type> iterator()
    {
        return new MapSetIterator<>(content.keySet().iterator(), content);
    }

    @Override
    public int size()
    {
        return content.size();
    }

    @Override
    public boolean add(Element_Type element)
    {
        var contains = contains(element);
        content.put(element, false);
        return !contains;
    }

    @Override
    public boolean remove(Object o)
    {
        var contains = contains(o);
        content.remove(o);
        return contains;
    }

    public abstract Map<Element_Type, Object> createMap();
}
