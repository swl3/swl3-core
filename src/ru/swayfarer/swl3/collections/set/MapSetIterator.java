package ru.swayfarer.swl3.collections.set;

import java.util.Iterator;
import java.util.Map;

public class MapSetIterator<Element_Type> implements Iterator<Element_Type>
{
    public Iterator<Element_Type> wrappedIterator;
    public Map<Element_Type, ?> wrappedMap;

    public Element_Type current;

    public MapSetIterator(Iterator<Element_Type> wrappedIterator, Map<Element_Type, ?> wrappedMap)
    {
        this.wrappedIterator = wrappedIterator;
        this.wrappedMap = wrappedMap;
    }

    @Override
    public boolean hasNext() {
        return wrappedIterator.hasNext();
    }

    @Override
    public Element_Type next() {
        current = wrappedIterator.next();
        return current;
    }

    @Override
    public void remove() {
        if (current == null) {
            throw new IllegalStateException();
        }

        wrappedMap.remove(current);
    }
}
