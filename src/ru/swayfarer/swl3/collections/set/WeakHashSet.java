package ru.swayfarer.swl3.collections.set;

import java.util.Map;
import java.util.WeakHashMap;

public class WeakHashSet<Element_Type> extends MapSet<Element_Type>
{
    @Override
    public Map<Element_Type, Object> createMap()
    {
        return new WeakHashMap<>();
    }
}
