package ru.swayfarer.swl3.collections.map;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.ExceptionOptional;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@SuppressWarnings("unchecked")
public class ExtendedMap<Key, Value> extends MapWrapper<Key, Value> implements Iterable<Map.Entry<Key, Value>> {
    
    public ExtendedMap()
    {
        super();
    }

    public ExtendedMap(@NonNull Map<Key, Value> wrappedMap)
    {
        super(wrappedMap);
    }

    public <T extends Value> T getValue(@NonNull Key key)
    {
        return (T) get(key);
    }

    public <T extends Value> ExtendedOptional<T> getOptionalValue(Key key)
    {
        return ExceptionOptional.of(getValue(key));
    }
    
    public <T extends Key> T getKey(Value value)
    {
    	return (T) this.exStream()
				.equalBy(Map.Entry::getValue, value)
				.limit(1)
				.map(Map.Entry::getKey)
				.findFirst()
				.orNull()
		;
    }
    
    public ExtendedMap<Key, Value> copy()
    {
        var ret = new ExtendedMap<>();
        ret.putAll(this);
        return this;
    }
    
    public <T extends Value> T getOrCreate(@NonNull Key key, @NonNull IFunction0<T> valueFun)
    {
        var ret = get(key);

        if (ret == null)
        {
            synchronized (this)
            {
                ret = get(key);
                if (ret == null)
                {
                    ret = valueFun.apply();
                    put(key, ret);
                }
            }
        }

        return (T) ret;
    }
    
    public <K extends Key, V extends Value> ExtendedMap<K, V> synchronize()
    {
        wrappedMap = Collections.synchronizedMap(wrappedMap);
        return ReflectionsUtils.cast(this);
    }

    public <K extends Key, V extends Value> ExtendedMap<K, V> concurrent()
    {
        wrappedMap = new ConcurrentHashMap<>(wrappedMap);
        return ReflectionsUtils.cast(this);
    }
    
    public ExtendedMap<Key, Value> each(@NonNull IFunction1NoR<Map.Entry<Key, Value>> fun)
    {
        for (var entry : this)
        {
            fun.apply(entry);
        }
        
        return this;
    }
    
    public ExtendedStream<Map.Entry<Key, Value>> exStream()
    {
        return ExtendedStream.of(entrySet());
    }
    
    public ExtendedStream<Map.Entry<Key, Value>> parallelExStream()
    {
        return ExtendedStream.of(entrySet(), true);
    }
    
    @Override
    public Iterator<Entry<Key, Value>> iterator()
    {
        return entrySet().iterator();
    }

}
