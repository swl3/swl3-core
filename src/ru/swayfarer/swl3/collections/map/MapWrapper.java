package ru.swayfarer.swl3.collections.map;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class MapWrapper<K, V> extends AbstractMap<K, V>
{
    @NonNull
    public Map<K, V> wrappedMap;
    
    public MapWrapper()
    {
        this(new HashMap<>());
    }
    
    public MapWrapper(@NonNull Map<K, V> wrappedMap)
    {
        this.wrappedMap = wrappedMap;
    }
    
    @Override
    public int size()
    {
        return wrappedMap.size();
    }

    @Override
    public boolean isEmpty()
    {
        return wrappedMap.isEmpty();
    }

    @Override
    public boolean containsKey(Object key)
    {
        return wrappedMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value)
    {
        return wrappedMap.containsValue(value);
    }

    @Override
    public V get(Object key)
    {
        return wrappedMap.get(key);
    }

    @Override
    public V put(K key, V value)
    {
        return wrappedMap.put(key, value);
    }

    @Override
    public V remove(Object key)
    {
        return wrappedMap.remove(key);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m)
    {
        wrappedMap.putAll(m);
    }

    @Override
    public void clear()
    {
        wrappedMap.clear();
    }

    @Override
    public Set<K> keySet()
    {
        return wrappedMap.keySet();
    }

    @Override
    public Collection<V> values()
    {
        return wrappedMap.values();
    }

    @Override
    public Set<Entry<K, V>> entrySet()
    {
        return wrappedMap.entrySet();
    }

    @Override
    public String toString()
    {
        return String.valueOf(wrappedMap);
    }
}
