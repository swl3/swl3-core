package ru.swayfarer.swl3.collections.list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.markers.Internal;

@Getter
@Setter
@Accessors(chain = true)
public class PriorityList<Element_Type> implements Iterable<Element_Type> {

    @Internal
    public List<Entry<Element_Type>> entries = new CopyOnWriteArrayList<>();

    public PriorityList<Element_Type> add(Element_Type elem, int priority)
    {
        entries.add(new Entry<>(priority, elem));
        updateSort();
        return this;
    }
    
    public PriorityList<Element_Type> updateSort()
    {
        entries.sort(Comparator.comparingInt(e -> e.priority));
        return this;
    }
    
    public ExtendedStream<Entry<Element_Type>> stream()
    {
        updateSort();
        return ExtendedStream.of(entries);
    }
    
    public PriorityList<Element_Type> clear()
    {
        entries.clear();
        return this;
    }
    
    @Override
    public Iterator<Element_Type> iterator()
    {
        var entriesList = new ArrayList<Entry<Element_Type>>(entries);
        
        var valuesList = new ArrayList<Element_Type>();
        
        for (var entry : entriesList)
        {
            valuesList.add(entry.value);
        }
        
        return valuesList.iterator();
    }
    
    public ExtendedList<Element_Type> asList()
    {
        var ret = new ExtendedList<Element_Type>();
        updateSort();
        
        for (var elem : this)
        {
            ret.add(elem);
        }
        
        return ret;
    }
    
    @SuppressWarnings("unchecked")
	public ExtendedStream<Entry<Element_Type>> entriesParallelExStream()
    {
    	updateSort();
    	return ExtendedStream.of(entries, true);
    }
    
    public ExtendedStream<Entry<Element_Type>> entriesExStream()
    {
    	updateSort();
    	return ExtendedStream.of(entries);
    }
    
    public ExtendedStream<Element_Type> parallelExStream()
    {
    	updateSort();
    	return asList().parallelExStream();
    }
    
    public ExtendedStream<Element_Type> exStream()
    {
    	updateSort();
    	return asList().exStream();
    }
    
    @Data
    @AllArgsConstructor
    public static class Entry<Element_Type> {
        public int priority;
        public Element_Type value;
    }
}
