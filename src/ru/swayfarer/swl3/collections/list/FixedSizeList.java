package ru.swayfarer.swl3.collections.list;

import ru.swayfarer.swl3.markers.Internal;

import java.util.AbstractList;
import java.util.List;

public class FixedSizeList<Element> extends AbstractList<Element>
{
    @Internal
    public List<Element> wrappedList;

    public FixedSizeList(List<Element> wrappedList)
    {
        this.wrappedList = wrappedList;
    }

    @Override
    public Element set(int index, Element element)
    {
        return wrappedList.set(index, element);
    }

    @Override
    public Element get(int index)
    {
        return wrappedList.get(index);
    }

    @Override
    public int size()
    {
        return wrappedList.size();
    }
}
