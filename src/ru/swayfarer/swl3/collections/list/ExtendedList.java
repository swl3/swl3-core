package ru.swayfarer.swl3.collections.list;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2NoR;
import ru.swayfarer.swl3.objects.EqualsUtils;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@SuppressWarnings("unchecked")
public class ExtendedList<Element_Type> extends ListWrapper<Element_Type> {

	public ExtendedList()
	{
		super();
	}

	public ExtendedList(@NonNull List<Element_Type> wrappedList)
	{
		super(wrappedList);
	}
	
	public boolean addExclusive(Element_Type elem) 
	{
		if (!contains(elem))
		{
			add(elem);
			return true;
		}
		
		return false;
	}
	
	public ExtendedList<Element_Type> syncronize()
	{
		this.wrappedList = Collections.synchronizedList(this.wrappedList);
		return this;
	}
	
	public ExtendedList<Element_Type> nonNull()
	{
		return filter((e) -> e != null);
	}
	
	public boolean isNotEmpty()
	{
		return !isEmpty();
	}
	
	public ExtendedList<Element_Type> addFirst(Element_Type elem)
	{
	    add(0, elem);
	    return this;
	}

	public ExtendedList<Element_Type> fixSize()
	{
		return fixSize(size());
	}

	public ExtendedList<Element_Type> fixSize(int size)
	{
		return fixSize(size, () -> null);
	}

	public ExtendedList<Element_Type> fixSize(int size, @NonNull IFunction0<Element_Type> defaultElementFun)
	{
		int currentListSize = size();
		if (currentListSize < size)
		{
			for (var i1 = 0; i1 < (size - currentListSize); i1 ++)
			{
				add(defaultElementFun.apply());
			}
		}

		else if (size < currentListSize)
			throw new IllegalStateException("List size is already bigger than selected size: " + size);

		wrappedList = new FixedSizeList<>(wrappedList);

		return this;
	}

	public Element_Type first()
	{
		return size() > 0 ? get(0) : null;
	}
	
	public ExtendedList<Element_Type> replace(Element_Type oldValue, Element_Type newValue)
	{
	    for (int i1 = 0; i1 < size(); i1 ++) 
	    {
	        if (EqualsUtils.objectEqualsAll(oldValue, get(i1)))
	            set(i1, newValue);
	    }
	    
	    return this;
	}


	public ExtendedList<Element_Type> sort()
	{
	    Collections.sort(ReflectionsUtils.cast(wrappedList));
	    return this;
	}

	public Element_Type move(int from, int to) {
		var size = size();

		if (isNotEmpty() && from >= 0 && to < size)
		{
			var prev = get(from);
			add(to, remove(from));

			return prev;
		}

		return null;
	}
	
	public <New_Element_Type> ExtendedList<New_Element_Type> map(IFunction1<Element_Type, New_Element_Type> mapper)
	{
		var newList = new ExtendedList<New_Element_Type>();
		
		for (var elem : this)
		{
			newList.add(mapper.apply(elem));
		}
		
		return newList;
	}

	public Element_Type random()
	{
		if (isEmpty())
			return null;

		var rand = new Random();
		var index = rand.nextInt(size());
		return get(index);
	}
	
	public ExtendedList<Element_Type> transform(@NonNull IFunction1<Element_Type, Element_Type> fun)
	{
	    for (int i1 = 0; i1 < size(); i1 ++)
	    {
	        var oldValue = get(i1);
	        var newValue = fun.apply(oldValue);
	        set(i1, newValue);
	    }
	    
	    return this;
	}
	
	public boolean containsSome(@NonNull Object... args)
    {
	    return containsSome(CollectionsSWL.iterable(args));
    }
	
	public boolean containsSome(@NonNull Iterable<?> iterable)
	{
	    for (var elem : iterable)
	    {
	        if (contains(elem))
	            return true;
	    }
	    
	    return false;
	}
	
	public ExtendedList<Element_Type> reverse()
	{
		Collections.reverse(this);
		return this;
	}
	
	public Element_Type last()
	{
		var size = size();
		return size > 0 ? get(size - 1) : null;
	}
	
	public <T extends Element_Type> T firstBy(@NonNull IFunction1<Element_Type, Object> valueFun, Object... availableValues)
	{
		var values = Arrays.asList(availableValues);
		
		for (var elem : this)
		{
			var value = valueFun.apply(elem);
			
			if (values.contains(value)) 
			{
				return (T) elem;
			}
		}
		
		return null;
	}
	
	public <T extends Element_Type> T first(@NonNull IFunction1<Element_Type, Boolean> filter)
	{
		for (var element : this)
			if (filter.apply(element) == Boolean.TRUE)
				return (T) element;
		
		return null;
	}
	
	public ExtendedList<Element_Type> distinct()
	{
		var newElems = wrappedList.stream().distinct().collect(Collectors.toList());
		clear();
		addAll(newElems);
		return this;
	}
	
	public <Key> ExtendedMap<Key, Element_Type> toMap(@NonNull IFunction1<Element_Type, Key> keyFun)
    {
	    return toMap(keyFun, (elem) -> elem);
    }
	
	public <Key, Value> ExtendedMap<Key, Value> toMap(@NonNull IFunction1<Element_Type, Key> keyFun, @NonNull IFunction1<Element_Type, Value> valueFun)
	{
	    var ret = new ExtendedMap<Key, Value>(new LinkedHashMap<>());
	    
	    for (var elem : this)
	    {
	        if (elem != null)
	        {
	            var key = keyFun.apply(elem);
	            var value = valueFun.apply(elem);
	            
	            if (key != null)
	            {
	                ret.put(key, value);
	            }
	        }
	    }
	    
	    return ret;
	}
	
	public <T extends Element_Type> ExtendedList<T> synchronyzed()
	{
		this.wrappedList = Collections.synchronizedList(wrappedList);
		return (ExtendedList<T>) this;
	}
	
	public ExtendedList<Element_Type> weak()
	{
		this.wrappedList = new WeakList<>(this);
		return this;
	}

	public ExtendedList<Element_Type> copyOnWrite()
	{
		this.wrappedList = new CopyOnWriteArrayList<>(this);
		return this;
	}
	
	public ExtendedList<Element_Type> removeFirst()
	{
		if (!isEmpty())
			remove(0);
		return this;
	}
	
	public ExtendedList<Element_Type> removeLast()
	{
		if (!isEmpty())
			remove(size() - 1);
		return this;
	}
	
	public ExtendedList<Element_Type> addAll(@NonNull Element_Type... elems)
	{
		addAll(Arrays.asList(elems));
		return this;
	}
	
	public ExtendedList<Element_Type> removeAll(@NonNull Element_Type... elems)
	{
		removeAll(Arrays.asList(elems));
		return this;
	}
	
	public ExtendedList<Element_Type> setAll(@NonNull Iterable<Element_Type> e)
	{
		clear();
		
		for (var elem : e)
		{
			add(elem);
		}
		
		return this;
	}
	
	public ExtendedList<Element_Type> setAll(@NonNull Element_Type... elems)
	{
		return setAll(CollectionsSWL.iterable(elems));
	}
	
	public <T> T[] toArray(Class<T> classOfArray)
	{
		var arr = (T[]) Array.newInstance(classOfArray, size());
		var iterator = iterator();
		
		for (int i1 = 0; i1 < size(); i1 ++)
			arr[i1] = (T) iterator.next();
		
		return arr;
	}

	public Object toArrayObject(Class<?> classOfArray)
	{
		var arr = Array.newInstance(classOfArray, size());
		var iterator = iterator();

		for (int i1 = 0; i1 < size(); i1 ++)
			Array.set(arr, i1, iterator.next());

		return arr;
	}
	
	public ExtendedList<Element_Type> each(@NonNull IFunction1NoR<Element_Type> fun) 
	{
		forEach(fun.asJavaConsumer());
		return this;
	}
	
	public ExtendedList<Element_Type> each(@NonNull IFunction2NoR<Integer, Element_Type> fun) 
	{
		int next = 0;
		
		for (var elem : this)
		{
			fun.apply(next ++, elem);
		}
		
		return this;
	}
	
	public Element_Type first(@NonNull Class<?> cl)
    {
	    return first((e) -> e != null && cl.isAssignableFrom(e.getClass()));
    }
	
	public ExtendedList<Element_Type> filter(@NonNull IFunction1<Element_Type, Boolean> filter) 
	{
		var iterator = iterator();
		
		while (iterator.hasNext()) 
		{
			var elem = iterator.next();
			
			if (!filter.apply(elem))
				iterator.remove();
		}
		
		return this;
	}
	
	public ExtendedList<Element_Type> sortBy(Comparator<Element_Type> comparator)
	{
	    sort(comparator);
	    return this;
	}
	
	public ExtendedStream<Element_Type> exStream()
	{
		return ExtendedStream.of(this, false);
	}
	
	public ExtendedStream<Element_Type> parallelExStream()
	{
		return ExtendedStream.of(this, true);
	}
	
	public ExtendedList<Element_Type> copy()
	{
		var newList = new ExtendedList<Element_Type>(new ArrayList<>(this));
		return newList;
	}
	
	public static <T> ExtendedList<T> of(@NonNull Iterable<T> itrable)
	{
		var list = new ArrayList<T>();
		
		for (var elem : itrable)
			list.add(elem);
		
		return new ExtendedList<>(list);
	}
	
	public static <T> ExtendedList<T> of(@NonNull Collection<T> collection)
	{
		return new ExtendedList<>(new ArrayList<>(collection));
	}
	
	public static <T> ExtendedList<T> of(@NonNull T... elements) 
	{
		return new ExtendedList<>(new ArrayList<>(Arrays.asList(elements)));
	}
}
