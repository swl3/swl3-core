package ru.swayfarer.swl3.collections.list;

import java.lang.ref.WeakReference;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import lombok.Synchronized;
import lombok.var;
import ru.swayfarer.swl3.markers.Internal;

/**
 * "Слабый" лист, который держит слабые ссылки на свои элементы 
 * @author swayfarer
 */
@SuppressWarnings({ "unchecked", "rawtypes" })
public class WeakList<Element_Type> implements List<Element_Type> {

	/** Был ли лист модифицирован? */
	@Internal
	public AtomicBoolean isModified = new AtomicBoolean(false);
	
	/** Лист ссылок */
	@Internal
	public List<WeakReference<Element_Type>> referencesList = new ArrayList<>();
	
	public WeakList() {}
	
	public WeakList(Collection<Element_Type> collection)
	{
		this.addAll(collection);
	}
	
	@Synchronized(value = "isModified")
	public void removeNullReferences()
	{
		if (!isModified.get())
			return;
		
		List<WeakReference<Element_Type>> toRemove = new ArrayList<WeakReference<Element_Type>>();
		
		for (WeakReference<Element_Type> ref : referencesList)
		{
			if (ref.get() == null)
				toRemove.add(ref);
		}
		
		referencesList.removeAll(toRemove);
		
		isModified.set(false);
	}
	
	/** Получить лист, состоящий не из ссылкок на элементы, а из самих элементов*/
	public List<Element_Type> asNonReferenceList()
	{
		return referencesList.stream().map((link) -> link.get()).collect(Collectors.toList());
	}
	
	/** Создать лист ссылок на элементы коллекции */
	public List<WeakReference<Element_Type>> createRefsList(Collection<? extends Element_Type> collection)
	{
		return collection.stream()
				.map((e) -> new WeakReference<Element_Type>(e))
		.collect(Collectors.toList());
				
	}

	/**
	 * Получить ссылку на элемент
	 * @param element Элемент, на который создается ссылка
	 * @return Ссылка на элемент
	 */
	public WeakReference<Element_Type> createRef(Object element)
	{
		return new WeakReference(element);
	}
	
	@Synchronized(value = "isModified")
	public void onModify()
	{
		removeNullReferences();
		isModified.set(true);
	}
	
	@Override
	public int size()
	{
		removeNullReferences();
		return referencesList.size();
	}

	@Override
	public boolean isEmpty()
	{
		removeNullReferences();
		return referencesList.isEmpty();
	}

	@Override
	public boolean contains(Object o)
	{
		removeNullReferences();
		return false;
	}

	@Override
	public Object[] toArray()
	{
		removeNullReferences();
		
		Object[] ret = new Object[referencesList.size()];
		int nextId = 0;
		
		for (WeakReference<Element_Type> ref : referencesList)
		{
			ret[nextId++] = ref.get();
		}
		
		return ret;
	}

	@Override
	public <T> T[] toArray(T[] a)
	{
		removeNullReferences();
		
		if (a.length < size())
		{
			a = (T[]) Array.newInstance(a.getClass(), size()); 
		}
		
		int nextId = 0;
		
		for (WeakReference<Element_Type> ref : referencesList)
			a[nextId ++] = (T) ref.get();
		
		return a;
	}

	@Override
	public boolean add(Element_Type e)
	{
		onModify();
		return referencesList.add(new WeakReference<Element_Type>(e));
	}
	
	@Override
	public boolean remove(Object o)
	{
		onModify();
		return referencesList.remove(createRef(o));
	}

	@Override
	public boolean containsAll(Collection<?> c)
	{
		return asNonReferenceList().containsAll(c);
	}

	@Override
	public boolean addAll(Collection<? extends Element_Type> c)
	{
		onModify();
		return referencesList.addAll(createRefsList(c));
	}

	@Override
	public boolean addAll(int index, Collection<? extends Element_Type> c)
	{
		onModify();
		return referencesList.addAll(index, createRefsList(c));
	}

	@Override
	public boolean removeAll(Collection<?> c)
	{
		onModify();
		var noRefList = asNonReferenceList();
		var ret = noRefList.removeAll(c);
		this.referencesList = createRefsList(noRefList);
		return ret;
	}

	@Override
	public boolean retainAll(Collection<?> c)
	{
		onModify();
		var noRefList = asNonReferenceList();
		var ret = noRefList.retainAll(c);
		this.referencesList = createRefsList(noRefList);
		return ret;
	}

	@Override
	public void clear()
	{
		isModified.set(false);
		referencesList.clear();
	}

	@Override
	public Element_Type get(int index)
	{
		removeNullReferences();
		return referencesList.get(index).get();
	}

	@Override
	public Element_Type set(int index, Element_Type element)
	{
		onModify();
		
		Element_Type prev = referencesList.get(index).get();
		
		referencesList.set(index, createRef(element));
		
		return prev;
	}

	@Override
	public void add(int index, Element_Type element)
	{
		onModify();
		referencesList.add(index, createRef(element));
	}

	@Override
	public Element_Type remove(int index)
	{
		onModify();
		Element_Type prev = referencesList.get(index).get();
		referencesList.remove(index);
		return prev;
	}

	@Override
	public int indexOf(Object o)
	{
		removeNullReferences();
		return referencesList.indexOf(createRef(o));
	}

	@Override
	public int lastIndexOf(Object o)
	{
		removeNullReferences();
		return referencesList.indexOf(createRef(o));
	}

	@Override
	public ListIterator<Element_Type> listIterator()
	{
		removeNullReferences();
		return asNonReferenceList().listIterator();
	}

	@Override
	public ListIterator<Element_Type> listIterator(int index)
	{
		removeNullReferences();
		return asNonReferenceList().listIterator(index);
	}

	@Override
	public List<Element_Type> subList(int fromIndex, int toIndex)
	{
		removeNullReferences();
		return asNonReferenceList().subList(fromIndex, toIndex);
	}

	@Override
	public Iterator<Element_Type> iterator()
	{
		return asNonReferenceList().iterator();
	}

}
