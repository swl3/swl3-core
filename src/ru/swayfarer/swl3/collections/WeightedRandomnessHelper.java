package ru.swayfarer.swl3.collections;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.objects.EqualsUtils;

@Getter
@Setter
@Accessors(chain = true)
public class WeightedRandomnessHelper<Element_Type>
{
    @NonNull
    @Internal
    public ExtendedList<WeightedEntry<Element_Type>> elements = new ExtendedList<>();

    public WeightedRandomnessHelper<Element_Type> add(int weight, Element_Type elem)
    {
        this.elements.add(new WeightedEntry<Element_Type>().setWeight(weight).setValue(elem));
        return this;
    }

    public WeightedRandomnessHelper<Element_Type> addAll(int weight, Iterable<Element_Type> elems)
    {
        for (var elem : elems)
            add(weight, elem);

        return this;
    }

    public WeightedRandomnessHelper<Element_Type> addAll(int weight, Element_Type... elems)
    {
        return addAll(weight, CollectionsSWL.iterable(elems));
    }


    public ExtendedStream<Element_Type> contentExStream()
    {
        return elements.exStream()
                .map(WeightedEntry::getValue)
        ;
    }

    public WeightedRandomnessHelper<Element_Type> clear()
    {
        this.elements.clear();
        return this;
    }

    public WeightedRandomnessHelper<Element_Type> remove(Element_Type elem)
    {
        this.elements.removeIf((e) -> EqualsUtils.objectEquals(elem, e.getValue()));
        return this;
    }

    public WeightedRandomnessHelper<Element_Type> removeIf(IFunction1<Element_Type, Boolean> filter)
    {
        this.elements.removeIf((e) -> Boolean.TRUE.equals(filter.apply(e.getValue())));
        return this;
    }

    public Element_Type getRandom()
    {
        if (!isEmpty() && size() == 1)
            return elements.first().getValue();

        // Compute the total weight of all items together.
        // This can be skipped of course if sum is already 1.
        double totalWeight = 0.0;
        for (var i : elements) {
            totalWeight += i.getWeight();
        }

        // Now choose a random item.
        int idx = 0;
        WeightedEntry<Element_Type> entry = null;
        for (double r = Math.random() * totalWeight; idx < elements.size() - 1; ++idx) {
            entry = elements.get(idx);
            r -= entry.getWeight();
            if (r <= 0.0) break;
        }

        return entry == null ? isEmpty() ? null : elements.first().getValue() : entry.getValue();
    }

    public boolean isEmpty()
    {
        return CollectionsSWL.isNullOrEmpty(elements);
    }

    public int size()
    {
        return elements.size();
    }

    public ExtendedList<Element_Type> getContent()
    {
        return contentExStream().toExList();
    }

    @Internal
    @Getter
    @Setter
    @Accessors(chain = true)
    public static class WeightedEntry<Element_Type> {
        public int weight;
        public Element_Type value;
    }
}
