package ru.swayfarer.swl3.collections.iterator;

import java.util.Iterator;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data @Accessors(chain = true)
public class ArrayIterable<Element_Type> implements Iterable<Element_Type> {

	@NonNull
	public Element_Type[] array;
	
	@Override
	public Iterator<Element_Type> iterator()
	{
		return new ArrayIterator<Element_Type>(array);
	}
}