package ru.swayfarer.swl3.collections.iterator;

import java.util.Iterator;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data @Accessors(chain = true)
public class ArrayIterator<Element_Type> implements Iterator<Element_Type> {

	public int pos;
	
	@NonNull
	public Element_Type[] array;

	@Override
	public boolean hasNext()
	{
		return array.length > pos;
	}

	@Override
	public Element_Type next()
	{
		return array[pos ++];
	}
	
	@Override
	public void remove()
	{
		throw new UnsupportedOperationException("Can't remove element from array!");
	}
}
