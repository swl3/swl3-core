package ru.swayfarer.swl3.collections.iterator;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.markers.Internal;

import java.lang.reflect.Array;
import java.util.Iterator;

@Getter
@Setter
@Accessors(chain = true)
public class ReflectionArrayIterator<Element_Type> implements Iterator<Element_Type>
{
    @Internal
    public int pos;

    @Internal
    @NonNull
    public Object array;

    @Internal
    public int arrayLength;

    public ReflectionArrayIterator(Object arrayObj)
    {
        this.array = arrayObj;
        this.arrayLength = Array.getLength(arrayObj);
    }

    @Override
    public boolean hasNext()
    {
        return arrayLength > pos;
    }

    @Override
    public Element_Type next()
    {
        return (Element_Type) Array.get(array, pos ++);
    }

    @Override
    public void remove()
    {
        throw new UnsupportedOperationException("Can't remove element from array!");
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    public static class ReflectionArrayIterable<Element_Type> implements Iterable<Element_Type>
    {
        @Internal
        @NonNull
        public Object arrayObj;

        public ReflectionArrayIterable(@NonNull Object arrayObj)
        {
            this.arrayObj = arrayObj;
        }

        @Override
        public Iterator<Element_Type> iterator()
        {
            return new ReflectionArrayIterator<>(arrayObj);
        }
    }
}
