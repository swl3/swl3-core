package ru.swayfarer.swl3.collections.iterator;

import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;

import java.util.Iterator;

public class MappedIterator<Value_Type, Wrapped_Value_Type> implements Iterator<Value_Type>
{
    @Internal
    public IFunction1<Wrapped_Value_Type, Value_Type> mapperFun;

    @Internal
    public Iterator<Wrapped_Value_Type> wrappedIterator;

    public MappedIterator(IFunction1<Wrapped_Value_Type, Value_Type> mapperFun, Iterator<Wrapped_Value_Type> wrappedIterator)
    {
        this.mapperFun = mapperFun;
        this.wrappedIterator = wrappedIterator;
    }

    @Override
    public boolean hasNext()
    {
        return wrappedIterator.hasNext();
    }

    @Override
    public Value_Type next()
    {
        return mapperFun.apply(wrappedIterator.next());
    }

    @Override
    public void remove()
    {
        wrappedIterator.remove();
    }
}
