package ru.swayfarer.swl3.collections.iterator;

import ru.swayfarer.swl3.markers.Internal;

import java.util.Enumeration;
import java.util.Iterator;

public class EnumerationIterator<T> implements Iterator<T>
{
    @Internal
    public Enumeration<T> enumeration;

    public EnumerationIterator(Enumeration<T> enumeration)
    {
        this.enumeration = enumeration;
    }

    @Override
    public boolean hasNext()
    {
        return enumeration.hasMoreElements();
    }

    @Override
    public T next()
    {
        return enumeration.nextElement();
    }
}
