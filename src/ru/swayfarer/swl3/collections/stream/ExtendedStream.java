package ru.swayfarer.swl3.collections.stream;

import java.lang.reflect.Array;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@SuppressWarnings( {"rawtypes", "unchecked"} )
public class ExtendedStream<Element_Type> {

	@Internal
	public IFunction0<Stream> streamCreationFun;

	@Internal
	public IFunction1NoR<Element_Type> ifNotPassFun = (o) -> {};

	public boolean isInvertFilter = false;
	public List<IFunction1<Element_Type, Boolean>> delayedFilters;
	
	public ExtendedStream(@NonNull IFunction0<Stream> streamCreationFun)
	{
		this.streamCreationFun = streamCreationFun;
	}
	
	/*
	 * Проверки на наличие чего-либо
	 */
	
	public boolean anyMatches(@NonNull IFunction1<Element_Type, Boolean> filter) 
	{
		return createStream().anyMatch(withFilterTransformInvert(filter).asJavaPredicate());
	}
	
	public boolean allMatches(@NonNull IFunction1<Element_Type, Boolean> filter) 
	{
		return createStream().allMatch(withFilterTransformInvert(filter).asJavaPredicate());
	}
	
	public boolean noneMatches(@NonNull IFunction1<Element_Type, Boolean> filter) 
	{
		return createStream().noneMatch(withFilterTransformInvert(filter).asJavaPredicate());
	}
	
	/*
	 * Преобразования 
	 */
	
	public <T> ExtendedStream<Element_Type> distinctBy(@NonNull IFunction1<Element_Type, T> fun)
	{
	    var values = Collections.synchronizedSet(new HashSet<>());
	    
	    return filter((o) -> {
	        var valueByDistinct = fun.apply(o);
	        
	        if (values.contains(valueByDistinct))
	        {
	            return false;
	        }
	        
	        values.add(valueByDistinct);
	        return true;
	    });
	}
	
	public <New_Element_Type> ExtendedStream<New_Element_Type> map(@NonNull IFunction1<Element_Type, New_Element_Type> mapper) 
	{
		return appendTransform((stream) -> stream.map(mapper.asJavaFunction()));
	}
	
	public <New_Element_Type> ExtendedStream<New_Element_Type> flatMap(@NonNull IFunction1<? super Element_Type, ? extends Stream<? extends New_Element_Type>> mapper) 
	{
		return appendTransform((stream) -> stream.flatMap(mapper.asJavaFunction()));
	}
	
	public Element_Type first(@NonNull IFunction1<Element_Type, Boolean> filter)
	{
	    return findFirst(filter).orNull();
	}
	
	public Element_Type firstBy(@NonNull IFunction1<Element_Type, Object> valueFun, Object... values)
	{
	    return equalBy(valueFun, values).findFirst().orNull();
	}
	
	public ExtendedStream<Element_Type> limit(long limit) 
	{
		return appendTransform((stream) -> stream.limit(limit));
	}
	
	public ExtendedStream<Element_Type> distinct()
	{
		return appendTransform((stream) -> stream.distinct());
	}
	
	public ExtendedStream<Element_Type> nonNull()
    {
	    return filter((e) -> e != null);
    }
	
	public ExtendedStream<Element_Type> filter(@NonNull IFunction1<Element_Type, Boolean> filter)
	{
		return appendTransform((stream) -> stream.filter(withFilterTransformInvert(filter).asJavaPredicate()));
	}

	@SafeVarargs
	public final ExtendedStream<Element_Type> filters(@NonNull IFunction1<Element_Type, Boolean>... filters)
	{
		return filters(CollectionsSWL.iterable(filters));
	}

	public ExtendedStream<Element_Type> filters(@NonNull Iterable<IFunction1<Element_Type, Boolean>> filters)
	{
		var retRef = new AtomicReference<IFunction1<Element_Type, Boolean>>((e) -> true);

		for (var filter : filters)
		{
			var ret = retRef.get();

			if (ret != null)
				ret = ret.and(filter);
			else
				ret = filter;

			retRef.set(ret);
		}

		return appendTransform((stream) -> stream.filter(withFilterTransformInvert(retRef.get()).asJavaPredicate()));
	}
	
	public ExtendedStream<Element_Type> equalBy(IFunction1<Element_Type, Object> valueFun, Object... objects)
	{
		var values = ExtendedStream.of(objects);
		return filter((e) -> values.contains(valueFun.apply(e)));
	}
	
	public ExtendedStream<Element_Type> not()
	{
		isInvertFilter = true;
		return this;
	}

	public ExtendedStream<Element_Type> throwIfNotPass(IFunction0<Throwable> fun)
	{
		return doIfNotPass((o) -> {
			var ex = fun.apply();
			if (ex != null)
				ExceptionsUtils.throwException(ex);
		});
	}

	public ExtendedStream<Element_Type> throwIfNotPass(IFunction1<Element_Type, Throwable> fun)
	{
		return doIfNotPass((o) -> {
			var ex = fun.apply(o);
			if (ex != null)
				ExceptionsUtils.throwException(ex);
		});
	}

	public ExtendedStream<Element_Type> doIfNotPass(IFunction1NoR<Element_Type> fun)
	{
		if (fun != null)
		{
			ifNotPassFun = ifNotPassFun.andAfter(fun);
		}

		return this;
	}
	
	public ExtendedStream<Element_Type> sort()
	{
		return appendTransform((stream) -> stream.sorted());
	}
	
	public ExtendedStream<Element_Type> sort(@NonNull Comparator<Element_Type> comparator)
	{
		return appendTransform((stream) -> stream.sorted(comparator));
	}

	public ExtendedStream<Element_Type> peek(@NonNull IFunction1NoR<Element_Type> fun)
	{
		return appendTransform((str) -> str.peek(fun.asJavaConsumer()));
	}

	/*
	 * Терминальные методы 
	 */
	
	public ExtendedStream<Element_Type> each(@NonNull IFunction1NoR<Element_Type> fun)
	{
		createStream().forEach(fun.asJavaConsumer());
		return this;
	}
	
	public long count() 
	{
		return createStream().count();
	}
	
	public List<Element_Type> toList()
	{
		return collect(Collectors.toList());
	}
	
	public ExtendedList<Element_Type> toExList()
	{
		return new ExtendedList(toList());
	}
	
	public <Key, Value> Map<Key, Value> toMap(IFunction1<Element_Type, Key> keyFun, IFunction1<Element_Type, Value> valueFun) 
	{
		return collect(Collectors.toMap(keyFun.asJavaFunction(), valueFun.asJavaFunction()));
	}
	
	public <Collection_Type, A> Collection_Type collect(@NonNull Collector<? super Element_Type, A, Collection_Type> collector)
	{
		return createStream().collect(collector);
	}
	
	public ExtendedOptional<Element_Type> min(@NonNull Comparator<Element_Type> comparator) 
	{
		return new ExtendedOptional<>(createStream().min(comparator));
	}

	public ExtendedOptional<Element_Type> findAny()
	{
		return new ExtendedOptional<>(createStream().findAny());
	}
	
	public boolean contains(Element_Type elem)
	{
	    return filter((e) -> elem == null ? e == null : elem.equals(e))
	            .count() > 0;
	}
    
    public boolean contains(@NonNull IFunction1<Element_Type, Boolean> filter)
    {
        return filter((e) -> Boolean.TRUE.equals(filter.apply(e)))
                .count() > 0;
    }
	
	public ExtendedOptional<Element_Type> findFirst() 
	{
		return new ExtendedOptional<>(createStream().findFirst());
	}
	
	public ExtendedOptional<Element_Type> findFirst(@NonNull IFunction1<Element_Type, Boolean> filter) 
	{
		return new ExtendedOptional<>(createStream().filter(withFilterTransformInvert(filter).asJavaPredicate()).findFirst());
	}
	
	public ExtendedOptional<Element_Type> max(@NonNull Comparator<Element_Type> comparator) 
	{
		return new ExtendedOptional<>(createStream().max(comparator));
	}
	
	public ExtendedOptional<Element_Type> reduce(@NonNull IFunction2<Element_Type, Element_Type, Element_Type> accumulator) 
	{
		return new ExtendedOptional<>(createStream().reduce((i1, i2) -> accumulator.apply(i1, i2)));
	}

	public Element_Type[] toArray(IFunction1<Integer, Element_Type[]> newArrayFun)
	{
		var queue = new ArrayDeque<>();
		each(queue::add);

		var arr = newArrayFun.apply(queue.size());
		var next = new AtomicInteger(0);

		queue.forEach((elem) -> {
			arr[next.getAndIncrement()] = (Element_Type) elem;
		});

		return arr;
	}

	public <Array_Element_Type extends Element_Type> Array_Element_Type[] toArray(Class<Array_Element_Type> typeOfArray)
	{
		if (typeOfArray.isPrimitive())
			ExceptionsUtils.ThrowToCaller(IllegalArgumentException.class, "Array type can't be primitive!");

		var result = toArray((size) -> ReflectionsUtils.cast(Array.newInstance(typeOfArray, size)));
		return ReflectionsUtils.cast(result);
	}
	
	/*
	 * Внутренние методы 
	 */
	
	@Internal
	public <T> ExtendedStream<T> appendTransform(@NonNull IFunction1<Stream, Stream> fun)
	{
		return create(streamCreationFun.andApply((stream) -> fun.apply(stream)));
	}
	
	public <T> ExtendedStream<T> create(@NonNull IFunction0<Stream> streamCreationFun)
	{
		return new ExtendedStream<>(streamCreationFun);
	}
	
	@Internal
	public Stream<Element_Type> createStream() 
	{
		var stream = streamCreationFun.apply();
		
		if (stream == null)
			throw new NullPointerException("Can't create stream for " + this);
		
		return stream;
	}
	
	/*
	 * Статические фактори
	 */

	public static <T> ExtendedStream<T> empty()
	{
		return new ExtendedStream<>(Stream::empty);
	}

	public static <T> ExtendedStream<T> infinite(@NonNull IFunction0<T> generator)
	{
		return new ExtendedStream<>(() -> Stream.generate(generator.asJavaSupplier()));
	}
    
    public static <T> ExtendedStream<T> of(@NonNull Enumeration<T> elements)
    {
        return of(Arrays.asList(elements), false);
    }
	
	public static <T> ExtendedStream<T> of(@NonNull T... elements)
	{
		return of(Arrays.asList(elements), false);
	}
	
	public static <T> ExtendedStream<T> parallel(@NonNull T... elements)
	{
		return of(Arrays.asList(elements), true);
	}
	
	public static <T> ExtendedStream<T> of(@NonNull Iterable<T> elements, boolean isParallel)
	{
		return of(listOf(elements), isParallel);
	}
	
	public static <T> ExtendedStream<T> of(@NonNull Collection<T> elements)
	{
	    return of(elements, false);
	}
	
	public static <T> ExtendedStream of(@NonNull Collection<T> elements, boolean isParallel)
	{
		return new ExtendedStream<>(isParallel ? () -> elements.parallelStream() : () -> elements.stream());
	}

	public static <T> ExtendedStream<T> concat(ExtendedStream<? extends T> a, ExtendedStream<? extends T> b)
	{
		return new ExtendedStream<>(() -> Stream.concat(a.createStream(), b.createStream()));
	}
	
	protected <T> IFunction1<T, Boolean> withFilterTransformInvert(IFunction1<T, Boolean> fun)
	{
		var ret = fun;
		
		if (isInvertFilter)
			ret = (obj) -> !fun.apply(obj);

		var withInvert = ret;
		var ifNotPass = ifNotPassFun;

		ret = (o) -> {
			var result = Boolean.TRUE.equals(withInvert.apply(o));
			if (!result)
			{
				ifNotPass.apply(ReflectionsUtils.cast(o));
			}

			return result;
		};
		
		isInvertFilter = false;
		ifNotPassFun = (o) -> {};
			
		return ret;
	}
	
	@Internal
	private static <Element_Type> List<Element_Type> listOf(@NonNull Iterable<Element_Type> i)
	{
		var list = new ArrayList<Element_Type>();
		
		for (var elem : i) 
		{
			list.add(elem);
		}
		
		return list;
	}
}
