package ru.swayfarer.swl3.collections.stream;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

@SuppressWarnings("unchecked")
@Data @Accessors(chain = true)
public class AbstractFiltering <Element_Type, Filtering_Type, Ret_Type> {

	@NonNull
	public IFunction1<IFunction1<Element_Type, Boolean>, Ret_Type> filterFun;
	
	public boolean isRevert;
	
	public Ret_Type any()
	{
		return filter((e) -> true);
	}
	
	public Ret_Type filter(@NonNull IFunction1<Element_Type, Boolean> filter)
	{
		var isRevert = this.isRevert;
		this.setRevert(false);
		return filterFun.apply(isRevert ? (e) -> !filter.apply(e) : filter);
	}
	
	public Filtering_Type not()
	{
		this.setRevert(true);
		return (Filtering_Type) this;
	}
}
