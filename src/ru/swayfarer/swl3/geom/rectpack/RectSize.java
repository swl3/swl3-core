package ru.swayfarer.swl3.geom.rectpack;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class RectSize {
	public int width;
	public int height;
}
