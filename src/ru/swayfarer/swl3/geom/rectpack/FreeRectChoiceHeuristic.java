package ru.swayfarer.swl3.geom.rectpack;

public enum FreeRectChoiceHeuristic {
	/**
	 * < -BSSF: Positions the rectangle against the short side of a free rectangle
	 * into which it fits the best.
	 */
	RectBestShortSideFit,

	/**
	 * < -BLSF: Positions the rectangle against the long side of a free rectangle
	 * into which it fits the best.
	 */

	RectBestLongSideFit,

	/**
	 * -BAF: Positions the rectangle into the smallest free rect into which it fits.
	 */

	RectBestAreaFit,

	RectBottomLeftRule,

	/**
	 * < -CP: Choosest the placement where the rectangle touches other rects as much
	 * as possible.
	 */
	RectContactPointRule
}