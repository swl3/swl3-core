package ru.swayfarer.swl3.geom.rectpack;

import java.util.List;

public class DisjointRectCollection {

	public List<Rect> rects;

	public boolean add(Rect r) {
		// Degenerate rectangles are ignored.
		if (r.getWidth() == 0 || r.getHeight() == 0)
			return true;

		if (!disjoint(r))
			return false;
		rects.add(r);
		return true;
	}

	public void clear() {
		rects.clear();
	}

	public boolean disjoint(IHasDimensions r) {
		// Degenerate rectangles are ignored.
		if (r.getWidth() == 0 || r.getHeight() == 0)
			return true;

		for (int i = 0; i < rects.size(); ++i)
			if (!disjoint(rects.get(i), r))
				return false;
		return true;
	}

	public static boolean disjoint(IHasDimensions a, IHasDimensions b) {
		if (a.getX() + a.getWidth() <= b.getX() || b.getX() + b.getWidth() <= a.getX()
				|| a.getY() + a.getHeight() <= b.getY() || b.getY() + b.getHeight() <= a.getY())
			return true;
		return false;
	}

}
