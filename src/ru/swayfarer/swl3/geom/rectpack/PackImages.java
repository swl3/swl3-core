package ru.swayfarer.swl3.geom.rectpack;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.var;
import ru.swayfarer.swl3.io.file.FileSWL;

public class PackImages {

	@SneakyThrows
	public static void main(String[] args) {
		
		for (var alg : FreeRectChoiceHeuristic.values())
		{
			try
			{
				testPack(alg);
			}
			catch (Throwable e)
			{
				System.out.println("For alg: " + alg);
				e.printStackTrace();
			}
		}
	}
	
	@SneakyThrows
	public static void testPack(FreeRectChoiceHeuristic alg)
	{
		var dir = FileSWL.of("D:\\Games\\Minecraft\\A Viam II\\resourcepacks\\§2Ascensionis Craft V 1.2 Alpha 1.7.x\\assets\\minecraft\\textures\\blocks");
		var images = new ArrayList<ImageContainer>();
		
		dir.findAllSubfiles()
			.filter((f) -> f.getExtension().equals("png"))
			.each((f) -> {
				images.add(ImageContainer.of(f));
			})
		;
		
		var packResult = PackUtils.pack(-1, -1, 128, 128, images, 1.25, alg);
		
		var atlas = new BufferedImage(packResult.width, packResult.height, BufferedImage.TYPE_INT_ARGB);
		
		var atlasG = atlas.createGraphics();
		
		for (var packedImageContainer : images)
		{
			var x = packedImageContainer.getX();
			var y = packedImageContainer.getY();
			
			System.out.println(x + " " + y);
			
			atlasG.drawImage(packedImageContainer.getImage(), x, y, null);
		}
		
		atlasG.dispose();
		
		ImageIO.write(atlas, "png", FileSWL.of("result_" + alg + ".png"));
	}
	
	@Getter @Setter
	public static class ImageContainer implements IHasDimensions{
		public int x, y, width, height;
		public BufferedImage image;
		
		@SneakyThrows
		public static ImageContainer of(FileSWL imageFile)
		{
			var ret = new ImageContainer();
			var image = ImageIO.read(imageFile);
			
			ret.setImage(image);
			ret.setWidth(image.getWidth());
			ret.setHeight(image.getHeight());
			
			if (ret.getHeight() == 0)
			{
				System.out.println(imageFile.getAbsolutePath());
				System.out.println(ret.getWidth());
				System.out.println(ret.getHeight());
			}
			
			return ret;
		}
	}
}
