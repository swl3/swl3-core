package ru.swayfarer.swl3.geom.rectpack;

import lombok.Data;

@Data
public class Rect implements IHasDimensions {
	public int x;
	public int y;
	public int width;
	public int height;

	public Rect() {
	}

	public Rect(IHasDimensions r) {
		setX(r.getX());
		setY(r.getY());
		setWidth(r.getWidth());
		setHeight(r.getHeight());
	}

	public static boolean isContainedIn(IHasDimensions a, IHasDimensions b) {
		return a.getX() >= b.getX() && a.getY() >= b.getY() && a.getX() + a.getWidth() <= b.getX() + b.getWidth()
				&& a.getY() + a.getHeight() <= b.getY() + b.getHeight();
	}

}