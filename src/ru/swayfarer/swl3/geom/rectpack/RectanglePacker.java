package ru.swayfarer.swl3.geom.rectpack;

import static java.lang.System.out;

import java.util.Collections;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import lombok.var;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class RectanglePacker {

	@Builder.Default
	public int maxTextureWidth = Integer.MAX_VALUE;
	
	@Builder.Default
	public int maxTextureHeight = Integer.MAX_VALUE;
	
	public float widthCapacity;
	public float heightCapacity;
	
	public int startTextureWidth;
	public int startTextureHeight;
	
	@Builder.Default
	public boolean useSlotHeightAsWidth = false;
	
	public FreeRectChoiceHeuristic algorithm;
	
	public PackResult pack(List<? extends IHasDimensions> rects)
	{
		var binWidth = startTextureWidth;
		var binHeight = startTextureHeight;
		
		var expandWidth = true;
		
		pack:
		{
			for (;;)
			{
				try
				{
					RectsBinPack bin = new RectsBinPack();
					bin.init(binWidth, binHeight);
		
					Collections.sort(rects, IHasDimensions.COMPARATOR);
					
					for (var rect : rects) 
					{
						int rectWidth = rect.getWidth();
						int rectHeight = rect.getHeight();
						
						if (useSlotHeightAsWidth)
							rectHeight = rectWidth;
		
						Rect packedRect = bin.insert(rectWidth, rectHeight, algorithm);
		
						if (packedRect.getHeight() > 0)
						{
							rect.setX(packedRect.getX());
							rect.setY(packedRect.getY());
						} 
						else
						{
							throw new CantStichException();
						}
					}

					out.printf("Done. All rectangles packed.\n");
					break pack;
				}
				catch (CantStichException e)
				{
					var visitedLimitis = 0;
					
					if (expandWidth && binWidth >= maxTextureWidth)
					{
						visitedLimitis ++;
						expandWidth = !expandWidth;
					}
					
					if (!expandWidth && binHeight >= maxTextureHeight)
					{
						visitedLimitis ++;
						expandWidth = !expandWidth;
					}
					
					if (visitedLimitis > 1)
					{
						throw new RuntimeException("Can't fit to current tex size (" + binWidth + ", " + binHeight + ")");
					}
					
					if (expandWidth)
						binWidth = Math.min((int) Math.round(binWidth * widthCapacity), maxTextureWidth);
					else
						binHeight = Math.min((int) Math.round(binHeight * heightCapacity), maxTextureHeight);
					
					expandWidth = !expandWidth;
				}
			}
		}
		
		int maxX = 0, maxY = 0;
		
		for (var rect : rects)
		{
			var x = rect.getX() + rect.getWidth();
			var y = rect.getY() + rect.getHeight();
			
			if (x > maxX)
				maxX = x;
			
			if (y > maxY)
				maxY = y;
		}
		
		return PackResult.builder()
				.width(maxX)
				.height(maxY)
				.build()
		;
	}
	
	public static RectanglePacker getInstance()
	{
		return new RectanglePacker();
	}
}
