package ru.swayfarer.swl3.geom.rectpack;

import static java.lang.Math.abs;
import static ru.swayfarer.swl3.geom.rectpack.Rect.isContainedIn;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class RectsBinPack {
	
	public boolean useFlip = false;

	private int binWidth;

	private int binHeight;

	private List<Rect> usedRectangles = new ArrayList<Rect>();

	private List<Rect> freeRectangles = new ArrayList<Rect>();
	
	public RectsBinPack() {
		binWidth = 0;
		binHeight = 0;
	}

	public RectsBinPack(int width, int height) {
		init(width, height);
	}

	public void init(int width, int height) {
		binWidth = width;
		binHeight = height;

		Rect n = new Rect();
		n.setX(0);
		n.setY(0);
		n.setWidth(width);
		n.setHeight(height);

		usedRectangles.clear();

		freeRectangles.clear();
		freeRectangles.add(n);
	}

	public Rect insert(int width, int height, FreeRectChoiceHeuristic method) {
		Rect newNode = null;
		AtomicInteger score1 = new AtomicInteger(0); // Unused in this function. We
												// don't need to know the score
												// after finding the position.
		AtomicInteger score2 = new AtomicInteger(0);

		switch (method) {
		case RectBestShortSideFit:
			newNode = findPositionForNewNodeBestShortSideFit(width, height, score1, score2);
			break;
		case RectBottomLeftRule:
			newNode = findPositionForNewNodeBottomLeft(width, height, score1, score2);
			break;
		case RectContactPointRule:
			newNode = findPositionForNewNodeContactPoint(width, height, score1);
			break;
		case RectBestLongSideFit:
			newNode = findPositionForNewNodeBestLongSideFit(width, height, score2, score1);
			break;
		case RectBestAreaFit:
			newNode = findPositionForNewNodeBestAreaFit(width, height, score1, score2);
			break;
		}

		if (newNode.getHeight() == 0)
			return newNode;

		int numRectanglesToProcess = freeRectangles.size();
		for (int i = 0; i < numRectanglesToProcess; ++i) {
			if (splitFreeNode(freeRectangles.get(i), newNode)) {
				freeRectangles.remove(freeRectangles.get(i));
				--i;
				--numRectanglesToProcess;
			}
		}

		pruneFreeList();

		usedRectangles.add(newNode);
		return newNode;
	}

	public void insert(List<RectSize> rects, List<Rect> dst, FreeRectChoiceHeuristic method) {
		dst.clear();

		while (rects.size() > 0) {
			// int bestScore1 = std::numeric_limits<int>::max();
			// int bestScore2 = std::numeric_limits<int>::max();
			AtomicInteger bestScore1 = new AtomicInteger(Integer.MAX_VALUE);
			AtomicInteger bestScore2 = new AtomicInteger(Integer.MAX_VALUE);
			int bestRectIndex = -1;
			Rect bestNode = null;

			for (int i = 0; i < rects.size(); ++i) {
				AtomicInteger score1 = new AtomicInteger(0);
				AtomicInteger score2 = new AtomicInteger(0);
				Rect newNode = scoreRect(rects.get(i).width, rects.get(i).height, method, score1, score2);

				if (score1.get() < bestScore1.get()
						|| (score1.get() == bestScore1.get() && score2.get() < bestScore2.get())) {
					bestScore1.set(score1.get());
					bestScore2.set(score2.get());
					bestNode = newNode;
					bestRectIndex = i;
				}
			}

			if (bestRectIndex == -1)
				return;

			placeRect(bestNode);
			rects.remove(rects.get(bestRectIndex));
		}
	}

	public void placeRect(Rect node) {
		int numRectanglesToProcess = freeRectangles.size();
		for (int i = 0; i < numRectanglesToProcess; ++i) {
			if (splitFreeNode(freeRectangles.get(i), node)) {
				freeRectangles.remove(freeRectangles.get(i));
				--i;
				--numRectanglesToProcess;
			}
		}

		pruneFreeList();

		usedRectangles.add(node);
	}

	public Rect scoreRect(int width, int height, FreeRectChoiceHeuristic method, AtomicInteger score1, AtomicInteger score2) {
		Rect newNode = null;
		score1.set(Integer.MAX_VALUE);
		score2.set(Integer.MAX_VALUE);
		switch (method) {
		case RectBestShortSideFit:
			newNode = findPositionForNewNodeBestShortSideFit(width, height, score1, score2);
			break;
		case RectBottomLeftRule:
			newNode = findPositionForNewNodeBottomLeft(width, height, score1, score2);
			break;
		case RectContactPointRule:
			newNode = findPositionForNewNodeContactPoint(width, height, score1);
			score1.set(-score1.get()); // Reverse since we are minimizing, but
										// for
			// contact point score bigger is better.
			break;
		case RectBestLongSideFit:
			newNode = findPositionForNewNodeBestLongSideFit(width, height, score2, score1);
			break;
		case RectBestAreaFit:
			newNode = findPositionForNewNodeBestAreaFit(width, height, score1, score2);
			break;
		}

		// Cannot fit the current rectangle.
		if (newNode.getHeight() == 0) {
			// score1 = std::numeric_limits<int>::max();
			// score2 = std::numeric_limits<int>::max();
			score1.set(Integer.MAX_VALUE);
			score2.set(Integer.MAX_VALUE);
		}

		return newNode;
	}

	// / Computes the ratio of used surface area.
	public float occupancy() {
		long usedSurfaceArea = 0;
		for (int i = 0; i < usedRectangles.size(); ++i)
			usedSurfaceArea += usedRectangles.get(i).getWidth() * usedRectangles.get(i).getHeight();

		return (float) usedSurfaceArea / (binWidth * binHeight);
	}

	// Rect FindPositionForNewNodeBottomLeft(int width, int height, int &bestY,
	// int &bestX)
	public Rect findPositionForNewNodeBottomLeft(int width, int height, AtomicInteger bestY, AtomicInteger bestX) {
		Rect bestNode = new Rect();
		// memset(&bestNode, 0, sizeof(Rect));

		bestY.set(Integer.MAX_VALUE);

		for (int i = 0; i < freeRectangles.size(); ++i) {
			// Try to place the rectangle in upright (non-flipped) orientation.
			if (freeRectangles.get(i).getWidth() >= width && freeRectangles.get(i).getHeight() >= height) {
				int topSideY = freeRectangles.get(i).getY() + height;
				if (topSideY < bestY.get() || (topSideY == bestY.get() && freeRectangles.get(i).getX() < bestX.get())) {
					bestNode.setX(freeRectangles.get(i).getX());
					bestNode.setY(freeRectangles.get(i).getY());
					bestNode.setWidth(width);
					bestNode.setHeight(height);
					bestY.set(topSideY);
					bestX.set(freeRectangles.get(i).getX());
				}
			}
			if (useFlip)
			{
				if (freeRectangles.get(i).getWidth() >= height && freeRectangles.get(i).getHeight() >= width) {
					int topSideY = freeRectangles.get(i).getY() + width;
					if (topSideY < bestY.get() || (topSideY == bestY.get() && freeRectangles.get(i).getX() < bestX.get())) {
						bestNode.setX(freeRectangles.get(i).getX());
						bestNode.setY(freeRectangles.get(i).getY());
						bestNode.setWidth(height);
						bestNode.setHeight(width);
						bestY.set(topSideY);
						bestX.set(freeRectangles.get(i).getX());
					}
				}
			}
		}
		return bestNode;
	}

	// Rect FindPositionForNewNodeBestShortSideFit(int width, int height,
	// int &bestShortSideFit, int &bestLongSideFit)
	public Rect findPositionForNewNodeBestShortSideFit(int width, int height, AtomicInteger bestShortSideFit,
			AtomicInteger bestLongSideFit) {
		Rect bestNode = new Rect();
		// memset(&bestNode, 0, sizeof(Rect));

		bestShortSideFit = new AtomicInteger(Integer.MAX_VALUE);// std::numeric_limits<int>::max();

		for (int i = 0; i < freeRectangles.size(); ++i) {
			// Try to place the rectangle in upright (non-flipped) orientation.
			if (freeRectangles.get(i).getWidth() >= width && freeRectangles.get(i).getHeight() >= height) {
				int leftoverHoriz = abs(freeRectangles.get(i).getWidth() - width);
				int leftoverVert = abs(freeRectangles.get(i).getHeight() - height);
				int shortSideFit = Math.min(leftoverHoriz, leftoverVert);
				int longSideFit = Math.max(leftoverHoriz, leftoverVert);

				if (shortSideFit < bestShortSideFit.get()
						|| (shortSideFit == bestShortSideFit.get() && longSideFit < bestLongSideFit.get())) {
					bestNode.setX(freeRectangles.get(i).getX());
					bestNode.setY(freeRectangles.get(i).getY());
					bestNode.setWidth(width);
					bestNode.setHeight(height);
					bestShortSideFit.set(shortSideFit);
					bestLongSideFit.set(longSideFit);
				}
			}

			if (useFlip) 
			{
				if (freeRectangles.get(i).getWidth() >= height && freeRectangles.get(i).getHeight() >= width) {
					int flippedLeftoverHoriz = abs(freeRectangles.get(i).getWidth() - height);
					int flippedLeftoverVert = abs(freeRectangles.get(i).getHeight() - width);
					int flippedShortSideFit = Math.min(flippedLeftoverHoriz, flippedLeftoverVert);
					int flippedLongSideFit = Math.max(flippedLeftoverHoriz, flippedLeftoverVert);

					if (flippedShortSideFit < bestShortSideFit.get() || (flippedShortSideFit == bestShortSideFit.get()
							&& flippedLongSideFit < bestLongSideFit.get())) {
						bestNode.setX(freeRectangles.get(i).getX());
						bestNode.setY(freeRectangles.get(i).getY());
						bestNode.setWidth(height);
						bestNode.setHeight(width);
						bestShortSideFit.set(flippedShortSideFit);
						bestLongSideFit.set(flippedLongSideFit);
					}
				}
			}
		}
		return bestNode;
	}

	// Rect FindPositionForNewNodeBestLongSideFit(int width, int height,
	// int &bestShortSideFit, int &bestLongSideFit) const
	public Rect findPositionForNewNodeBestLongSideFit(int width, int height, AtomicInteger bestShortSideFit,
			AtomicInteger bestLongSideFit) {
		Rect bestNode = new Rect();
		// memset(&bestNode, 0, sizeof(Rect));

		bestLongSideFit = new AtomicInteger(Integer.MAX_VALUE);

		for (int i = 0; i < freeRectangles.size(); ++i) {
			// Try to place the rectangle in upright (non-flipped) orientation.
			if (freeRectangles.get(i).getWidth() >= width && freeRectangles.get(i).getHeight() >= height) {
				int leftoverHoriz = abs(freeRectangles.get(i).getWidth() - width);
				int leftoverVert = abs(freeRectangles.get(i).getHeight() - height);
				int shortSideFit = Math.min(leftoverHoriz, leftoverVert);
				int longSideFit = Math.max(leftoverHoriz, leftoverVert);

				if (longSideFit < bestLongSideFit.get()
						|| (longSideFit == bestLongSideFit.get() && shortSideFit < bestShortSideFit.get())) {
					bestNode.setX(freeRectangles.get(i).getX());
					bestNode.setY(freeRectangles.get(i).getY());
					bestNode.setWidth(width);
					bestNode.setHeight(height);
					bestShortSideFit.set(shortSideFit);
					bestLongSideFit.set(longSideFit);
				}
			}

			if (useFlip)
			{
				if (freeRectangles.get(i).getWidth() >= height && freeRectangles.get(i).getHeight() >= width) {
					int leftoverHoriz = abs(freeRectangles.get(i).getWidth() - height);
					int leftoverVert = abs(freeRectangles.get(i).getHeight() - width);
					int shortSideFit = Math.min(leftoverHoriz, leftoverVert);
					int longSideFit = Math.max(leftoverHoriz, leftoverVert);

					if (longSideFit < bestLongSideFit.get()
							|| (longSideFit == bestLongSideFit.get() && shortSideFit < bestShortSideFit.get())) {
						bestNode.setX(freeRectangles.get(i).getX());
						bestNode.setY(freeRectangles.get(i).getY());
						bestNode.setWidth(height);
						bestNode.setHeight(width);
						bestShortSideFit.set(shortSideFit);
						bestLongSideFit.set(longSideFit);
					}
				}
			}
		}
		return bestNode;
	}

	// Rect FindPositionForNewNodeBestAreaFit(int width, int height,
	// int &bestAreaFit, int &bestShortSideFit)
	public Rect findPositionForNewNodeBestAreaFit(int width, int height, AtomicInteger bestAreaFit, AtomicInteger bestShortSideFit) {
		Rect bestNode = new Rect();
		// memset(&bestNode, 0, sizeof(Rect));

		// bestAreaFit = std::numeric_limits<int>::max();
		bestAreaFit = new AtomicInteger(Integer.MAX_VALUE);

		for (int i = 0; i < freeRectangles.size(); ++i) {
			int areaFit = freeRectangles.get(i).getWidth() * freeRectangles.get(i).getHeight() - width * height;

			// Try to place the rectangle in upright (non-flipped) orientation.
			if (freeRectangles.get(i).getWidth() >= width && freeRectangles.get(i).getHeight() >= height) {
				int leftoverHoriz = abs(freeRectangles.get(i).getWidth() - width);
				int leftoverVert = abs(freeRectangles.get(i).getHeight() - height);
				int shortSideFit = Math.min(leftoverHoriz, leftoverVert);

				if (areaFit < bestAreaFit.get()
						|| (areaFit == bestAreaFit.get() && shortSideFit < bestShortSideFit.get())) {
					bestNode.setX(freeRectangles.get(i).getX());
					bestNode.setY(freeRectangles.get(i).getY());
					bestNode.setWidth(width);
					bestNode.setHeight(height);
					bestShortSideFit.set(shortSideFit);
					bestAreaFit.set(areaFit);
				}
			}

			if (useFlip)
			{
				if (freeRectangles.get(i).getWidth() >= height && freeRectangles.get(i).getHeight() >= width) {
					int leftoverHoriz = abs(freeRectangles.get(i).getWidth() - height);
					int leftoverVert = abs(freeRectangles.get(i).getHeight() - width);
					int shortSideFit = Math.min(leftoverHoriz, leftoverVert);

					if (areaFit < bestAreaFit.get()
							|| (areaFit == bestAreaFit.get() && shortSideFit < bestShortSideFit.get())) {
						bestNode.setX(freeRectangles.get(i).getX());
						bestNode.setY(freeRectangles.get(i).getY());
						bestNode.setWidth(height);
						bestNode.setHeight(width);
						bestShortSideFit.set(shortSideFit);
						bestAreaFit.set(areaFit);
					}
				}
			}
		}
		return bestNode;
	}

	// / Returns 0 if the two intervals i1 and i2 are disjoint, or the length of
	// their overlap otherwise.
	public int сommonIntervalLength(int i1start, int i1end, int i2start, int i2end) {
		if (i1end < i2start || i2end < i1start)
			return 0;
		return Math.min(i1end, i2end) - Math.max(i1start, i2start);
	}

	public int contactPointScoreNode(int x, int y, int width, int height) {
		int score = 0;

		if (x == 0 || x + width == binWidth)
			score += height;
		if (y == 0 || y + height == binHeight)
			score += width;

		for (int i = 0; i < usedRectangles.size(); ++i) {
			if (usedRectangles.get(i).getX() == x + width
					|| usedRectangles.get(i).getX() + usedRectangles.get(i).getWidth() == x)
				score += сommonIntervalLength(usedRectangles.get(i).getY(),
						usedRectangles.get(i).getY() + usedRectangles.get(i).getHeight(), y, y + height);
			if (usedRectangles.get(i).getY() == y + height
					|| usedRectangles.get(i).getY() + usedRectangles.get(i).getHeight() == y)
				score += сommonIntervalLength(usedRectangles.get(i).getX(),
						usedRectangles.get(i).getX() + usedRectangles.get(i).getWidth(), x, x + width);
		}
		return score;
	}

	// Rect FindPositionForNewNodeContactPoint(int width, int height, int
	// &bestContactScore)
	public Rect findPositionForNewNodeContactPoint(int width, int height, AtomicInteger bestContactScore) {
		Rect bestNode = new Rect();
		// memset(&bestNode, 0, sizeof(Rect));

		bestContactScore = new AtomicInteger(-1);

		for (int i = 0; i < freeRectangles.size(); ++i) {
			// Try to place the rectangle in upright (non-flipped) orientation.
			if (freeRectangles.get(i).getWidth() >= width && freeRectangles.get(i).getHeight() >= height) {
				int score = contactPointScoreNode(freeRectangles.get(i).getX(), freeRectangles.get(i).getY(), width,
						height);
				if (score > bestContactScore.get()) {
					bestNode.setX(freeRectangles.get(i).getX());
					bestNode.setY(freeRectangles.get(i).getY());
					bestNode.setWidth(width);
					bestNode.setHeight(height);
					bestContactScore.set(score);
				}
			}
			
			if (useFlip)
			{
				if (freeRectangles.get(i).getWidth() >= height && freeRectangles.get(i).getHeight() >= width) {
					int score = contactPointScoreNode(freeRectangles.get(i).getX(), freeRectangles.get(i).getY(), width,
							height);
					if (score > bestContactScore.get()) {
						bestNode.setX(freeRectangles.get(i).getX());
						bestNode.setY(freeRectangles.get(i).getY());
						bestNode.setWidth(height);
						bestNode.setHeight(width);
						bestContactScore.set(score);
					}
				}
			}
		}
		return bestNode;
	}

	// boolean SplitFreeNode(Rect freeNode, const Rect &usedNode)
	public boolean splitFreeNode(IHasDimensions freeNode, IHasDimensions usedNode) {
		// Test with SAT if the rectangles even intersect.
		if (usedNode.getX() >= freeNode.getX() + freeNode.getWidth()
				|| usedNode.getX() + usedNode.getWidth() <= freeNode.getX()
				|| usedNode.getY() >= freeNode.getY() + freeNode.getHeight()
				|| usedNode.getY() + usedNode.getHeight() <= freeNode.getY())
			return false;

		if (usedNode.getX() < freeNode.getX() + freeNode.getWidth()
				&& usedNode.getX() + usedNode.getWidth() > freeNode.getX()) {
			// New node at the top side of the used node.
			if (usedNode.getY() > freeNode.getY() && usedNode.getY() < freeNode.getY() + freeNode.getHeight()) {
				Rect newNode = new Rect(freeNode);
				newNode.setHeight(usedNode.getY() - newNode.getY());
				freeRectangles.add(newNode);
			}

			// New node at the bottom side of the used node.
			if (usedNode.getY() + usedNode.getHeight() < freeNode.getY() + freeNode.getHeight()) {
				Rect newNode = new Rect(freeNode);
				newNode.setY(usedNode.getY() + usedNode.getHeight());
				newNode.setHeight(freeNode.getY() + freeNode.getHeight() - (usedNode.getY() + usedNode.getHeight()));
				freeRectangles.add(newNode);
			}
		}

		if (usedNode.getY() < freeNode.getY() + freeNode.getHeight()
				&& usedNode.getY() + usedNode.getHeight() > freeNode.getY()) {
			// New node at the left side of the used node.
			if (usedNode.getX() > freeNode.getX() && usedNode.getX() < freeNode.getX() + freeNode.getWidth()) {
				Rect newNode = new Rect(freeNode);
				newNode.setWidth(usedNode.getX() - newNode.getX());
				freeRectangles.add(newNode);
			}

			// New node at the right side of the used node.
			if (usedNode.getX() + usedNode.getWidth() < freeNode.getX() + freeNode.getWidth()) {
				Rect newNode = new Rect(freeNode);
				newNode.setX(usedNode.getX() + usedNode.getWidth());
				newNode.setWidth(freeNode.getX() + freeNode.getWidth() - (usedNode.getX() + usedNode.getWidth()));
				freeRectangles.add(newNode);
			}
		}

		return true;
	}

	public void pruneFreeList() {
		/*
		 * /// Would be nice to do something like this, to avoid a Theta(n^2) loop
		 * through each pair. /// But unfortunately it doesn't quite cut it, since we
		 * also want to detect containment. /// Perhaps there's another way to do this
		 * faster than Theta(n^2).
		 * 
		 * if (freeRectangles.size() > 0) clb::sort::QuickSort(&freeRectangles[0],
		 * freeRectangles.size(), NodeSortCmp);
		 * 
		 * for(int i = 0; i < freeRectangles.size()-1; ++i) if (freeRectangles[i].x ==
		 * freeRectangles[i+1].x && freeRectangles[i].y == freeRectangles[i+1].y &&
		 * freeRectangles[i].width == freeRectangles[i+1].width &&
		 * freeRectangles[i].height == freeRectangles[i+1].height) {
		 * freeRectangles.erase(freeRectangles.begin() + i); --i; }
		 */

		// / Go through each pair and remove any rectangle that is redundant.
		for (int i = 0; i < freeRectangles.size(); ++i)
			for (int j = i + 1; j < freeRectangles.size(); ++j) {
				if (isContainedIn(freeRectangles.get(i), freeRectangles.get(j))) {
					freeRectangles.remove(freeRectangles.get(i));
					--i;
					break;
				}
				if (isContainedIn(freeRectangles.get(j), freeRectangles.get(i))) {
					freeRectangles.remove(freeRectangles.get(j));
					--j;
				}
			}
	}
}
