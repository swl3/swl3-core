package ru.swayfarer.swl3.geom.rectpack;

import java.util.Comparator;

public interface IHasDimensions {
	
	public static final Comparator<IHasDimensions> COMPARATOR = (o1, o2) -> {
		return o2.getWidth() * o2.getHeight() - o1.getWidth() * o1.getHeight();
	};
	
	int getX();

	void setX(int x);

	int getY();

	void setY(int y);

	int getWidth();

	void setWidth(int width);

	int getHeight();

	void setHeight(int height);

}