package ru.swayfarer.swl3.geom.rectpack;

import lombok.var;

import static java.lang.System.out;

import java.util.Collections;
import java.util.List;

public class PackUtils {

	public static PackResult pack(int maxBinWidth, int maxBinHeight, int binWidth, int binHeight, List<? extends IHasDimensions> rects, double capacity, FreeRectChoiceHeuristic alg)
	{
		if (maxBinHeight <= 0)
			maxBinHeight = Integer.MAX_VALUE;
		
		if (maxBinWidth <= 0)
			maxBinWidth = Integer.MAX_VALUE;
		
		var expandW = true;
		
		pack:
		{
			for (;;)
			{
				try
				{
					RectsBinPack bin = new RectsBinPack();
					bin.init(binWidth, binHeight);
		
					Collections.sort(rects, IHasDimensions.COMPARATOR);
					
					for (var rect : rects) 
					{
						int rectWidth = rect.getWidth();
						int rectHeight = rect.getHeight();
		
						FreeRectChoiceHeuristic heuristic = alg;
						Rect packedRect = bin.insert(rectWidth, rectHeight, heuristic);
		
						if (packedRect.getHeight() > 0)
						{
							rect.setX(packedRect.getX());
							rect.setY(packedRect.getY());
						} 
						else
						{
							throw new CantStichException();
						}
					}

					out.printf("Done. All rectangles packed.\n");
					break pack;
				}
				catch (CantStichException e)
				{
					var i = 0;
					
					if (expandW && binWidth >= maxBinWidth)
					{
						i ++;
						expandW = !expandW;
					}
					
					if (!expandW && binHeight >= maxBinHeight)
					{
						i ++;
						expandW = !expandW;
					}
					
					if (i > 1)
					{
						throw new RuntimeException("Can't fit to current tex size (" + binWidth + ", " + binHeight + ")");
					}
					
					if (expandW)
						binWidth = Math.min((int) Math.round(binWidth * capacity), maxBinWidth);
					else
						binHeight = Math.min((int) Math.round(binHeight * capacity), maxBinHeight);
					
					expandW = !expandW;
				}
			}
		}
		
		int maxX = 0, maxY = 0;
		
		for (var rect : rects)
		{
			var x = rect.getX() + rect.getWidth();
			var y = rect.getY() + rect.getHeight();
			
			if (x > maxX)
				maxX = x;
			
			if (y > maxY)
				maxY = y;
		}
		
		return PackResult.builder()
				.width(maxX)
				.height(maxY)
				.build()
		;
	}
}
