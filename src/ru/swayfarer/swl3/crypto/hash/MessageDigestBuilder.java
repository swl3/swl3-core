package ru.swayfarer.swl3.crypto.hash;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.io.BinaryUtils;
import ru.swayfarer.swl3.io.buffers.DynamicByteBuffer;
import ru.swayfarer.swl3.io.streams.StreamsUtils;

@Data
@Accessors(chain = true)
public class MessageDigestBuilder implements DefaultHashAlgorithms {

    @NonNull
    public IFunction0<ExceptionsHandler> exceptionsHandler;
    
    @NonNull
    public IFunction0<BinaryUtils> binaryUtils;
    
    public MessageDigest digest;

    public MessageDigestBuilder md5()
    {
        return algorithm(Md5);
    }
    
    public MessageDigestBuilder sha256()
    {
        return algorithm(Sha256);
    }
    
    public MessageDigestBuilder algorithm(@NonNull String algorithm)
    {
        var exceptionsHandler = getExceptionsHandler();
        
        return exceptionsHandler.safeReturn(() -> {
            digest = MessageDigest.getInstance(algorithm);
            return this;
        }, null, "");
    }
    
    public MessageDigestBuilder in(@NonNull byte[] bytes)
    {
        digest.update(bytes, 0, bytes.length);
        return this;
    }
    
    public MessageDigestBuilder in(@NonNull DynamicByteBuffer buff)
    {
        return in(buff.byteBuffer);
    }
    
    public MessageDigestBuilder in(@NonNull ByteBuffer buff)
    {
        var exceptionsHandler = getExceptionsHandler(); 
        
        exceptionsHandler.safe(() -> {
            digest.update(buff);
        }, "Error while reading buffer", buff);
        
        return this;
    }
    
    public MessageDigestBuilder in(@NonNull InputStream in) 
    {
        var exceptionsHandler = getExceptionsHandler(); 
        
        exceptionsHandler.safe(() -> {
            byte[] buffer = new byte[StreamsUtils.BUFFER_SIZE];
            int sizeRead = -1;
            while ((sizeRead = in.read(buffer)) != -1) {
                digest.update(buffer, 0, sizeRead);
            }
            in.close();
        }, "Error while reading stream", in);
        
        return this;
    }
    
    public byte[] asBytes()
    {
        return digest.digest();
    }
    
    public String asString(@NonNull Charset charset)
    {
        return new String(asBytes(), charset);
    }
    
    public String asHEX()
    {
        var binaryUtils = getBinaryUtils();
        return binaryUtils.toHexString(asBytes());
    }
    
    public String asUTF8()
    {
        return asString(StandardCharsets.UTF_8);
    }
    
    public BinaryUtils getBinaryUtils()
    {
        return binaryUtils.apply();
    }
    
    public ExceptionsHandler getExceptionsHandler()
    {
        return exceptionsHandler.apply();
    }
}
