package ru.swayfarer.swl3.crypto.hash;

public interface DefaultHashAlgorithms {

    public String Md5 = "MD5";
    public String Sha256 = "SHA-256";
    
}
