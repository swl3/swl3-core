package ru.swayfarer.swl3.crypto;

import javax.crypto.spec.IvParameterSpec;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CipherParamsBuilder {

    @NonNull
    public CipherBuilder builder;
    
    public CipherBuilder ivBytes(@NonNull byte[] bytes)
    {
        builder.algorithmParameterSpec = new IvParameterSpec(bytes);
        return builder;
    }
}
