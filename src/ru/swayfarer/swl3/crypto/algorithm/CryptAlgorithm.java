package ru.swayfarer.swl3.crypto.algorithm;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.crypto.CipherEvent;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;

@Data
@Accessors(chain = true)
public class CryptAlgorithm {
    
    public static String defaultPadding = "PKCS5Padding";

    public IObservable<CipherEvent> eventPreCreate = Observables.createObservable();
    public IObservable<CipherEvent> eventPostCreate = Observables.createObservable();

    @NonNull
    public String name;
    
    @NonNull
    public String mode = "";
    
    @NonNull
    public String padding = defaultPadding;
    
    public boolean assync;
    
    public String toFullAlgorithmString()
    {
        var str = new DynamicString();
        str.append(name);
        
        if (!StringUtils.isBlank(mode))
        {
            str.append("/");
            str.append(mode);
        }
        
        if (!StringUtils.isBlank(padding))
        {
            str.append("/");
            str.append(padding);
        }
        
        return str.toString();
    }

    public void onPreCreate(CipherEvent event)
    {
        eventPreCreate.next(event);
    }

    public void onPostCreate(CipherEvent event)
    {
        eventPostCreate.next(event);
    }
}
