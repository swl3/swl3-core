package ru.swayfarer.swl3.crypto.algorithm;

import lombok.var;

public class DefaultCryptoAlgorithms
{
    public static CryptAlgorithm RSA = new CryptAlgorithm("RSA")
            .setAssync(true)
            .setMode("")
            .setPadding("")
    ;
    
    public static CryptAlgorithm AES = new CryptAlgorithm("AES")
            .setAssync(false)
            .setMode("CBC")
            .setPadding(CryptAlgorithm.defaultPadding)
    ;

    static {
        AES.getEventPostCreate().subscribe()
                .by((event) -> {
                    var builder = event.getCipherBuilder();

                    if (builder.getAlgorithmParameterSpec() == null)
                        builder.param().ivBytes(new byte[event.getCipher().getBlockSize()]);
                })
        ;
    }
}
