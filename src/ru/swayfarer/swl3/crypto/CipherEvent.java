package ru.swayfarer.swl3.crypto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.observable.event.AbstractEvent;

import javax.crypto.Cipher;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class CipherEvent extends AbstractEvent
{
    public CipherBuilder cipherBuilder;
    public Cipher cipher;
}
