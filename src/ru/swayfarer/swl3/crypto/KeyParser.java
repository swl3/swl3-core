package ru.swayfarer.swl3.crypto;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.spec.SecretKeySpec;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.crypto.algorithm.CryptAlgorithm;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.io.BinaryUtils;

@Getter
@Setter
@Accessors(chain = true)
@SuppressWarnings("unchecked")
public class KeyParser {

    @NonNull
    public IFunction0<ExceptionsHandler> exceptionsHandler;

    @NonNull
    public IFunction0<BinaryUtils> binaryUtils;

    public KeySpec keySpec;

    public CryptAlgorithm algorithm;
    public Mode mode = Mode.Secret;
    public int multiplicity = 1;

    public KeyParser(@NonNull IFunction0<ExceptionsHandler> exceptionsHandler, @NonNull IFunction0<BinaryUtils> binaryUtils)
    {
        this.exceptionsHandler = exceptionsHandler;
        this.binaryUtils = binaryUtils;
    }

    public KeyParser mode(@NonNull Mode mode)
    {
        this.mode = mode;
        return this;
    }
    
    public KeyParser publicKey()
    {
        return mode(Mode.Public);
    }
    
    public KeyParser privateKey()
    {
        return mode(Mode.Private);
    }
    
    public KeyParser secret()
    {
        return mode(Mode.Secret);
    }

    public KeyParser ensureMultiplicity(int multiplicity)
    {
        this.multiplicity = multiplicity;
        return this;
    }

    public KeyParser algorithm(@NonNull CryptAlgorithm algorithm)
    {
        this.algorithm = algorithm;
        return this;
    }

    public KeyParser fromUTF8(@NonNull String str)
    {
        return from(str, StandardCharsets.UTF_8);
    }

    public KeyParser from(@NonNull String str, @NonNull Charset charset)
    {
        return from(str.getBytes(charset));
    }

    public KeyParser from(@NonNull byte[] bytes)
    {
        bytes = binaryUtils.apply().ensureMultiplicity(multiplicity, bytes);

        ExceptionsUtils.IfNull(mode, IllegalStateException.class, "Key mode was not set!");

        switch (mode)
        {
            case Secret:
            {
                keySpec = new SecretKeySpec(bytes, algorithm.getName());
                break;
            }

            case Public:
            {
                keySpec = new X509EncodedKeySpec(bytes);
                break;
            }

            case Private:
            {
                keySpec = new PKCS8EncodedKeySpec(bytes);
                break;
            }
        }

        return this;
    }

    public <T extends Key> T get()
    {
        var exceptionsHandler = getExceptionsHandler();

        return exceptionsHandler.safeReturn(() -> {

            switch (mode)
            {
                case Private:
                {
                    var keyFactory = KeyFactory.getInstance(algorithm.getName());
                    return (T) keyFactory.generatePrivate(keySpec);
                }

                case Public:
                {
                    var keyFactory = KeyFactory.getInstance(algorithm.getName());
                    return (T) keyFactory.generatePublic(keySpec);
                }

                case Secret:
                {
                    return (T) keySpec;
                }
            }

            return null;
        }, null, "Error while creating public key");
    }
    
    public ExceptionsHandler getExceptionsHandler()
    {
        return exceptionsHandler.apply();
    }

    public static enum Mode
    {
        Public, Private, Secret
    }
}
