package ru.swayfarer.swl3.crypto;

import java.security.SecureRandom;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.crypto.algorithm.CryptAlgorithm;
import ru.swayfarer.swl3.crypto.base64.Base64Helper;
import ru.swayfarer.swl3.crypto.hash.MessageDigestBuilder;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.io.BinaryUtils;

@Data
@Accessors(chain = true)
public class CryptoUtils {

    public static CryptoUtils INSTANCE = new CryptoUtils();

    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
    
    public BinaryUtils binaryUtils = BinaryUtils.getInstance();
    
    public CipherBuilder cipher()
    {
        return new CipherBuilder(this, this::getExceptionsHandler);
    }
    
    public MessageDigestBuilder hash()
    {
        return new MessageDigestBuilder(this::getExceptionsHandler, this::getBinaryUtils);
    }
    
    public KeyBuilder key()
    {
        return new KeyBuilder(this::getExceptionsHandler, this::getBinaryUtils);
    }

    public Base64Helper.IModeSelectStage base64()
    {
        return new Base64Helper.Chain();
    }

    public SecretKey newRandomKey(@NonNull CryptAlgorithm algorithm)
    {
        return exceptionsHandler.safeReturn(() -> {
            KeyGenerator keyGenerator = KeyGenerator.getInstance(algorithm.toFullAlgorithmString());

            SecureRandom secureRandom = new SecureRandom();
            int keyBitSize = 256;
            keyGenerator.init(keyBitSize, secureRandom);

            SecretKey secretKey = keyGenerator.generateKey();
            
            return secretKey;
        }, null, "Error while creating a new key of", algorithm);
    }
    
    public SecretKey newKey(@NonNull byte[] bytes)
    {
        return new SecretKeySpec(bytes, "RawBytes");
    }

    public static CryptoUtils getInstance()
    {
        return INSTANCE;
    }
}
