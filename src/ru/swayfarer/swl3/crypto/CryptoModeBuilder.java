package ru.swayfarer.swl3.crypto;

import java.security.Key;
import java.security.KeyPair;

import javax.crypto.Cipher;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CryptoModeBuilder {
    
    @NonNull
    public CipherBuilder builder;
    
    public CryptoModeBuilder encrypt()
    {
        builder.setMode(Cipher.ENCRYPT_MODE);
        return this;
    }
    
    public CryptoModeBuilder decrypt()
    {
        builder.setMode(Cipher.DECRYPT_MODE);
        return this;
    }

    public CryptoModeBuilder pair()
    {
        builder.setMode(-1);
        return this;
    }
    
    public CipherBuilder by(@NonNull KeyPair key) 
    {
        builder.lastKeyObject = key;
        builder.key = builder.getMode() == Cipher.ENCRYPT_MODE ? key.getPrivate() : key.getPublic();
        return builder;
    }
    
    public CipherBuilder by(@NonNull Key key) 
    {
        builder.lastKeyObject = key;
        builder.key = key;
        return builder;
    }
}
