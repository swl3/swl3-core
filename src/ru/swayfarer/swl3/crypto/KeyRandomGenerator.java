package ru.swayfarer.swl3.crypto;

import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;

import javax.crypto.KeyGenerator;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.crypto.algorithm.CryptAlgorithm;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;

@Data
@Accessors(chain = true)
@SuppressWarnings("unchecked")
public class KeyRandomGenerator {

    @NonNull
    public IFunction0<ExceptionsHandler> exceptionsHandler;

    public SecureRandom secureRandom = new SecureRandom();
    
    public CryptAlgorithm algorithm;
    public int keySizeInBits;
    
    public <T extends Key> T gen()
    {
        var exceptionsHandler = getExceptionsHandler();

        return exceptionsHandler.safeReturn(() -> {
            var keyGen = KeyGenerator.getInstance(algorithm.getName());
            keyGen.init(keySizeInBits, secureRandom);
            return (T) keyGen.generateKey();
        }, null, "Error while generating random key pair");
    }
    
    public KeyPair genPair()
    {
        var exceptionsHandler = getExceptionsHandler();

        return exceptionsHandler.safeReturn(() -> {
            var keyGen = KeyPairGenerator.getInstance(algorithm.getName());
            keyGen.initialize(keySizeInBits, secureRandom);
            
            return keyGen.genKeyPair();
        }, null, "Error while generating random key pair");
    }
    
    public KeyRandomGenerator algorithm(@NonNull CryptAlgorithm algorithm)
    {
        this.algorithm = algorithm;
        return this;
    }

    public KeyRandomGenerator keySize(int keyBitsSize)
    {
        this.keySizeInBits = keyBitsSize;
        return this;
    }
    
    public ExceptionsHandler getExceptionsHandler()
    {
        return exceptionsHandler.apply();
    }
}
