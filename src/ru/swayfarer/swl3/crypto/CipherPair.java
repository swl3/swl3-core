package ru.swayfarer.swl3.crypto;

import javax.crypto.Cipher;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CipherPair {

    @NonNull
    public Cipher encrypt;
    
    @NonNull
    public Cipher decrypt;
}
