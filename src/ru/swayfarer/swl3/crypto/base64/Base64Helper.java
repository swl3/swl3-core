package ru.swayfarer.swl3.crypto.base64;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;

import java.util.Base64;

public class Base64Helper
{
    public static class Chain implements IResultSelectStage, ISourceSelectStage, IModeSelectStage{
        @Internal
        @NonNull
        public byte[] source;

        @Internal
        @NonNull
        public IFunction1<byte[], byte[]> resultProcessingFun;

        @Override
        public String asUTF8String()
        {
            return asString("UTF-8");
        }

        @SneakyThrows
        public String asString(String encoding)
        {
            return new String(asBytes(), encoding);
        }

        public byte[] asBytes()
        {
            return resultProcessingFun.apply(source);
        }

        @Override
        public IResultSelectStage fromUTF8String(String str)
        {
            return fromString(str, "UTF-8");
        }

        @SneakyThrows
        @Override
        public IResultSelectStage fromString(String str, String encoding)
        {
            return fromBytes(str.getBytes(encoding));
        }

        @Override
        public IResultSelectStage fromBytes(byte[] bytes)
        {
            if (bytes == null)
                bytes = new byte[0];

            this.source = bytes;
            return this;
        }

        @Override
        public ISourceSelectStage encode()
        {
            this.resultProcessingFun = Base64.getEncoder()::encode;
            return this;
        }

        @Override
        public ISourceSelectStage decode()
        {
            this.resultProcessingFun = Base64.getEncoder()::encode;
            return this;
        }
    }

    public static interface IModeSelectStage {
        public ISourceSelectStage encode();
        public ISourceSelectStage decode();
    }

    public static interface ISourceSelectStage {
        public IResultSelectStage fromUTF8String(String str);
        public IResultSelectStage fromString(String str, String encoding);
        public IResultSelectStage fromBytes(byte[] bytes);
    }

    public static interface IResultSelectStage {
        public String asUTF8String();
        public String asString(String encoding);
        public byte[] asBytes();
    }
}
