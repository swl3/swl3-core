package ru.swayfarer.swl3.crypto;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.io.BinaryUtils;
import ru.swayfarer.swl3.markers.Alias;

@Data
@Accessors(chain = true)
public class KeyBuilder {
    
    @NonNull
    public IFunction0<ExceptionsHandler> exceptionsHandler;

    @NonNull
    public IFunction0<BinaryUtils> binaryUtils;

    @Alias("of")
    public KeyParser parse()
    {
        return of();
    }
    
    public KeyParser of()
    {
        return new KeyParser(this::getExceptionsHandler, binaryUtils);
    }
    
    public KeyRandomGenerator random()
    {
        return new KeyRandomGenerator(this::getExceptionsHandler);
    }
    
    public ExceptionsHandler getExceptionsHandler()
    {
        return exceptionsHandler.apply();
    }
}
