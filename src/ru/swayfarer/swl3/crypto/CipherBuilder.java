package ru.swayfarer.swl3.crypto;

import java.security.Key;
import java.security.KeyPair;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.crypto.algorithm.CryptAlgorithm;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;

@Data
@Accessors(chain = true)
public class CipherBuilder {

    @NonNull
    public CryptoUtils cryptoUtils;
    
    public CryptAlgorithm algorithm;
    public int mode = -1;
    public Key key;
    
    public Object lastKeyObject;

    @NonNull
    public IFunction0<ExceptionsHandler> exceptionsHandler;
    
    public AlgorithmParameterSpec algorithmParameterSpec;

    public CryptoModeBuilder mode()
    {
        return new CryptoModeBuilder(this);
    }

    public CipherParamsBuilder param()
    {
        return new CipherParamsBuilder(this);
    }
    
    public CipherBuilder alghoritm(@NonNull CryptAlgorithm algorithm)
    {
        this.algorithm = algorithm;
        return this;
    }

    public ExceptionsHandler getExceptionsHandler()
    {
        return exceptionsHandler.apply();
    }

    public CipherPair getPair()
    {
        Cipher encrypt = null;
        Cipher decrypt = null;
        
        if (lastKeyObject instanceof Key)
        {
            var builder = mode().encrypt().by((Key) lastKeyObject);
            encrypt = builder.get();
            
            builder = builder.mode().decrypt().by((Key) lastKeyObject);
            decrypt = builder.get();
        }
        else if (lastKeyObject instanceof KeyPair)
        {
            var builder = mode().encrypt().by((KeyPair) lastKeyObject);
            encrypt = builder.get();
            
            builder = builder.mode().decrypt().by((KeyPair) lastKeyObject);
            decrypt = builder.get();
        }
        
        return new CipherPair(encrypt, decrypt);
    }
    
    public Cipher get()
    {
        var exceptionsHandler = getExceptionsHandler();

        return exceptionsHandler.safeReturn(() -> {

            ExceptionsUtils.If(mode == -1, IllegalStateException.class, "Mode was not set!");
            ExceptionsUtils.IfNull(algorithm, IllegalStateException.class, "Algorithm was not set!");
            ExceptionsUtils.IfNull(key, IllegalStateException.class, "Key was not set!");

            var preCreateEvent = CipherEvent.builder()
                    .cipherBuilder(this)
                    .cipher(null)
                    .build()
            ;

            algorithm.onPreCreate(preCreateEvent);

            var cipher = Cipher.getInstance(algorithm.toFullAlgorithmString());

            var postCreateEvent = CipherEvent.builder()
                    .cipherBuilder(this)
                    .cipher(cipher)
                    .build()
            ;

            algorithm.onPostCreate(postCreateEvent);

            if (algorithmParameterSpec != null)
            {
                cipher.init(mode, key, algorithmParameterSpec);
            }
            else
            {
                cipher.init(mode, key);
            }

            return cipher;
        }, null, "Error while creating cipher");
    }
}
