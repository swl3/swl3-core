package ru.swayfarer.swl3.math;

import lombok.var;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

public class MathUtils {

	public static MathUtils INSTANCE = new MathUtils();

	public ReflectionsUtils reflectionsUtils = ReflectionsUtils.getInstance();
	
	public int getNearestPowerOf2Value(int num)
	{
		var pow = getNearestPowerOf2(num);
		return (int) Math.pow(2, pow);
	}
	
	public int getNearestPowerOf2(int num)
	{
		int pow = 1;
		int n = 2;
		
		while (true)
		{
			if (n < 0)
				return -1;
			
			if (n > num)
			{
				return pow;
			}
			
			pow ++;
			n *= 2;
		}
	}

	public Object getNumberValue(Class<?> type, Number num)
	{
		var types = reflectionsUtils.types();

		if (types.equalsIgnoreBoxing(type, byte.class))
		{
			return num.byteValue();
		}
		if (types.equalsIgnoreBoxing(type, short.class))
		{
			return num.shortValue();
		}
		if (types.equalsIgnoreBoxing(type, int.class))
		{
			return num.intValue();
		}
		if (types.equalsIgnoreBoxing(type, long.class))
		{
			return num.longValue();
		}
		else if (types.equalsIgnoreBoxing(type, float.class))
		{
			return num.floatValue();
		}
		else if (types.equalsIgnoreBoxing(type, double.class))
		{
			return num.doubleValue();
		}

		ExceptionsUtils.throwException(new IllegalArgumentException("Type " + type + " is not a number type!"));
		return null;
	}

	public double getDistance(double x1, double y1, double z1, double x2, double y2, double z2)
	{
		double f = (float)(x1 - x2);
		double f1 = (float)(y1 - y2);
		double f2 = (float)(z1 - z2);
		return Math.sqrt(f * f + f1 * f1 + f2 * f2);
	}
	
	public static MathUtils getInstance()
	{
		return INSTANCE;
	}
}
