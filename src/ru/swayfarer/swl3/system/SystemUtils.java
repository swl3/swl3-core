package ru.swayfarer.swl3.system;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.LibTags.Exceptions;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.StringValue;
import ru.swayfarer.swl3.string.converters.StringConverters;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@Getter
@Setter
@Accessors(chain = true)
public class SystemUtils {

	public static SystemUtils INSTANCE = new SystemUtils();

	public static AtomicInteger nextSystemUtilsShutdownThreadID = new AtomicInteger();
	public AtomicBoolean isShutdownEventSubscribed = new AtomicBoolean();
	public IObservable<Void> eventShutdown = Observables.createObservable();

	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	
	public StringConverters propertyConverters = new StringConverters()
			.setExceptionsHandler(exceptionsHandler)
			.defaultTypes()
	;

	@Internal
	@NonNull
	public ExtendedList<IFunction0NoR> ansiOutputEnablers = new ExtendedList<>();

	public SystemUtils()
	{
		if (isWindows())
		{
			var isWindowsRegApplied = new AtomicBoolean();
			ansiOutputEnablers.add(() -> {
				if (!isWindowsRegApplied.get())
				{
					exceptionsHandler.handle()
							.message("Error while trying to enable ansi colors output in windows cmd")
							.tag(Exceptions.tagTryEnableAnsiOutput)
							.tag(Exceptions.tagShortPrint)
							.start(() -> {
								var proc = Runtime.getRuntime().exec("reg add HKCU\\Console /f /v VirtualTerminalLevel /t REG_DWORD /d 1");
								proc.waitFor();
							})
					;

					isWindowsRegApplied.set(true);
				}
			});
		}
	}

	public void addShutdownHook(IFunction0NoR fun)
	{
		if (!isShutdownEventSubscribed.get())
		{
			Runtime.getRuntime().addShutdownHook(new Thread(() -> eventShutdown.next(null), "SystemUtilsShutdownHook-" + nextSystemUtilsShutdownThreadID.getAndIncrement()));
			isShutdownEventSubscribed.set(true);
		}

		eventShutdown.subscribe().by(fun);
	}
	
	/** Получить кол-во ядер процессора */
	public int getCpuCoresCount()
	{
		return Runtime.getRuntime().availableProcessors();
	}
	
	public ExtendedList<URL> getClasspathAndClassloaderURLs()
	{
	    var ret = new ExtendedList<URL>();
        var classLoader = Thread.currentThread().getContextClassLoader();
        
        if (classLoader instanceof URLClassLoader)
        {
            for (var url : ((URLClassLoader) classLoader).getURLs())
            {
                ret.add(url);
            }
        }
        
        ret.addAll(getClasspathURLs());
        
        return ret;
	}
	
	/** Получить все элементы класпаса */
	public List<String> getClasspath()
	{
		var ret = new ArrayList<String>();
		var classpathEntries = System.getProperty("java.class.path").split(File.pathSeparator);
		
		for (var classPathEntry : classpathEntries)
			ret.add(classPathEntry);
		
		return ret;
	}

	public void tryToEnableConsoleColors()
	{
		ansiOutputEnablers.each(IFunction0NoR::apply);
	}
	
	/** Получит все элементы класпаса как URL */
	public List<URL> getClasspathURLs()
	{
		return exceptionsHandler.safeReturn(() -> {
			var classpath = getClasspath();
			var ret = new ArrayList<URL>();
			
			for (var entry : classpath)
			{
				if (!entry.endsWith(".jar"))
					entry += "/";
				
				ret.add(new URL("file:" + entry));
			}
			
			return ret;
		}, new ArrayList<>(), "Error while getting classpath urls");
	}
	
	/** Является ли система 32-bit'ной? */
	public boolean is32bit()
	{
		return !is64bit();
	}
	
	/** Является ли система 64-bit'ной? */
	public boolean is64bit()
	{
		return System.getProperty("sun.arch.data.model").equals("64");
	}
	
	/** Является ли система Windows? */
	public boolean isWindows()
	{
		return System.getProperty("os.name").toLowerCase().contains("win");
	}
	
	/** Является ли система Mac? */
	public boolean isMac()
	{
		return System.getProperty("os.name").toLowerCase().contains("mac");
	}
	
	/** Является ли система Linux? */
	public boolean isLinux()
	{
		return !isWindows() && !isMac();
	}
	
	public String getOS()
	{
	    return isWindows() ? "windows" : isMac() ? "mac" : "linux";
	}
	
	/**
	 * Получть значение environment'а (переменные окружения)
	 * @param envName Имя переменной окружения
	 * @return Конвертируемая строка, которая позволит представить значение как понадобится
	 */
	public StringValue findEnv(String envName)
	{
		return findEnv(envName, null);
	}
	
	/**
	 * Получть значение environment'а (переменные окружения)
	 * @param envName Имя переменной окружения
	 * @param defaultValue Значение, которое будет использовано, если не найдется 
	 * @return Конвертируемая строка, которая позволит представить значение как понадобится
	 */
	public StringValue findEnv(String envName, String defaultValue)
	{
		return new StringValue(propertyConverters, () -> {
			var envValue = System.getenv(envName);
			return envValue == null ? defaultValue : envValue;
		})
				.appendExceptionsHandler(exceptionsHandler)
				.updateValue()
		;
	}
	
	/**
	 * Получть значение системных проперти (при старте передаются через <code>-Dname=value</code>)
	 * @param propertyName Имя проперти
	 * @return Конвертируемая строка, которая позволит представить значение как понадобится
	 */
	public StringValue findProperty(String propertyName)
	{
		return findProperty(propertyName, null);
	}
	
	/**
	 * Получть значение системных проперти (при старте передаются через <code>-Dname=value</code>)
	 * @param propertyName Имя проперти
	 * @param defaultValue Значение, которое будет использовано, если не найдется 
	 * @return Конвертируемая строка, которая позволит представить значение как понадобится
	 */
	public StringValue findProperty(String propertyName, String defaultValue)
	{
		return new StringValue(propertyConverters, () -> System.getProperty(propertyName, defaultValue))
				.appendExceptionsHandler(exceptionsHandler)
				.updateValue()
		;
	}

	public ExtendedOptional<StringValue> findAppProperty(String propertyName)
	{
		var envNames = new ExtendedList<String>();
		envNames.add(propertyName);
		envNames.add(propertyName.replace('.', '_'));
		envNames.add(propertyName.replace('.', '-'));

		envNames.copy().each((name) -> {
			envNames.add(name.toLowerCase());
			envNames.add(name.toUpperCase());
		});

		for (var envName : envNames)
		{
			var value = System.getProperty(envName);
			if (StringUtils.isBlank(value))
			{
				value = System.getenv(envName);
			}

			if (!StringUtils.isBlank(value))
			{
				var finalValue = value;
				var result = new StringValue(propertyConverters, () -> finalValue).appendExceptionsHandler(exceptionsHandler);
				return ExtendedOptional.of(result);
			}
		}

		return ExtendedOptional.empty();
	}

	public <T> ExtendedList<T> findServices(Class<T> serviceClass)
	{
		return CollectionsSWL.list(ServiceLoader.load(serviceClass));
	}
	
	public static SystemUtils getInstance()
	{
		return INSTANCE;
	}
}
