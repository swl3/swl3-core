package ru.swayfarer.swl3.threads.lock;

import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;

public class NotifyLock {

    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
    public volatile Object lock = new Object();
    
    public void waitFor()
    {
        synchronized (lock)
        {
            exceptionsHandler.safe(lock::wait, "Error while trying to wait thread", Thread.currentThread(), "on object", lock);
        }
    }
    
    public void waitFor(long timeout)
    {
        synchronized (lock)
        {
            exceptionsHandler.safe(() -> lock.wait(timeout), "Error while trying to wait thread", Thread.currentThread(), "on object", lock);
        }
    }
    
    public void notifyLock()
    {
        synchronized (lock)
        {
            exceptionsHandler.safe(lock::notify, "Error while notifying lock object", lock);
        }
    }

    public void notifyLockAll()
    {
        synchronized (lock)
        {
            exceptionsHandler.safe(lock::notifyAll, "Error while notifying lock object", lock);
        }
    }
    
}
