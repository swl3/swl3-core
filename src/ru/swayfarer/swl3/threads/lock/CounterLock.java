package ru.swayfarer.swl3.threads.lock;

import java.util.concurrent.atomic.AtomicInteger;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.markers.Alias;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@SuppressWarnings("unchecked")
public class CounterLock {

    public NotifyLock lock = new NotifyLock();
    public AtomicInteger counter = new AtomicInteger(0);
    
    public CounterLock(int initialValue)
    {
        setCounter(initialValue);
    }
    
    public void waitFor()
    {
        if (counter.get() <= 0)
            return;
        
        lock.waitFor();
    }
    
    public void waitFor(long timeout)
    {
        if (counter.get() <= 0)
        return;
    
        lock.waitFor(timeout);
    }
    
    public void increment()
    {
        counter.incrementAndGet();
    }
    
    public void reduce()
    {
        if (counter.decrementAndGet() == 0)
            lock.notifyLockAll();
    }
    
    @Alias("setCounter")
    public <T extends CounterLock> T setValue(int value) 
    {
        return (T) setCounter(value);
    }
    
    public <T extends CounterLock> T setCounter(int value) 
    {
        this.counter.set(value);
        return (T) this;
    }
}
