package ru.swayfarer.swl3.threads;

public class ManagedThread extends Thread {

    public ThreadSettings threadSettings = new ThreadSettings();
    
    public ManagedThread()
    {
        super();
    }

    public ManagedThread(Runnable target, String name)
    {
        super(target, name);
    }

    public ManagedThread(Runnable target)
    {
        super(target);
    }

    public ManagedThread(String name)
    {
        super(name);
    }

    public ManagedThread(ThreadGroup group, Runnable target, String name, long stackSize)
    {
        super(group, target, name, stackSize);
    }

    public ManagedThread(ThreadGroup group, Runnable target, String name)
    {
        super(group, target, name);
    }

    public ManagedThread(ThreadGroup group, Runnable target)
    {
        super(group, target);
    }

    public ManagedThread(ThreadGroup group, String name)
    {
        super(group, name);
    }
    
    @Override
    public synchronized void start()
    {
        super.start();
    }
}
