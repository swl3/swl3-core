package ru.swayfarer.swl3.threads;

import ru.swayfarer.swl3.observable.IObservable;

public interface ITask {

    public long getDuration();
    public EnumTaskState getState();
    public IObservable<ITask> eventStart();
    public IObservable<ITask> eventEnd();
    public <T> T getResult();
    
}
