package ru.swayfarer.swl3.threads;

public enum EnumTaskState
{
    Waiting,
    Started,
    Ended
}
