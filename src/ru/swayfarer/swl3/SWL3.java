package ru.swayfarer.swl3;

import lombok.SneakyThrows;
import lombok.var;

public class SWL3 {

    @SneakyThrows
    public static void main(String[] args)
    {
        var method = ParentClass.class.getDeclaredMethod("helloWorld");
        method.setAccessible(true);

        var child = new ChildClass();
        method.invoke(child);
    }

    public static class ChildClass extends ParentClass{
        public void helloWorld()
        {
            System.out.println("Child!");
        }
    }

    public static class ParentClass {
        public void helloWorld()
        {
            System.out.println("Parent!");
        }
    }

    public static enum NewENUM {
        HELLO,
        WORLD
    }
    
}
