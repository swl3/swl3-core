package ru.swayfarer.swl3.version;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import lombok.var;
import ru.swayfarer.swl3.string.StringUtils;

@Data
@Accessors(chain = true)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class Version3i implements Comparable<Version3i> {
	public int major;
	public int minor;
	public int patch;
	
	@Builder.Default
	public String label = "";
	
	public static Version3i of(String str)
	{
		if (StringUtils.isBlank(str))
		{
			return null;
		}
		
		var split = str.split("\\.");
		var rad = 10;
		
		var ret = new Version3i();
		ret.major = Integer.parseInt(split[0], rad);
		
		if (split.length > 1)
			ret.minor = Integer.parseInt(split[1], rad);
		
		if (split.length > 2)
			ret.patch = Integer.parseInt(split[2], rad);
		
		if (split.length > 3)
			ret.label = StringUtils.concatWith("", 3, -1, (Object[]) split);
		
		return ret;
	}
	
	@Override
	public int compareTo(Version3i o) {
		if (o == null)
			return 1;
		
		int i = 0;
		
		if ((i = major - o.major) != 0)
			return i;
		
		if ((i = minor - o.minor) != 0)
			return i;
		
		return patch - o.patch;
	}
	
	public String toString()
	{
		return major + "." + minor + "." + patch;
	}
}
