package ru.swayfarer.swl3.markers;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

@Retention(RUNTIME)
public @interface HierarchySensitive {

}
