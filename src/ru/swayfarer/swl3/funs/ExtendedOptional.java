package ru.swayfarer.swl3.funs;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Stream;

import lombok.Data;
import lombok.NonNull;
import lombok.SneakyThrows;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

@Data
@SuppressWarnings("unchecked")
public class ExtendedOptional<Element_Type>
{

	@NonNull
	public Optional<Element_Type> wrappedOptional;

	public ExtendedOptional<Element_Type> wrap(@NonNull Optional<Element_Type> wrapped)
	{
		this.wrappedOptional = wrapped;
		return this;
	}

	public Element_Type get()
	{
		return wrappedOptional.get();
	}
	
	public Element_Type orNull()
	{
		return orElse(null);
	}
	
	public ExtendedOptional<Element_Type> set(Element_Type elem)
	{
		return wrap(Optional.ofNullable(elem));
	}

	public boolean isPresent()
	{
		return wrappedOptional.isPresent();
	}

	public boolean isEmpty()
	{
		return !isPresent();
	}

	public void ifPresent(@NonNull IFunction1NoR<Element_Type> fun)
	{
		wrappedOptional.ifPresent(fun.asJavaConsumer());
	}

	public void ifPresentOrElseThrow(@NonNull IFunction1NoR<Element_Type> fun)
	{
		fun.apply(orElseThrow());
	}

	public void ifPresentOrElse(@NonNull IFunction1NoR<Element_Type> fun, @NonNull IFunction0NoR ifNotPresentFun)
	{
		if (isPresent())
		{
			fun.apply(get());
		}
		else
		{
			ifNotPresentFun.apply();
		}
	}

	public ExtendedOptional<Element_Type> filter(@NonNull IFunction1<? super Element_Type, Boolean> filter)
	{
		return wrap(wrappedOptional.filter(filter.asJavaPredicate()));
	}

	public <New_Element_Type> ExtendedOptional<New_Element_Type> map(@NonNull IFunction1<? super Element_Type, ? extends New_Element_Type> mapper)
	{
		if (!isPresent())
		{
			return empty();
		}
		else
		{
			return wrap(mapper.apply(get()));
		}
	}

	public <New_Element_Type> ExtendedOptional<New_Element_Type> flatMap(@NonNull IFunction1<? super Element_Type, ? extends ExtendedOptional<? extends New_Element_Type>> mapper)
	{
		if (!isPresent())
		{
			return empty();
		}
		else
		{
			return (ExtendedOptional<New_Element_Type>) mapper.apply(get());
		}
	}

	public ExtendedOptional<Element_Type> or(@NonNull IFunction0<? extends ExtendedOptional<? extends Element_Type>> supplier)
	{
		if (isPresent())
		{
			return this;
		}
		else
		{
			return (ExtendedOptional<Element_Type>) supplier.apply();
		}
	}
	
	public ExtendedStream<Element_Type> stream()
	{
		if (!isPresent())
		{
			return new ExtendedStream<>(Stream::empty);
		}
		else
		{
			return ExtendedStream.of(get());
		}
	}

	public <R> ExceptionOptional<R> wrap(R value)
	{
		return new ExceptionOptional<>(Optional.ofNullable(value));
	}

	public Element_Type orElse(Element_Type other)
	{
		return wrappedOptional.orElse(other);
	}

	public Element_Type orElseGet(@NonNull IFunction0<? extends Element_Type> supplier)
	{
		return wrappedOptional.orElseGet(supplier.asJavaSupplier());
	}

	public Element_Type orElseThrow()
	{
		return orElseThrow(() -> new NoSuchElementException("No value present"));
	}

	@SneakyThrows
	public <Exception_Type extends Throwable> Element_Type orElseThrow(@NonNull IFunction0<? extends Exception_Type> exceptionSupplier)
	{
		return wrappedOptional.orElseThrow(exceptionSupplier.asJavaSupplier());
	}
	
	public static <T> ExtendedOptional<T> ofNonNull(@NonNull T value)
	{
		return new ExtendedOptional<T>(Optional.of(value));
	}

	public static <T> ExtendedOptional<T> of(T value)
	{
		return new ExtendedOptional<T>(Optional.ofNullable(value));
	}

	public static <T> ExtendedOptional<T> empty()
	{
		return new ExtendedOptional<T>(Optional.empty());
	}
}
