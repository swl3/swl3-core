package ru.swayfarer.swl3.funs;

public interface IUnsafeRunnable {

	public void apply() throws Throwable;
	
}
