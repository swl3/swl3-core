package ru.swayfarer.swl3.funs.chain;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

@Data
@Accessors(chain = true)
public class FunctionsChain1<Argument_Type, Return_Type>{

    public IFunction0<Return_Type> defaultValueFun = () -> null;
    public IFunction1<Return_Type, Boolean> filterFun = (e) -> e != null;
    public ExtendedList<WeightedEntry<IFunction1<Argument_Type, Return_Type>>> funs = new ExtendedList<>();
    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
    
    public Return_Type apply(Argument_Type argument)
    {
        for (var entry : funs)
        {
            var fun = entry.getValue();
            
            try
            {
                var ret = fun.apply(argument);
                
                if (isAcceptsFilter(ret))
                {
                    return ret;
                }
            }
            catch (Throwable e)
            {
                exceptionsHandler.handle(e, "Error while handling function " + fun + " in chain");
            }
        }
        
        return defaultValueFun.apply();
    }
    
    public FunctionsChain1<Argument_Type, Return_Type> filter(IFunction1<Return_Type, Boolean> fun)
    {
        this.filterFun = fun;
        return this;
    }
    
    public FunctionsChain1<Argument_Type, Return_Type> add(IFunction1<Argument_Type, Return_Type> fun)
    {
        this.funs.add(new WeightedEntry<>(fun));
        sortFuns();
        return this;
    }
    
    public FunctionsChain1<Argument_Type, Return_Type> add(IFunction1<Argument_Type, Return_Type> fun, int priority)
    {
        this.funs.add(new WeightedEntry<>(fun).setWeight(priority));
        sortFuns();
        return this;
    }
    
    public FunctionsChain1<Argument_Type, Return_Type> sortFuns()
    {
        this.funs.sort((e1, e2) -> e1.getWeight() - e2.getWeight());
        return this;
    }
    
    public boolean isAcceptsFilter(Return_Type arg)
    {
        return filterFun == null || filterFun.apply(arg);
    }
    
    @Data
    @Accessors(chain = true)
    public static class WeightedEntry<T> {
        public int weight;
        
        @NonNull
        T value;
    }
}
