package ru.swayfarer.swl3.funs;

public interface IUnsafeRunnableWithReturn<T> {

	public T apply() throws Throwable;
	
}
