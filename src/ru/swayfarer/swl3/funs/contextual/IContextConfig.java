package ru.swayfarer.swl3.funs.contextual;

import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;

import java.util.Set;

public interface IContextConfig
{
    public IContextConfig withPrefix(String prefix);

    public String getPrefix();
    public <T> ExtendedOptional<T> getValue(String key);
    public void putValue(String key, Object value);
    public <T> T getValue(String key, IFunction0<T> newValueFun);
    public void clear();

    public Set<String> getKeys();

    public static IContextConfig getConfig()
    {
        return ContextConfigInternal.getConfig();
    }

    public static IContextConfig byPrefix(String prefix)
    {
        return getConfig().withPrefix(prefix);
    }
}
