package ru.swayfarer.swl3.funs.contextual;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.markers.Internal;

import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
@Accessors(chain = true)
public class ContextConfig implements IContextConfig
{
    @Internal
    @NonNull
    public ExtendedMap<String, Object> properties;

    public String prefix;

    public ContextConfig()
    {
        this("");
    }

    public ContextConfig(String prefix)
    {
        this(new ExtendedMap<>(), prefix);
    }

    public ContextConfig(@NonNull ExtendedMap<String, Object> properties, String prefix)
    {
        this.properties = properties;
        this.prefix = prefix;
    }

    @Override
    public IContextConfig withPrefix(String prefix)
    {
        return new ContextConfig(properties, this.prefix + prefix);
    }

    @Override
    public String getPrefix()
    {
        return prefix;
    }

    @Override
    public <T> ExtendedOptional<T> getValue(String key)
    {
        return properties.getOptionalValue(prefix.concat(key));
    }

    @Override
    public void putValue(String key, Object value)
    {
        properties.put(key, value);
    }

    @Override
    public void clear()
    {
        ExtendedStream.of(getKeys())
                .each(properties::remove)
        ;
    }

    @Override
    public <T> T getValue(String key, IFunction0<T> newValueFun)
    {
        return properties.getOrCreate(key, newValueFun);
    }

    @Override
    public Set<String> getKeys()
    {
        return ExtendedStream.of(properties.keySet())
            .filter((s) -> s.startsWith(prefix))
            .collect(Collectors.toSet())
        ;
    }
}
