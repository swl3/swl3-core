package ru.swayfarer.swl3.funs.contextual;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.thread.ThreadsUtils;

@Getter
@Setter
@Accessors(chain = true)
public class ContextConfigInternal
{
    @Internal
    @NonNull
    public static IFunction0<IContextConfig> contextConfig = ThreadsUtils.threadLocal(ContextConfig::new);

    public static IContextConfig getConfig()
    {
        return contextConfig.apply();
    }
}
