package ru.swayfarer.swl3.funs;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.markers.Internal;

import java.util.Optional;

@Getter
@Setter
@Accessors(chain = true)
public class ExceptionOptional<T> extends ExtendedOptional<T>
{
    @Internal
    public Throwable exception;

    public ExceptionOptional(@NonNull Optional<T> wrappedOptional)
    {
        super(wrappedOptional);
    }

    @SneakyThrows
    @Override
    public T orElseThrow()
    {
        if (exception != null)
            throw exception;

        return super.orElseThrow();
    }

    public static <T> ExceptionOptional<T> empty()
    {
        return ofAction(() -> null);
    }

    public static <T> ExceptionOptional<T> ofAction(IFunction0<T> fun)
    {
        if (fun == null)
            return ofThrowable(null);

        try
        {
            return new ExceptionOptional<>(Optional.ofNullable(fun.apply()));
        }
        catch (Throwable e)
        {
            return ofThrowable(e);
        }
    }

    public static <T> ExceptionOptional<T> ofThrowable(Throwable e)
    {
        return new ExceptionOptional<T>(Optional.empty()).setException(e);
    }
}
