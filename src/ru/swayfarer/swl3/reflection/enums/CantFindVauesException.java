package ru.swayfarer.swl3.reflection.enums;

public class CantFindVauesException extends RuntimeException
{
    public CantFindVauesException()
    {
    }

    public CantFindVauesException(String message)
    {
        super(message);
    }

    public CantFindVauesException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public CantFindVauesException(Throwable cause)
    {
        super(cause);
    }
}
