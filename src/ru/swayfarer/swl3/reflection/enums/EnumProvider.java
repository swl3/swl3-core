package ru.swayfarer.swl3.reflection.enums;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.thread.ThreadsUtils;

import java.lang.reflect.Array;
import java.lang.reflect.Field;

@Getter
@Setter
@Accessors(chain = true)
public class EnumProvider<Enum_Type>
{
    public ReflectionsUtils reflectionsUtils;
    public Class<Enum_Type> enumClass;
    public Field valuesField;

    public EnumProvider(ReflectionsUtils reflectionsUtils, Class<Enum_Type> enumClass)
    {
        this.reflectionsUtils = reflectionsUtils;
        this.enumClass = enumClass;

        var fieldFilter = reflectionsUtils.fields().filters();

        valuesField = reflectionsUtils.fields().stream(enumClass)
                .filter(fieldFilter.name().equal("$VALUES", "ENUM$VALUES"))
                .findFirst()
                .orElseThrow(() -> new CantFindVauesException("Can't get VALUES field in enum class" + enumClass.getName()))
        ;
    }

    public Enum_Type newEnum(Object... args)
    {
        var constructorFilters = reflectionsUtils.constructors().filtering();

        var constructor = reflectionsUtils.constructors().stream(enumClass)
                .filter(constructorFilters.params().canAcceptValues(args))
                .findFirst()
                .orNull()
        ;

        ExceptionsUtils.IfNull(constructor, IllegalArgumentException.class, "Can't find constructor that accepts this args!");

        var instance = (Enum_Type) reflectionsUtils.reflectionsFactory().invokeConstructor(constructor, args);
        insertIntoValues(instance);

        return (Enum_Type) instance;
    }

    @SneakyThrows
    public void insertIntoValues(Enum_Type value)
    {
        if (valuesField != null)
        {
            var lock = getInsertionLock();
            synchronized (lock)
            {
                var values = (Enum_Type[]) valuesField.get(null);
                var length = values.length;

                System.out.println(length);

                Enum_Type[] newValues = (Enum_Type[]) Array.newInstance(enumClass, length + 1);

                System.arraycopy(values, 0, newValues, 0, length);
                newValues[length] = value;

                valuesField.set(null, newValues);
            }
        }
    }

    public Object getInsertionLock()
    {
        return ThreadsUtils.getGlobalLock("swl3_enum_helper_ins_" + enumClass.getName());
    }
}
