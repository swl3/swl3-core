package ru.swayfarer.swl3.reflection;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.obf.IObfuscationHelper;

import java.lang.reflect.Member;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class ObfuscationReflectionsUtils
{
    @Internal
    public ExtendedList<IObfuscationHelper> obfuscationHelpers = new ExtendedList<>();

    @Internal
    public ReflectionsUtils reflectionsUtils;

    public ExtendedList<String> getNames(Member member)
    {
        var ret = CollectionsSWL.list(member.getName());

        for (var obfuscationHelper : obfuscationHelpers)
            ret.addAll(obfuscationHelper.getObfMemberName(member));

        ret.distinct();

        return ret;
    }

    public ObfuscationReflectionsUtils addHelper(IObfuscationHelper helper)
    {
        if (helper == null)
            return this;

        obfuscationHelpers.add(helper);

        return this;
    }
}
