package ru.swayfarer.swl3.reflection;

import lombok.SneakyThrows;

import java.util.Optional;

public class ClassUtil
{
    @SneakyThrows
    public static Class<?> forName(String className)
    {
        return Class.forName(
                className,
                false, Optional.ofNullable(Thread.currentThread().getContextClassLoader()).orElse(ClassUtil.class.getClassLoader())
        );
    }
}
