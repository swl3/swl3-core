package ru.swayfarer.swl3.reflection;

import java.lang.reflect.InvocationTargetException;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandleBuilder;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

@Data
@Accessors(chain = true, fluent = true)
@Builder
@AllArgsConstructor @NoArgsConstructor
@SuppressWarnings("unchecked")
public class MethodInvocationResult {

	public boolean isInvoked;
	
	public Object returnValue;
	
	public Throwable methodException;
	
	public MethodInvocationResult ifExecuted(@NonNull IFunction1NoR<Object> fun)
	{
		if (isInvoked)
			fun.apply(returnValue);
		return this;
	}
	
	public MethodInvocationResult ifNotExecuted(@NonNull IFunction0NoR fun)
	{
		if (!isInvoked)
			fun.apply();
		
		return this;
	}
	
	@SneakyThrows
	public <T> T orThrow()
	{
	    if (methodException != null)
	        throw methodException;
	    
	    return (T) returnValue;
	}

	public <T> T orHandle(@NonNull ExceptionsHandler exceptionsHandler, IFunction1NoR<ExceptionsHandleBuilder> cfg)
	{
		if (methodException != null)
		{
			var handleBuilder = exceptionsHandler.handle();
			cfg.apply(handleBuilder);
			handleBuilder.throwable(methodException);
			handleBuilder.start();
			return null;
		}

		return (T) returnValue;
	}
	
	public <T> T orHandle(@NonNull ExceptionsHandler exceptionsHandler, Object... message)
    {
	    if (methodException != null)
	    {
	        exceptionsHandler.handle(methodException, message);
	        return null;
	    }
        
        return (T) returnValue;
    }
	
	public MethodInvocationResult ifException(@NonNull IFunction1NoR<Throwable> fun)
	{
		if (methodException != null)
		{
			var exception = methodException;
			
			if (exception instanceof InvocationTargetException) 
			{
				exception = exception.getCause();
			}
			
			fun.apply(exception);
		}
		return this;
	}
	
	public static MethodInvocationResult notFound()
	{
		return new MethodInvocationResult();
	}
}
