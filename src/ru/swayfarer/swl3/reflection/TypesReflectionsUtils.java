package ru.swayfarer.swl3.reflection;

import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.ReflectPermission;
import java.security.AllPermission;
import java.security.CodeSource;
import java.security.Permissions;
import java.security.ProtectionDomain;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.LibTags.Exceptions;
import ru.swayfarer.swl3.funs.ExceptionOptional;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.objects.EqualsUtils;
import ru.swayfarer.swl3.reflection.filers.TypesFilters;

@Accessors(chain = true)
public class TypesReflectionsUtils {
	
	@Internal
	@Setter
	public ReflectionsUtils reflectionsUtils;
	
	/** Типы, считающиеся эквивалентными */
	@Internal
	public List<List<Class<?>>> equalTypes = new ArrayList<>();
	
	/** Примитивные типы */
	@Internal
	public List<Class<?>> privitiveTypes = new ArrayList<>();
	
	/** Цепочка возрастания прититивных типов (int можно передать в double, т.е. int до double) */
	@Internal
	public List<Class<?>> numbersExpansionChain = Arrays.asList(byte.class, short.class, int.class, long.class, float.class, double.class);
	
	/** Цепочка возрастания прититивных типов (int можно передать в double, т.е. int до double) */
	@Internal
	public List<Class<?>> numbersWrappersExpansionChain = Arrays.asList(Byte.class, Short.class, Integer.class, Long.class, Float.class, Double.class);

	@Internal
	public ExtendedMap<Class<?>, Class<?>> primitiveToWrapper = CollectionsSWL.exMap(
			byte.class, Byte.class,
			short.class, Short.class,
			int.class, Integer.class,
			long.class, Long.class,
			float.class, Float.class,
			double.class, Double.class,
			char.class, Character.class,
			boolean.class, Boolean.class
	);

	/**
	 * Зарегистрировать классы, считающиейся примитивами
	 * @param classes Классы
	 * @return this
	 */
	public TypesReflectionsUtils primitives(Class<?>... classes)
	{
		this.privitiveTypes.addAll(Arrays.asList(classes));
		return this;
	}
    
    /**
     * Получить цепочку наследования класса <br>
     * Классы в списке будут расположены от самого первого, исключая {@link Object} к переданному. 
     * @param cl Сканируемый класс
     * @return Список классов
     */
    @Internal
    public ExtendedList<Class<?>> findInheritanceChain(@NonNull Class<?> cl)
    {
        var ret = new ExtendedList<Class<?>>();
        
        var parent = cl;
        
        while (parent != null && parent != Object.class)
        {
            ret.add(0, parent);
            parent = parent.getSuperclass();
        }
        
        return ret;
    }
    
    public int getInheritanceOffset(Class<?> start, Class<?> end)
    {
        var chain = findInheritanceChain(start);
        
        int offset = chain.size() - 1;
        
        for (var current : chain)
        {
            if (current.equals(end))
                return offset;
            
            // Есть ли интерфейс у класса
            if (ExtendedStream.of(current.getInterfaces()).anyMatches((elem) -> end.equals(elem)))
            {
                return offset + 1;
            }
            
            current = current.getSuperclass();
            
            if (current != null)
            {
                if (end.equals(current))
                    return offset;
            }
            
            offset --;
        }
        
        return -1;
    }
    
    /**
     * Загрузить класс напрямую из байтов
     * @param name Имя класса
     * @param classBytes Байты класса
     * @return Загруженный класс или <code>null</code>, если не найдется
     */
    public Class<?> defineClass(@NonNull String name, @NonNull byte[] classBytes)
    {
        return defineClass(name, classBytes, getDomain(null), Thread.currentThread().getContextClassLoader());
    }

    public Class<?> loadOrDefine(@NonNull String name, @NonNull byte[] classBytes)
	{
		try
		{
			var result = ClassUtil.forName(name);
			return result;
		}
		catch (Throwable e)
		{
			// Nope!
		}

		return defineClass(name, classBytes);
	}
	
	/**
	 * Загрузить класс напрямую из байтов
	 * @param name Имя класса
	 * @param classBytes Байты класса
	 * @param protectionDomain {@link ProtectionDomain} загружаемого класса
	 * @return Загруженный класс или <code>null</code>, если не найдется
	 */
	public Class<?> defineClass(@NonNull String name, @NonNull byte[] classBytes, @NonNull ProtectionDomain protectionDomain)
	{
		return defineClass(name, classBytes, protectionDomain, Thread.currentThread().getContextClassLoader());
	}
	
	/**
	 * Загрузить класс напрямую из байтов
	 * @param name Имя класса
	 * @param classBytes Байты класса
	 * @param protectionDomain {@link ProtectionDomain} загружаемого класса
	 * @param classloader Загрузчик классов, через который будет произведена загрузка
	 * @return Загруженный класс или <code>null</code>, если не найдется
	 */
	@SneakyThrows
	public Class<?> defineClass(@NonNull String name, @NonNull byte[] classBytes, @NonNull ProtectionDomain protectionDomain, ClassLoader classloader)
	{
		return reflectionsUtils.exceptionsHandler.handle()
			.message("Error while defining class", name)
			.tag(Exceptions.tagDynamicClassDefineAccess)
			.andReturn(() -> {
				AtomicReference<Method> defineClassMethod = new AtomicReference<>();

				ExceptionsUtils.doOnExceptionAndThrow(() -> {
					var method = ClassLoader.class.getDeclaredMethod("defineClass", String.class, byte[].class, int.class, int.class, ProtectionDomain.class);
					method.setAccessible(true);
					defineClassMethod.set(method);
				}, (ex) -> {
					if (EqualsUtils.objectEquals(findClass("java.lang.reflect.InaccessibleObjectException", true), ex.getClass()))
					{
						System.err.println();
						System.err.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
						System.err.println("   Warn: Hello from new java versions that does not love reflection) Please, add --add-opens java.base/java.lang.reflect=ALL-UNNAMED --add-opens java.base/java.lang=ALL-UNNAMED JVM options to program on start! Thank!   ");
						System.err.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
						System.err.println();
					}
				});

				return (Class<?>) defineClassMethod.get().invoke(classloader, name, classBytes, 0, classBytes.length, protectionDomain);
			}, null)
		;
	}
	
	/**
	 * Отметить классы как эквивалентные для метода {@link #equalsIgnoreBoxing(Class, Class)}
	 * @param classes Эквивалентные классы
	 * @return this
	 */
	public TypesReflectionsUtils equalType(Class<?>... classes)
	{
		equalTypes.add(Arrays.asList(classes));
		return this;
	}
	
	/**
	 * Считается ли класс примитивом? <br>
	 * Подсказка: <code>int.class</code> - примитив, <code>Integer.class</code> - нет
	 * @param cl Проверяемый класс
	 * @return True, если класс - примитив
	 */
	public boolean isPrimitive(Class<?> cl)
	{
		return privitiveTypes == null ? false : privitiveTypes.contains(cl);
	}
	
	/**
	 * Загрузить класс
	 * @param className Имя класса
	 * @return Загруженный класс или <code>null</code>
	 */
	public ExceptionOptional<Class<?>> findClass(@NonNull String className)
	{
		return ExceptionOptional.ofAction(() -> ClassUtil.forName(className));
	}

	@SneakyThrows
	public Class<?> findClass(@NonNull String className, boolean silent)
	{
		if (silent)
		{
			try
			{
				return ClassUtil.forName(className);
			}
			catch (Throwable e)
			{
				// Nope!
				return null;
			}
		}
		else
		{
			return findClass(className).orElseThrow();
		}
	}

	public ExceptionOptional<Class<?>> findClass(@NonNull ExtendedList<String> classNames)
	{
		for (var className : classNames)
		{
			var cl = findClass(className);

			if (!cl.isEmpty())
			{
				return cl;
			}
		}

		return ExceptionOptional.empty();
	}

	/**
	 * Эквивалентны ли классы? <br>
	 * Игнорирует боксинг, например, <code>'int.class'</code> эквивалетнтен <code>'Integer.class'</code> и наоборот
	 * @param cl1 Сравниваемый класс
	 * @param cl2 Сравниваемый класс
	 * @return True, если эквиваленты
	 */
	public boolean equalsIgnoreBoxing(Class<?> cl1, Class<?> cl2)
	{
		for (var equalList : equalTypes)
		{
			if (equalList != null)
			{
				if (equalList.contains(cl1) && equalList.contains(cl2))
					return true;
			}
		}
		
		return cl1 == null ? cl2 == null : cl1.equals(cl2);
	}

	public boolean isNumber(Class<?> type)
	{
		return numbersExpansionChain.contains(type) || numbersWrappersExpansionChain.contains(type);
	}
	
	/**
	 * Можно ли поместить число указанного типа в другой тип?
	 * 
	 * <h1> Например: </h1>
	 * <code>isNumbersCompatible(byte.class, long.class) = true </code><br>
	 * <code>isNumbersCompatible(long.class, byte.class) = false </code><br>
	 * <code>isNumbersCompatible(byte.class, float.class) = false </code><br>
	 * <code>isNumbersCompatible(float.class, int.class) = false </code><br>
	 * @param type1 Помещаемый тип
	 * @param type2 Тип, с которым сравниваем
	 * @return True, если возможно
	 */
	public boolean isNumbersCompatible(Class<?> type1, Class<?> type2)
	{
		int indexOf1 = numbersExpansionChain.indexOf(type1);
		
		if (indexOf1 < 0)
			indexOf1 = numbersWrappersExpansionChain.indexOf(type1);
		
		int indexOf2 = numbersExpansionChain.indexOf(type2);

		if (indexOf2 < 0)
			indexOf2 = numbersWrappersExpansionChain.indexOf(type2);
		
		return indexOf1 < 0 ? false : indexOf2 < 0 ? false : indexOf1 <= indexOf2;
	}
	
	public ProtectionDomain getDomain(CodeSource source)
	{
		return new ProtectionDomain(source, getPermissions());
	}

	public Permissions getPermissions()
	{
		Permissions permissions = new Permissions();
		permissions.add(new AllPermission());
		permissions.add(new ReflectPermission("suppressAccessChecks"));
		return permissions;
	}

	public Class<?> wrapPrimitive(Class<?> primitiveClass)
	{
		return isPrimitive(primitiveClass) ? primitiveToWrapper.get(primitiveClass) : primitiveClass;
	}

	public <T> TypesFilters<Class<T>> filter()
	{
		return new TypesFilters<>(
				(e) -> e,
					(annotations, annotationType) ->
						reflectionsUtils.annotations().findAnnotation()
							.ofType(ReflectionsUtils.cast(annotationType))
							.in(annotations)
							.get() != null)
				;
	}

	public Class<?> getMemberOwner(Member member)
	{
		return ClassUtil.forName(member.getDeclaringClass().getTypeName());
	}

	public boolean isDefined(String typeName)
	{
		try
		{
			ClassUtil.forName(typeName);

			return true;
		}
		catch (Throwable e)
		{
			// Nope!
		}

		return false;
	}

}
