package ru.swayfarer.swl3.reflection;

import java.lang.reflect.Parameter;

import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.reflection.filers.ParameterFilters;

@Setter @Accessors(fluent = true, chain = true)
public class ParamsReflectionsUtils {

	public ReflectionsUtils reflectionsUtils;
	
	public boolean isParamsAccepted(@NonNull Parameter[] params, boolean isFully, @NonNull Object... args)
	{
		var classes = new Class<?>[args.length];
		
		for (int i1 = 0; i1 < args.length; i1 ++)
		{
			var arg = args[i1];
			classes[i1] = arg == null ? null : arg.getClass();
		}
		
		return isParamsAccepted(params, isFully, classes);
	}
	
	/**
	 * Считать ли параметры подходящими 
	 * @param params Массив параметров
	 * @param isFully Считать ли null-аргументы совпадающими
	 * @param argumentsTypes Массив аргументов
	 * @return Подходят ли?
	 */
	public boolean isParamsAccepted(@NonNull Parameter[] params, boolean isFully, @NonNull Class<?>... argumentsTypes)
	{
		if (params.length != argumentsTypes.length)
			return false;

		if (argumentsTypes.length == 0)
			return true;
		
		for (int i1 = 0; i1 < params.length; i1 ++)
		{
			var argType = argumentsTypes[i1];
			var paramType = params[i1].getType();
			
			// Если аргумент null, то подойдет любой параметер, тип которого не примитивен
			if (argType == null && !reflectionsUtils.types().isPrimitive(paramType))
			{
				return !isFully;
			}
			
			// Если тип параметра доступен из типа аргумента
			else if (paramType.isAssignableFrom(argType))
			{
				return true;
			}
			
			else if (reflectionsUtils.types().isNumbersCompatible(argType, paramType))
			{
				return true;
			}
			
			// Если типы эквивалентны, игнорируя боксинг (int -> Integer и наоборот), то типы подходят друг другу
			else if (reflectionsUtils.types().equalsIgnoreBoxing(paramType, argType))
			{
				return true;
			}
		}
		
		return false;
	}

	public ParameterFilters filters()
	{
		return new ParameterFilters(reflectionsUtils);
	}
}
