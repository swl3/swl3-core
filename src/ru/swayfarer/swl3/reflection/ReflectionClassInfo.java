package ru.swayfarer.swl3.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Stream;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.Singular;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns;
import ru.swayfarer.swl3.markers.Internal;

/**
 * Информация о классе, которую возможно использовать для работы через рефлексию 
 * <p>
 * Бутылочное горлышко рефлексии - сканирование классов и попытки получить доступ к их элементам <br>
 * Для решения этой проблемы используется кэширование таких объектов, которые позволяют быстро получить необходимую информацию <br>
 * @author swayfarer
 *
 */
@SuppressWarnings("rawtypes")
@Data @Accessors(fluent = true, chain = true)
@NoArgsConstructor @AllArgsConstructor
public class ReflectionClassInfo {
	
	/** Класс, информация о котором содержится в объекте */
	@NonNull
	public Class<?> targetClass;

	/** Интерфейсы класса */
	@NonNull
	public ExtendedList<Class<?>> interfaces = new ExtendedList<>();
	
	/** Поля, к которым получилось получить доступ. <code>Имя_поля</code> -> <code>Поле</code> */
	@Singular
	public Map<String, Field> accessibleFields = new HashMap<>();
	
	/** Методы, к которым получилось получить доступ. <code>Имя_метода</code> -> <code>Метод</code> */
	@Singular
	public Map<String, List<Method>> accessibleMethods = new HashMap<>();
	
	/** Конструкторы, к которым удалось получить доступ */
	@Singular
	public List<Constructor> accessibleConstructors = new ArrayList<>();
	
	/** Информация об аннотациях класса */
	@Singular
	@Internal
	public List<Annotation> annotations = new ArrayList<>();
	
	public ReflectionClassInfo accessibleMethod(@NonNull Method method)
	{
		var methodName = method.getName();
		
		var list = accessibleMethods.get(methodName);
		
		if (list == null)
		{
			list = new ArrayList<>();
			accessibleMethods.put(methodName, list);
		}
		
		list.add(method);
		
		return this;
	}
	
	public ReflectionClassInfo accessibleField(@NonNull Field field)
	{
		accessibleFields.put(field.getName(), field);
		return this;
	}
	
	public ReflectionClassInfo annotation(Annotation annotation)
	{
		this.annotations.add(annotation);
		return this;
	}
	
	public ReflectionClassInfo accessibleConstructor(@NonNull Constructor<?> constructor)
	{
		accessibleConstructors.add(constructor);
		return this;
	}
	
	/**
	 * Получить информацию о классе
	 * @param cl Сканируемый класс
	 * @param reflectionsUtils Утилиты, при помощи которых будет проведено сканирование
	 * @return Информация о классе
	 */
	public static ReflectionClassInfo of(@NonNull Class<?> cl, @NonNull ReflectionsUtils reflectionsUtils)
	{
		var classesTree = reflectionsUtils.types().findInheritanceChain(cl);

		var info = new ReflectionClassInfo();
		info.targetClass(cl);
		
		Stream.of(cl.getDeclaredConstructors())
			.filter(reflectionsUtils::tryToAccess)
			.forEach((info::accessibleConstructor));


		var alreadyAddedFilter = new AlreadyAddedMethodFilter();

		for (var classInChain : classesTree)
		{
			var fields = classInChain.getDeclaredFields();
			var methods = classInChain.getDeclaredMethods();
			var interfaces = classInChain.getInterfaces();


			Stream.of(classInChain.getAnnotations())
					.forEach(info::annotation)
			;

			Stream.of(interfaces)
					.forEach(info.interfaces()::add)
			;

			Stream.of(fields)
				.filter(reflectionsUtils::tryToAccess)
				.forEach(info::accessibleField)
			;
			
			Stream.of(methods)
				.filter(reflectionsUtils::tryToAccess)
				.filter(alreadyAddedFilter)
				.forEach(info::accessibleMethod)
			;
		}
		
		return info;
	}

	public static class AlreadyAddedMethodFilter implements Predicate<Method> {

		public ExtendedList<AlreadyAddedMethodEntry> entries = new ExtendedList<>();

		@Override
		public boolean test(Method method)  {
			var entry = AlreadyAddedMethodEntry.of(method);

			if (entries.exStream().anyMatches((e) -> e.canOverride(entry)))
				return false;

			entries.add(entry);

			return true;
		}
	}

	@Setter
	@Getter
	@Accessors(chain = true)
	@SuperBuilder
	@NoArgsConstructor
	@AllArgsConstructor
	public static class AlreadyAddedMethodEntry {
		public String name;
		public Class<?>[] params;
		public Class<?> returnType;

		public boolean canOverride(AlreadyAddedMethodEntry methodEntry)
		{
			if (!name.equals(methodEntry.name))
				return false;

			if (!Arrays.equals(params, methodEntry.params))
				return false;

			if (!(returnType.isAssignableFrom(methodEntry.returnType) || methodEntry.returnType.isAssignableFrom(returnType)))
				return false;

			return true;
		}

		public static AlreadyAddedMethodEntry of(Method method)
		{
			return AlreadyAddedMethodEntry.builder()
					.name(method.getName())
					.params(ExtendedStream.of(method.getParameters())
							.map(Parameter::getType)
							.toArray(Class.class)
					)
					.returnType(method.getReturnType())
					.build()
			;
		}
	}
}
