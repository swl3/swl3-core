package ru.swayfarer.swl3.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.reflection.generic.GenericObject;
import ru.swayfarer.swl3.reflection.generic.GenericParser;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class GenericsReflectionsUtils {
    public ReflectionsUtils reflectionsUtils;
    
    public ExtendedList<GenericObject> of(@NonNull Parameter parameter)
    {
        return of(parameter.getParameterizedType());
    }
    
    public ExtendedList<GenericObject> of(@NonNull Method method)
    {
        return of(method.getGenericReturnType());
    }
    
    public ExtendedList<GenericObject> ofInterface(@NonNull Object obj, @NonNull Class<?> marker)
    {
        return ofInterface(obj.getClass(), marker);
    }
    
    public ExtendedList<GenericObject> ofInterface(@NonNull Class<?> cl, @NonNull Class<?> marker)
    {
        var interfaces = cl.getGenericInterfaces();
        int minOffet = Integer.MAX_VALUE;
        Type nearestType = null;
        
        for (var i : interfaces)
        {
            Class<?> classOfInterface = null;
            
            if (i instanceof ParameterizedType)
            {
                classOfInterface = (Class<?>) ((ParameterizedType) i).getRawType();
            }
            else
            {
                classOfInterface = (Class<?>) i;
            }
            
            var offset = reflectionsUtils.types().getInheritanceOffset(classOfInterface, marker);
            
            if (offset < minOffet)
            {
                minOffet = offset;
                nearestType = i;
            }
        }
        
        return nearestType == null ? null : of(nearestType);
    }

    public GenericObject create(Class<?> initialClass, Object... generics)
    {
        var genericObject = new GenericObject();
        genericObject.setTypeCanonicalName(initialClass.getCanonicalName());

        for (var generic : generics)
        {
            if (generic instanceof Type)
                genericObject.getChilds().add(parse((Type) generic));
            else if (generic instanceof GenericObject)
                genericObject.getChilds().add((GenericObject) generic);
        }

        return genericObject;
    }
    
    public ExtendedList<GenericObject> ofParent(@NonNull Object obj)
    {
        return ofParent(obj.getClass());
    }
    
    public ExtendedList<GenericObject> ofParent(@NonNull Class<?> cl)
    {
        var superClass = cl.getGenericSuperclass();

        if (superClass == null)
            return null;

        return of(superClass);
    }
    
    public ExtendedList<GenericObject> of(@NonNull Field field)
    {
        return of(field.getGenericType());
    }
    
    public ExtendedList<GenericObject> of(@NonNull Type genericTypes)
    {
        var parser = new GenericParser();
        parser.parse(reflectionsUtils.exceptionsHandler, genericTypes.getTypeName());
        var firstElement = parser.result.first();
        return firstElement == null ? new ExtendedList<>() : firstElement.getChilds();
    }

    public GenericObject parse(Type genericType)
    {
        if (genericType == null)
            return null;

        if (genericType instanceof ParameterizedType)
        {
            var parameterizedType = (ParameterizedType) genericType;
            var rawType = parameterizedType.getRawType();

            if (rawType instanceof Class)
            {
                var result = new GenericObject();
                result.setTypeCanonicalName(((Class<?>) rawType).getName());

                var typeParams = parameterizedType.getActualTypeArguments();

                if (!CollectionsSWL.isNullOrEmpty(typeParams))
                {
                    for (var typeParam : parameterizedType.getActualTypeArguments())
                    {
                        result.getChilds().add(parse(typeParam));
                    }
                }

                return result;
            }
        }

        var parser = new GenericParser();
        parser.parse(reflectionsUtils.exceptionsHandler, genericType.getTypeName());
        return parser.result.first();
    }
}
