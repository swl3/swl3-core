package ru.swayfarer.swl3.reflection;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.fields.FieldsUnfinalHelper;
import ru.swayfarer.swl3.reflection.filers.FieldFilters;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class FieldsReflectionsUtils {

	public ReflectionsUtils reflectionsUtils;
	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
	public FieldsUnfinalHelper fieldsUnfinalHelper = new FieldsUnfinalHelper()
			.setReflectionsUtils(this::reflectionsUtils)
	;

	public ExtendedStream<Field> stream(@NonNull Object instance)
	{
		return stream(instance.getClass());
	}
	
	public ExtendedStream<Field> stream(@NonNull Class<?> classOfInstance)
	{
		var classInfo = reflectionsUtils.infos().findOrCreateClassInfo(classOfInstance);
		return ExtendedStream.of(classInfo.accessibleFields().values());
	}
	
	public Object getValue(@NonNull Class<?> classOfField, @NonNull String... names)
	{
		return getValue(classOfField, null, names);
	}
	
	public Object getValue(@NonNull Object instance, @NonNull String... names)
	{
		return getValue(instance.getClass(), instance, names);
	}
	
	
	public FieldsReflectionsUtils setValue(@NonNull Class<?> classOfInstance, @NonNull Object value, @NonNull String... names)
	{
		return setValue(classOfInstance, null, value, names);
	}
	
	public FieldsReflectionsUtils setValue(@NonNull Object instance, @NonNull Object value, @NonNull String... names)
	{
		return setValue(instance.getClass(), instance, value, names);
	}
	
	@Internal
	protected FieldsReflectionsUtils setValue(@NonNull Class<?> classOfInstance, Object instance, Object value, @NonNull String... names)
	{
		var fields = reflectionsUtils.infos().findOrCreateClassInfo(classOfInstance);
		
		for (var name : names)
		{
			var field = fields.accessibleFields().get(name);
			
			if (field != null)
			{
				setValue(field, instance, value);
				return this;
			}
		}
		
		ExceptionsUtils.ThrowToCaller(NoSuchFieldException.class, "Can't find field named", Arrays.asList(names), "at object", instance, "of class", classOfInstance);
		return null;
	}
	
	@Internal
	protected Object getValue(@NonNull Class<?> classOfInstance, Object instance, @NonNull String... names)
	{
		var fields = reflectionsUtils.infos().findOrCreateClassInfo(classOfInstance);
		
		for (var name : names)
		{
			var field = fields.accessibleFields().get(name);
			
			if (field != null)
				return getValue(field, instance);
		}
		
		ExceptionsUtils.ThrowToCaller(NoSuchFieldException.class, "Can't find field named", Arrays.asList(names), "at object", instance);
		return null;
	}
	
	public Object getValue(Field field, Object instance)
	{
		return exceptionsHandler.safeReturn(() -> {
			field.setAccessible(true);
			return field.get(instance);
		}, null, "Error while getting value of field", field);
	}

	public FieldsReflectionsUtils setStaticValue(Field field, Object value)
	{
		return setValue(field, null, value);
	}

	public FieldsReflectionsUtils setValue(Field field, Object instance, Object value)
	{
		unfinal(field);

		exceptionsHandler.safe(() -> {
			field.setAccessible(true);
			field.set(instance, value);
		}, "Error while getting value of field", field);
		
		return this;
	}

	public Field unfinal(Field field)
	{
		int mods = field.getModifiers();
		if (Modifier.isFinal(mods)) {
			fieldsUnfinalHelper.unfinal(field);
		}

		return field;
	}

//	{
//		try
//		{
//			var fieldLookup = MethodHandles.privateLookupIn(Field.class, MethodHandles.lookup());
//			fieldModifiers = fieldLookup.findVarHandle(Field.class, "modifiers", int.class);
//		}
//		catch (IllegalAccessException | NoSuchFieldException ex) {
//			throw new RuntimeException(ex);
//		}
//	}

	public FieldFilters filters()
	{
		return new FieldFilters(reflectionsUtils);
	}
	
}
