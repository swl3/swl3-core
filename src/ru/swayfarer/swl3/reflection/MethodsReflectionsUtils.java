package ru.swayfarer.swl3.reflection;

import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.reflection.filers.MethodFilters;

import java.lang.reflect.Method;
import java.util.ArrayList;

@Setter @Accessors(chain = true, fluent = true)
public class MethodsReflectionsUtils {

	public ReflectionsUtils reflectionsUtils;
	
	public Method findMethod(Class<?> classOfInstance, String name, Object... args) 
	{
		var classInfo = reflectionsUtils.infos().findOrCreateClassInfo(classOfInstance);
		var methods = classInfo.accessibleMethods();
		
		var methodsList = methods.get(name);
		
		if (methodsList == null)
			return null;
		
		
		return methodsList.stream()
				.filter((method) -> reflectionsUtils.params().isParamsAccepted(method.getParameters(), false, args))
			.findFirst().orElse(null);
	}
	
	public MethodInvocationResult invoke(@NonNull Object instance, @NonNull String name, @NonNull Object... args)
	{
		var method = findMethod(instance.getClass(), name, args);
		
		if (method == null)
			return MethodInvocationResult.notFound();
		
		return invoke(instance, method, args);
	}
	
	public ExtendedStream<Method> stream(@NonNull Object instance)
	{
		return stream(instance.getClass());
	}
	
	public ExtendedStream<Method> stream(@NonNull Class<?> classOfMethods)
	{
		var classInfo = reflectionsUtils.infos().findOrCreateClassInfo(classOfMethods);
		var methods = classInfo.accessibleMethods().values();
		var methodsList = new ArrayList<Method>();
		
		for (var method : methods)
		{
			methodsList.addAll(method);
		}
				
		return ExtendedStream.of(methodsList);
	}
	
	public MethodInvocationResult invoke(@NonNull Class<?> classOfMethod, @NonNull String name, @NonNull Object... args)
	{
		var method = findMethod(classOfMethod, name, args);
		
		if (method == null)
			return MethodInvocationResult.notFound();
		
		return invoke(null, method, args);
	}
	
	public MethodInvocationResult invoke(Object instance, @NonNull Method method, @NonNull Object... args)
	{
		var resultBuilder = MethodInvocationResult.builder();
		
		try
		{
			resultBuilder.returnValue(method.invoke(instance, args));
			resultBuilder.isInvoked(true);
		}
		catch (Throwable e)
		{
			resultBuilder.methodException(e);
		}
		
		return resultBuilder.build();
	}

	public MethodFilters filters()
	{
		return new MethodFilters(reflectionsUtils);
	}
}
