package ru.swayfarer.swl3.reflection.annotations;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;

@Retention(RUNTIME)
public @interface AnnotationAlias
{
	public Class<? extends Annotation> value();
	public String[] name() default {};
}
