package ru.swayfarer.swl3.reflection.annotations.property;

import lombok.SneakyThrows;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.reflection.annotations.AnnotationAlias;
import ru.swayfarer.swl3.reflection.annotations.AnnotationCollect;
import ru.swayfarer.swl3.reflection.annotations.finder.AnnotationFinderV2;

import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.lang.reflect.Method;

public class AnnotationPropertyFinderV2
{
    @Internal
    public AnnotationFinderV2 annotationFinderV2;

    @Internal
    public ReflectionsUtils reflectionsUtils;

    public AnnotationPropertyFinderV2(AnnotationFinderV2 annotationFinderV2, ReflectionsUtils reflectionsUtils)
    {
        this.annotationFinderV2 = annotationFinderV2;
        this.reflectionsUtils = reflectionsUtils;
    }

    @SneakyThrows
    public <T> T findValue(Annotation[] annotations, AnnotationPropertyTarget target)
    {
        var isTargetArray = target.getType().isArray();

        var allAssignableToMarkerAnnotations = AnnotationsWalker.walk(annotations)
                .filter((a) -> annotationFinderV2.isAnnotationAssignable(a, target.getMarkerAnnotationType()))
                .toExList()
        ;

        var methodFilters = reflectionsUtils.methods().filters();

        var resultPropertyFound = false;
        var resultListIfTargetIsArray = new ExtendedList();
        IFunction0<T> returnResultListAsArrayFun = () -> (T) resultListIfTargetIsArray.toArrayObject(target.getType().getComponentType());

        for (var annotation : allAssignableToMarkerAnnotations)
        {
            var availablePropertyMethods = reflectionsUtils.methods().stream(annotation.annotationType())
                    .filter(methodFilters.type().equal(target.getType()))
                    .filter((method) -> isAcceptsByName(method, target))
                    .toExList()
            ;

            for (var method : availablePropertyMethods)
            {
                resultPropertyFound = true;
                var methodReturnValue = (T) method.invoke(annotation);

                if (!isTargetArray)
                    return methodReturnValue;

                var collectAnnotation = getCollectAnnotation(method);
                resultListIfTargetIsArray.addAll(asObjectArray(methodReturnValue));

                if (collectAnnotation != null)
                {
                    if (!collectAnnotation.value())
                    {
                        return returnResultListAsArrayFun.apply();
                    }
                }
                else
                {
                    return returnResultListAsArrayFun.apply();
                }
            }
        }

        if (resultPropertyFound && isTargetArray)
            return returnResultListAsArrayFun.apply();

        return null;
    }

    public Object[] asObjectArray(Object obj)
    {
        var type = obj.getClass();

        if (!type.isArray())
            return null;

        var arrayElementType = type.getComponentType();

        if (arrayElementType.isPrimitive())
        {
            var size = Array.getLength(obj);
            var result = new Object[size];
            for (var i1 = 0; i1 < size; i1 ++)
            {
                result[i1] = Array.get(obj, i1);
            }

            return result;
        }

        return (Object[]) obj;
    }

    public boolean isAcceptsByName(Method method, AnnotationPropertyTarget target)
    {
        var aliasAnnotation = getAliasAnnotation(method);

        if (aliasAnnotation != null && !CollectionsSWL.isNullOrEmpty(aliasAnnotation.name()) && annotationFinderV2.isAnnotationAssignable(aliasAnnotation.value(), target.getMarkerAnnotationType()))
        {
            return ExtendedStream.of(aliasAnnotation.name()).contains(target.getName());
        }
        else
        {
            return method.getName().equals(target.getName());
        }
    }

    public AnnotationAlias getAliasAnnotation(Method method)
    {
        return annotationFinderV2.findAllIn(method.getAnnotations(), AnnotationAlias.class).findFirst().orNull();
    }

    public AnnotationCollect getCollectAnnotation(Method method)
    {
        return annotationFinderV2.findAllIn(method.getAnnotations(), AnnotationCollect.class).findFirst().orNull();
    }
}
