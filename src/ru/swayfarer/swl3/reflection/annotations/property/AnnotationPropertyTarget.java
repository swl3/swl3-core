package ru.swayfarer.swl3.reflection.annotations.property;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.markers.Internal;

import java.lang.annotation.Annotation;

@Data
@Accessors(chain = true)
public class AnnotationPropertyTarget
{
    @Internal
    public String name;

    @Internal
    public Class<?> type;

    @Internal
    public Class<? extends Annotation> markerAnnotationType;
}
