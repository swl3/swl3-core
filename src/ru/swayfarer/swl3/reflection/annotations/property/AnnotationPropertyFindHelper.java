package ru.swayfarer.swl3.reflection.annotations.property;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Getter
@Setter
@Accessors(chain = true)
public class AnnotationPropertyFindHelper<Ret_Type> {

    public AnnotationPropertyTarget propertyTarget = new AnnotationPropertyTarget();
    public AnnotationPropertyFinderV2 annotationPropertyFinderV2;
    public Annotation[] source;

	public AnnotationPropertyFindHelper(AnnotationPropertyFinderV2 annotationPropertyFinderV2)
	{
		this.annotationPropertyFinderV2 = annotationPropertyFinderV2;
	}

	public AnnotationPropertyFindHelper<Ret_Type> name(@NonNull String name)
	{
		propertyTarget.setName(name);
		return this;
	}
	
	public <T> AnnotationPropertyFindHelper<T> type(@NonNull Class<T> type)
	{
		propertyTarget.setType(type);
		return ReflectionsUtils.cast(this);
	}
    
    public AnnotationPropertyFindHelper<Ret_Type> in(Parameter param)
    {
        return in(param.getAnnotations());
    }
    
    public AnnotationPropertyFindHelper<Ret_Type> in(Class<?> cl)
    {
        return in(cl.getAnnotations());
    }
    
    public AnnotationPropertyFindHelper<Ret_Type> in(Constructor<?> constructor)
    {
        return in(constructor.getAnnotations());
    }
    
    public AnnotationPropertyFindHelper<Ret_Type> in(Field field)
    {
        return in(field.getAnnotations());
    }
	
	public AnnotationPropertyFindHelper<Ret_Type> in(Method method)
	{
	    return in(method.getAnnotations());
	}
	
	public AnnotationPropertyFindHelper<Ret_Type> in(Annotation[] annotations)
    {
		source = annotations;
	    return this;
    }
	
	public AnnotationPropertyFindHelper<Ret_Type> marker(@NonNull Class<? extends Annotation> classOfMarker)
	{
		propertyTarget.setMarkerAnnotationType(classOfMarker);
		return this;
	}
	
	public <T extends Ret_Type> ExtendedOptional<T> optional()
    {
	    return ExtendedOptional.of(get());
    }
	
	public <T extends Ret_Type> T orNull()
	{
		return orElse(null);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Ret_Type> T orElse(T value)
	{
		return (T) optional().orElse(value);
	}
	
	public <T extends Ret_Type> T get()
	{
	    if (source == null)
	        return null;

	    return annotationPropertyFinderV2.findValue(source, propertyTarget);
	}
}
