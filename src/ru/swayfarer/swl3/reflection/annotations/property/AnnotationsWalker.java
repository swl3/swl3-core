package ru.swayfarer.swl3.reflection.annotations.property;

import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.markers.Internal;

import java.lang.annotation.Annotation;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.StreamSupport;

public class AnnotationsWalker implements Iterator<Annotation>
{
    @Internal
    public ExtendedList<Annotation> visitedAnnotations = new ExtendedList<>();

    @Internal
    public Deque<Annotation> queue = new ArrayDeque<>();

    public AnnotationsWalker(Iterable<Annotation> annotations)
    {
        loadAnnotations(annotations);
    }

    @Override
    public boolean hasNext()
    {
        return !queue.isEmpty();
    }

    @Override
    public Annotation next()
    {
        var next = queue.poll();
        var annotationsAtQueuedAnnotation = next.annotationType().getAnnotations();

        if (!CollectionsSWL.isNullOrEmpty(annotationsAtQueuedAnnotation))
            loadAnnotations(CollectionsSWL.iterable(annotationsAtQueuedAnnotation));

        return next;
    }

    public AnnotationsWalker loadAnnotations(Iterable<Annotation> annotations)
    {
        for (var annotation : annotations)
        {
            if (!visitedAnnotations.contains(annotation))
            {
                visitedAnnotations.add(annotation);
                queue.add(annotation);
            }
        }

        return this;
    }

    public static ExtendedStream<Annotation> walk(Class<?> annotationsHolderClass)
    {
        return walk(annotationsHolderClass.getAnnotations());
    }

    public static ExtendedStream<Annotation> walk(Iterable<Annotation> annotations)
    {
        return new ExtendedStream<>(() -> {
            var annotationsWalker = new AnnotationsWalker(annotations);

            var spliterator =
                    Spliterators.spliteratorUnknownSize(annotationsWalker, Spliterator.DISTINCT);

            return StreamSupport
                    .stream(spliterator, false)
            ;
        });
    }

    public static ExtendedStream<Annotation> walk(Annotation[] annotations)
    {
        return walk(CollectionsSWL.iterable(annotations));
    }
}
