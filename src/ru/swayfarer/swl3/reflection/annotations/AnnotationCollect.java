package ru.swayfarer.swl3.reflection.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface AnnotationCollect
{
    public boolean value() default true;
}
