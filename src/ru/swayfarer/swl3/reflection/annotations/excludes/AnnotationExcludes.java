package ru.swayfarer.swl3.reflection.annotations.excludes;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.reflection.annotations.AnnotationExclude;

@Accessors(chain = true)
@SuppressWarnings("unchecked")
public class AnnotationExcludes {

	@Setter 
	public ReflectionsUtils reflectionsUtils;
	public Map<Class<? extends Annotation>, IFunction1<Annotation, List<Class<? extends Annotation>>>> cachedExcludes = new HashMap<>();
	
	public List<Class<? extends Annotation>> findOrGetExcludes(Annotation annotation)
	{
		var ret = cachedExcludes.get(annotation.annotationType());
		
		if (ret == null)
		{
			addExcludes(annotation.annotationType());
			ret = cachedExcludes.get(annotation.annotationType());
		}
		
		return ret.apply(annotation);
	}
	
	public AnnotationExcludes addExcludes(Class<? extends Annotation> annotationType)
	{
		var methodFilters = reflectionsUtils.methods().filters();

		var excludeMethods = reflectionsUtils.methods().stream(annotationType)
				.filter(methodFilters.annotation().contains(AnnotationExclude.class))
				.filter(methodFilters.type().equal(Class[].class))
			.toList();
		;
		
		IFunction1<Annotation, List<Class<? extends Annotation>>> ret = (a) -> new ArrayList<>();
		
		if (!excludeMethods.isEmpty())
		{
			ret = (currentAnnotation) -> {
				var excludeTypesList = new ArrayList<Class<? extends Annotation>>();
				for (var method : excludeMethods)
				{
					var excludes = ((Class<? extends Annotation>[]) reflectionsUtils.exceptionsHandler.safeReturn(
							() -> method.invoke(currentAnnotation), 
							null, 
							"Error while getting annotation", currentAnnotation, "property named", method.getName())
					);
					
					Stream.of(excludes).forEach(excludeTypesList::add);
				}
				return excludeTypesList;
			};
		}
		
		synchronized (cachedExcludes)
		{
			cachedExcludes.put(annotationType, ret);
		}
		
		return this;
	}
	
}
