package ru.swayfarer.swl3.reflection.annotations.finder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@Accessors(chain = true)
@SuppressWarnings("unchecked")
public class AnnotationFindHelper<Annotation_Type extends Annotation> {

	@NonNull
	@Setter
	public AnnotationFinderV2 annotationFinder;
	
	@NonNull
	public Class<? extends Annotation> targetAnnotationClass;
	
	@NonNull
	public List<Annotation> scanAnnotations = new ArrayList<>();

	@NonNull
	@Setter
	public ReflectionsUtils reflectionsUtils;
	
	public <T extends Annotation> AnnotationFindHelper<T> ofType(@NonNull Class<T> classOfAnnotation)
	{
		this.targetAnnotationClass = classOfAnnotation;
		return ReflectionsUtils.cast(this);
	}
	
	public AnnotationFindHelper<Annotation_Type> in(@NonNull Parameter param)
	{
		Stream.of(param.getDeclaredAnnotations())
			.forEach(scanAnnotations::add);
	
		return this;
	}
	
	public AnnotationFindHelper<Annotation_Type> in(@NonNull Constructor<?> constructor)
	{
		Stream.of(constructor.getDeclaredAnnotations())
			.forEach(scanAnnotations::add);
	
		return this;
	}
	
	public AnnotationFindHelper<Annotation_Type> in(@NonNull Method method)
	{
		Stream.of(method.getDeclaredAnnotations())
			.forEach(scanAnnotations::add);
	
		return this;
	}
	
	public AnnotationFindHelper<Annotation_Type> in(@NonNull Field field)
	{
		Stream.of(field.getDeclaredAnnotations())
			.forEach(scanAnnotations::add);
	
		return this;
	}
	
	public AnnotationFindHelper<Annotation_Type> in(@NonNull Object obj)
	{
		return in(obj.getClass());
	}
	
	public AnnotationFindHelper<Annotation_Type> in(@NonNull Annotation... annotations)
    {
	    for (var annotation : annotations)
	    {
	        scanAnnotations.add(annotation);
	    }
	    
	    return this;
    }
	
	public AnnotationFindHelper<Annotation_Type> in(@NonNull Class<?> cl)
	{
		scanAnnotations.addAll(reflectionsUtils.infos().findOrCreateClassInfo(cl).annotations());
		return this;
	}
	
	public <T extends Annotation_Type> T get()
	{
		return (T) annotationFinder.findAllIn(scanAnnotations, targetAnnotationClass).findFirst().orNull();
	}
	
	public <T extends Annotation_Type> ExtendedList<T> getAll()
    {
    	var result =  annotationFinder.findAllIn(scanAnnotations, targetAnnotationClass).toExList();
        return ReflectionsUtils.cast(result);
    }
}
