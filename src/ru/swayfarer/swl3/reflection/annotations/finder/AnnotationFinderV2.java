package ru.swayfarer.swl3.reflection.annotations.finder;

import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.annotations.property.AnnotationsWalker;

import java.lang.annotation.Annotation;

public class AnnotationFinderV2
{
    public <T extends Annotation> ExtendedStream<T> findAllIn(Annotation[] annotations, Class<T> marker)
    {
        return findAllIn(CollectionsSWL.iterable(annotations), marker);
    }

    public <T extends Annotation> ExtendedStream<T> findAllIn(Iterable<Annotation> annotations, Class<T> marker)
    {
        return AnnotationsWalker.walk(annotations)
                .filter((a) -> a.annotationType() == marker)
                .map((a) -> (T) a)
        ;
    }

    @Internal
    public boolean isAnnotationAssignable(Annotation source, Class<?> marker)
    {
        if (source.annotationType() == marker)
            return true;

        return AnnotationsWalker.walk(new Annotation[] {source})
                .filter((annotation) -> annotation.annotationType() == marker)
                .count() != 0
        ;
    }

    @Internal
    public boolean isAnnotationAssignable(Class<?> source, Class<?> marker)
    {
        if (source == marker)
            return true;

        return AnnotationsWalker.walk(source.getAnnotations())
                .filter((annotation) -> annotation.annotationType() == marker)
                .count() != 0
        ;
    }
}
