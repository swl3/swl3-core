package ru.swayfarer.swl3.reflection;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Member;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.exception.LibTags;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.exception.CanNotOpenAccessException;
import ru.swayfarer.swl3.reflection.factory.ReflectionsFactory;
import ru.swayfarer.swl3.reflection.wrapper.ReflectionObjectWrapper;
import ru.swayfarer.swl3.string.ExpressionsList;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;
import ru.swayfarer.swl3.thread.ThreadsUtils;

/**
 * Утилиты для работы с рефлексией
 * @author swayfarer
 *
 */
@SuppressWarnings("unchecked")
public class ReflectionsUtils {

	public static ReflectionsUtils INSTANCE = new ReflectionsUtils();

	public ExpressionsList traceFailedAccesses = new ExpressionsList();

	@Internal
	public AnnotationsReflectionsUtils annotationsUtils = new AnnotationsReflectionsUtils(this);

	@Internal
	public ClassInfoReflectionsUtils classInfoReflectionsUtils = new ClassInfoReflectionsUtils()
		    .reflectionsUtils(this)
	;

	@Internal
	public ConstructorReflectionsUtils constructorReflectionsUtils = new ConstructorReflectionsUtils()
			.reflectionsUtils(this)
	;

	@Internal
	public FieldsReflectionsUtils fieldsReflectionsUtils = new FieldsReflectionsUtils()
		    .reflectionsUtils(this)
	;

	@Internal
	public MethodsReflectionsUtils methodsReflectionsUtils = new MethodsReflectionsUtils()
			.reflectionsUtils(this)
	;

	@Internal
	public GenericsReflectionsUtils genericsReflectionsUtils = new GenericsReflectionsUtils()
	        .reflectionsUtils(this)
	;

	@Internal
	public EnumsReflectionsUtils enumsReflectionsUtils = new EnumsReflectionsUtils()
			.reflectionsUtils(this)
	;

	@Internal
	public InterfacesReflectionsUtils interfacesReflectionsUtils = new InterfacesReflectionsUtils()
		.reflectionsUtils(this)
	;

	@Internal
	public TypesReflectionsUtils typesReflectionsUtils = new TypesReflectionsUtils()

			.setReflectionsUtils(this)

			.primitives(byte.class, short.class, int.class, long.class, float.class, double.class, boolean.class, char.class)

			.equalType(byte.class, Byte.class)
			.equalType(short.class, Short.class)
			.equalType(int.class, Integer.class)
			.equalType(long.class, Long.class)
			.equalType(float.class, Float.class)
			.equalType(double.class, Double.class)
			.equalType(boolean.class, Boolean.class)
			.equalType(char.class, Character.class)
	;

	@Internal
	public ParamsReflectionsUtils paramsReflectionsUtils = new ParamsReflectionsUtils()
			.reflectionsUtils(this)
	;

	@Internal
	public ObfuscationReflectionsUtils obfuscationReflectionsUtils = new ObfuscationReflectionsUtils()
			.reflectionsUtils(this)
	;

	public IFunction0<ReflectionsFactory> reflectionsFactoryFun = ThreadsUtils.singleton(() -> new ReflectionsFactory(this));

	/** Обработчик ошибок, возникающих при работе утилит */
	@Internal
	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();

	public GenericsReflectionsUtils generics()
	{
	    return genericsReflectionsUtils;
	}

	/** Утилиты для работы с типами */
	public TypesReflectionsUtils types()
	{
		return typesReflectionsUtils;
	}

	/** Утилиты для работы с методами */
	public MethodsReflectionsUtils methods()
	{
		return methodsReflectionsUtils;
	}

	/** Утилиты для работы с параметрами */
	public ParamsReflectionsUtils params()
	{
		return paramsReflectionsUtils;
	}

	/** Утилиты для работы с полями */
	public FieldsReflectionsUtils fields()
	{
		return fieldsReflectionsUtils;
	}

	/** Утилиты для работы с enum'ами */
	public EnumsReflectionsUtils enums()
	{
		return enumsReflectionsUtils;
	}

	/** Утилиты для работы с интерфейсами */
	public InterfacesReflectionsUtils interfaces()
	{
		return interfacesReflectionsUtils;
	}

	/** Утилиты для работы с информацией о классах */
	public ClassInfoReflectionsUtils infos()
	{
		return classInfoReflectionsUtils;
	}

	public ReflectionsFactory reflectionsFactory()
	{
		return reflectionsFactoryFun.apply();
	}

	/** Утилиты для работы с конструкторами  */
	public ConstructorReflectionsUtils constructors()
	{
		return constructorReflectionsUtils;
	}

	/** Сканирование аннотаций */
	public AnnotationsReflectionsUtils annotations()
	{
		return annotationsUtils;
	}

	/** Утилиты для работы с обфускацией */
	public ObfuscationReflectionsUtils obf()
	{
		return obfuscationReflectionsUtils;
	}
    
    public static String getClassSimpleName(@NonNull String classname)
    {
        int lastIndexOfPoint = classname.lastIndexOf('.');
        return lastIndexOfPoint > 0 ? classname.substring(lastIndexOfPoint + 1) : "";
    }

    public static String getShortedClassName(@NonNull String className, int maxPackageSectorLength)
    {
        var pkgName = ReflectionsUtils.getPackageName(className);
        var clName = ReflectionsUtils.getClassSimpleName(className);
        
        if (!StringUtils.isEmpty(pkgName))
        {
            var resultBuffer = new DynamicString();
            
            CollectionsSWL.list(pkgName.split("\\."))
                .map((s) -> StringUtils.limit(s, maxPackageSectorLength))
                .each((s) -> {
                    resultBuffer.append(s);
                    resultBuffer.append(".");
                })
            ;
            
            resultBuffer.append(clName);
            return resultBuffer.toString();
        }
        
        return className;
    }
	
	public static String getPackageName(@NonNull String classname)
	{
	    int lastIndexOfPoint = classname.lastIndexOf('.');
	    return lastIndexOfPoint >= 0 ? classname.substring(0, lastIndexOfPoint) : "";
	}

	/**
	 * Попробовать получить доступ к объекту <br>
	 * Для более кастомной обработки ошибок получения доступа можно настроить {@link #exceptionsHandler}
	 * @param accessibleObject Объект
	 * @return True, если удалось получить доступ
	 */
	public boolean tryToAccess(@NonNull AccessibleObject accessibleObject)
	{
		try
		{
			accessibleObject.setAccessible(true);
			return true;
		}
		catch (Throwable e)
		{
			if (traceFailedAccesses.isMatches(accessibleObject))
			{
				exceptionsHandler.handle()
						.tag(
								LibTags.Exceptions.tagShortPrint
						)
						.message("Error while accessing object", accessibleObject)
						.throwable(new CanNotOpenAccessException(e))
						.start()
				;
			}
		}
		
		return false;
	}

	public ReflectionObjectWrapper wrap(Object obj)
	{
		return new ReflectionObjectWrapper(obj, this);
	}

	public ReflectionObjectWrapper wrapStatic(@NonNull Class<?> classToWrap)
	{
		return new ReflectionObjectWrapper(classToWrap, this);
	}
	
	public static <T> T cast(Object obj)
	{
	    return (T) obj;
	}
	
	public static ReflectionsUtils getInstance()
	{
		return INSTANCE;
	}
}
