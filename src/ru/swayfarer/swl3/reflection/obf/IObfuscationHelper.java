package ru.swayfarer.swl3.reflection.obf;

import ru.swayfarer.swl3.collections.list.ExtendedList;

import java.lang.reflect.Member;

public interface IObfuscationHelper
{
    public ExtendedList<String> getObfMemberName(Member member);
}
