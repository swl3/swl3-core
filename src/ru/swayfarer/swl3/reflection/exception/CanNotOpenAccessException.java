package ru.swayfarer.swl3.reflection.exception;

@SuppressWarnings("serial")
public class CanNotOpenAccessException extends RuntimeException{

	public CanNotOpenAccessException() {
		super();
	}

	public CanNotOpenAccessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CanNotOpenAccessException(String message, Throwable cause) {
		super(message, cause);
	}

	public CanNotOpenAccessException(String message) {
		super(message);
	}

	public CanNotOpenAccessException(Throwable cause) {
		super(cause);
	}

}
