package ru.swayfarer.swl3.reflection;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.markers.Internal;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Collections;
import java.util.NoSuchElementException;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class InterfacesReflectionsUtils
{
    @Internal
    public ReflectionsUtils reflectionsUtils;

    public ExtendedStream<Class<?>> stream(@NonNull Object instanceWithInterfaces)
    {
        return stream(instanceWithInterfaces.getClass());
    }

    public ExtendedStream<Class<?>> stream(@NonNull Class<?> classWithInterfaces)
    {
        var classInfo = reflectionsUtils.infos().findOrCreateClassInfo(classWithInterfaces);
        return new ExtendedStream<>(classInfo.interfaces()::stream);
    }

    public boolean has(Object targetInstance, Class<?> interfaceClass)
    {
        return has(targetInstance.getClass(), interfaceClass);
    }

    public boolean has(Class<?> targetClass, Class<?> interfaceClass)
    {
        return interfaceClass.isAssignableFrom(targetClass);
    }

    public <Compound_Proxy> Compound_Proxy createCompoundProxy(Object... parts)
    {
        return createCompoundProxy(CollectionsSWL.iterable(parts));
    }

    public <Compound_Proxy> Compound_Proxy createCompoundProxy(Iterable<?> parts)
    {
        return createCompoundProxy(parts, Collections.emptyList());
    }

    public <Compound_Proxy> Compound_Proxy createCompoundProxy(Iterable<?> parts, Iterable<Class<?>> interfaces)
    {
        var partsList = CollectionsSWL.list(parts);
        var interfacesList = CollectionsSWL.list(interfaces);

        if (interfacesList.isEmpty())
        {
            partsList.exStream()
                    .each((type) -> {
                        stream(type).each(interfacesList::add);
                    })
            ;

            interfacesList.distinct();
        }

        var interfaceToPart = new ExtendedMap<Class<?>, Object>();

        interfacesList.exStream()
            .each((i) -> {
                interfaceToPart.getOrCreate(i, () -> {
                    var partWithInterface = partsList.exStream().first((part) -> has(part, i));
                    if (partWithInterface == null)
                    {
                        ExceptionsUtils.throwException(new NoSuchElementException("Can't find part with assignable " + i.getName() + " interface!"));
                    }

                    return partWithInterface;
                });
            })
        ;

        var invocationHandler = new CompoundInvocationHandler();

        interfaceToPart.each((entry) -> {
            var interfaceClass = entry.getKey();
            reflectionsUtils.methods().stream(interfaceClass)
                    .each((method) -> invocationHandler.methodToPart.put(method, entry.getValue()))
            ;
        });

        return (Compound_Proxy) Proxy.newProxyInstance(
                Thread.currentThread().getContextClassLoader(),
                interfacesList.toArray(Class.class),
                invocationHandler)
        ;
    }

    public class CompoundInvocationHandler implements InvocationHandler
    {
        public ExtendedMap<Method, Object> methodToPart = new ExtendedMap<>().synchronize();

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
        {
            return method.invoke(methodToPart.get(method), args);
        }
    }
}
