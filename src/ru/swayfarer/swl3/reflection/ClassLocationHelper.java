package ru.swayfarer.swl3.reflection;

import java.net.JarURLConnection;
import java.net.URL;
import java.security.CodeSource;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.io.path.PathHelper;
import ru.swayfarer.swl3.string.StringUtils;

@Getter
@Setter
@Accessors(chain = true)
public class ClassLocationHelper {
    
    @SneakyThrows
    public static ClassLocation findClassLocation(@NonNull Class<?> cl)
    {
        var protectionDomain = cl.getProtectionDomain();
        var classResourceUrl = cl.getResource(cl.getSimpleName() + ".class");

        URL classLocation = null;
        String classEntryPath = null;
        CodeSource codeSource = null;
        
        if (protectionDomain != null)
        {
            codeSource = protectionDomain.getCodeSource();
            
            if (codeSource != null)
            {
                var location = codeSource.getLocation();
                
                if (location != null)
                {
                    classLocation = location;
                }
            }
        }
        
        tryToGetByClassLoc:
        {
            if (classResourceUrl != null)
            {
                var protocol = classResourceUrl.getProtocol();
                
                if (protocol.equals("file"))
                {
                    if (classLocation == null)
                    {
                        var classPackage = cl.getPackage().getName();
                        var countOfPoints = classPackage.split("\\.").length;
                        
                        var file = FileSWL.of(classResourceUrl).getParentFile();
                        
                        for (int i1 = 0; i1 < countOfPoints; i1 ++)
                        {
                            if (file == null)
                            {
                                break tryToGetByClassLoc;
                            }
                            
                            file = file.getParentFile();
                        }
                       
                        classLocation = file.asURL();
                    }
                    
                    classEntryPath = cl.getName().replace(".", "/").concat(".class");
                }
                else if (protocol.equals("jar"))
                {
                    JarURLConnection connection = (JarURLConnection) classResourceUrl.openConnection();
                    classEntryPath = connection.getJarEntry().getName();
                    
                    if (classLocation == null)
                    {
                        classLocation = connection.getJarFileURL();
                    }
                }
            }
        }
        
        if (StringUtils.isBlank(classEntryPath))
        {
            classEntryPath = cl.getName().replace(".", "/").concat(".class");
        }
        
        var ret = new ClassLocation()
                .setPath(classEntryPath)
                .setContainerLocation(classLocation)
                .setCodeSource(codeSource)
        ;
        
        if (classLocation != null && classLocation.getProtocol().equals("file"))
        {
            ret.setContainerFile(FileSWL.of(classLocation));
        }
        
        return ret;
    }
    
    @Data
    @Accessors(chain = true)
    public static class ClassLocation {
        public String path;
        public URL containerLocation;
        public FileSWL containerFile;
        public CodeSource codeSource;
        
        public boolean isContainerFile()
        {
            return containerFile != null;
        }
        
        public String getContainerSimpleName()
        {
            if (containerLocation == null)
                return "<unknown>";
            
            return PathHelper.getSimpleName(containerLocation.toExternalForm());
        }
    }
}
