package ru.swayfarer.swl3.reflection.wrapper;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Getter
@Setter
@Accessors(chain = true)
public class ReflectionObjectWrapper
{
    @Internal
    public Object instance;

    @Internal
    public Class<?> instanceClass;

    @Internal
    public ReflectionsUtils reflectionsUtils;

    @Internal
    public IObservable<FieldFilerFindEvent> eventFindFieldFilter = Observables.createObservable();

    @Internal
    public IObservable<MethodFilterFindEvent> eventFindMethodFilter = Observables.createObservable();

    @Internal
    public boolean throwOnNotFound = true;

    public ReflectionObjectWrapper(Object instance, ReflectionsUtils reflectionsUtils)
    {
        this.instance = instance;
        this.reflectionsUtils = reflectionsUtils;

        this.instanceClass = instance != null ? instance.getClass() : null;
        init();
    }

    public ReflectionObjectWrapper(Class<?> classOfInstance, ReflectionsUtils reflectionsUtils)
    {
        this.reflectionsUtils = reflectionsUtils;
        this.instanceClass = classOfInstance;
        init();
    }

    public ReflectionObjectWrapper configure(IFunction1NoR<ReflectionObjectWrapper> configurerFun)
    {
        configurerFun.apply(this);
        return this;
    }

    @SneakyThrows
    public ReflectionObjectWrapper getField(String name)
    {
        if (isDummy())
            return this;

        var field = getFieldByName(name);

        if (field == null)
            return createEmpty();

        return createChild(field.get(instance));
    }

    public ReflectionObjectWrapper getByAccessor(String name)
    {
        return invoke(StringUtils.toCamelCase(false, "set", name));
    }

    @SneakyThrows
    public ReflectionObjectWrapper setByAccessor(String name, Object value)
    {
        invoke(StringUtils.toCamelCase(false, "set", name), value);
        return this;
    }

    @SneakyThrows
    public ReflectionObjectWrapper setField(String name, Object value)
    {
        if (isDummy())
            return this;

        var field = getFieldByName(name);

        if (field != null)
            reflectionsUtils.fields().setValue(field, instance, value);

        return this;
    }

    @SneakyThrows
    public ReflectionObjectWrapper invoke(String name, Object... args)
    {
        if (isDummy())
            return this;

        var method = findMethodByNameAndArgs(name, args);

        if (method == null)
            return createEmpty();

        try
        {
            var result = method.invoke(instance, args);
            return createChild(result);
        }
        catch (Throwable e)
        {
            if (e instanceof InvocationTargetException)
                throw e.getCause();

            throw e;
        }
    }

    public <T> T orIfDummy(T value)
    {
        return isDummy() ? value : asValue();
    }

    public <T> T asValue()
    {
        return (T) instance;
    }

    public boolean isDummy()
    {
        return instance == null && instanceClass == null;
    }

    @Internal
    public ReflectionObjectWrapper loadFrom(ReflectionObjectWrapper otherWrapper)
    {
        this.setEventFindFieldFilter(otherWrapper.eventFindFieldFilter)
            .setEventFindMethodFilter(otherWrapper.eventFindMethodFilter)
            .setThrowOnNotFound(otherWrapper.throwOnNotFound)
        ;

        return this;
    }

    @Internal
    public ExtendedStream<Field> createFieldsStream()
    {
        return instance != null ? reflectionsUtils.fields().stream(instance) : reflectionsUtils.fields().stream(instanceClass);
    }

    @Internal
    public ExtendedStream<Method> createMethodsStream()
    {
        return instance != null ? reflectionsUtils.methods().stream(instance) : reflectionsUtils.methods().stream(instanceClass);
    }

    @Internal
    public ReflectionObjectWrapper createChild(Object instance)
    {
        return new ReflectionObjectWrapper(instance, reflectionsUtils)
                .loadFrom(this)
        ;
    }

    @Internal
    public ReflectionObjectWrapper createEmpty()
    {
        return new ReflectionObjectWrapper(null, reflectionsUtils)
                .loadFrom(this)
        ;
    }

    @SneakyThrows
    @Internal
    private Field getFieldByName(String name)
    {
        var filterFindEvent = FieldFilerFindEvent.builder()
                .name(name)
                .ownerClass(instanceClass)
                .reflectionsUtils(reflectionsUtils)
                .build()
        ;

        eventFindFieldFilter.next(filterFindEvent);

        var resultFilters = filterFindEvent.getResult();

        if (CollectionsSWL.isNullOrEmpty(resultFilters))
            throw new IllegalStateException("No one filter found! Check reflection object wrapper config!");

        var field = createFieldsStream()
                .filters(resultFilters)
                .findFirst()
        ;

        if (field == null && throwOnNotFound)
            throw new NoSuchMethodException("No field '" + name + "' found in " + instanceClass);

        return field.orNull();
    }

    @SneakyThrows
    private Method findMethodByNameAndArgs(String name, Object[] args)
    {
        var argTypes = ExtendedStream.of(args)
                .map((o) -> o == null ? null : o.getClass())
                .map((cl) -> (Class) cl)
                .toExList()
        ;

        var findFiltersEvent = MethodFilterFindEvent.builder()
                .reflectionsUtils(reflectionsUtils)
                .argTypes(ReflectionsUtils.cast(argTypes))
                .ownerClass(instanceClass)
                .name(name)
                .build()
        ;

        eventFindMethodFilter.next(findFiltersEvent);

        var resultFilters = findFiltersEvent.getResult();

        if (CollectionsSWL.isNullOrEmpty(resultFilters))
            throw new IllegalStateException("No one filter found! Check reflection object wrapper config!");

        var method = createMethodsStream()
                .filters(resultFilters)
                .findFirst()
                .orNull()
        ;

        if (method == null && throwOnNotFound)
            throw new NoSuchMethodException("No method '" + name + "' found for arg types " + argTypes + " in " + instanceClass);

        return method;
    }

    @Internal
    public void init()
    {
        eventFindFieldFilter.subscribe().by((event) -> {
            event.getResult().addAll(
                    ReflectionObjectWrapperFilters.byFieldName(event),
                    ReflectionObjectWrapperFilters.byFieldType(event)
            );
        });

        eventFindMethodFilter.subscribe().by((event) -> {
            event.getResult().addAll(
                    ReflectionObjectWrapperFilters.byMethodName(event),
                    ReflectionObjectWrapperFilters.byMethodParamTypes(event)
            );
        });
    }
}
