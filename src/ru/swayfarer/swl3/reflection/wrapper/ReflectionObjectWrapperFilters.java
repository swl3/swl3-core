package ru.swayfarer.swl3.reflection.wrapper;

import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectionObjectWrapperFilters
{
    public static IFunction1<Field, Boolean> byFieldType(FieldFilerFindEvent event)
    {
        return event.getReflectionsUtils().fields()
                .filters().type().equal(event.getFieldType())
        ;
    }

    public static IFunction1<Method, Boolean> byMethodParamTypes(MethodFilterFindEvent event)
    {
        return event.getReflectionsUtils().methods()
                .filters().params().canAcceptTypes(event.getArgTypes())
        ;
    }

    public static IFunction1<Field, Boolean> byFieldName(FieldFilerFindEvent event)
    {
        return event.getReflectionsUtils().fields()
                .filters().name().equal(event.getName())
        ;
    }

    public static IFunction1<Method, Boolean> byMethodName(MethodFilterFindEvent event)
    {
        return event.getReflectionsUtils().methods()
                .filters().name().equal(event.getName())
        ;
    }
}
