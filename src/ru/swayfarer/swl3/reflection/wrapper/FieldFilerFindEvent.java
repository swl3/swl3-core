package ru.swayfarer.swl3.reflection.wrapper;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.event.AbstractEvent;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.lang.reflect.Field;

@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class FieldFilerFindEvent extends AbstractEvent
{
    @Internal
    public ReflectionsUtils reflectionsUtils;

    @Internal
    public String name;

    @Internal
    public Class<?> ownerClass;

    @Internal
    public Class<?> fieldType;

    @Builder.Default
    @Internal
    public ExtendedList<IFunction1<Field, Boolean>> result = new ExtendedList<>();
}
