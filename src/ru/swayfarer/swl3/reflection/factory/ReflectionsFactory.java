package ru.swayfarer.swl3.reflection.factory;

import lombok.Getter;
import lombok.SneakyThrows;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.thread.ThreadsUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

@Getter
public class ReflectionsFactory
{
    public ReflectionsUtils reflectionsUtils;

    public Object lock = new Object();

    public ExtendedList<String> reflectionsFactoryClasses = CollectionsSWL.list(
            "jdk.internal.reflect.ReflectionFactory",
            "sun.reflect.ReflectionFactory"
    );

    public ExtendedList<String> constructorAccessorClasses = CollectionsSWL.list(
            "jdk.internal.reflect.ConstructorAccessor",
            "sun.reflect.ConstructorAccessor"
    );

    public IFunction0<Class<?>> classOfReflectionsFactory = ThreadsUtils.singleton(() ->
        reflectionsUtils.types().findClass(getReflectionsFactoryClasses()).get()
    );

    public IFunction0<Class<?>> constructorAccessorClass = ThreadsUtils.singleton(() ->
       reflectionsUtils.types().findClass(getConstructorAccessorClasses()).get()
    );

    public IFunction0<Object> reflectionsFactoryInstance = ThreadsUtils.singleton(() ->
        getReflectionsFactoryInstanceMethod().invoke(null)
    );

    public IFunction0<Method> reflectionsFactoryInstanceMethod = ThreadsUtils.singleton(() -> {
        var methodFilters = reflectionsUtils.methods().filters();

        return reflectionsUtils.methods().stream(getClassOfReflectionsFactory())
            .filter(methodFilters.name().equal("getReflectionFactory"))
            .filter(methodFilters.params().count().equal(0))
            .findFirst()
            .orElseThrow(() -> new ReflectionsInitException("Can't get reflections factory singleton method!"));
        }
    );

    public IFunction0<Method> constructorAccessorMethod = ThreadsUtils.singleton(() -> {
        var methodFilters = reflectionsUtils.methods().filters();

        return reflectionsUtils.methods().stream(getClassOfReflectionsFactory())
            .filter(methodFilters.name().equal("newConstructorAccessor"))
            .filter(methodFilters.params().canAcceptTypes(Constructor.class))
            .findFirst()
            .orElseThrow(() -> new ReflectionsInitException("Can't get newConstructorAccessor method from reflections factory!"));
        }
    );

    public IFunction0<Method> constructorAccessorInvokeMethod = ThreadsUtils.singleton(() -> {
        var methodFilters = reflectionsUtils.methods().filters();

        return reflectionsUtils.methods().stream(getConstructorAccessorClass())
            .filter(methodFilters.name().equal("newInstance"))
            .filter(methodFilters.params().canAcceptTypes(Object[].class))
            .findFirst()
            .orElseThrow(() -> new ReflectionsInitException("Can't get newInstance method from constructor accessor!"));
        }
    );

    public ReflectionsFactory(ReflectionsUtils reflectionsUtils)
    {
        this.reflectionsUtils = reflectionsUtils;
    }

    @SneakyThrows
    public <T> T invokeConstructor(Constructor<?> constructor, Object... args)
    {
        var ca = createConstructorAccessor(constructor);
        return (T) getConstructorAccessorInvokeMethod().invoke(ca, (Object) (args));
    }

    @SneakyThrows
    public Object createConstructorAccessor(Constructor<?> constructor)
    {
        return getConstructorAccessorMethod().invoke(getReflectionsFactoryInstance(), constructor);
    }

    public Class<?> getConstructorAccessorClass()
    {
        return constructorAccessorClass.apply();
    }

    public Class<?> getClassOfReflectionsFactory()
    {
        return classOfReflectionsFactory.apply();
    }

    public Method getConstructorAccessorInvokeMethod()
    {
        return constructorAccessorInvokeMethod.apply();
    }

    public Method getConstructorAccessorMethod()
    {
        return constructorAccessorMethod.apply();
    }

    public Method getReflectionsFactoryInstanceMethod()
    {
        return reflectionsFactoryInstanceMethod.apply();
    }

    public Object getReflectionsFactoryInstance()
    {
        return reflectionsFactoryInstance.apply();
    }
}
