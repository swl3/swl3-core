package ru.swayfarer.swl3.reflection.factory;

public class ReflectionsInitException extends RuntimeException
{
    public ReflectionsInitException()
    {
    }

    public ReflectionsInitException(String message)
    {
        super(message);
    }

    public ReflectionsInitException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ReflectionsInitException(Throwable cause)
    {
        super(cause);
    }
}
