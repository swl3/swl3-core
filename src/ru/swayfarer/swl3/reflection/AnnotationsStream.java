package ru.swayfarer.swl3.reflection;

import java.lang.annotation.Annotation;
import java.util.stream.Stream;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

@SuppressWarnings( {"rawtypes", "unchecked"} )
public class AnnotationsStream extends ExtendedStream<Annotation> {

	public AnnotationsStream(@NonNull IFunction0<Stream> streamCreationFun)
	{
		super(streamCreationFun);
	}
	
	public AnnotationsStream type(Class<?>... types)
	{
		return filter((annotation) -> {
			for (var type : types)
			{
				if (annotation.annotationType() == type)
					return true;
			}
			
			return false;
		});
	}
	
	@Override
	public <T> ExtendedStream<T> create(@NonNull IFunction0<Stream> streamCreationFun)
	{
		return (ExtendedStream<T>) new AnnotationsStream(streamCreationFun);
	}
	
	@Override
	public AnnotationsStream filter(@NonNull IFunction1<Annotation, Boolean> filter)
	{
		return (AnnotationsStream) super.filter(filter);
	}
}
