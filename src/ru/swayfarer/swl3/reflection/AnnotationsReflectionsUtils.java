package ru.swayfarer.swl3.reflection;

import java.lang.annotation.Annotation;

import ru.swayfarer.swl3.reflection.annotations.finder.AnnotationFindHelper;
import ru.swayfarer.swl3.reflection.annotations.finder.AnnotationFinderV2;
import ru.swayfarer.swl3.reflection.annotations.property.AnnotationPropertyFinderV2;
import ru.swayfarer.swl3.reflection.annotations.property.AnnotationPropertyFindHelper;

/**
 * Утилиты для работы с аннотациями
 * @author swayfarer
 *
 */
public class AnnotationsReflectionsUtils {

	public AnnotationPropertyFinderV2 annotationPropertyFinderV2;
	public AnnotationFinderV2 annotationFinderV2;
	public ReflectionsUtils reflectionsUtils;
	
	public AnnotationsReflectionsUtils(ReflectionsUtils reflectionsUtils)
	{
		this.reflectionsUtils = reflectionsUtils;
		this.annotationFinderV2 = new AnnotationFinderV2();
		this.annotationPropertyFinderV2 = new AnnotationPropertyFinderV2(annotationFinderV2, reflectionsUtils);
	}
	
	public <T extends Annotation> AnnotationFindHelper<T> findAnnotation()
	{
		return new AnnotationFindHelper<T>()
				.setAnnotationFinder(annotationFinderV2)
				.setReflectionsUtils(reflectionsUtils)
		;
	}
	
	public AnnotationPropertyFindHelper<Object> findProperty()
	{
		return new AnnotationPropertyFindHelper<>(annotationPropertyFinderV2);
	}
}
