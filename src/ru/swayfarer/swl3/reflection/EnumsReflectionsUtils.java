package ru.swayfarer.swl3.reflection;

import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.reflection.enums.EnumProvider;

@Setter
@Accessors(chain = true, fluent = true)
public class EnumsReflectionsUtils
{
    public ExtendedMap<Class<?>, EnumProvider<?>> registeredProviders = new ExtendedMap<>().synchronize();

    public ReflectionsUtils reflectionsUtils;

    public <T> EnumProvider<T> provider(Class<T> enumClass)
    {
        return registeredProviders.getOrCreate(enumClass, () ->
            new EnumProvider<>(reflectionsUtils, enumClass)
        );
    }

    public <T> T newInstance(Class<T> enumClass, Object... constructorArgs)
    {
        return provider(enumClass).newEnum(constructorArgs);
    }
}
