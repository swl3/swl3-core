package ru.swayfarer.swl3.reflection;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.markers.Internal;

/**
 * Утилиты для работы с информацией о классах <br>
 * Быстрее, чем каждый раз обращаться к {@link Class#getDeclaredFields()} и т.п., потому, что использует кэширование
 * @author swayfarer
 *
 */
@Data @Accessors(fluent = true, chain = true)
public class ClassInfoReflectionsUtils {

	/** Утилиты для работы с рефлексией */
	@Internal
	public ReflectionsUtils reflectionsUtils;
	
	/** Кэшированная информация о классах*/
	@Internal
	public Map<String, ReflectionClassInfo> classInfos = new HashMap<>();
	
	/**
	 * Найти информцию о классе в кэше
	 * @param className Имя класса (см {@link Class#getName()})
	 * @return Найденная информация или null
	 */
	public ReflectionClassInfo findClassInfo(@NonNull String className)
	{
		return classInfos.get(className);
	}

	/**
	 * Найти информацию о классе в кэше или добавить ее туда
	 * @param cl Целевой класс
	 * @return Инормация о классе
	 */
	public ReflectionClassInfo findOrCreateClassInfo(Class<?> cl)
	{
		var className = cl.getName();
		var cachedClassInfo = findClassInfo(className);
		
		if (cachedClassInfo == null)
		{
			classInfo(cl);
		}
		
		return findClassInfo(className);
	}
	
	/**
	 * Просканировать класс и добавить информацию о нем в кэш
	 * @param cl Сканируемый класс
	 * @return this
	 */
	public ClassInfoReflectionsUtils classInfo(@NonNull Class<?> cl)
	{
		var classInfo = ReflectionClassInfo.of(cl, reflectionsUtils);
		
		synchronized (classInfos)
		{
			classInfos.put(classInfo.targetClass().getName(), classInfo);
		}
		
		return this;
	}
}
