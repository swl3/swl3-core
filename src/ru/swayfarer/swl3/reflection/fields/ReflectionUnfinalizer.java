package ru.swayfarer.swl3.reflection.fields;

import lombok.SneakyThrows;
import lombok.var;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class ReflectionUnfinalizer implements IFieldsUnfinalizer
{
    @SneakyThrows
    @Override
    public void unfinalField(Field field)
    {
        var modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
    }
}
