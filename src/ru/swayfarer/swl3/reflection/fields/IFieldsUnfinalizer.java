package ru.swayfarer.swl3.reflection.fields;

import java.lang.reflect.Field;

public interface IFieldsUnfinalizer
{
    public void unfinalField(Field field);
}
