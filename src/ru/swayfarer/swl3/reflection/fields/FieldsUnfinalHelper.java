package ru.swayfarer.swl3.reflection.fields;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.io.streams.StreamsUtils;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.reflection.varhandle.VarHandleHelperFactory;

import java.lang.reflect.Field;
import java.util.concurrent.atomic.AtomicReference;

@Getter
@Setter
@Accessors(chain = true)
public class FieldsUnfinalHelper
{
    public static final AtomicReference<Class<?>> varArgsUnfinalzerClass = new AtomicReference<>();

    @Internal
    public IFunction0<ReflectionsUtils> reflectionsUtils;

    @Internal
    public IFieldsUnfinalizer fieldsUnfinalizer;

    public void init()
    {
        if (fieldsUnfinalizer == null)
        {
            var reflectionsUtils = this.reflectionsUtils.apply();

            if (VarHandleHelperFactory.isVarHandlesSupported())
            {
                System.out.println("VarHandles API found in classpath, using it to unfinal fields!");
                var varHandlerFieldUnfinalizer = getVarHandlerUnfinalizerClass();
                fieldsUnfinalizer = reflectionsUtils.constructors()
                        .newInstance(varHandlerFieldUnfinalizer)
                        .orHandle(
                                reflectionsUtils.exceptionsHandler,
                                "Can't create VarHandle fields unfinalizer!"
                        )
                ;
            }
            else
            {
                System.out.println("VarHandles API not found in classpath, using legacy reflection way to unfinal fields!");
                fieldsUnfinalizer = new ReflectionUnfinalizer();
            }
        }
    }

    public void unfinal(Field field)
    {
        init();

        if (fieldsUnfinalizer != null)
            fieldsUnfinalizer.unfinalField(field);
    }

    @SneakyThrows
    public Class<?> getVarHandlerUnfinalizerClass()
    {
        var result = varArgsUnfinalzerClass.get();

        if (result == null)
        {
            synchronized (varArgsUnfinalzerClass)
            {
                result = varArgsUnfinalzerClass.get();
                if (result == null)
                {
                    result = reflectionsUtils.apply().types().loadOrDefine(
                        "ru.swayfarer.swl3.reflection.fields.VarHandlesUnfinalizer", StreamsUtils.readAll(FieldsUnfinalHelper.class.getResourceAsStream("VarHandlesUnfinalizer.class.bytes")))
                    ;

                    varArgsUnfinalzerClass.set(result);
                }
            }
        }

        return result;
    }
}
