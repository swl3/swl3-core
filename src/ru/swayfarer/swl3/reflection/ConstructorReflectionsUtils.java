package ru.swayfarer.swl3.reflection;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.reflection.filers.ConstructorFiltering;

import java.lang.reflect.Constructor;
import java.util.Arrays;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class ConstructorReflectionsUtils {

	public ReflectionsUtils reflectionsUtils;
	
	public ExtendedStream<Constructor<Object>> stream(@NonNull Object instance)
	{
		return stream(instance.getClass());
	}
	
	public ExtendedStream<Constructor<Object>> stream(@NonNull Class<?> cl)
	{
		var constructors = reflectionsUtils.infos().findOrCreateClassInfo(cl).accessibleConstructors();
		return ReflectionsUtils.cast(ExtendedStream.of(constructors));
	}
	
	public MethodInvocationResult newInstance(@NonNull Constructor<?> method, Object... args)
	{
		var resultBuilder = MethodInvocationResult.builder();
		
		try
		{
			resultBuilder.returnValue(method.newInstance(args));
			resultBuilder.isInvoked(true);
		}
		catch (Throwable e)
		{
			resultBuilder.methodException(e);
		}
		
		return resultBuilder.build();
	}

	public MethodInvocationResult newInstance(@NonNull Class<?> classOfInstance)
    {
	    var resultBuilder = MethodInvocationResult.builder();
        
        try
        {
            var constructorFiltering = filtering();

            var constructor = stream(classOfInstance)
                    .filter(constructorFiltering.params().count().equal(0))
                    .findAny()
                    .orElseThrow(() -> new NoSuchMethodException("Can't find no-args constructor in class " + classOfInstance.getName()))
            ;
            
            resultBuilder.returnValue(constructor.newInstance());
            resultBuilder.isInvoked(true);
        }
        catch (Throwable e)
        {
            resultBuilder.methodException(e);
        }
        
        return resultBuilder.build();
    }

    public MethodInvocationResult newInstance(@NonNull Class<?> cl, @NonNull Object... args)
    {
        var resultBuilder = MethodInvocationResult.builder();

        var constructorFilters = filtering();

        try
        {
            var constructor = stream(cl)
                    .filter(constructorFilters.params().canAcceptValues(args))
                    .findAny()
                    .orElseThrow(() -> new NoSuchMethodException("Can't find constructor for args: " + Arrays.asList(args) + " in class " + cl.getName()));
                    
            resultBuilder.returnValue(constructor.newInstance(args));
            resultBuilder.isInvoked(true);
        }
        catch (Throwable e)
        {
            resultBuilder.methodException(e);
        }
        
        return resultBuilder.build();
    }

    public <T> ConstructorFiltering<T> filtering()
    {
        return new ConstructorFiltering(reflectionsUtils);
    }
}
