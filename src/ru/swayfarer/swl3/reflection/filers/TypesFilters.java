package ru.swayfarer.swl3.reflection.filers;

import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.objects.EqualsUtils;

import java.lang.annotation.Annotation;

public class TypesFilters<Object_Type> extends AbstractFiltering<Object_Type, Class<?>>
{
    @Internal
    public IFunction2<Annotation[], Class<?>, Boolean> isAnnotationAssignableFun;

    public TypesFilters(
            IFunction1<Object_Type, Class<?>> getValueFun,
            IFunction2<Annotation[], Class<?>, Boolean> isAnnotationAssignableFun
    )
    {
        super(getValueFun);
        this.isAnnotationAssignableFun = isAnnotationAssignableFun;
    }

    public IFunction1<Object_Type, Boolean> equal(Class<?>... types)
    {
        return nonNullMatches(EqualsUtils::objectEquals, types);
    }

    public IFunction1<Object_Type, Boolean> assignables(Class<?>... types)
    {
        return nonNullMatches((value, assignableType) -> assignableType.isAssignableFrom(value), types);
    }

    public IFunction1<Object_Type, Boolean> canAssign(Class<?>... types)
    {
        return nonNullMatches((value, assignableType) -> value.isAssignableFrom(assignableType), types);
    }

    public IFunction1<Object_Type, Boolean> interfaces()
    {
        return nonNullFilter(Class::isInterface);
    }

    public IFunction1<Object_Type, Boolean> anonymous()
    {
        return nonNullFilter(Class::isAnonymousClass);
    }

    public IFunction1<Object_Type, Boolean> annotations()
    {
        return nonNullFilter(Class::isAnnotation);
    }

    public IFunction1<Object_Type, Boolean> arrays()
    {
        return nonNullFilter(Class::isArray);
    }

    public IFunction1<Object_Type, Boolean> enums()
    {
        return nonNullFilter(Class::isEnum);
    }

    public IFunction1<Object_Type, Boolean> primitives()
    {
        return nonNullFilter(Class::isPrimitive);
    }

    public IFunction1<Object_Type, Boolean> synthetics()
    {
        return nonNullFilter(Class::isSynthetic);
    }

    public ModifierFilters<Object_Type> mods()
    {
        return new ModifierFilters<>((obj) -> getValueFun.apply(obj).getModifiers());
    }

    public StringFilters<Object_Type> fullName()
    {
        return new StringFilters<>((obj) -> getValueFun.apply(obj).getName());
    }

    public StringFilters<Object_Type> canonicalName()
    {
        return new StringFilters<>((obj) -> getValueFun.apply(obj).getCanonicalName());
    }

    public StringFilters<Object_Type> simpleName()
    {
        return new StringFilters<>((obj) -> getValueFun.apply(obj).getSimpleName());
    }

    public AnnotationsFilters<Object_Type> annotation()
    {
        return new AnnotationsFilters<Object_Type>(isAnnotationAssignableFun, (obj) -> getValueFun.apply(obj).getAnnotations());
    }
}
