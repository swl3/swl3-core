package ru.swayfarer.swl3.reflection.filers;

import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

public class IntegerFiltering<Object_Type> extends AbstractFiltering<Object_Type, Integer>
{
    public IntegerFiltering(IFunction1<Object_Type, Integer> getValueFun)
    {
        super(getValueFun);
    }

    public IFunction1<Object_Type, Boolean> equal(Integer... nums)
    {
        return nonNullMatches((value, matcherValue) -> value.intValue() == matcherValue.intValue(), nums);
    }

    public IFunction1<Object_Type, Boolean> biggerOrEqualThan(int min)
    {
        return nonNullFilter((value) -> min <= value);
    }

    public IFunction1<Object_Type, Boolean> smallerOrEqualThan(int max)
    {
        return nonNullFilter((value) -> max >= value);
    }

    public IFunction1<Object_Type, Boolean> between(int i1, int i2)
    {
        var min = Math.min(i1, i2);
        var max = Math.max(i1, i2);

        return biggerOrEqualThan(max).and(smallerOrEqualThan(min));
    }
}
