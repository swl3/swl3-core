package ru.swayfarer.swl3.reflection.filers;

import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.markers.Internal;

public class AbstractFiltering<Object_Type, Matcher_Type>
{
    @Internal
    public IFunction1<Object_Type, Matcher_Type> getValueFun;

    public AbstractFiltering(IFunction1<Object_Type, Matcher_Type> getValueFun)
    {
        this.getValueFun = getValueFun;
    }

    public IFunction1<Object_Type, Boolean> nonNullFilter(IFunction1<Matcher_Type, Boolean> filter)
    {
        return (obj) -> {
            var value = getValueFun.apply(obj);

            if (value == null)
                return false;

            return filter.apply(value);
        };
    }

    public IFunction1<Object_Type, Boolean> nonNullMatches(IFunction2<Matcher_Type, Matcher_Type, Boolean> matcher, Matcher_Type... regexes)
    {
        return matches((value, matchStr) -> {
            if (value == null || matchStr == null)
                return value == null && matchStr == null;

            return matcher.apply(value, matchStr);
        }, regexes);
    }

    public IFunction1<Object_Type, Boolean> matches(IFunction2<Matcher_Type, Matcher_Type, Boolean> matcher, Matcher_Type... regexes)
    {
        return (obj) -> {
            var value = getValueFun.apply(obj);

            for (var regex : regexes)
            {
                if (matcher.apply(value, regex))
                    return true;
            }

            return false;
        };
    }
}
