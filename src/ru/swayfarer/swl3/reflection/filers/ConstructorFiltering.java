package ru.swayfarer.swl3.reflection.filers;

import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.lang.reflect.Constructor;

public class ConstructorFiltering<T> extends AbstractMethodFilters<Constructor<T>>
{
    public ConstructorFiltering(ReflectionsUtils reflectionsUtils)
    {
        super(
                reflectionsUtils.obf()::getNames,
                Constructor::getModifiers,
                Constructor::getDeclaringClass,
                Constructor::getAnnotations,
                Constructor::getParameters,
                reflectionsUtils
        );
    }
}
