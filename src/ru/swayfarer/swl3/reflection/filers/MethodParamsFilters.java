package ru.swayfarer.swl3.reflection.filers;

import lombok.var;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.lang.reflect.Parameter;

public class MethodParamsFilters<Object_Type>
{
    @Internal
    public ReflectionsUtils reflectionsUtils;

    @Internal
    public IFunction1<Object_Type, Parameter[]> getParamsFun;

    public MethodParamsFilters(ReflectionsUtils reflectionsUtils, IFunction1<Object_Type, Parameter[]> getParamsFun)
    {
        this.reflectionsUtils = reflectionsUtils;
        this.getParamsFun = getParamsFun;
    }

    public IntegerFiltering<Object_Type> count()
    {
        return new IntegerFiltering<>((obj) -> getParamsFun.apply(obj).length);
    }

    public IFunction1<Object_Type, Boolean> canAcceptTypes(Iterable<Class<?>> types)
    {
        return canAcceptTypes(ExtendedStream.of(types, false).toArray(Class.class));
    }

    public IFunction1<Object_Type, Boolean> canAcceptTypes(Class<?>... types)
    {
        return (obj) -> {
            var params = getParamsFun.apply(obj);
            if (params == null)
                return false;

            return reflectionsUtils.params().isParamsAccepted(params, false, types);
        };
    }

    public IFunction1<Object_Type, Boolean> canAcceptValues(Object... args)
    {
        return (obj) -> {
            var params = getParamsFun.apply(obj);
            if (params == null)
                return true;

            return reflectionsUtils.params().isParamsAccepted(params, false, args);
        };
    }
}
