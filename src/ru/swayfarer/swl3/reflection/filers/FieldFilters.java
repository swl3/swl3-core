package ru.swayfarer.swl3.reflection.filers;

import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.lang.reflect.Field;

public class FieldFilters extends AbstractClassEntryFilters<Field>
{
    @Internal
    public ReflectionsUtils reflectionsUtils;

    public FieldFilters(ReflectionsUtils reflectionsUtils)
    {
        super(
                reflectionsUtils.obf()::getNames,
                Field::getModifiers,
                Field::getType,
                Field::getAnnotations,

                (annotations, annotationType) ->
                        reflectionsUtils.annotations().findAnnotation()
                                .ofType(ReflectionsUtils.cast(annotationType))
                                .in(annotations).get() != null
        );

        this.reflectionsUtils = reflectionsUtils;
    }
}
