package ru.swayfarer.swl3.reflection.filers;

import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.lang.reflect.Parameter;

public class ParameterFilters extends AbstractClassEntryFilters<Parameter>
{
    @Internal
    public ReflectionsUtils reflectionsUtils;

    public ParameterFilters(ReflectionsUtils reflectionsUtils)
    {
        super(
                (param) -> CollectionsSWL.list(param.getName()),
                Parameter::getModifiers,
                Parameter::getType,
                Parameter::getAnnotations,

                (param, annotationType) ->
                    reflectionsUtils.annotations().findAnnotation()
                        .ofType(ReflectionsUtils.cast(annotationType))
                        .in(param)
                        .get() != null
        );

        this.reflectionsUtils = reflectionsUtils;
    }
}
