package ru.swayfarer.swl3.reflection.filers;

import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.lang.reflect.Method;

public class MethodFilters extends AbstractMethodFilters<Method>
{
    @Internal
    public ReflectionsUtils reflectionsUtils;

    public MethodFilters(ReflectionsUtils reflectionsUtils)
    {
        super(
                reflectionsUtils.obf()::getNames,
                Method::getModifiers,
                Method::getReturnType,
                Method::getAnnotations,
                Method::getParameters,
                reflectionsUtils
        );

        this.reflectionsUtils = reflectionsUtils;
    }
}
