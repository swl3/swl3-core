package ru.swayfarer.swl3.reflection.filers;

import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

public class StringsFiltering<Object_Type> extends CollectionsFilter<Object_Type, StringFilters<Object_Type>, String>
{
    public StringsFiltering(IFunction1<Object_Type, ExtendedList<String>> getValueFun)
    {
        super(getValueFun);
        newFilteringFun = StringFilters::new;
    }

    public IFunction1<Object_Type, Boolean> equal(String... strs)
    {
        return matches((filtering) -> filtering.equal(strs));
    }

    public IFunction1<Object_Type, Boolean> equalIgnoreCase(String... strs)
    {
        return matches((filtering) -> filtering.equalIgnoreCase(strs));
    }

    public IFunction1<Object_Type, Boolean> startsWith(String... strs)
    {
        return matches((filtering) -> filtering.startsWith(strs));
    }

    public IFunction1<Object_Type, Boolean> startsWith(int offset, String... strs)
    {
        return matches((filtering) -> filtering.startsWith(offset, strs));
    }

    public IFunction1<Object_Type, Boolean> endsWith(String... strs)
    {
        return matches((filtering) -> filtering.endsWith(strs));
    }

    public IFunction1<Object_Type, Boolean> matchesRegex(String... strs)
    {
        return matches((filtering) -> filtering.matchesRegex(strs));
    }

    public IFunction1<Object_Type, Boolean> matchesMask(String... strs)
    {
        return matches((filtering) -> filtering.matchesMask(strs));
    }
}
