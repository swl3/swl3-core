package ru.swayfarer.swl3.reflection.filers;

import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.markers.Internal;

import java.lang.annotation.Annotation;

public class AbstractClassEntryFilters<Object_Type> extends AbstractFiltering<Object_Type, Object_Type>
{
    @Internal
    public IFunction1<Object_Type, ExtendedList<String>> getNameFun;

    @Internal
    public IFunction1<Object_Type, Integer> getModsFun;

    @Internal
    public IFunction1<Object_Type, Class<?>> getTypeFun;

    @Internal
    public IFunction1<Object_Type, Annotation[]> getAnnotationsFun;

    @Internal
    public IFunction2<Annotation[], Class<?>, Boolean> isAnnotationAssignableFun;

    public AbstractClassEntryFilters(
            IFunction1<Object_Type, ExtendedList<String>> getNameFun,
            IFunction1<Object_Type, Integer> getModsFun,
            IFunction1<Object_Type, Class<?>> getTypeFun,
            IFunction1<Object_Type, Annotation[]> getAnnotationsFun,
            IFunction2<Annotation[], Class<?>, Boolean> isAnnotationAssignableFun
    )
    {
        super((e) -> e);
        this.getNameFun = getNameFun;
        this.getModsFun = getModsFun;
        this.getTypeFun = getTypeFun;
        this.getAnnotationsFun = getAnnotationsFun;
        this.isAnnotationAssignableFun = isAnnotationAssignableFun;
    }

    public TypesFilters<Object_Type> type()
    {
        return new TypesFilters<>(getTypeFun, isAnnotationAssignableFun);
    }

    public StringsFiltering<Object_Type> name()
    {
        return new StringsFiltering<>(getNameFun);
    }

    public ModifierFilters<Object_Type> mod()
    {
        return new ModifierFilters<>(getModsFun);
    }

    public AnnotationsFilters<Object_Type> annotation()
    {
        return new AnnotationsFilters<>(isAnnotationAssignableFun, getAnnotationsFun);
    }
}
