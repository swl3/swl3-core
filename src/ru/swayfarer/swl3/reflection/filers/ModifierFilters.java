package ru.swayfarer.swl3.reflection.filers;

import lombok.NonNull;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

import java.lang.reflect.Modifier;

public class ModifierFilters<Object_Type> extends AbstractFiltering<Object_Type, Integer>
{
    public ModifierFilters(@NonNull IFunction1<Object_Type, Integer> getModsFun)
    {
        super(getModsFun);
    }

    public IFunction1<Object_Type, Boolean> hasModifier(int mod)
    {
        return nonNullFilter((value) -> (value & mod) != 0);
    }

    public IFunction1<Object_Type, Boolean> statics()
    {
        return hasModifier(Modifier.STATIC);
    }

    public IFunction1<Object_Type, Boolean> transients()
    {
        return hasModifier(Modifier.TRANSIENT);
    }

    public IFunction1<Object_Type, Boolean> abstracts()
    {
        return hasModifier(Modifier.ABSTRACT);
    }

    public IFunction1<Object_Type, Boolean> publics()
    {
        return hasModifier(Modifier.PUBLIC);
    }

    public IFunction1<Object_Type, Boolean> protecteds()
    {
        return hasModifier(Modifier.PROTECTED);
    }

    public IFunction1<Object_Type, Boolean> privates()
    {
        return hasModifier(Modifier.PRIVATE);
    }

    public IFunction1<Object_Type, Boolean> finals()
    {
        return hasModifier(Modifier.FINAL);
    }

}
