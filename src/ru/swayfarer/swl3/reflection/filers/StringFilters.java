package ru.swayfarer.swl3.reflection.filers;

import lombok.var;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.string.StringUtils;

public class StringFilters<Object_Type> extends AbstractFiltering<Object_Type, String>
{
    public StringFilters(IFunction1<Object_Type, String> getValueFun)
    {
        super(getValueFun);
    }

    public IFunction1<Object_Type, Boolean> equal(String... values)
    {
        var valuesStream = ExtendedStream.of(values);
        return (obj) -> valuesStream.contains(getValueFun.apply(obj));
    }

    public IFunction1<Object_Type, Boolean> equalIgnoreCase(String... values)
    {
        return nonNullMatches(String::equalsIgnoreCase, values);
    }

    public IFunction1<Object_Type, Boolean> startsWith(String... starts)
    {
        return startsWith(0, starts);
    }

    public IFunction1<Object_Type, Boolean> startsWith(int offset, String... starts)
    {
        return nonNullMatches((value, start) -> value.startsWith(start, offset), starts);
    }

    public IFunction1<Object_Type, Boolean> endsWith(String... ends)
    {
        return nonNullMatches(String::endsWith, ends);
    }

    public IFunction1<Object_Type, Boolean> matchesRegex(String... regexes)
    {
        return matches((value, regex) -> StringUtils.isMatchesByRegex(regex, value), regexes);
    }

    public IFunction1<Object_Type, Boolean> matchesMask(String... masks)
    {
        return matches((value, mask) -> StringUtils.isMatchesByMask(mask, value), masks);
    }
}
