package ru.swayfarer.swl3.reflection.filers;

import lombok.var;
import ru.swayfarer.swl3.collections.stream.ExtendedStream;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.objects.EqualsUtils;

import java.lang.annotation.Annotation;

public class AnnotationsFilters<Object_Type>
{
    @Internal
    public IFunction1<Object_Type, Annotation[]> getAnnotationsFun;

    @Internal
    public IFunction2<Annotation[], Class<?>, Boolean> isAssignableFun;

    public AnnotationsFilters(
            IFunction2<Annotation[], Class<?>, Boolean> isAssignableFun,
            IFunction1<Object_Type, Annotation[]> getAnnotationsFun
    )
    {
        this.isAssignableFun = isAssignableFun;
        this.getAnnotationsFun = getAnnotationsFun;
    }

    public IFunction1<Object_Type, Boolean> contains(Class<?>... typesOfAnnotations)
    {
        return (obj) -> {
            var annotations = getAnnotationsFun.apply(obj);
            var annotationsTypes = ExtendedStream.of(annotations)
                    .map(Annotation::annotationType)
                    .toArray(Class.class)
            ;

            for (var type : typesOfAnnotations)
            {
                if (EqualsUtils.objectEqualsSome(type, (Object[]) annotationsTypes))
                    return true;
            }

            return false;
        };
    }

    public IFunction1<Object_Type, Boolean> assignable(Class<?>... typesOfAnnotations)
    {
        return (obj) -> {
            var annotations = getAnnotationsFun.apply(obj);

            for (var type : typesOfAnnotations)
            {
                if (Boolean.TRUE.equals(isAssignableFun.apply(annotations, type)))
                    return true;
            }

            return false;
        };
    }
}
