package ru.swayfarer.swl3.reflection.filers;

import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.markers.Alias;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Parameter;

public class AbstractMethodFilters<Object_Type> extends AbstractClassEntryFilters<Object_Type>
{
    @Internal
    public ReflectionsUtils reflectionsUtils;

    @Internal
    public IFunction1<Object_Type, Parameter[]> getParamsFun;

    public AbstractMethodFilters(
            IFunction1<Object_Type, ExtendedList<String>> getNameFun,
            IFunction1<Object_Type, Integer> getModsFun,
            IFunction1<Object_Type, Class<?>> getTypeFun,
            IFunction1<Object_Type, Annotation[]> getAnnotationsFun,
            IFunction1<Object_Type, Parameter[]> getParamsFun,
            ReflectionsUtils reflectionsUtils
    )
    {
        super(
                getNameFun,
                getModsFun,
                getTypeFun,
                getAnnotationsFun,

                (annotations, annotationType) ->
                    reflectionsUtils.annotations().findAnnotation()
                            .ofType(ReflectionsUtils.cast(annotationType))
                            .in(annotations)
                            .get() != null
        );

        this.getParamsFun = getParamsFun;
        this.reflectionsUtils = reflectionsUtils;
    }

    public MethodParamsFilters<Object_Type> params()
    {
        return new MethodParamsFilters<>(reflectionsUtils, getParamsFun);
    }

    @Alias("params()")
    public MethodParamsFilters<Object_Type> args()
    {
        return params();
    }
}
