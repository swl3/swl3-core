package ru.swayfarer.swl3.reflection.generic;

import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.string.dynamic.DynamicString;
import ru.swayfarer.swl3.string.dynamic.StrReader;

public class GenericParser {

    public static String genericStart = "<";
    public static String genericEnd = ">";
    public static String elementSplitter = ", ";

    public DynamicString typeBuffer = new DynamicString();
    public DynamicString childBuffer = new DynamicString();
    
    public StrReader reader;
    
    public ExceptionsHandler exceptionsHandler;
    
    public ExtendedList<GenericObject> result = CollectionsSWL.list();
    
    public void parse(ExceptionsHandler exceptionsHandler, String genericString)
    {
        this.exceptionsHandler = exceptionsHandler;
        reader = new StrReader(genericString);
        
        int openedGenerics = 0;
        
        newGeneric();
        
        while (reader.hasNextElement())
        {
            if (reader.skipSome(genericStart))
            {
                openedGenerics ++;
                
                if (openedGenerics > 1)
                {
                    childBuffer.append(genericStart);
                }
            }
            else if (reader.skipSome(genericEnd))
            {
                openedGenerics --;
                
                if (openedGenerics < 0)
                    throw new GenericParseException("Invalid generic close at " +reader.pos + "! No opened generics found there!");
                else if (openedGenerics == 0)
                {
                    parseChildren();
                }
                else
                {
                    childBuffer.append(genericEnd);
                }
            }
            else if (openedGenerics == 0)
            {
                if (reader.skipSome(elementSplitter))
                {
                    newGeneric();
                }
                else 
                {
                    typeBuffer.append(reader.next());
                }
            }
            else
            {
                childBuffer.append(reader.next());
            }
        }
        
        closeGeneric();
    }
    
    public void parseChildren()
    {
        String genericString = childBuffer.toString();
        childBuffer.clear();
        
        GenericParser genericsParser = new GenericParser();
        genericsParser.parse(exceptionsHandler, genericString);
        GenericObject current = getCurrentObject();
        
        if (current != null && !CollectionsSWL.isNullOrEmpty(genericsParser.result))
        {
            current.childs.addAll(genericsParser.result);
        }
    }
    
    public void newGeneric()
    {
        closeGeneric();
        GenericObject object = new GenericObject();
        object.setExceptionsHandler(exceptionsHandler);
        result.add(object);
    }
    
    public void closeGeneric()
    {
        GenericObject object = getCurrentObject();
        
        if (object != null)
        {
            String string = typeBuffer.toString();
            object.setTypeCanonicalName(string);
            typeBuffer.clear();
            childBuffer.clear();
        }
    }
    
    public GenericObject getCurrentObject()
    {
        return result.last();
    }
}