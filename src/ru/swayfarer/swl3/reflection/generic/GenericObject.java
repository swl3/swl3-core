package ru.swayfarer.swl3.reflection.generic;

import java.lang.reflect.Type;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.objects.ObjectsUtils;
import ru.swayfarer.swl3.reflection.ClassUtil;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;
import ru.swayfarer.swl3.thread.ThreadsUtils;

@Getter
@Setter
@Accessors(chain = true)
public class GenericObject {

    public static GenericObject objectType = new GenericObject();

    public ExtendedList<GenericObject> childs = CollectionsSWL.list();
    public String typeCanonicalName = "<empty>";
    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
    public IFunction0<Class<?>> cachedClass = ThreadsUtils.singleton(() -> getExceptionsHandler().safeReturn(() -> ClassUtil.forName(typeCanonicalName), null));

    public GenericObject()
    {
        this(CollectionsSWL.list());
    }

    public GenericObject(ExtendedList<GenericObject> children)
    {
        this(Object.class, children);
    }

    public GenericObject(Class<?> classOfObject, ExtendedList<GenericObject> children)
    {
        this.childs = ObjectsUtils.orDefault(children, ExtendedList::new);
    }

    public boolean isClassPresented()
    {
        try
        {
            loadClass();
            return true;
        }
        catch (Throwable e)
        {
            
        }
        
        return false;
    }
    
    public boolean hasChildren()
    {
        return !CollectionsSWL.isNullOrEmpty(childs);
    }
    
    public Class<?> loadClass()
    {
        return cachedClass.apply();
    }
    
    public Type asGenericType()
    {
        if (childs.isEmpty())
            return loadClass();

        return new GenericObjectTypeImplementation(this);
    }
    
    public String toString(int indent)
    {
        DynamicString ret = new DynamicString();
        
        String indentStr = StringUtils.createSpacesSeq(4 * indent);
        
        ret.append("\n");
        ret.append(indentStr);
        ret.append(typeCanonicalName);
        
        if (!CollectionsSWL.isNullOrEmpty(childs))
        {
            ret.append("{");
            
            for (var child : childs)
            {
                ret.append(child.toString(indent + 1));
            }

            
            ret.append("\n");
            ret.append(indentStr);
            ret.append("}");
        }
        
        return ret.toString();
    }
    
    public String toGenericString()
    {
        var buffer = new DynamicString();
        buffer.append(typeCanonicalName);
        
        if (!CollectionsSWL.isNullOrEmpty(childs))
        {
            var first = true;
            buffer.append("<");
            for (var child : childs)
            {
                if (!first)
                    buffer.append(",");
                
                first = false;
                
                buffer.append(child.toGenericString());
            }
            buffer.append(">");
        }
        return buffer.toString();
    }
    
    public String toString()
    {
        return toString(0);
    }
}
