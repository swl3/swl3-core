package ru.swayfarer.swl3.reflection.generic;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.markers.Internal;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

@Getter
@Setter
@Accessors(chain = true)
public class GenericObjectTypeImplementation implements ParameterizedType
{
    @Internal
    public Class<?> rawType;

    @Internal
    public GenericObject genericObject;

    public GenericObjectTypeImplementation(
            GenericObject genericObject
    )
    {
        this.rawType = genericObject.loadClass();
        this.genericObject = genericObject;
    }

    @Override
    public Type[] getActualTypeArguments()
    {
        return getTypesFromChilds(genericObject);
    }

    public Type[] getTypesFromChilds(GenericObject genericObject)
    {
        var childs = genericObject.getChilds();
        var types = new ExtendedList<Type>();

        for (var child : childs)
        {
            if (CollectionsSWL.isNullOrEmpty(child.getChilds()))
            {
                types.add(genericObject.loadClass());
            }
            else
            {
                types.add(new GenericObjectTypeImplementation(child));
            }
        }

        return types.exStream().toArray(Type[]::new);
    }

    @Override
    public String getTypeName()
    {
        return genericObject.toGenericString();
    }

    @Override
    public Type getRawType()
    {
        return rawType;
    }

    @Override
    public Type getOwnerType()
    {
        return null;
    }

    public String toString()
    {
        return getTypeName();
    }
}
