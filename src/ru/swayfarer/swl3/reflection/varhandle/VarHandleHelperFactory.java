package ru.swayfarer.swl3.reflection.varhandle;

import lombok.SneakyThrows;
import lombok.var;
import ru.swayfarer.swl3.reflection.ClassUtil;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodHandles.Lookup;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicBoolean;

public class VarHandleHelperFactory
{
    public static VarHandleHelperFactory INSTANCE = new VarHandleHelperFactory();

    public AtomicBoolean inited = new AtomicBoolean();
    public Class<?> classOfVarHandle;
    public Class<?> classOfLookup;
    public Class<?> classOfMethodHandles = MethodHandles.class;
    public Method privateLookupMethod;
    public Method findVarHandleMethod;

    @SneakyThrows
    public void init()
    {
        if (!inited.get())
        {
            classOfLookup = Lookup.class;
            classOfVarHandle = ClassUtil.forName("java.lang.invoke.VarHandle");;

            privateLookupMethod = classOfMethodHandles.getDeclaredMethod(
                    "privateLookupIn",
                    Class.class, Lookup.class
            );

            findVarHandleMethod = classOfLookup.getDeclaredMethod(
                    "findVarHandle",
                    Class.class, String.class, Class.class
            );

            inited.set(true);
        }
    }

    @SneakyThrows
    public VarHandleHelper getFieldVarHandle(Class<?> targetClass, String fieldName, Class<?> fieldType)
    {
        init();
        var lookup = MethodHandles.lookup();
        var privateLookup = privateLookupMethod.invoke(null, targetClass, lookup);
        var varHandle = findVarHandleMethod.invoke(privateLookup, Field.class, fieldName, fieldType);

        var helper = new VarHandleHelper(varHandle);
        return helper;
    }

    public static boolean isVarHandlesSupported()
    {
        try
        {
            ClassUtil.forName("java.lang.invoke.VarHandle");
            return true;
        }
        catch (Throwable e)
        {
            // Nope!
        }

        return false;
    }

    public static VarHandleHelperFactory getInstance()
    {
        return INSTANCE;
    }
}
