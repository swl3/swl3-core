package ru.swayfarer.swl3.reflection.varhandle;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ClassUtil;

import java.lang.reflect.Method;

@Getter
@Setter
@Accessors(chain = true)
public class VarHandleHelper
{
    @Internal
    public Class<?> varHandleClass;

    @Internal
    public Method setValueMethod;

    @Internal
    public Object varHandleInstance;

    @SneakyThrows
    public VarHandleHelper(Object varHandleInstance)
    {
        this.varHandleInstance = varHandleInstance;

        varHandleClass = ClassUtil.forName("java.lang.invoke.VarHandle");

        setValueMethod = varHandleClass
                .getDeclaredMethod("set", Object[].class)
        ;
    }

    @SneakyThrows
    public void setValue(Object owner, Object value)
    {
        setValueMethod.invoke(varHandleInstance, (Object) new Object[] {owner, value});
    }
}
