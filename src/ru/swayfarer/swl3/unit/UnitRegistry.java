package ru.swayfarer.swl3.unit;

import static ru.swayfarer.swl3.string.CharFilters.its;
import static ru.swayfarer.swl3.string.CharFilters.not;
import static ru.swayfarer.swl3.string.CharFilters.nums;
import static ru.swayfarer.swl3.string.CharFilters.oneShot;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.string.StringUtils;

@Getter
@Setter
@Accessors(chain = true)
@SuppressWarnings("unchecked")
public class UnitRegistry {
    
    public static final int NumIndex = 0;
    public static final int UnitIndex = 1;
    
    public IFunction1<Character, Boolean> firstNotNumberFilter = 
            not(
                nums().or(oneShot(its(',', '.')))
            )
    ;
    
    public ExtendedMap<String, Double> registeredUnits = CollectionsSWL.exMap(
            "default", 1d
    );
    
    public <T extends UnitRegistry> T unit(@NonNull String str, double weight)
    {
        registeredUnits.put(str, weight);
        return (T) this;
    }
    
    public Double parse(@NonNull String bytesWithUnit)
    {
        if (StringUtils.isBlank(bytesWithUnit))
            return -1d;
        
        var split = bytesWithUnit.split(" ");
        
        double ret = 0;
        
        for (var part : split)
        {
            var unitAndNum = splitUnitAndNum(part);
            
            var num = StringUtils.strip(unitAndNum[NumIndex])
                    .replace(",", ".")
            ;
            
            var units = StringUtils.strip(unitAndNum[UnitIndex]);
            
            ExceptionsUtils.IfNot(
                    StringUtils.isDouble(num), 
                    IllegalArgumentException.class, 
                    "'" + num + "' in '" + bytesWithUnit + "' is not a double! Please, use number at this place!"
            );
            
            var numDouble = Double.valueOf(num);
            
            ExceptionsUtils.IfNot(
                    numDouble > 0, 
                    IllegalArgumentException.class, 
                    "'" + numDouble + "' in '" + bytesWithUnit + "' smaller than 0! Please, use number bigger than 0!"
            );
            
            var unitWeight = registeredUnits.get(units);
            
            ExceptionsUtils.IfNull(
                    unitWeight, 
                    IllegalArgumentException.class, 
                    "Unit '" + units + "' in '" + bytesWithUnit + "' is not registered unit! Please, registered unit, as", registeredUnits.keySet(), "!"
            );
            
            ret += Math.round(numDouble * unitWeight);
        }
        
        return ret;
    }
    
    public String[] splitUnitAndNum(@NonNull String str)
    {
        var indexOfLastNonNum = StringUtils.indexOf(str, firstNotNumberFilter);
        
        if (indexOfLastNonNum > 0)
        {
            var num = str.substring(0, indexOfLastNonNum);
            var unit = indexOfLastNonNum < str.length() ? str.substring(indexOfLastNonNum) : "default";
            
            return new String[] {num, unit};
        }
        else if (indexOfLastNonNum == 0)
        {
            return new String[] {"1", str};
        }
        
        return new String[] {str, "default"};
    }
}
