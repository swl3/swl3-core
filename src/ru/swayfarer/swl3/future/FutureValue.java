package ru.swayfarer.swl3.future;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandleBuilder;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;

@Getter
@Setter
@Accessors(chain = true)
public class FutureValue<Result_Type>
{
    public IFunction0<Result_Type> invokeFun;

    public Result_Type getResult()
    {
        return invokeFun.apply();
    }

    public <T> T orHandle(ExceptionsHandler exceptionsHandler, Object message)
    {
        return orHandle(exceptionsHandler, String.valueOf(message));
    }

    public <T> T orHandle(ExceptionsHandler exceptionsHandler, String message)
    {
        return orHandle(exceptionsHandler, (handle) -> {
           handle.message(message);
        });
    }

    public <T> T orHandle(ExceptionsHandler exceptionsHandler, IFunction1NoR<ExceptionsHandleBuilder> configurator)
    {
        if (exceptionsHandler == null)
            return null;

        var handleBuilder= exceptionsHandler.handle();

        if (configurator != null)
        {
            configurator.apply(handleBuilder);
        }

        return (T) handleBuilder.andReturn(invokeFun);
    }

    public Result_Type orThrow(IFunction1<Throwable, Throwable> wrapperFun)
    {
        try
        {
            return invokeFun.apply();
        }
        catch (Throwable e)
        {
            var wrappedException = wrapperFun.apply(e);
            ExceptionsUtils.throwException(wrappedException);
        }

        return null;
    }
}
