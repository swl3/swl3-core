package ru.swayfarer.swl3.observable.event;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class EventSource {

	@NonNull
	public Throwable sourceThrowable = new Throwable();
	
	public volatile Object lock = new Object();
	public volatile ExtendedList<StackTraceElement> cachedStacktrace;
	
	public int offset = 1;
	
	public StackTraceElement getSource()
	{
		var elements = getStacktrace();
		return offset < 0 || elements.size() <= offset ? null : elements.get(offset);
	}
	
	public EventSource(int offset)
	{
	    this.offset = offset;
	}
	
	public EventSource incrementOffset()
	{
	    offset ++;
	    return this;
	}
	
	public ExtendedList<StackTraceElement> getStacktrace()
	{
	   
	    if (cachedStacktrace == null)
	    {
	        synchronized (lock)
	        {
	            if (cachedStacktrace == null)
	            {
	                cachedStacktrace = CollectionsSWL.list(sourceThrowable.getStackTrace());
	            }
            }
	    }
	    
	    return cachedStacktrace;
	}
}
