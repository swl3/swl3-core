package ru.swayfarer.swl3.observable.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.markers.Internal;

/**
 * Простой шаблон для отменяемого события 
 * @author swayfarer
 */

@Data @EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@SuppressWarnings("unchecked")
public abstract class AbstractCancelableEvent extends AbstractEvent{
	
	/** Отменено ли событие */
	@Internal
	public boolean isCanceled;
	
	/** Задать специальную функцию, которая может запрерить, или разрешить применение {@link #setCanceled(boolean)} */
	@Internal
	public IFunction2<StackTraceElement, AbstractCancelableEvent, Boolean> cancelAcceptor;
	
	/** Отменено ли событие */
	public boolean isCanceled()
	{
		return isCanceled;
	}
	
	/** 
	 * Задать статус события 
	 * @param isCanceled Отменено ли?
	 * @return Оригинальное событие с примененными изменениями 
	 */
	public <T extends AbstractCancelableEvent> T setCanceled(boolean isCanceled)
	{
		this.isCanceled = isCanceled;
		return (T) this;
	}
}
