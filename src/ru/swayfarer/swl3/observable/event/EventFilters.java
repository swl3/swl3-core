package ru.swayfarer.swl3.observable.event;

import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.objects.EqualsUtils;

import java.util.Collection;

public class EventFilters
{
    public static <T> IFunction1<EventHandlingEvent<T>, Boolean> equal(Object... objects)
    {
        return (handlingEvent) -> EqualsUtils.objectEqualsSome(handlingEvent.getEventObject(), objects);
    }

    public static <T> IFunction1<EventHandlingEvent<T>, Boolean> notCanceled()
    {
        return (handlingEvent) -> {
            var eventObj = handlingEvent.getEventObject();

            if (eventObj == null)
                return true;

            if (eventObj instanceof AbstractCancelableEvent)
                return !((AbstractCancelableEvent) eventObj).isCanceled();

            return true;
        };
    }


    public static <T, E extends Collection<?>> IFunction1<EventHandlingEvent<T>, Boolean> isNotEmpty(IFunction1<T, E> getter)
    {
        return is(getter, (value) -> !CollectionsSWL.isNullOrEmpty(value));
    }

    public static <T, E> IFunction1<EventHandlingEvent<T>, Boolean> is(IFunction1<T, E> getter, IFunction1<E, Boolean> filter)
    {
        return (handlingEvent) -> {
            var eventObj = handlingEvent.getEventObject();

            if (eventObj == null)
                return true;

            var value = getter.apply(eventObj);
            return filter.apply(value);
        };
    }
}
