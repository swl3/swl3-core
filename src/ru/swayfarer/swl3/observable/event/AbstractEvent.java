package ru.swayfarer.swl3.observable.event;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public abstract class AbstractEvent {
    
    @Builder.Default
    @NonNull
    public Map<String, Object> customAttributes = new HashMap<>();
    
    public ExtendedMap<String, Object> getCustomAttributes()
    {
        return new ExtendedMap<>(customAttributes);
    }
    
    public ExtendedList<String> getTags()
    {
    	var arrributesList = getCustomAttributes();
		var tagsList = arrributesList.getOrCreate("tags", () -> new ExtendedList<String>());
		return tagsList;
    }

    public AbstractEvent tag(@NonNull Iterable<String> tags)
    {
        var tagsList = getTags();
        for (var e : tags)
            tagsList.add(e);

        return this;
    }
    
    public AbstractEvent tag(@NonNull String... tags)
    {
		var tagsList = getTags();
		tagsList.addAll(tags);
		return this;
    }

    public boolean hasTag(@NonNull String... tags)
    {
        return getTags().containsAll(Arrays.asList(tags));
    }
}
