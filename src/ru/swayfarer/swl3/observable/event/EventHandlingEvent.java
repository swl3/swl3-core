package ru.swayfarer.swl3.observable.event;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.sub.Subscription;

@Getter
@Setter
@Accessors(chain = true)
public class EventHandlingEvent<Event_Type> extends AbstractEvent
{
    @Internal
    public Event_Type eventObject;

    @Internal
    public Subscription<Event_Type> subscription;

    public static IFunction0NoR getAllCanceler(EventHandlingEvent<?> event)
    {
        return event.getCustomAttributes().getValue("swl3/events/handling/canceler");
    }

    public static void setAllCanceler(EventHandlingEvent<?> event, IFunction0NoR cancelerFun)
    {
        event.getCustomAttributes().put("swl3/events/handling/canceler", cancelerFun);
    }
}
