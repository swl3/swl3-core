package ru.swayfarer.swl3.observable.sub;

import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.WeakList;
import ru.swayfarer.swl3.exception.LibTags.Events;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.observable.bus.BusSubscriptionHelper;
import ru.swayfarer.swl3.observable.event.EventHandlingEvent;
import ru.swayfarer.swl3.string.StringUtils;

import java.util.Arrays;

public class SubscriptionFilters
{
    public static <T> IFunction1<Subscription<T>, Boolean> eventBusOwner(Object... instances)
    {
        var instancesList = new WeakList<>(Arrays.asList(instances));
        return SubscriptionFilters.<T>eventBus().and((sub) ->
            instancesList.contains(BusSubscriptionHelper.getBusOwnerInstance(sub))
        );
    }

    public static <T> IFunction1<Subscription<T>, Boolean> eventBus()
    {
        return hasTags(Events.tagEventBus);
    }

    public static <T> IFunction1<Subscription<T>, Boolean> hasTags(String... tags)
    {
        var tagsList = CollectionsSWL.list(tags);
        return (sub) -> {
            for (var tag : tagsList)
            {
                if (!StringUtils.isBlank(tag) && !sub.hasTag(tag))
                    return false;
            }

            return true;
        };
    }

    public static <T> IFunction1<EventHandlingEvent<T>, Boolean> asEventFilter(IFunction1<Subscription<T>, Boolean> subFilter)
    {
        return (handleEvent) -> subFilter.apply(handleEvent.getSubscription());
    }
}
