package ru.swayfarer.swl3.observable.sub;

import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2NoR;

import java.util.Deque;
import java.util.Queue;

public class ElementSelectors
{
    public static <Event_Type> IFunction2NoR<Deque<Event_Type>, Deque<Event_Type>> last()
    {
        return (allQueue, current) -> {
            if (!allQueue.isEmpty())
                current.add(allQueue.pollLast());
        };
    }

    public static <Event_Type> IFunction2NoR<Deque<Event_Type>, Deque<Event_Type>> last(int max)
    {
        return (allQueue, current) -> {
            if (!allQueue.isEmpty())
            {
                var size = allQueue.size();
                var maxIndex = Math.min(max, size);

                for (var i1 = 0; i1 < maxIndex; i1 ++)
                {
                    current.add(allQueue.pollLast());
                }
            }
        };
    }
}
