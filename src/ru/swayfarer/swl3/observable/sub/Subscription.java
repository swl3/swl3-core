package ru.swayfarer.swl3.observable.sub;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.exception.LibTags;
import ru.swayfarer.swl3.exception.LibTags.Exceptions;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction3NoR;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.markers.Alias;
import ru.swayfarer.swl3.observable.Observable;
import ru.swayfarer.swl3.observable.event.EventHandlingEvent;
import ru.swayfarer.swl3.observable.executor.ObservableExcecutors;

@SuppressWarnings("unchecked")
@Data
@Accessors(chain = true)
public class Subscription<Event_Type> {

    public int priority;
    
	@NonNull 
	public ExceptionsHandler exceptionsHandler;
	
	/**
	 *  Объект, который подписывали на {@link Observable}. <br>
	 *  В некоторых случаях, при подписке чего-либо создается обертка для соответствия {@link #task} <br>
	 *  Для того, чтобы определить исходного подписчика, его хранят в этом поле
	 */
	@NonNull
	public Object taskObject;
	
	@NonNull
	public ExtendedMap<String, Object> customAttributes = new ExtendedMap<>().concurrent();
	
	@NonNull 
	public IFunction1NoR<EventHandlingEvent<Event_Type>> task;
	
	@NonNull 
	public ISubExecutor<Event_Type> executor = (event) -> event.getSubscription().getTask().apply(event);
	
	@NonNull
	public List<IFunction1NoR<Throwable>> errorHandlers = Collections.synchronizedList(new ArrayList<>());
	
	@NonNull
	public FunctionsChain1<EventHandlingEvent<Event_Type>, Boolean> filters = new FunctionsChain1<>();
	
	@NonNull
	public AtomicBoolean disposed = new AtomicBoolean(false);

	protected static AtomicLong nextDelaySubscriptionExecutionThread = new AtomicLong();

	public Subscription(@NonNull ExceptionsHandler exceptionsHandler, @NonNull IFunction1NoR<EventHandlingEvent<Event_Type>> task)
	{
		this.exceptionsHandler = exceptionsHandler;
		this.task = task;
	}
	
	public Subscription<Event_Type> dispose()
	{
		disposed.set(true);
		return this;
	}
	
	public <T> T getTaskObject()
	{
	    return (T) taskObject;
	}
	
	public boolean isDisposed()
	{
		return disposed.get();
	}
	
	public void execute(EventHandlingEvent<Event_Type> event)
	{
	    if (isNotAccepts(event))
	    {
	        return;
	    }
	    
		try
		{
			executor.apply(event);
		}
		catch (Throwable e)
		{
			for (var fun : errorHandlers)
			{
				exceptionsHandler.safe(() -> {
					fun.apply(e);
				}, "Error while processing error handler", fun); 
			}

			exceptionsHandler.handle()
					.tag(Exceptions.tagObservable)
					.tag(Exceptions.tagObservableSubscription)
					.data(Exceptions.keyObservableSubscription, this)
					.message("Error while processing subscribed task", taskObject)
					.throwable(e)
					.start()
			;
		}
	}

    public boolean isNotAccepts(EventHandlingEvent<Event_Type> event)
    {
        return filters.funs.size() > 0 && !Boolean.TRUE.equals(filters.apply(event));
    }
    
    public Subscription<Event_Type> executeBy(Executor executor)
    {
        return executeBy(ObservableExcecutors.forSubscription(executor));    
    }
	
	public Subscription<Event_Type> executeBy(ISubExecutor<Event_Type> executor)
	{
		this.executor = executor;
		return this;
	}
	
	public TimedObservableListener<Event_Type> executePer(long milisis, boolean isDaemon)
	{
		var listener = TimedObservableListener.of(this);
		
		setExecutor(listener::addEvent);

		listener.setName("DelaySubscriptionExecutionThread-" + nextDelaySubscriptionExecutionThread.getAndIncrement());
		listener.listenDelay.set(milisis);
		listener.setDaemon(isDaemon);
		listener.start();
		
		return listener;
	}
	
	@Alias("filter")
	public Subscription<Event_Type> executeIf(@NonNull IFunction1<EventHandlingEvent<Event_Type>, Boolean> fun)
    {
	    return filter(fun);
    }
	
	public Subscription<Event_Type> filter(@NonNull IFunction1<EventHandlingEvent<Event_Type>, Boolean> fun)
	{
	    filters.add(fun);
	    return this;
	}
	
	public boolean hasTag(@NonNull String tag)
	{
	    ExtendedList<String> tags = customAttributes.getValue("tags");
	    
	    return tags == null ? false : tags.contains(tag);
	}
    
    public Subscription<Event_Type> tag(@NonNull String tag)
    {
        ExtendedList<String> tags = customAttributes.getValue("tags");
        
        if (tags == null)
        {
            synchronized (customAttributes)
            {
                tags = customAttributes.getValue("tags");
                
                if (tags == null)
                {
                    tags = new ExtendedList<>().synchronyzed();
                    customAttributes.put("tags", tags);
                }
            }
        }
        
        tags.add(tag);
        return this;
    }
	
	public Subscription<Event_Type> attribute(@NonNull String key, @NonNull Object value)
	{
	    customAttributes.put(key, value);
	    return this;
	}
	
	public Subscription<Event_Type> onError(IFunction1NoR<Throwable> fun)
	{
		errorHandlers.add(fun);
		return this;
	}
	
	public static interface ISubExecutor <Event_Type> extends IFunction1NoR<EventHandlingEvent<Event_Type>> {}
	
}
