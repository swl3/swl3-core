package ru.swayfarer.swl3.observable.sub;

import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicLong;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2NoR;
import ru.swayfarer.swl3.observable.event.EventHandlingEvent;

@Getter @Setter @Accessors(chain = true, fluent = true)
public class TimedObservableListener<Event_Type> extends Thread {

	public ExceptionsHandler exceptionsHandler;
	public Subscription<Event_Type> subscription;

	public IFunction2NoR<Deque<EventHandlingEvent<Event_Type>>, Deque<EventHandlingEvent<Event_Type>>> elementsSelector = (all, current) -> current.addAll(all);
	public IFunction1NoR<TimedObservableListener<Event_Type>> backpressureHandler = (listener) -> listener.currentEventsQueue.clear();
	
	public volatile Deque<EventHandlingEvent<Event_Type>> eventsQueue = new ConcurrentLinkedDeque<>();
	public volatile Deque<EventHandlingEvent<Event_Type>> currentEventsQueue = new ConcurrentLinkedDeque<>();
	
	public AtomicLong listenDelay = new AtomicLong(1);
	
	public TimedObservableListener<Event_Type> addEvent(EventHandlingEvent<Event_Type> event)
	{	
		eventsQueue.add(event);
		return this;
	}
	
	public void checkBackPressure()
	{
		if (!currentEventsQueue.isEmpty())
		{
			synchronized (eventsQueue) {
				
				exceptionsHandler.safe(() -> {
					backpressureHandler.apply(this);
				}, "Error while applying backpressure handler");
				
			}
		}
	}
	
	@SneakyThrows
	@Override
	public void run() 
	{
		for (;;)
		{
			exceptionsHandler.safe(() -> {
				
				if (subscription.isDisposed())
					return;
				
				checkBackPressure();
				
				synchronized (eventsQueue) {
					elementsSelector.apply(eventsQueue, currentEventsQueue);
					eventsQueue.clear();
				}

				EventHandlingEvent<Event_Type> event = null;
				
				while ((event = currentEventsQueue.poll()) != null)
				{
					processEvent(event);
				}
				
			}, "Error while listeting observable");
			
			sleep(listenDelay.get());
		}
	}
	
	public void processEvent(EventHandlingEvent<Event_Type> event)
	{
		subscription.task.apply(event);
	}
	
	public static <Event_Type> TimedObservableListener<Event_Type> of(Subscription<Event_Type> subscription)
	{
		return new TimedObservableListener<Event_Type>()
				.subscription(subscription)
				.exceptionsHandler(subscription.getExceptionsHandler())
		;
	}
	
}
