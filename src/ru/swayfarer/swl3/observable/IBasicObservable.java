package ru.swayfarer.swl3.observable;

import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

public interface IBasicObservable <Event_Type>
{
    public IObservable<Event_Type> next(Event_Type event);
    public <New_Event_Type> IObservable<New_Event_Type> map(IFunction1<Event_Type, New_Event_Type> mapper);

    public SubscriptionHelper<Event_Type> subscribe();
    public SubscriptionExceptionsHelper<Event_Type> exceptions();
    public SubscriptionExecutionHelper<Event_Type> execution();
    public TaskObjectsHelper<Event_Type> tasks();
}
