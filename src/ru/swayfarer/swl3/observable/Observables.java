package ru.swayfarer.swl3.observable;

import ru.swayfarer.swl3.observable.property.ObservableProperty;

public class Observables {

	public static <T> IObservable<T> createObservable()
	{
		return new Observable<>();
	}
	
	public static <T> ObservableProperty<T> createProperty(T value)
	{
	    return new ObservableProperty<T>()
	            .setValue(value)
        ;
	}
}
