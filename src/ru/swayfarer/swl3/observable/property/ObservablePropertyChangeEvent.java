package ru.swayfarer.swl3.observable.property;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.observable.event.AbstractCancelableEvent;

@Data @EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@SuperBuilder
public class ObservablePropertyChangeEvent<Element_Type> extends AbstractCancelableEvent {
    
    public Element_Type newValue;
    
    public Element_Type oldValue;
    
    @NonNull
    public ObservableProperty<Element_Type> targetProperty;
}
