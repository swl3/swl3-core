package ru.swayfarer.swl3.observable.property;

import lombok.Data;
import lombok.NonNull;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.observable.property.binding.BindingHelper;

@Data
@Accessors(chain = true)
@SuppressWarnings("unchecked")
public class ObservableProperty<Element_Type> {
    
    @ToString.Exclude
    @NonNull
    public IObservable<ObservablePropertyChangeEvent<Element_Type>> eventPreChange = Observables.createObservable();
    
    @ToString.Exclude
    @NonNull
    public IObservable<ObservablePropertyChangeEvent<Element_Type>> eventPostChange = Observables.createObservable();
    
    @ToString.Include
    @Internal
    @Deprecated
    public Element_Type value;
    
    public <T extends ObservableProperty<Element_Type>> T setValue(Element_Type value)
    {
        var event = ObservablePropertyChangeEvent.<Element_Type>builder()
                .targetProperty(this)
                .oldValue(getValue())
                .newValue(value)
                .build()
        ;
        
        eventPreChange.next(event);
        
        if (event.isCanceled())
            return (T) this;
        
        this.value = event.getNewValue();
        
        eventPostChange.next(event);
        
        return (T) this;
    }
    
    public <T extends Element_Type> T getValue()
    {
        return (T) value;
    }
    
    public ObservableProperty<Element_Type> onPreUpdateValue(@NonNull IFunction1NoR<ObservablePropertyChangeEvent<Element_Type>> fun)
    {
        getEventPreChange().subscribe().by(fun);
        return this;
    }
    
    public ObservableProperty<Element_Type> onPostUpdateValue(@NonNull IFunction1NoR<ObservablePropertyChangeEvent<Element_Type>> fun)
    {
        getEventPostChange().subscribe().by(fun);
        return this;
    }
    
    public BindingHelper<Element_Type> retranslate()  
    {
        //jfldfdjfjdkfj
        return new BindingHelper<>(this);
    }
}
