package ru.swayfarer.swl3.observable.property;

import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.unit.UnitRegistry;

import java.util.Date;

@Getter
@Setter
@Accessors(chain = true)
public class DateTimeUtils {

    public static DateTimeUtils INSTANCE = new DateTimeUtils();

    @Internal
    public UnitRegistry timeUnitRegistry = new UnitRegistry()
            .unit("sec", 1000)
            .unit("min", 1000 * 60)
            .unit("hour", 1000 * 60 * 60)
            .unit("day", 1000 * 60 * 60 * 24)
            .unit("week", 1000 * 60 * 60 * 24 * 7)
    ;
    
    public Long milisecondsOf(@NonNull String timeString)
    {
        return timeUnitRegistry.parse(timeString).longValue();
    }

    public static DateTimeUtils getInstance()
    {
        return INSTANCE;
    }
}
