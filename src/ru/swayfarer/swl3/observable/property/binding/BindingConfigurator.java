package ru.swayfarer.swl3.observable.property.binding;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.observable.property.ObservableProperty;
import ru.swayfarer.swl3.observable.property.PropertyBinding;

@Data
@Accessors(chain = true)
public class BindingConfigurator<Element_Type> {
    
    @NonNull
    public ObservableProperty<Element_Type> targetProperty;
    
    @NonNull
    public PropertyBinding<Element_Type> binding = new PropertyBinding<>();
    
    public BindingConfigurator<Element_Type> to(@NonNull ObservableProperty<? super Element_Type> property)
    {
        binding.listeners.add(property);
        return this;
    }
    
    public ObservableProperty<Element_Type> bind()
    {
        targetProperty.eventPostChange.subscribe().by(binding);
        return targetProperty;
    }
}
