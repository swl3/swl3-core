package ru.swayfarer.swl3.observable.property.binding;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.observable.property.ObservableProperty;
import ru.swayfarer.swl3.observable.property.PropertyBinding;

@Data
@Accessors(chain = true)
public class BindingRemover<Element_Type> {
    
    @NonNull
    public ObservableProperty<Element_Type> targetProperty;
    
    @NonNull
    public ExtendedList<ObservableProperty<? super Element_Type>> propertiesToRemove = new ExtendedList<>();
    
    public BindingRemover<Element_Type> that(@NonNull ObservableProperty<? super Element_Type> property)
    {
        propertiesToRemove.add(property);
        return this;
    }
    
    public ObservableProperty<Element_Type> property()
    {
        return targetProperty;
    }
    
    public BindingRemover<Element_Type> remove()
    {
        var eventPostUpdate = targetProperty.getEventPostChange();
        
        var bindings = eventPostUpdate.tasks()
                .findTaskObjects(PropertyBinding.class)
        ;
        
        for (var binding : bindings)
        {
            var listeners = binding.getListeners();
            listeners.removeAll(propertiesToRemove);
        }
        
        return this;
    }
}
