package ru.swayfarer.swl3.observable.property.binding;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.observable.property.ObservableProperty;

@Data
@Accessors(chain = true)
public class BindingHelper<Element_Type>  {
    
    @NonNull
    public ObservableProperty<Element_Type> targetProperty;
    
    public BindingConfigurator<Element_Type> add()
    {
        return new BindingConfigurator<>(targetProperty);
    }
    
    public BindingRemover<Element_Type> remove()
    {
        return new BindingRemover<>(targetProperty);
    }
    
    public ObservableProperty<Element_Type> addTwin(@NonNull ObservableProperty<Element_Type> property)
    {
        targetProperty.retranslate().add().to(property).bind();
        property.retranslate().add().to(targetProperty).bind();
        
        return targetProperty;
    }
}
