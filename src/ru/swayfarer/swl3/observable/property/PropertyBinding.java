package ru.swayfarer.swl3.observable.property;

import java.util.concurrent.atomic.AtomicLong;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.thread.ThreadsUtils;

@Data
@Accessors(chain = true)
public class PropertyBinding<Element_Type> implements IFunction1NoR<ObservablePropertyChangeEvent<Element_Type>> {

    public IFunction0<AtomicLong> currentBindingState = ThreadsUtils.threadLocal(AtomicLong::new);
    
    public IFunction0<ExtendedList<ObservableProperty<? super Element_Type>>> alreadyBinded = ThreadsUtils.threadLocal(
            () -> new ExtendedList<>()
    );
    
    public ExtendedList<ObservableProperty<? super Element_Type>> listeners = new ExtendedList<>();
    
    @Override
    public void applyNoRUnsafe(ObservablePropertyChangeEvent<Element_Type> event)
    {
        var property = event.getTargetProperty();
        
        var state = currentBindingState.apply();
        var bindings = alreadyBinded.apply();
        var value = event.getNewValue();
        
        state.incrementAndGet();
        
        for (var listener : listeners)
        {
            if (!bindings.contains(listener))
            {
                bindings.add(property);
                listener.setValue(value);
            }
        }
        
        var currentState = state.decrementAndGet();
        
        if (currentState == 0)
        {
            bindings.clear();
        }
    }
}
