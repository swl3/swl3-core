package ru.swayfarer.swl3.observable;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2NoR;
import ru.swayfarer.swl3.observable.event.EventHandlingEvent;
import ru.swayfarer.swl3.observable.sub.Subscription;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Getter
@Setter
@Accessors(chain = true)
public class Observable<Event_Type> implements IObservable<Event_Type> {

    @NonNull
    public IFunction1NoR<EventHandlingEvent<Event_Type>> executor = (handleEvent) -> handleEvent.getSubscription().execute(handleEvent);

    public IFunction1<EventHandlingEvent<Event_Type>, Boolean> executionFilter;
    
	public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();

	public List<Subscription<Event_Type>> subscriptions = new CopyOnWriteArrayList<>();
	
	@Override
	public IObservable<Event_Type> next(Event_Type event) {

		sortSubscriptions();
		subscriptions.removeIf(Subscription::isDisposed);
		
		for (var sub : subscriptions)
		{
			var handlingEvent = createEvent(event, sub);
		    executor.apply(handlingEvent);
		}
		
		return this;
	}

	public EventHandlingEvent<Event_Type> createEvent(Event_Type event, Subscription<Event_Type> subscription)
	{
		var newEvent = new EventHandlingEvent<Event_Type>();
		newEvent.setSubscription(subscription);
		newEvent.setEventObject(event);

		return newEvent;
	}

	@Override
	public <New_Event_Type> IObservable<New_Event_Type> map(IFunction1<Event_Type, New_Event_Type> mapper) {
		var mapped = new Observable<New_Event_Type>();
		subscribe().by((evt) -> mapped.next(mapper.apply(evt)));
		return mapped;
	}

	@Override
	public SubscriptionHelper<Event_Type> subscribe()
	{
		return new SubscriptionHelper<>(this, this::subscribe);
	}

	@Override
	public SubscriptionExceptionsHelper<Event_Type> exceptions()
	{
		return new SubscriptionExceptionsHelper<>(this, this::getExceptionsHandler);
	}

	@Override
	public SubscriptionExecutionHelper<Event_Type> execution()
	{
		return new SubscriptionExecutionHelper<Event_Type>(this, this::setExecutor, this::setExecutionFilter, this::getExecutionFilter);
	}

	@Override
	public TaskObjectsHelper<Event_Type> tasks()
	{
		return new TaskObjectsHelper<>(this, this::getSubscriptions);
	}

	public Subscription<Event_Type> subscribe(IFunction1NoR<EventHandlingEvent<Event_Type>> handler)
	{
		var sub = new Subscription<Event_Type>(exceptionsHandler, makeFilteredTask(handler));
		sub.setTaskObject(handler);
		subscriptions.add(sub);

		return sub;
	}

	public IFunction1NoR<EventHandlingEvent<Event_Type>> makeFilteredTask(IFunction1NoR<EventHandlingEvent<Event_Type>> original)
	{
		return (event) -> {
			if (isNeedsToExec(event))
			{
				original.apply(event);
			}
		};
	}

    public synchronized IObservable<Event_Type> sortSubscriptions()
    {
        subscriptions.sort(Comparator.comparingInt(Subscription::getPriority));
        return this;
    }

	public List<Subscription<Event_Type>> getSubscriptions()
    {
        return subscriptions;
    }


	public boolean isNeedsToExec(EventHandlingEvent<Event_Type> event)
	{
		if (executionFilter != null)
		{
			return Boolean.TRUE.equals(executionFilter.apply(event));
		}

		return true;
	}
}
