package ru.swayfarer.swl3.observable;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2NoR;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.bus.BusSubscriptionHelper;
import ru.swayfarer.swl3.observable.event.EventHandlingEvent;
import ru.swayfarer.swl3.observable.sub.Subscription;

@RequiredArgsConstructor
public class SubscriptionHelper<Event_Type>
{
    @NonNull
    @Internal
    public final IObservable<Event_Type> wrappedObservable;

    @NonNull
    @Internal
    public final IFunction1<IFunction1NoR<EventHandlingEvent<Event_Type>>, Subscription<Event_Type>> subscribeHandler;

    public Subscription<Event_Type> by(IFunction2NoR<Subscription<Event_Type>, Event_Type> handler, int priority)
    {
        var sub = by(handler);
        sub.setPriority(priority);
        return sub;
    }

    public Subscription<Event_Type> by(IFunction2NoR<Subscription<Event_Type>, Event_Type> handler)
    {
        return byHandler((handleEvent) ->
            handler.apply(handleEvent.getSubscription(), handleEvent.getEventObject())
        ).setTaskObject(handler);
    }

    public Subscription<Event_Type> by(IFunction1NoR<Event_Type> handler, int priority)
    {
        var ret = by((sub, event) -> handler.apply(event)).setPriority(priority);
        ret.setTaskObject(handler);

        return ret;
    }

    public Subscription<Event_Type> by(IFunction0NoR handler, int priority)
    {
        var ret = by((event) -> handler.apply()).setPriority(priority);
        ret.setTaskObject(handler);

        return ret;
    }

    public Subscription<Event_Type> by(IFunction1NoR<Event_Type> handler)
    {
        return by((sub, event) -> handler.apply(event))
                .setTaskObject(handler)
        ;
    }

    public Subscription<Event_Type> by(IFunction0NoR handler)
    {
        return by((event) -> handler.apply())
                .setTaskObject(handler)
        ;
    }

    public Subscription<Event_Type> byHandler(IFunction1NoR<EventHandlingEvent<Event_Type>> handler)
    {
        return subscribeHandler.apply(handler).setTaskObject(handler);
    }

    public ExtendedList<Subscription<Event_Type>> byBus(Object owner)
    {
        var busHelper = new BusSubscriptionHelper<>(
                wrappedObservable,
                owner
        );

        busHelper.start();
        return busHelper.subscriptions;
    }

    public IObservable<Event_Type> byChild()
    {
        return byChild(0);
    }

    public IObservable<Event_Type> byChild(int priority)
    {
        var childObservable = new Observable<Event_Type>();
        by(childObservable::next, priority);
        return childObservable;
    }
}
