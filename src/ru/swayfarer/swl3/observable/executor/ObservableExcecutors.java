package ru.swayfarer.swl3.observable.executor;

import java.util.concurrent.Executor;

import ru.swayfarer.swl3.observable.sub.Subscription.ISubExecutor;

public class ObservableExcecutors {

    public static <Event_Type> ISubExecutor<Event_Type> forSubscription(Executor executor)
    {
        return (handleEvent) -> executor.execute(() -> handleEvent.getSubscription().getTask().apply(handleEvent));
    }
    
    public static <Event_Type> IObservableExecutor<Event_Type> forObservable(Executor executor)
    {
        return (handleEvent) -> executor.execute(() -> handleEvent.getSubscription().execute(handleEvent));
    }
}
