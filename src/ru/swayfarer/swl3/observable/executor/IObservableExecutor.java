package ru.swayfarer.swl3.observable.executor;

import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2NoR;
import ru.swayfarer.swl3.observable.event.EventHandlingEvent;
import ru.swayfarer.swl3.observable.sub.Subscription;

public interface IObservableExecutor<Event_Type> extends IFunction1NoR<EventHandlingEvent<Event_Type>>
{}
