package ru.swayfarer.swl3.observable.bus;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.LibTags;
import ru.swayfarer.swl3.exception.LibTags.Events;
import ru.swayfarer.swl3.funs.GeneratedFuns;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2NoR;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.sub.Subscription;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.lang.reflect.Method;

@RequiredArgsConstructor
public class BusSubscriptionHelper<Event_Type>
{
    @Internal
    public final IObservable<Event_Type> wrappedObservable;

    @Internal
    public final Object owner;

    @Internal
    public ReflectionsUtils reflectionsUtils = ReflectionsUtils.getInstance();

    @Internal
    public ExtendedList<Subscription<Event_Type>> subscriptions = new ExtendedList<>();

    public void start()
    {
        registerMethodsWithSub();
        registerMethodsWithoutSub();
    }

    public void registerMethodsWithSub()
    {
        var methodsFilter = reflectionsUtils.methods().filters();

        var methods = reflectionsUtils.methods().stream(owner)
                .not().filter(methodsFilter.mod().abstracts())
                .filter(methodsFilter.type().equal(void.class))
                .filter(methodsFilter.params().count().equal(2))
                .filter(isFirstParamNotPrimitive())
                .filter((method) ->
                        reflectionsUtils.params()
                                .isParamsAccepted(method.getParameters(), false, Subscription.class, null)
                )
                .toExList()
        ;

        for (var method : methods)
        {
            registerBusMethod(1, method, (sub, event) -> new Object[] {sub, event});
        }
    }

    @NonNull
    private IFunction1<Method, Boolean> isFirstParamNotPrimitive()
    {
        return (method) -> !method.getParameters()[0].getType().isPrimitive();
    }

    public void registerMethodsWithoutSub()
    {
        var methodFilters = reflectionsUtils.methods().filters();

        var methods = reflectionsUtils.methods().stream(owner)
                .not().filter(methodFilters.mod().abstracts())
                .filter(methodFilters.type().equal(void.class))
                .filter(methodFilters.params().count().equal(1))
                .filter(isFirstParamNotPrimitive())
                .toExList()
        ;

        for (var method : methods)
        {
            registerBusMethod(0, method, (sub, event) -> new Object[] {event});
        }
    }

    private void registerBusMethod(int eventParamIndex, Method method, IFunction2<Subscription<Event_Type>, Event_Type, Object[]> argsFun)
    {
        var parameters = method.getParameters();
        var eventParam = parameters[eventParamIndex];

        var busMethod = new BusMethod<Event_Type>();
        busMethod.method = method;
        busMethod.owner = owner;
        busMethod.argsFun = argsFun;
        busMethod.classOfEvent = eventParam.getType();

        var priority = reflectionsUtils.annotations().findProperty()
                .type(int.class)
                .name("priority")
                .marker(ListenEvent.class)
                .in(method)
                .orElse(0)
        ;

        var tags = reflectionsUtils.annotations().findProperty()
                .type(String[].class)
                .name("tags")
                .marker(ListenEvent.class)
                .in(method)
                .orElse(new String[0])
        ;

        var sub = wrappedObservable.subscribe().by(busMethod, priority);

        sub.tag(Events.tagEventBus);
        setBusMethod(sub, method);
        setBusOwnerInstance(sub, owner);

        for (var tag : tags)
        {
            sub.tag(tag);
        }

        subscriptions.add(sub);
    }

    public static Method getBusMethod(Subscription<?> sub)
    {
        return sub.getCustomAttributes().getValue("swl3/events/bus/method");
    }

    public static void setBusMethod(Subscription<?> sub, Method method)
    {
        sub.getCustomAttributes().put("swl3/events/bus/method", method);
    }

    public static Object getBusOwnerInstance(Subscription<?> sub)
    {
        return sub.getCustomAttributes().get("swl3/events/bus/owner");
    }

    public static void setBusOwnerInstance(Subscription<?> sub, Object instance)
    {
        sub.getCustomAttributes().put("swl3/events/bus/owner", instance);
    }

    @RequiredArgsConstructor
    @Getter
    @Setter
    @Accessors(chain = true)
    public static class BusMethod<Event_Type> implements IFunction2NoR<Subscription<Event_Type>, Event_Type>{

        @Internal
        public Object owner;

        @Internal
        public Method method;

        @Internal
        public Class<?> classOfEvent;

        @Internal
        public IFunction2<Subscription<Event_Type>, Event_Type, Object[]> argsFun;

        @Override
        public void applyNoRUnsafe(Subscription<Event_Type> sub, Event_Type event) throws Throwable
        {
            if (event != null && classOfEvent.isAssignableFrom(event.getClass()))
            {
                var args = argsFun.apply(sub, event);
                method.invoke(owner, args);
            }
        }
    }
}
