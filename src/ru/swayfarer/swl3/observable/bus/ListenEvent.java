package ru.swayfarer.swl3.observable.bus;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface ListenEvent
{
    public int priority() default 0;
    public String[] tags() default {};
}
