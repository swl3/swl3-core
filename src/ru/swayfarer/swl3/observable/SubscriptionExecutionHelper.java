package ru.swayfarer.swl3.observable;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.var;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.event.AbstractCancelableEvent;
import ru.swayfarer.swl3.observable.event.EventHandlingEvent;
import ru.swayfarer.swl3.observable.sub.Subscription;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@RequiredArgsConstructor
public class SubscriptionExecutionHelper<Event_Type>
{
    @NonNull
    @Internal
    public final IObservable<Event_Type> wrappedObservable;

    public final IFunction1NoR<IFunction1NoR<EventHandlingEvent<Event_Type>>> executorSetFun;
    public final IFunction1NoR<IFunction1<EventHandlingEvent<Event_Type>, Boolean>> executionFilterSetFun;
    public final IFunction0<IFunction1<EventHandlingEvent<Event_Type>, Boolean>> executionFilterGetFun;

    public <T extends Event_Type> IObservable<T> executor(@NonNull IFunction1NoR<EventHandlingEvent<Event_Type>> fun)
    {
        executorSetFun.apply(fun);
        return ReflectionsUtils.cast(wrappedObservable);
    }

    public <T extends Event_Type> IObservable<T> useFilter(IFunction1<EventHandlingEvent<Event_Type>, Boolean> filter)
    {
        executionFilterSetFun.apply(filter);
        return ReflectionsUtils.cast(wrappedObservable);
    }

    public <T extends Event_Type> IObservable<T> appendFilter(IFunction1<EventHandlingEvent<Event_Type>, Boolean> filter)
    {
        var oldFun = executionFilterGetFun.apply();

        if (oldFun != null)
        {
            executionFilterSetFun.apply(oldFun.and(filter));
        }
        else
        {
            executionFilterSetFun.apply(filter);
        }

        return ReflectionsUtils.cast(wrappedObservable);
    }

    public <T extends Event_Type> IObservable<T> doSubscriptionsIfNotCanceled()
    {
        return useFilter((event) -> {
            var eventObject = event.getEventObject();

            if (eventObject == null)
                return false;

            if (eventObject instanceof AbstractCancelableEvent)
            {
                return !((AbstractCancelableEvent) eventObject).isCanceled();
            }

            return true;
        });
    }
}
