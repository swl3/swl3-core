package ru.swayfarer.swl3.observable;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.sub.Subscription;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

import java.util.List;

@RequiredArgsConstructor
public class TaskObjectsHelper<Event_Type>
{
    @NonNull
    @Internal
    public final IObservable<Event_Type> wrappedObservable;

    @NonNull
    @Internal
    public final IFunction0<List<Subscription<Event_Type>>> subscriptionsFun;

    public <T extends Event_Type> IObservable<T> removeAll()
    {
        subscriptionsFun.apply().clear();
        return ReflectionsUtils.cast(wrappedObservable);
    }

    public <T extends Event_Type> IObservable<T> removeAll(@NonNull Object... taskObjects)
    {
        var taskObjs = CollectionsSWL.list(taskObjects);
        subscriptionsFun.apply().removeIf((sub) -> taskObjs.contains(sub.getTaskObject()));

        return ReflectionsUtils.cast(wrappedObservable);
    }

    public <T> ExtendedList<T> findTaskObjects(@NonNull Class<T> classOfT)
    {
        return findByTaskObjects(classOfT)
                .map((sub) -> (T) sub.getTaskObject())
        ;
    }

    public <T> ExtendedList<Subscription<T>> findByTaskObjects(@NonNull Class<T> classOfT)
    {
        var subscriptions = new ExtendedList<>(subscriptionsFun.apply());

        var ret = subscriptions.exStream()
                .filter((sub) -> {
                    var taskObj = sub.getTaskObject();

                    if (taskObj != null)
                    {
                        return classOfT.isAssignableFrom(taskObj.getClass());
                    }

                    return false;
                })
                .nonNull()
                .toExList()
        ;

        return ReflectionsUtils.cast(ret);
    }

    public ExtendedList<Subscription<Event_Type>> removeByTag(@NonNull String tag)
    {
        var listOfRemovedSubs = new ExtendedList<Subscription<Event_Type>>();

        subscriptionsFun.apply().removeIf((sub) -> {
            if (sub != null && sub.hasTag(tag))
            {
                listOfRemovedSubs.add(sub);
                return true;
            }

            return false;
        });

        return listOfRemovedSubs;
    }
}
