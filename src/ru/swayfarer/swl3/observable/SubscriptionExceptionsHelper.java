package ru.swayfarer.swl3.observable;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;

@RequiredArgsConstructor
public class SubscriptionExceptionsHelper <Event_Type>
{
    @NonNull
    @Internal
    public final IObservable<Event_Type> wrappedObservable;

    @NonNull
    @Internal
    public final IFunction0<ExceptionsHandler> exceptionsHandlerFun;

    public <T extends Event_Type> IObservable<T> throwOnFail()
    {
        exceptionsHandlerFun.apply()
                .configure()
                .rule()
                .any().thanSkip().throwEx()
        ;

        return ReflectionsUtils.cast(wrappedObservable);
    }

    public <T extends Event_Type> IObservable<T> configure(IFunction1NoR<ExceptionsHandler> configurator)
    {
        configurator.apply(exceptionsHandlerFun.apply());
        return ReflectionsUtils.cast(wrappedObservable);
    }

}
