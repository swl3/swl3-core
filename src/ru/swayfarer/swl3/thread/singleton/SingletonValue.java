package ru.swayfarer.swl3.thread.singleton;

import java.util.concurrent.atomic.AtomicBoolean;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;

@Data
@Accessors(chain = true)
public class SingletonValue<Value_Type> implements IFunction0<Value_Type> {

    @NonNull
    public AtomicBoolean isCreated = new AtomicBoolean();
    
    @NonNull
    public IFunction0<Value_Type> newValueFun;
    
    public volatile Value_Type value;
    
    public Value_Type getValue()
    {
        if (value == null)
        {
            if (!isCreated.get())
            {
                isCreated.set(true);
                
                synchronized (this)
                {
                    if (value == null)
                    {
                        value = newValueFun.apply();
                    }
                }
                
            }
        }
        
        return value;
    }

    @Override
    public Value_Type applyUnsafe() throws Throwable
    {
        return getValue();
    }
    
}
