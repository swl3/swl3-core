package ru.swayfarer.swl3.thread.executor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor.AbortPolicy;
import java.util.concurrent.TimeUnit;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true, fluent = true)
public class ThreadPoolExecutorBuidler {

    public long keepAliveTime = 50;
    public int maxThreadsCount = 1;
    public int startThreadsCount;
    
    @NonNull
    public ThreadFactory threadFactory;
    
    @NonNull
    public BlockingQueue<Runnable> workQueue;
    
    @NonNull
    public RejectedExecutionHandler handler;
    
    @NonNull 
    public TimeUnit keepAliveTimeUnit;
    
    public ThreadPoolExecutorBuidler keepAliveTime(long time, @NonNull TimeUnit timeUnit)
    {
        this.keepAliveTimeUnit = timeUnit;
        this.keepAliveTime = time;
        
        return this;
    }
    
    public ThreadPoolExecutor get()
    {
        if (keepAliveTimeUnit == null)
        {
            keepAliveTimeUnit = TimeUnit.MILLISECONDS;
        }
        
        if (threadFactory == null)
        {
            threadFactory = Executors.defaultThreadFactory();
        }
        
        if (workQueue == null)
        {
            workQueue = new LinkedBlockingDeque<>();
        }
        
        if (handler == null)
        {
            handler = new AbortPolicy();
        }
        
        return new ThreadPoolExecutor(
                startThreadsCount, 
                maxThreadsCount, 
                keepAliveTime, 
                keepAliveTimeUnit, 
                workQueue, 
                threadFactory, 
                handler
        );
    }
}
