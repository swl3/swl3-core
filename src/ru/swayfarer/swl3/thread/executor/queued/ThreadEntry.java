package ru.swayfarer.swl3.thread.executor.queued;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class ThreadEntry {
    
    @NonNull
    public Thread thread;
    
    @Builder.Default
    @NonNull
    public AtomicBoolean hasActiveTask = new AtomicBoolean();
    
    @Builder.Default
    @NonNull
    public AtomicLong lastCompleteTime = new AtomicLong(System.currentTimeMillis());
    
    public boolean canAcceptTask()
    {
        return !hasActiveTask.get() && !isInterrupted() && thread.isAlive();
    }
    
    public ThreadEntry enableTask()
    {
        hasActiveTask.set(true);
        return this;
    }
    
    public ThreadEntry disableTask()
    {
        hasActiveTask.set(false);
        return this;
    }
    
    public ThreadEntry updateLastComplete()
    {
        lastCompleteTime.set(System.currentTimeMillis());
        return this;
    }
    
    public boolean isInterrupted()
    {
        return thread.isInterrupted();
    }
}
