package ru.swayfarer.swl3.thread.executor.queued;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

@Data
@Accessors(chain = true)
public class QueuedExecutorSettings {
    
    public static AtomicLong nextPoolId = new AtomicLong(0);
    
    @NonNull
    public ExceptionsHandler exceptionsHandler = new ExceptionsHandler();
    
    @NonNull
    public AtomicLong keepAliveTime = new AtomicLong(500);
    
    @NonNull
    public AtomicInteger minThreadsInPool = new AtomicInteger(0);
    
    @NonNull
    public AtomicInteger maxThreadsInPool = new AtomicInteger(1);
    
    @NonNull
    public AtomicLong executorId = new AtomicLong(nextPoolId.getAndIncrement());
    
    @NonNull
    public AtomicLong nextThreadId = new AtomicLong();
    
    @NonNull
    public String displayName = "Exec";
    
    @NonNull
    public IFunction1<IFunction0NoR, Thread> threadCreationFun = (e) -> new Thread(
            e.asJavaRunnable(), 
            displayName + "-" + executorId.get() + "-" + nextThreadId.incrementAndGet()
    );
    
    public QueuedExecutorSettings displayName(@NonNull String name)
    {
        this.displayName = name;
        return this;
    }
    
    public QueuedExecutorSettings maxThreadsCount(int count)
    {
        this.maxThreadsInPool.set(count);
        return this;
    }
    
    public QueuedExecutorSettings minThreadsCount(int count)
    {
        this.minThreadsInPool.set(count);
        return this;
    }
    
    public QueuedExecutorSettings keepAliveTime(long time, @NonNull TimeUnit timeUnit)
    {
        var millis = timeUnit.toMillis(time);
        return keepAliveTime(millis);
    }
    
    public QueuedExecutorSettings keepAliveTime(long time)
    {
        keepAliveTime.set(time);
        return this;
    }
}
