package ru.swayfarer.swl3.thread.executor.queued;

import java.util.Queue;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.threads.lock.CounterLock;

@Data 
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class QueuedExecutor extends AbstractExecutorService {
    
    public QueuedExecutorSettings settings = new QueuedExecutorSettings();
    public ExtendedList<ThreadEntry> workers = new ExtendedList<>().synchronyzed();
    public Queue<IFunction0NoR> tasks = new ConcurrentLinkedDeque<>();
    
    public CounterLock activeTasksCounterLock = new CounterLock();
    
    public AtomicBoolean isShutdown = new AtomicBoolean();
    
    public synchronized void exec(@NonNull IFunction0NoR task)
    {
        var freeThread = findFreeThreadEntry();
        
        tasks.add(task);
        activeTasksCounterLock.increment();
        
        if (freeThread == null && canCreateNewWorker())
        {
            var threadEntry = createThreadEntry();
            workers.add(threadEntry);
            
            threadEntry.getThread().start();
        }
    }
    
    public QueuedExecutor configure(@NonNull IFunction1NoR<QueuedExecutorSettings> settingsFun)
    {
        settingsFun.apply(getSettings());
        return this;
    }
    
    public ThreadEntry createThreadEntry()
    {
        var threadEntry = new ThreadEntry();
        var execution = getThreadExecution(threadEntry);
        
        var thread = settings.getThreadCreationFun().apply(execution);
        threadEntry.setThread(thread);
        
        return threadEntry;
    }
    
    public boolean canCreateNewWorker()
    {
        return workers.size() < settings.getMaxThreadsInPool().get();
    }
    
    public ThreadEntry findFreeThreadEntry()
    {
        return workers.exStream().findFirst(ThreadEntry::canAcceptTask).orNull();
    }
    
    public boolean canRemoveWorker()
    {
        return isShutdown() || workers.size() > settings.getMinThreadsInPool().get();
    }
    
    public IFunction0NoR getThreadExecution(@NonNull ThreadEntry threadEntry)
    {
        return () -> {
            for (;;)
            {
                var exceptionsHandler = settings.getExceptionsHandler();
                var task = tasks.poll();
                
                while (task != null)
                {
                    try
                    {
                        var currentTask = task;
                        task = tasks.poll();
                        
                        threadEntry.enableTask();
                        
                        try
                        {
                            currentTask.apply();
                        }
                        finally
                        {
                            activeTasksCounterLock.reduce();
                        }
                        
                        threadEntry.updateLastComplete();
                    }
                    catch (Throwable e)
                    {
                        exceptionsHandler.handle(e, "Errror while processing task");
                    }
                }

                threadEntry.disableTask();
                
                Thread.sleep(1);
                
                if (canRemoveWorker())
                {
                    var currentMilis = System.currentTimeMillis();
                    var lastCompleteTime = threadEntry.getLastCompleteTime().get();

                    if (currentMilis - lastCompleteTime > settings.getKeepAliveTime().get())
                    {
                        workers.remove(threadEntry);
                        return;
                    }
                }
            }
        };
    }
    
    @Override
    public void execute(Runnable command)
    {
        exec(() -> command.run());
    }

    @Override
    public void shutdown()
    {
        isShutdown.set(true);
    }

    @Override
    public ExtendedList<Runnable> shutdownNow()
    {
        shutdown();
        
        for (var worker : workers)
        {
            worker.getThread().interrupt();
        }
        
        var ret = new ExtendedList<Runnable>();
        
        for (var task : tasks)
        {
            ret.add(task.asJavaRunnable());
        }
        
        return ret;
    }
    
    public void shutdownWorkers()
    {
        isShutdown.set(true);
    }

    @Override
    public boolean isShutdown()
    {
        return isShutdown.get();
    }

    @Override
    public boolean isTerminated()
    {
        return isShutdown.get() && tasks.isEmpty();
    }

    @Override
    public boolean awaitTermination(long timeout, TimeUnit unit)
    {
        activeTasksCounterLock.waitFor(unit.toMillis(timeout));
        return tasks.isEmpty();
    }
}
