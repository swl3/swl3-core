package ru.swayfarer.swl3.thread;

import lombok.SneakyThrows;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.markers.Alias;
import ru.swayfarer.swl3.markers.Internal;

import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Map;

public class OnceTaskHelper implements IFunction1NoR<IFunction0NoR>
{
    @Internal
    public Map<Object, Boolean> processedTasksMap = Collections.synchronizedMap(new IdentityHashMap<>());

    @Alias("apply")
    @SneakyThrows
    public void doOnce(IFunction0NoR fun)
    {
        applyNoRUnsafe(fun);
    }

    @Override
    public void applyNoRUnsafe(IFunction0NoR fun) throws Throwable
    {
        if (fun != null)
        {
            if (!processedTasksMap.containsKey(fun))
            {
                synchronized (processedTasksMap)
                {
                    if (!processedTasksMap.containsKey(fun))
                    {
                        processedTasksMap.put(fun, true);
                        fun.apply();
                    }
                }
            }
        }
    }
}
