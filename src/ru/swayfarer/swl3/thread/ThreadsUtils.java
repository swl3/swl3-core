package ru.swayfarer.swl3.thread;

import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.thread.executor.ThreadPoolExecutorBuidler;
import ru.swayfarer.swl3.thread.singleton.SingletonValue;
import ru.swayfarer.swl3.threads.lock.CounterLock;

public class ThreadsUtils {

	public static <T> IFunction0<T> threadLocal(@NonNull IFunction0<T> fun)
	{
	    return fun.threadLocal();
	}
	
	public static <T> IFunction0<T> singleton(@NonNull IFunction0<T> fun)
	{
	    return new SingletonValue<>(fun);
	}
	
	public static Executor waitExecutor(@NonNull Executor originalExecutor, @NonNull CounterLock counterLock)
	{
	    return (task) -> {
	        counterLock.increment();
	        
	        originalExecutor.execute(() -> {
	            try
	            {
	                originalExecutor.execute(task);
	            }
	            finally 
	            {
	                counterLock.reduce();
	            }
	        });
	    };
	}
    
    public static Thread newThread(@NonNull IFunction0NoR fun)
    {
		var thread = new Thread(fun.asJavaRunnable());
		thread.start();
		return thread;
    }
	
	public static Thread newThread(@NonNull IFunction0NoR fun, @NonNull String threadName)
	{
		var thread = new Thread(fun.asJavaRunnable(), threadName);
	    thread.start();
	    return thread;
	}
	
	public static void sleep(long delay)
    {
	    sleep(delay, TimeUnit.MILLISECONDS);
    }
	
	public static void sleep(long delay, @NonNull TimeUnit timeUnit)
	{
	    ExceptionsUtils.If(delay < 0, IllegalArgumentException.class, "Delay value must be bigger than 0!");
	    ExceptionsUtils.safe(() -> {
	        Thread.sleep(timeUnit.toMillis(delay));
	    });
	}

	public static IFunction1NoR<IFunction0NoR> once()
	{
		return new OnceTaskHelper();
	}
	
	public static ThreadPoolExecutorBuidler threadsExecutor()
	{
	    return new ThreadPoolExecutorBuidler();
	}

	public static Object getGlobalLock(String lockName)
	{
		return lockName.intern();
	}

	public static <T> T doubleCheckSingleton(@NonNull IFunction0<T> getValueFun, @NonNull IFunction1NoR<T> setValueFun, @NonNull IFunction0<T> newValueFun, @NonNull Object lock)
	{
	    var value = getValueFun.apply();
	    
	    if (value == null)
	    {
	        synchronized (lock) {
	            value = getValueFun.apply();
	            
	            if (value == null)
	            {
	                value = newValueFun.apply();
	                setValueFun.apply(value);
	            }
            }
	    }
	    
	    return value;
	}
}
