package ru.swayfarer.swl3.thread.group;

import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Map;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.markers.Internal;

@SuppressWarnings("unchecked")
@Data @Accessors(chain = true)
public class ThreadGroupLocal<Value_Type> {

	@Internal
	public volatile boolean useParent = true;
	
	@Internal
	public volatile Map<ThreadGroup, Value_Type> values = Collections.synchronizedMap(new IdentityHashMap<>());
	
	public ThreadGroupLocal<Value_Type> value(@NonNull ThreadGroup threadGroup, Value_Type value)
	{
		this.values.put(threadGroup, value);
		return this;
	}
	
	public ThreadGroupLocal<Value_Type> value(Value_Type value)
	{
		return value(Thread.currentThread().getThreadGroup(), value);
	}
	
	public <T extends Value_Type> T getValue()
	{
		return (T) get().orNull();
	}
	
	public <T extends Value_Type> T getValue(@NonNull ThreadGroup threadGroup)
	{
		return (T) get(threadGroup).orNull();
	}
	
	public <T extends Value_Type> ExtendedOptional<T> get()
	{
		return get(Thread.currentThread().getThreadGroup());
	}
	
	public <T extends Value_Type> ExtendedOptional<T> get(@NonNull ThreadGroup threadGroup)
	{
		var group = threadGroup;
		
		while (group != null)
		{
			if (values.containsKey(group))
			{
				var value = values.get(group);
				
				if (value != null)
					return ExtendedOptional.of((T) value);
			}

			if (!useParent)
				break;
			
			group = group.getParent();
		}
		
		return ExtendedOptional.empty();
	}
}
