package ru.swayfarer.swl3.objects;

import ru.swayfarer.swl3.funs.ExtendedOptional;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;

import java.util.Arrays;

public class ObjectsUtils {

    public static ExtendedOptional<Object> getNumberValue(Number numValue, String targetClass)
    {
        switch (targetClass)
        {
            case "byte":
            case "Byte":
                return ExtendedOptional.of(numValue.byteValue());
            case "short":
            case "Short":
                return ExtendedOptional.of(numValue.shortValue());
            case "int":
            case "Integer":
                return ExtendedOptional.of(numValue.intValue());
            case "long":
            case "Long":
                return ExtendedOptional.of(numValue.longValue());
            case "float":
            case "Float":
                return ExtendedOptional.of(numValue.floatValue());
            case "double":
            case "Double":
                return ExtendedOptional.of(numValue.doubleValue());
        }

        return ExtendedOptional.empty();
    }
    public static int hash(Object... objects)
    {
        return Arrays.hashCode(objects);
    }

    public static <T> T orDefault(T value, T defaultValue)
    {
        return orDefault(value, () -> defaultValue);
    }

    public static <T> T orDefault(T value, IFunction0<T> defaultValueFun)
    {
        return value == null ? defaultValueFun.apply() : value;
    }
}
