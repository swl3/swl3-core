package ru.swayfarer.swl3.objects;

import lombok.var;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;

/**
 * Утилиты для проверки равенства 
 * @author swayfarer
 */
public class EqualsUtils {

	/**
	 * Проверка на равенство объектов через {@link Object#equals(Object)} со всеми указанными объектами 
	 * @param obj Объект, который сравнивается
	 * @param objects Объекты, с которыми сравниваются 
	 */
	public static boolean objectEqualsAll(Object obj, Object... objects)
	{
		return objectEquals(obj, true, objects);
	}
	
	/**
	 * Проверка на равенство объектов через {@link Object#equals(Object)} с хотя бы одним указанным объектом 
	 * @param obj Объект, который сравнивается
	 * @param objects Объекты, с которыми сравниваются 
	 */
	public static boolean objectEqualsSome(Object obj, Object... objects)
	{
		return objectEquals(obj, false, objects);
	}
	
	/**
	 * Проверка на равенство объектов через {@link Object#equals(Object)}
	 * @param obj Объект, который сравнивается
	 * @param allMatches Все ли должны быть равны? 
	 * @param objects Объекты, с которыми сравниваются 
	 */
	public static boolean objectEquals(Object obj, boolean allMatches, Object... objects)
	{
		if (CollectionsSWL.isNullOrEmpty(objects))
			return false;

		var someMatches = false;
		for (Object o : objects)
		{
			var objectEquals = objectEquals(o, obj);
			
			if (objectEquals)
			{
			    someMatches = true;
			    
			    if (!allMatches)
			        return true;
			}
			else if (allMatches)
                return false;
			
			if (!allMatches && objectEquals)
			    return true;
			else if (allMatches && !objectEquals)
			    return false;
		}
		
		return someMatches;
	}
	
	public static boolean objectEquals(Object obj1, Object obj2)
	{
	    if (obj1 == null || obj2 == null)
	        return obj1 == null && obj2 == null;
	    
	    var result = false;
	    
	    try
	    {
	        result = obj1.equals(obj2);
	    }
	    catch (Throwable e)
	    {
	        var exceptionsHandler = ExceptionsHandler.getOrCreateContextHandler();
	        exceptionsHandler.handle(e, "Error while comparing objects", obj1, obj2);
	    }
	    
	    return result;
	}
}
