package ru.swayfarer.swl3.api.logger;

import ru.swayfarer.swl3.string.StringUtils;

public class LogHelper
{
    public static <T> T findLogLevel(String str, T devLevel, T infoLevel, T warnLevel, T errorLevel)
    {
        if (StringUtils.isBlank(str))
        {
            return infoLevel;
        }

        switch (str.toLowerCase())
        {
            case "info":
                return infoLevel;
            case "err":
            case "error":
                return errorLevel;
            case "warn":
            case "warning":
                return warnLevel;
            case "dbg":
            case "debug":
            case "dev":
                return devLevel;
        }

        return devLevel;
    }
}
