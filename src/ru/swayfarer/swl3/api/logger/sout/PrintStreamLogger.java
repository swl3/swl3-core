package ru.swayfarer.swl3.api.logger.sout;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.ILoggerFactory;
import ru.swayfarer.swl3.api.logger.LogCreatingEvent;
import ru.swayfarer.swl3.api.logger.LogHelper;
import ru.swayfarer.swl3.api.logger.builder.LogBuilder;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;
import ru.swayfarer.swl3.thread.ThreadsUtils;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Accessors(chain = true)
public class PrintStreamLogger implements ILogger{

	public static boolean useColors = true;
	public static PrintStream loggersOut = System.out;
	public static IFunction0<DataOutStream> dos = ThreadsUtils.singleton(() -> FileSWL.of("soutlogger.txt").append());
	public static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH.mm.ss");
	public PrintStream out = loggersOut;
	
	@NonNull
	public String name;

	public PrintStreamLogger(@NonNull String name)
	{
		this.name = name;
	}

	@Override
	public void dev(Object... text) {
		print(LogColors.devColor,"Dev", text);
	}

	@Override
	public void info(Object... text) {
		print(LogColors.infoColor,"Info", text);
	}

	@Override
	public void warn(Object... text) {
		print(LogColors.warningColor, "Warn", text);
	}

	@Override
	public void error(Object... text) {
		print(LogColors.errorColor,"Error", text);
	}

	@Override
	public void dev(Throwable e, Object... text) {
		print(LogColors.devColor,"Dev", getStacktraceString(e, text));
	}

	@Override
	public void info(Throwable e, Object... text) {
		print(LogColors.infoColor,"Info", getStacktraceString(e, text));
	}

	@Override
	public void warn(Throwable e, Object... text) {
		print(LogColors.warningColor,"Warn", getStacktraceString(e, text));
	}

	@Override
	public void error(Throwable e, Object... text) {
		print(LogColors.errorColor,"Error", getStacktraceString(e, text));
	}

	@Override
	public LogBuilder log()
	{
		var ret = new LogBuilder();
		ret.processFun = this::processBuilder;

		return ret;
	}

	public void processBuilder(LogBuilder builder)
	{
		var logLevel = builder.logLevel;
		var color = LogHelper.findLogLevel(logLevel, LogColors.devColor, LogColors.infoColor, LogColors.warningColor, LogColors.errorColor);
		var logText = builder.contentFun.apply();
		var throwable = builder.throwable;

		if (throwable != null)
			print(color, logLevel, getStacktraceString(throwable, logText));
		else
			print(color, logLevel, logText);
	}

	public void printLn(Object... text)
	{
		print(LogColors.infoColor, null, text);
	}
	
	public void print(String colPrefix, String prefix, Object... text)
	{
		var str = new DynamicString();

		if (useColors)
		{
			str.append(colPrefix);
		}

		var date = dateFormat.format(new Date());

		str.append("(");
		str.append(date);
		str.append(") ");

		var isNameBlank = StringUtils.isBlank(name);
		var isPrefixBlank = StringUtils.isBlank(prefix);

		if (!isNameBlank || !isPrefixBlank)
		{
			str.append("[");

			if (!isNameBlank)
			{
				str.append(name);
			}

			if (!isPrefixBlank)
			{
				if (!isNameBlank)
					str.append("/");

				str.append(prefix);
			}

			str.append("] ");
		}
		
		str.append(StringUtils.concatWithSpaces(text));

		if (useColors)
		{
			str.append("\033[0m");
		}
		
		out.println(str);
		dos.apply().writeLine(str);
	}

	public String getStacktraceString(Throwable e, Object... text)
	{
		var dynStr = new DynamicString();
		var textStr = StringUtils.concatWithSpaces(text);

		if (!StringUtils.isEmpty(textStr))
			dynStr.append(textStr);

		dynStr.newLine();

		var prefix = "";

		while (e != null)
		{
			dynStr.append(prefix + e.toString());
			dynStr.newLine();

			for (var st : cleanStacktrace(e))
			{
				dynStr.append("|    at " + st.toString());
				dynStr.newLine();
			}

			e = e.getCause();
			prefix = "Caused by: ";
		}

		return dynStr.toString();
	}
	
	public List<StackTraceElement> cleanStacktrace(Throwable e)
	{
		var list = new ArrayList<>(Arrays.asList(e.getStackTrace()));
		
		list.removeIf((st) -> st.getClassName().startsWith("ru.swayfarer.swl3.funs.GeneratedFuns"));
		list.removeIf((st) -> st.getClassName().startsWith("java."));
		list.removeIf((st) -> st.getClassName().startsWith("jdk."));
		
		return list;
	}
	
	public static class Factory implements ILoggerFactory
	{

		@Override
		public ILogger applyUnsafe(LogCreatingEvent logCreatingEvent) throws Throwable {
			return new PrintStreamLogger(logCreatingEvent.getName());
		}
	}
	public static class LogColors {
		public static String infoColor = "\033[32;1;5m";
		public static String warningColor = "\033[93;1;5m";
		public static String errorColor = "\033[91;1;5m";
		public static String devColor = "\033[36;1;5m";
	}
}
