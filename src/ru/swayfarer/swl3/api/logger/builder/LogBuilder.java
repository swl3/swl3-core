package ru.swayfarer.swl3.api.logger.builder;

import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.var;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.observable.event.EventSource;
import ru.swayfarer.swl3.string.StringUtils;

@NoArgsConstructor
public class LogBuilder
{
    @Internal
    public String logLevel;

    @Internal
    public IFunction0<String> contentFun = () -> "";

    @Internal
    public Throwable throwable;

    @Internal
    public ExtendedMap<String, Object> customAttributes = new ExtendedMap<>().concurrent();

    @Internal
    public EventSource source = new EventSource();

    @Internal
    public IFunction1NoR<LogBuilder> processFun;

    public LogBuilder(IFunction1NoR<LogBuilder> processFun)
    {
        this.processFun = processFun;
    }

    public LogBuilder dev()
    {
        return level("dev");
    }

    public LogBuilder info()
    {
        return level("info");
    }

    public LogBuilder warn()
    {
        return level("warn");
    }

    public LogBuilder err()
    {
        return level("err");
    }

    public LogBuilder level(@NonNull String level)
    {
        logLevel = level;
        return this;
    }

    public LogBuilder tag(@NonNull String... tags)
    {
        for (var tag : tags)
        {
            var customTags = getTags(this);
            customTags.add(tag);
            return this;
        }

        return this;
    }

    public void text(String text)
    {
        contentFun = () -> StringUtils.orEmpty(text);
        process();
    }

    public void text(Object... text)
    {
        contentFun = () -> StringUtils.concatWithSpaces(text);
        process();
    }

    public void textf(String format, Object... args)
    {
        contentFun = () -> String.format(format, args);
        process();
    }

    public LogBuilder ex(@NonNull Throwable e)
    {
        return throwable(e);
    }

    public LogBuilder throwable(@NonNull Throwable e)
    {
        this.throwable = e;
        return this;
    }

    @Internal
    public void process()
    {
        if (processFun != null)
            processFun.apply(this);
    }

    public static ExtendedList<String> getTags(LogBuilder builder)
    {
        return builder.customAttributes.getOrCreate("custom-tags", () -> new ExtendedList<String>().copyOnWrite());
    }
}
