package ru.swayfarer.swl3.api.logger;

import ru.swayfarer.swl3.api.logger.builder.LogBuilder;
import ru.swayfarer.swl3.markers.ConcattedString;
import ru.swayfarer.swl3.string.StringUtils;

/**
 * Интерфейс для взаимодействия с логгером <br>
 * Для возможности использовать разные реализации логгеров используется интерфейс {@link ILogger} <br> 
 * Для установки конкретной реализации логгера {@link LogFactory} <br> 
 * Такой подход позволяет в пару строк обновить все логгеры
 * @author swayfarer
 *
 */
public interface ILogger {

	/** 
	 * Лог на уровне Debug 
	 * @param text Текст, который будет сконкачен в строку через {@link StringUtils#concatWithSpaces(Object...)}
	 */
	public void dev(@ConcattedString Object... text);
	
	/** 
	 * Лог на уровне Info 
	 * @param text Текст, который будет сконкачен в строку через {@link StringUtils#concatWithSpaces(Object...)}
	 */
	public void info(@ConcattedString Object... text);
	
	/** 
	 * Лог на уровне Warning 
	 * @param text Текст, который будет сконкачен в строку через {@link StringUtils#concatWithSpaces(Object...)}
	 */
	public void warn(@ConcattedString Object... text);
	
	/** 
	 * Лог на уровне Error 
	 * @param text Текст, который будет сконкачен в строку через {@link StringUtils#concatWithSpaces(Object...)}
	 */
	public void error(@ConcattedString Object... text);
	
	/** 
	 * Лог на уровне Debug 
	 * @param e Прикрепленное к логу исключение
	 * @param text Текст, который будет сконкачен в строку через {@link StringUtils#concatWithSpaces(Object...)}
	 */
	public void dev(Throwable e, @ConcattedString Object... text);
	
	/** 
	 * Лог на уровне Info 
	 * @param e Прикрепленное к логу исключение
	 * @param text Текст, который будет сконкачен в строку через {@link StringUtils#concatWithSpaces(Object...)}
	 */
	public void info(Throwable e, @ConcattedString Object... text);
	
	/** 
	 * Лог на уровне Warning 
	 * @param e Прикрепленное к логу исключение
	 * @param text Текст, который будет сконкачен в строку через {@link StringUtils#concatWithSpaces(Object...)}
	 */
	public void warn(Throwable e, @ConcattedString Object... text);
	
	/** 
	 * Лог на уровне Error 
	 * @param e Прикрепленное к логу исключение
	 * @param text Текст, который будет сконкачен в строку через {@link StringUtils#concatWithSpaces(Object...)}
	 */
	public void error(Throwable e, @ConcattedString Object... text);

	public LogBuilder log();

	public default LogBuilder dev()
	{
		return log().dev();
	}

	public default LogBuilder info()
	{
		return log().info();
	}

	public default LogBuilder warn()
	{
		return log().warn();
	}

	public default LogBuilder error()
	{
		return log().err();
	}
}
