package ru.swayfarer.swl3.api.logger;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.api.logger.builder.LogBuilder;
import ru.swayfarer.swl3.exception.LibTags.Exceptions;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.markers.Internal;

/**
 * Реализация {@link ILogger}, которая работает через другой логгер <br>
 * При создании логгера в {@link LogFactory}, может быть возвращена такая обертка на него, что позволит обновлять логгер уже в рантайме.
 * @author swayfarer
 *
 */
@Getter
@Setter
@Accessors(chain = true)
public class LoggerReference implements ILogger {

	/** Обернутый логгер */
	@Internal
	public ILogger logger;

	@NonNull
	public LogCreatingEvent logCreatingEvent;

	public LoggerReference(ILogger logger, LogCreatingEvent logCreatingEvent)
	{
		this.logger = logger;
		this.logCreatingEvent = logCreatingEvent;
	}

	public void refactorLogger(ExceptionsHandler exceptionsHandler, int sourcesOffset, int logsOffset, ILoggerFactory loggerFactoryFun)
	{
		try
		{
			var newLoggerEvent = logCreatingEvent.copy();
			newLoggerEvent.setSourceOffset(sourcesOffset + 1);
			newLoggerEvent.setLogOffset(logsOffset + 1);

			var newLogger = loggerFactoryFun.apply(newLoggerEvent);

			if (newLogger != null)
				this.logger = newLogger;
		}
		catch (Throwable e)
		{
			exceptionsHandler.handle()
				.tag(Exceptions.tagLoggerRefactor)
				.message("Error while refactoring logger", logger, "by new factory fun!")
				.throwable(e)
				.start()
			;
		}
	}

	@Override
	public void dev(Object... text)
	{
		logger.dev(text);;
	}

	@Override
	public void info(Object... text)
	{
		logger.info(text);
	}

	@Override
	public void warn(Object... text)
	{
		logger.warn(text);
	}

	@Override
	public void error(Object... text)
	{
		logger.error(text);
	}

	@Override
	public void dev(Throwable e, Object... text)
	{
		logger.dev(e, text);
	}

	@Override
	public void info(Throwable e, Object... text)
	{
		logger.info(e, text);
	}

	@Override
	public void warn(Throwable e, Object... text)
	{
		logger.warn(e, text);
	}

	@Override
	public void error(Throwable e, Object... text)
	{
		logger.error(e, text);
	}

	@Override
	public LogBuilder log()
	{
		return logger.log();
	}
}
