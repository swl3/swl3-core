package ru.swayfarer.swl3.api.logger;

import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;

public interface ILoggerFactory extends IFunction1<LogCreatingEvent, ILogger>
{
}
