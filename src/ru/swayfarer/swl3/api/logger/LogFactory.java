package ru.swayfarer.swl3.api.logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ServiceLoader;
import java.util.WeakHashMap;

import lombok.NonNull;
import lombok.SneakyThrows;
import lombok.var;
import ru.swayfarer.swl3.api.logger.jul.JulLogger;
import ru.swayfarer.swl3.api.logger.sout.PrintStreamLogger;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.exception.handler.ExceptionsHandler;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction3;
import ru.swayfarer.swl3.markers.Internal;
import ru.swayfarer.swl3.objects.EqualsUtils;
import ru.swayfarer.swl3.reflection.ClassUtil;
import ru.swayfarer.swl3.string.StringValue;
import ru.swayfarer.swl3.system.SystemUtils;

/**
 * Фабрика логгеров <br>
 * Позволяет в несколько строк определить, какую реализацию будут иметь логгеры {@link ILogger} <br>
 * @author swayfarer
 *
 */
public class LogFactory {
	
	/** Функция, которая создает логгеры */
	@Internal
	public static ILoggerFactory loggerFun = new PrintStreamLogger.Factory();
	
	/** Ссылки на созданные логгеры */
	@Internal
	public static List<LoggerReference> createdLoggers = Collections.synchronizedList(new ArrayList<>());

	@Internal
	public static ExtendedMap<Class<?>, ILogger> cachedLoggers = new ExtendedMap<>(new WeakHashMap<>()).synchronize();

	private static void findAndLoadLoggerFactoryProvider()
	{
		var someCustomLoggerFound = false;
		var logger = getLogger("Logging Initialization");
		try
		{
			var systemUtils = SystemUtils.getInstance();

			IFunction1<ILoggerFactoryProvider, Integer> getPriorityFun = (provider) -> {
				var priority = systemUtils.findAppProperty("swl3.logging.provider." + provider.getId() + ".priority")
						.map((value) -> value.getValue(Integer.class))
						.orNull()
				;

				return priority == null ? Integer.MAX_VALUE : priority;
			};

			var preferredProvider = systemUtils.findAppProperty("swl3.logging.provider.preferred")
					.map(StringValue::getStringValue)
					.orNull()
			;

			var customFactoryProvider = (ILoggerFactoryProvider) null;
			var loggerFactories = systemUtils.findServices(ILoggerFactoryProvider.class);

			findFactory:
			{
				if (loggerFactories.isNotEmpty())
				{
					if (loggerFactories.size() == 1)
					{
						customFactoryProvider = loggerFactories.first();
						break findFactory;
					}
					else
					{
						for (var factoryProvider : loggerFactories) {
							if (EqualsUtils.objectEquals(factoryProvider.getId(), preferredProvider))
							{
								customFactoryProvider = factoryProvider;
								break findFactory;
							}
						}

						customFactoryProvider = loggerFactories.exStream()
								.sort(Comparator.comparingInt(getPriorityFun::apply))
								.findFirst().get()
						;
					}
				}
			}

			if (customFactoryProvider != null)
			{
				loggerFun = customFactoryProvider.getLoggerFactory();
				someCustomLoggerFound = true;
				refactorLoggers();
				logger.info("Found custom logging factory provider:", customFactoryProvider.getId(), "at", customFactoryProvider.getClass().getName());
			}
		}
		catch (Throwable e)
		{
			logger.error(e, "Can't auto-load swl3 logger service");
		}

		if (!someCustomLoggerFound)
			logger.warn("Skipping logging-v3 factory service loading because no one correct service found!");
	}

	/**
	 * Получить логгер 
	 * @param classOfInstance Класс, для которого получают логгер
	 * @return Созданный логгер
	 */
	public static ILogger getLogger(@NonNull Class<?> classOfInstance)
	{
		return cachedLoggers.getOrCreate(classOfInstance, () -> {
			var loggerCreationEvent = LogCreatingEvent.builder()
					.classOfInstance(classOfInstance)
					.logOffset(1)
					.sourceOffset(1)
					.ownerInstance(null)
					.name(classOfInstance.getSimpleName())
					.build()
			;

			var loggerReference = new LoggerReference(loggerFun.apply(loggerCreationEvent), loggerCreationEvent);
			createdLoggers.add(loggerReference);

			return loggerReference;
		});
	}
	
	/**
	 * Получить логгер 
	 * @return Созданный логгер
	 */
	@SneakyThrows
	public static ILogger getLogger()
	{
		var ownerClass = ClassUtil.forName(ExceptionsUtils.caller().getClassName());
		return cachedLoggers.getOrCreate(ownerClass, () -> {
			var loggerCreationEvent = LogCreatingEvent.builder()
					.classOfInstance(ownerClass)
					.logOffset(1)
					.sourceOffset(1)
					.ownerInstance(null)
					.name(ownerClass.getSimpleName())
					.build()
			;

			var loggerReference = new LoggerReference(loggerFun.apply(loggerCreationEvent), loggerCreationEvent);
			createdLoggers.add(loggerReference);

			return loggerReference;
		});
	}

	/**
	 * Получить логгер
	 * @param loggerName Имя нового логгера
	 * @return Созданный логгер
	 */
	@SneakyThrows
	public static ILogger getLogger(@NonNull String loggerName)
	{
		var ownerClass = ClassUtil.forName(ExceptionsUtils.caller().getClassName());

		return cachedLoggers.getOrCreate(ownerClass, () -> {
			var loggerCreationEvent = LogCreatingEvent.builder()
					.classOfInstance(ownerClass)
					.logOffset(1)
					.sourceOffset(1)
					.ownerInstance(null)
					.name(loggerName)
					.build()
			;

			var loggerReference = new LoggerReference(loggerFun.apply(loggerCreationEvent), loggerCreationEvent);
			createdLoggers.add(loggerReference);

			return loggerReference;
		});
	}
	
	/**
	 * Получить логгер 
	 * @param instance Объект, для которого получают логгер
	 * @return Созданный логгер
	 */
	public static ILogger getLogger(@NonNull Object instance)
	{
		var ownerClass = instance.getClass();

		return cachedLoggers.getOrCreate(ownerClass, () -> {
			var loggerCreationEvent = LogCreatingEvent.builder()
					.classOfInstance(ownerClass)
					.logOffset(1)
					.sourceOffset(1)
					.ownerInstance(instance)
					.name(ownerClass.getSimpleName())
					.build()
			;

			var loggerReference = new LoggerReference(loggerFun.apply(loggerCreationEvent), loggerCreationEvent);
			createdLoggers.add(loggerReference);

			return loggerReference;
		});
	}

	public static void refactorLoggers()
	{
		refactorLoggers(ExceptionsHandler.getContextHandler());
	}

	public static void refactorLoggers(ExceptionsHandler exceptionsHandler)
	{
		refactorLoggers(exceptionsHandler, 1, 0);
	}

	public static void refactorLoggers(ExceptionsHandler exceptionsHandler, int sourceOffset, int logsOffset)
	{
		if (exceptionsHandler == null)
			return;

		for (var cachedLogger : createdLoggers)
		{
			cachedLogger.refactorLogger(exceptionsHandler, sourceOffset + 1, logsOffset, loggerFun);
		}
	}

	static {
		findAndLoadLoggerFactoryProvider();
	}
}
