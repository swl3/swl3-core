package ru.swayfarer.swl3.api.logger.jul;

import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import lombok.NonNull;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.Accessors;
import lombok.var;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.LogHelper;
import ru.swayfarer.swl3.api.logger.builder.LogBuilder;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction3;
import ru.swayfarer.swl3.markers.ConcattedString;
import ru.swayfarer.swl3.string.StringUtils;

@Accessors(chain = true) @Setter
public class JulLogger implements ILogger
{
	public int stacktraceOffset = 0;
	
	@NonNull
	public Logger julLogger;
	
	@Override
	public void dev(@ConcattedString Object... text)
	{
		julLogger.log(createRecord(Level.FINEST, null, text));
	}

	@Override
	public void info(@ConcattedString Object... text)
	{
		julLogger.log(createRecord(Level.INFO, null, text));
	}

	@Override
	public void warn(@ConcattedString Object... text)
	{
		julLogger.log(createRecord(Level.WARNING, null, text));
	}

	@Override
	public void error(@ConcattedString Object... text)
	{
		var record = new LogRecord(Level.SEVERE, StringUtils.concatWithSpaces(text));
		julLogger.log(record);
	}

	@Override
	public void dev(Throwable e, @ConcattedString Object... text)
	{
		julLogger.log(createRecord(Level.FINEST, e, text));
	}

	@Override
	public void info(Throwable e, @ConcattedString Object... text)
	{
		julLogger.log(createRecord(Level.INFO, e, text));
	}

	@Override
	public void warn(Throwable e, @ConcattedString Object... text)
	{
		julLogger.log(createRecord(Level.WARNING, e, text));
	}

	@Override
	public void error(Throwable e, @ConcattedString Object... text)
	{
		julLogger.log(createRecord(Level.SEVERE, e, text));
	}

	@Override
	public LogBuilder log()
	{
		var ret = new LogBuilder();
		ret.processFun = this::processBuilder;
		return ret;
	}

	public void processBuilder(LogBuilder builder)
	{
		var julLevel = getLevel(builder.logLevel);
		var text = builder.contentFun.apply();
		julLogger.log(julLevel, text);
	}

	public LogRecord createRecord(Level level, Throwable e, @ConcattedString Object... text)
	{
		var stacktrace = new Throwable().getStackTrace()[3];
		var record = new LogRecord(level, StringUtils.concatWithSpaces(text));
		record.setThrown(e);
		record.setSourceClassName(stacktrace.getClassName());
		record.setSourceMethodName(stacktrace.getMethodName());
		
		return record;
	}
	
	public static class Factory implements IFunction3<Integer, Object, Class<?>, ILogger>{

		public Method julFactoryMethod = findJulFactoryMethod();
		
		@SneakyThrows
		@Override
		public ILogger applyUnsafe(Integer stacktraceOffset, Object instance, Class<?> classOfInstance)
		{
			return new JulLogger()
					.setJulLogger((Logger) julFactoryMethod.invoke(null, classOfInstance.getSimpleName(), classOfInstance))
					.setStacktraceOffset(stacktraceOffset)
			;
		}
		
		public Method findJulFactoryMethod()
		{
			try
			{
				var julClass = Logger.class;
				var method = julClass.getDeclaredMethod("getLogger", String.class, Class.class);
				method.setAccessible(true);
				
				return method;
			} 
			catch (Throwable e)
			{
				throw new JulLoggerException("Can't find or access java.util.logging.Logger.getLogger(String, Class) method!", e);
			}
		}
	}

	public static Level getLevel(String str)
	{
		return LogHelper.findLogLevel(str, Level.FINE, Level.INFO, Level.WARNING, Level.SEVERE);
	}
	
	@SuppressWarnings("serial")
	public static class JulLoggerException extends RuntimeException {
		
		public JulLoggerException(String message, Throwable cause)
		{
			super(message, cause);
		}
	}

}
