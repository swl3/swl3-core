package ru.swayfarer.swl3.api.logger;

public interface ILoggerFactoryProvider
{
    public String getId();
    public ILoggerFactory getLoggerFactory();
}
