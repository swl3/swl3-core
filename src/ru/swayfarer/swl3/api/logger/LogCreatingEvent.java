package ru.swayfarer.swl3.api.logger;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.observable.event.AbstractEvent;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class LogCreatingEvent extends AbstractEvent
{
    public String name;
    public Object ownerInstance;
    public Class<?> classOfInstance;
    public int logOffset;
    public int sourceOffset;

    public LogCreatingEvent copy()
    {
        return builder()
                .name(name)
                .ownerInstance(ownerInstance)
                .classOfInstance(classOfInstance)
                .logOffset(logOffset)
                .sourceOffset(sourceOffset)
                .build()
        ;
    }
}
