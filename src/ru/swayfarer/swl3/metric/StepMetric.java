package ru.swayfarer.swl3.metric;

import lombok.var;

public class StepMetric {
	public long time = -1;
	
	public void step(String name)
	{
		var currentTime = System.nanoTime();
		
		if (time != -1)
		{
			name += " in " + (currentTime - time);
		}
		
		time = currentTime;
		
		System.out.println(name);
	}
	
	public void clear()
	{
		time = -1;
	}
}
